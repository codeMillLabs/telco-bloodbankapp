/*
 * FILENAME
 *     DonateAsAtDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.dao.impl;

import java.util.Collections;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.telcoapp.bba.dao.DonateAsAtDao;
import com.hashcode.telcoapp.bba.dao.DonateAsAtSearchCriteria;
import com.hashcode.telcoapp.bba.data.DonateAsAt;
import com.hashcode.telcoapp.common.dao.impl.GenericDaoImpl;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Donate as at dao implementation.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
@Transactional
@Repository
public class DonateAsAtDaoImpl extends GenericDaoImpl<DonateAsAt, Long> implements DonateAsAtDao
{

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public List<DonateAsAt> fetchDonationHistories(final DonateAsAtSearchCriteria searchCriteria)
    {
        Session session = (Session) entityManager.getDelegate();
        Criteria crit = session.createCriteria(DonateAsAt.class);

        boolean atLeastOneSearchCriteria = false;

        if (searchCriteria.isSearchAll())
        {
            return crit.list();
        }

        if (searchCriteria.getSubscriber() != null)
        {
            crit.add(Restrictions.eq("subscriber", searchCriteria.getSubscriber()));
            atLeastOneSearchCriteria = true;
        }

        if (searchCriteria.getState() != null)
        {
            crit.add(Restrictions.eq("state", searchCriteria.getState()));
            atLeastOneSearchCriteria = true;
        }

        if (searchCriteria.getRefNo() != null)
        {
            crit.add(Restrictions.eq("refNo", searchCriteria.getRefNo()));
            atLeastOneSearchCriteria = true;
        }

        if (!atLeastOneSearchCriteria)
        {
            return Collections.emptyList();
        }

        if (searchCriteria.getStartPosition() >= 0)
        {
            crit.setFirstResult(searchCriteria.getStartPosition());
            crit.setMaxResults(searchCriteria.getEndPosition());
        }

        return crit.list();
    }
}
