/*
 * FILENAME
 *     JSONController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.android.api;

import static com.hashcode.telcoapp.bba.android.api.domain.StatusCodes.S1000;
import static com.hashcode.telcoapp.bba.android.api.domain.StatusCodes.S4012;
import static com.hashcode.telcoapp.bba.android.api.domain.StatusCodes.S4014;
import static com.hashcode.telcoapp.bba.util.BBAUtils.isNotNullOrEmpty;
import static com.hashcode.telcoapp.bba.util.BBAUtils.mapBloodType;
import static com.hashcode.telcoapp.bba.util.BBAUtils.mapSubBloodType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.hashcode.telcoapp.bba.android.api.domain.AppResponse;
import com.hashcode.telcoapp.bba.android.api.domain.BloodStockModel;
import com.hashcode.telcoapp.bba.android.api.domain.UpdateStockRequest;
import com.hashcode.telcoapp.bba.android.api.domain.UpdateStockResponse;
import com.hashcode.telcoapp.bba.android.api.domain.ViewStockRequest;
import com.hashcode.telcoapp.bba.android.api.domain.ViewStockResponse;
import com.hashcode.telcoapp.bba.dao.BloodStockHistoryDao;
import com.hashcode.telcoapp.bba.dao.BloodStockHistorySearchCriteria;
import com.hashcode.telcoapp.bba.dao.DonationCenterDao;
import com.hashcode.telcoapp.bba.data.BloodStockHistory;
import com.hashcode.telcoapp.bba.data.DonationCenter;
import com.hashcode.telcoapp.bba.enums.BloodStatus;
import com.hashcode.telcoapp.bba.util.BBAUtils;
import com.hashcode.telcoapp.common.util.ServiceResolver;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * This controller will expose the system functionalities to other systems via REST api. eg: android app
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
@Controller
public class RESTMsgController
{

    protected static final Logger LOGGER = LoggerFactory.getLogger(RESTMsgController.class);

    protected static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(BBAUtils.ANDROID_API_DATE_FORMAT);

    /**
     * <p>
     * updateBloodStock.
     * </p>
     * 
     * @param request
     *            request
     * @return update response
     */
    @RequestMapping(value = "/stock/update", method = RequestMethod.POST)
    public @ResponseBody
    UpdateStockResponse updateBloodStock(@RequestBody final UpdateStockRequest request)
    {
        LOGGER.info("Stock update request received [{}]", request.toString());

        UpdateStockResponse updateStockResponse = new UpdateStockResponse();
        DonationCenter donationCenter = null;
        final String centerId = request.getCenterId();

        LOGGER.info("Stock update request received [centerId : {}]", centerId);
        if (!validate(updateStockResponse, centerId))
        {
            return updateStockResponse;
        }
        else
        {
            donationCenter = getService(DonationCenterDao.class).findByReferenceId(centerId);
            int savedCount = 0;
            for (BloodStockModel model : request.getBloodStocks())
            {
                BloodStockHistory bloodStockHistory = new BloodStockHistory();
                bloodStockHistory.setRecordDate(model.parseDate());
                bloodStockHistory.setCenter(donationCenter);
                bloodStockHistory.setBloodType(model.parseBloodType());
                bloodStockHistory.setSubBloodType(model.parseSubType());
                if (isNotNullOrEmpty(model.getStatusSubType()))
                    bloodStockHistory.setBloodStatus(model.getStatusSubType().equals(
                        BloodStatus.CROSS_MATCHED.toString()) ? BloodStatus.CROSS_MATCHED
                        : BloodStatus.UN_CROSS_MATCHED);
                bloodStockHistory.setStockCount(model.parseStock());
                getService(BloodStockHistoryDao.class).create(bloodStockHistory);
                ++savedCount;
            }

            LOGGER.info("Stock details saved in the database [ No of records saved : {}] ", savedCount);

            updateStockResponse.setStatus(S1000.getShortCode());
            updateStockResponse.setMessage(S1000.getDesc());
            return updateStockResponse;
        }
    }

    private boolean validate(final AppResponse appResponse, final String centerId)
    {
        DonationCenter donationCenter = getService(DonationCenterDao.class).findByReferenceId(centerId);
        if (centerId != null && donationCenter == null)
        {
            appResponse.setStatus(S4014.getShortCode());
            appResponse.setMessage(S4014.getDesc());
            return false;
        }
        else if ((null == centerId))
        {
            appResponse.setStatus(S4014.getShortCode());
            appResponse.setMessage(S4014.getDesc());
            return false;
        }
        return true;
    }

    /**
     * <p>
     * Find blood stock.
     * </p>
     * 
     * @param name
     * @return update response
     * 
     */
    @RequestMapping(value = "/get/centers", method = {
        RequestMethod.POST, RequestMethod.GET
    })
    public @ResponseBody
    String getAllCenterDetails(@RequestBody final String request)
    {
        JsonArray centerArr = new JsonArray();

        List<DonationCenter> donationCenters = getService(DonationCenterDao.class).getBloodBanks();

        for (DonationCenter center : donationCenters)
        {
            JsonObject centerObj = new JsonObject();
            centerObj.addProperty("id", center.getId());
            centerObj.addProperty("centerName", center.getCenterName());
            centerObj.addProperty("city", center.getCity());
            centerObj.addProperty("officer", center.getMainOffier());
            centerObj.addProperty("centerId", center.getReferenceId());
            centerArr.add(centerObj);
        }
        LOGGER.debug("All center details [ {} ] ", centerArr.toString());
        return centerArr.toString();
    }

    /**
     * <p>
     * Find blood stock.
     * </p>
     * 
     * @param name
     * @return update response
     * 
     */
    @RequestMapping(value = "/get/stock", method = {
        RequestMethod.POST, RequestMethod.GET
    })
    public @ResponseBody
    ViewStockResponse findBloodStock(@RequestBody final ViewStockRequest request)
    {
        LOGGER.debug("Stock view request received [{}]", request.toString());

        ViewStockResponse viewStockResponse = new ViewStockResponse();
        DonationCenter donationCenter = null;
        final String centerId = request.getCenterId();

        if (!validate(viewStockResponse, centerId))
        {
            return viewStockResponse;
        }

        donationCenter = getService(DonationCenterDao.class).findByReferenceId(centerId);
        BloodStockHistorySearchCriteria searchCriteria = new BloodStockHistorySearchCriteria();
        searchCriteria.setCenter(donationCenter);
        searchCriteria.setBloodType(mapBloodType(request.getBloodType()));
        searchCriteria.setSubBloodType(mapSubBloodType(request.getSubType()));
        searchCriteria.setDateTime(parseDate(request.getDate()));

        List<BloodStockHistory> histories = getService(BloodStockHistoryDao.class).findBySearchCriteria(searchCriteria);

        BloodStockModel[] stockArray = new BloodStockModel[histories.size()];
        int count = 0;

        for (BloodStockHistory bloodStockHistory : histories)
        {
            stockArray[count] = new BloodStockModel();
            stockArray[count].setDate(DATE_FORMATTER.format(bloodStockHistory.getRecordDate()));
            stockArray[count].setBloodType((bloodStockHistory.getBloodType() != null) ? bloodStockHistory
                .getBloodType().toString() : "");
            stockArray[count].setSubType((bloodStockHistory.getSubBloodType() != null) ? bloodStockHistory
                .getSubBloodType().toString() : "");
            stockArray[count].setStatusSubType((bloodStockHistory.getBloodStatus() != null) ? bloodStockHistory
                    .getBloodStatus().toString() : "");
            stockArray[count].setStockCount(String.valueOf(bloodStockHistory.getStockCount()));
            count++;
        }

        if (histories.isEmpty())
        {
            viewStockResponse.setStatus(S4012.getShortCode());
            viewStockResponse.setMessage(S4012.getDesc());
        }
        else
        {
            viewStockResponse.setStatus(S1000.getShortCode());
            viewStockResponse.setMessage(S1000.getDesc());
        }
        viewStockResponse.setBloodStocks(stockArray);
        return viewStockResponse;
    }

    private Date parseDate(final String dateStr)
    {
        try
        {
            if (BBAUtils.isNotNullOrEmpty(dateStr))
            {
                return DATE_FORMATTER.parse(dateStr);
            }
        }
        catch (ParseException e)
        {
            return new Date();
        }
        return new Date();
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
