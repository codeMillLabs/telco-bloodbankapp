/*
 * FILENAME
 *     UpdateStockRequest.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.android.api.domain;

import java.io.Serializable;
import java.util.List;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Update stock request.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class UpdateStockRequest implements Serializable
{

    private static final long serialVersionUID = 1648537280836702403L;

    private String centerId;
    private int size;
    private List<BloodStockModel> bloodStocks;

    /**
     * <p>
     * Getter for centerId.
     * </p>
     * 
     * @return the centerId
     */
    public String getCenterId()
    {
        return centerId;
    }

    /**
     * <p>
     * Setting value for centerId.
     * </p>
     * 
     * @param inCenterId
     *            the centerId to set
     */
    public void setCenterId(final String inCenterId)
    {
        this.centerId = inCenterId;
    }

    /**
     * <p>
     * Getter for size.
     * </p>
     * 
     * @return the size
     */
    public int getSize()
    {
        return size;
    }

    /**
     * <p>
     * Setting value for size.
     * </p>
     * 
     * @param inSize
     *            the size to set
     */
    public void setSize(final int inSize)
    {
        this.size = inSize;
    }

    /**
     * <p>
     * Getter for bloodStocks.
     * </p>
     * 
     * @return the bloodStocks
     */
    public List<BloodStockModel> getBloodStocks()
    {
        return bloodStocks;
    }

    /**
     * <p>
     * Setting value for bloodStocks.
     * </p>
     * 
     * @param inBloodStocks
     *            the bloodStocks to set
     */
    public void setBloodStocks(final List<BloodStockModel> inBloodStocks)
    {
        this.bloodStocks = inBloodStocks;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("UpdateStockRequest [centerId=%s, size=%s, bloodStocks=%s]", centerId, size, bloodStocks);
    }
}
