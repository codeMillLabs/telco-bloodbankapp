/*
 * FILENAME
 *     RunApp.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui;

import com.hashcode.telcoapp.common.ui.AppData;
import com.vaadin.Application;
import com.vaadin.ui.Window;

/**
 * <p>
 * Main Application Implementation.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class RunApp extends Application
{

    private static final long serialVersionUID = -5317158181454096981L;

    private static final String THEME = "hashcode";

    final AppData sessionData = new AppData(this);

    @Override
    public void init()
    {
        setTheme(THEME);

        getContext().addTransactionListener(sessionData);
        Window mainWindow = new Window("hashCode - NBB Admin Console ");
        setMainWindow(mainWindow);

        mainWindow.addComponent(new LoginWindow());
    }
}
