/*
 * FILENAME
 *     ViewStockResponse.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.android.api.domain;


//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * View Stock Response.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class ViewStockResponse extends AppResponse
{
    private static final long serialVersionUID = -2380796872631345300L;

    private BloodStockModel[] bloodStocks;

    /**
     * <p>
     * Getter for bloodStocks.
     * </p>
     * 
     * @return the bloodStocks
     */
    public BloodStockModel[] getBloodStocks()
    {
        return bloodStocks;
    }

    /**
     * <p>
     * Setting value for bloodStocks.
     * </p>
     * 
     * @param inBloodStocks
     *            the bloodStocks to set
     */
    public void setBloodStocks(final BloodStockModel[] inBloodStocks)
    {
        this.bloodStocks = inBloodStocks;
    }
}
