/*
 * FILENAME
 *     BulkMsgSenderServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.service.impl;

import static com.hashcode.telcoapp.bba.enums.BBAState.FAILED;
import static com.hashcode.telcoapp.bba.enums.BBAState.FAST_MSGS;
import static com.hashcode.telcoapp.bba.enums.BBAState.PENDING;
import static com.hashcode.telcoapp.bba.enums.BBAState.RETRY_1;
import static com.hashcode.telcoapp.bba.enums.BBAState.RETRY_2;
import static com.hashcode.telcoapp.bba.enums.BBAState.SENT;
import static com.hashcode.telcoapp.bba.enums.MessageTag.BLOOD_TYPE;
import static com.hashcode.telcoapp.bba.enums.MessageTag.DISTRICT;
import static com.hashcode.telcoapp.bba.enums.MessageTag.NAME;
import static com.hashcode.telcoapp.bba.enums.MessageTag.NIC;
import static com.hashcode.telcoapp.common.util.Configuration.getCurrentTime;
import static com.hashcode.telcoapp.bba.util.Configuration.getMessage;
import static java.lang.Integer.valueOf;
import hms.kite.samples.api.sms.messages.MtSmsResp;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.telcoapp.bba.dao.AppSubscriberDao;
import com.hashcode.telcoapp.bba.dao.BulkMessageDao;
import com.hashcode.telcoapp.bba.dao.MessageQueueDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.data.BulkMessage;
import com.hashcode.telcoapp.bba.data.MessageQueue;
import com.hashcode.telcoapp.bba.enums.BBAState;
import com.hashcode.telcoapp.bba.service.BulkMsgCriteria;
import com.hashcode.telcoapp.bba.service.BulkMsgSenderService;
import com.hashcode.telcoapp.bba.util.Configuration;
import com.hashcode.telcoapp.common.handlers.IMessageSender;
import com.hashcode.telcoapp.common.util.ServiceResolver;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Implementation of the {@link BulkMsgSenderService}.
 * </p>
 * 
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
@Service
@Transactional
public class BulkMsgSenderServiceImpl implements BulkMsgSenderService
{

    private static final Logger LOGGER = LoggerFactory.getLogger(BulkMsgSenderServiceImpl.class);

    private static final String FROM_ADDRESS = getMessage("message.from.address");
    private static final String BIRTHDAY_MSG_TXT = Configuration.getMessage("birth.day.greeting.msg");
    private static final String DONATE_ELIGIBILITY_MSG_TXT = Configuration.getMessage("notify.eligibility.msg");

    private static final ExecutorService EXECUTOR_SERVICE = Executors
        .newFixedThreadPool(valueOf(getMessage("message.sending.thread.pool.size")));

    private static final int BULK_MSG_BATCH_SIZE = valueOf(getMessage("bulk.message.batch.size"));
    private static final int MSG_BATCH_SIZE_TO_INCREMENT_TIME =
        valueOf(getMessage("message.batch.size.increment.time"));
    private static final int QUEUE_FETCH_TIME_GAP_MINS = valueOf(getMessage("message.queue.fetch.time.gap.mins"));

    private Map<String, String> successRespMap;
    private Map<String, String> retryRespMap;
    private Map<String, String> nonRetryRespMap;

    /**
     * {@inheritDoc}
     */
    public void createAndQueueBulkMessages(final BulkMessage bulkMessage)
    {
        LOGGER.debug("Going to populate the messages for bulk request");

        BulkMsgCriteria criteria = new BulkMsgCriteria();
        criteria.setBloodType(bulkMessage.getBloodType());
        criteria.setDistrict(bulkMessage.getDistrict());
        criteria.setNic(bulkMessage.getNic());
        criteria.setEligibleDateFrom(bulkMessage.getScheduledTime());
        criteria.setSearchAll(bulkMessage.isSendToAll());

        List<BBASubscriber> eligibleSubscribers = getService(AppSubscriberDao.class).findSubscribers(criteria);

        LOGGER.info("No Of subscribers found to send messages : {}", eligibleSubscribers.size());

        int msgCounter = 0, minsIncrementer = 5;

        for (BBASubscriber subscriber : eligibleSubscribers)
        {
            msgCounter++;
            minsIncrementer = getMinsIncrementByCounter(msgCounter, minsIncrementer);

            MessageQueue message = createMessageToQueue(bulkMessage, subscriber, minsIncrementer);

            bulkMessage.addMessage(message);

            LOGGER.debug("Message added to the queue, {}", message);
        }
        getService(BulkMessageDao.class).create(bulkMessage);

        LOGGER.info("Bulk message saved and queued successfully , [ Message : {}, No of subscribers :{} ]",
            bulkMessage, bulkMessage.getMessages().size());
    }

    private int getMinsIncrementByCounter(final int msgCounter, int minsIncrementer)
    {
        if (msgCounter % MSG_BATCH_SIZE_TO_INCREMENT_TIME == 0)
        {
            minsIncrementer++;
        }
        return minsIncrementer;
    }

    /**
     * {@inheritDoc}
     */
    public void createAndQueueMessages(final String txtMessage, final BBASubscriber subscriber)
    {
        LOGGER.debug("Going to populate the messages for request");

        if (null != txtMessage && null != subscriber)
        {
            MessageQueue message = createMessageToQueue(txtMessage, subscriber, PENDING, 5);
            getService(MessageQueueDao.class).create(message);
            LOGGER.debug("Message added to the queue, {}", message);
        }
        LOGGER.info("Message saved and queued successfully , [ Message : {}, Subscriber :{} ]", txtMessage,
            subscriber.getAddress());
    }

    /**
     * {@inheritDoc}
     */
    public void sendDonationsEligibaleReminders()
    {
        LOGGER.debug("Going to queue reminders/notifications to users");

        BulkMsgCriteria criteria = new BulkMsgCriteria();
        criteria.setEligibleDateOn(getCurrentTime());

        List<BBASubscriber> eligibleSubscribers = getService(AppSubscriberDao.class).findSubscribers(criteria);

        int msgCounter = 0, minsIncrementer = 5;
        for (BBASubscriber subscriber : eligibleSubscribers)
        {
            msgCounter++;
            minsIncrementer = getMinsIncrementByCounter(msgCounter, minsIncrementer);

            MessageQueue message =
                createMessageToQueue(DONATE_ELIGIBILITY_MSG_TXT, subscriber, PENDING, minsIncrementer);
            getService(MessageQueueDao.class).create(message);
            LOGGER.debug("Message added to the queue, {}", message);
        }
        LOGGER.info("Blood donations reminder notices count loaded to queue, [ No of subscribers :{} ]",
            eligibleSubscribers.size());
    }

    /**
     * {@inheritDoc}
     */
    public void sendBirthDayGreetings()
    {
        LOGGER.debug("Going to queue birth day greetings to users");

        BulkMsgCriteria criteria = new BulkMsgCriteria();
        criteria.setDob(getCurrentTime());

        List<BBASubscriber> eligibleSubscribers = getService(AppSubscriberDao.class).findSubscribers(criteria);

        
        int msgCounter = 0, minsIncrementer = 5;
        for (BBASubscriber subscriber : eligibleSubscribers)
        {
            msgCounter++;
            minsIncrementer = getMinsIncrementByCounter(msgCounter, minsIncrementer);

            MessageQueue message = createMessageToQueue(BIRTHDAY_MSG_TXT, subscriber, FAST_MSGS, minsIncrementer);
            getService(MessageQueueDao.class).create(message);
            LOGGER.debug("Message added to the queue, {}", message);
        }
        LOGGER.info("Birthday greetings count loaded to queue, [ No of subscribers :{} ]", eligibleSubscribers.size());
    }

    /**
     * {@inheritDoc}
     */
    public void sendMessageFromQueue()
    {
        LOGGER.debug("Going to send messages to users");

        DateTime dateTime = new DateTime(getCurrentTime().getTime()).plusMinutes(QUEUE_FETCH_TIME_GAP_MINS);

        BulkMsgCriteria criteria = new BulkMsgCriteria();
        criteria.getStatus().add(FAST_MSGS);
        criteria.getStatus().add(PENDING);
        criteria.getStatus().add(RETRY_1);
        criteria.getStatus().add(RETRY_2);
        criteria.setStartPosition(0);
        criteria.setEndPosition(BULK_MSG_BATCH_SIZE);
        criteria.setSenderQueueTimeFrom(getCurrentTime());
        criteria.setSenderQueueTimeTo(dateTime.toDate());
        criteria.setMessageSendingCrit(true);

        List<MessageQueue> queuedMsgs = getService(MessageQueueDao.class).findByCriteria(criteria);

        for (final MessageQueue msg : queuedMsgs)
        {
            synchronized (msg)
            {
                try
                {

                    EXECUTOR_SERVICE.submit(new Runnable()
                    {

                        public void run()
                        {
                            try
                            {
                                MtSmsResp mtResp =
                                    getService(IMessageSender.class).sendMessage(msg.getToAddress(), msg.getMessage());

                                handleMsgReponse(msg, mtResp);
                            }
                            catch (Throwable e)
                            {
                                msg.setSentDate(getCurrentTime());
                                msg.setStatus(BBAState.FAILED);
                                msg.setResponseMsg("System experienced an unexpected error");
                                LOGGER.error("Error encountered in sending message :{}", msg, e);
                            }
                            finally
                            {
                                getService(MessageQueueDao.class).update(msg);
                            }

                        }
                    });

                    Thread.sleep(Long.valueOf(getMessage("bulk.interval.between.msgs")));
                }
                catch (Exception e)
                {
                    LOGGER.error("Error encountered in sending message :{}", msg, e);
                }
            }
        }
    }

    private void handleMsgReponse(final MessageQueue message, final MtSmsResp mtResp)
    {
        String statusCode = mtResp.getStatusCode();
        message.setSentDate(getCurrentTime());

        if (successRespMap.containsKey(statusCode))
        {
            message.setStatus(SENT);
            message.setResponseMsg(successRespMap.get(statusCode));
            LOGGER.info("Message sent to subscriber successfully :{}", message);
        }
        else if (retryRespMap.containsKey(statusCode))
        {
            message.setResponseMsg(retryRespMap.get(statusCode));

            if (message.getStatus().equals(RETRY_1))
            {
                message.setStatus(RETRY_2);
            }
            else if (message.getStatus().equals(RETRY_2))
            {
                message.setStatus(FAILED);
            }
            else
            {
                message.setStatus(RETRY_1);
            }

            LOGGER.info("Message sending fail, will retry in next message sending cycle :{}", message);
        }
        else if (nonRetryRespMap.containsKey(statusCode))
        {
            message.setResponseMsg(nonRetryRespMap.get(statusCode));
            message.setStatus(FAILED);
            LOGGER.info("Message sending failed, {}", message);
        }
        else
        {
            message.setResponseMsg(nonRetryRespMap.get(statusCode));
            message.setStatus(FAILED);
            LOGGER.info("Message sending failed, {}", message);
        }
    }

    private MessageQueue createMessageToQueue(final BulkMessage bulkMessage, final BBASubscriber subscriber,
        final int minsIncrementer)
    {
        MessageQueue messageQueue = new MessageQueue();

        String msgText = formatMessage(bulkMessage, subscriber);
        messageQueue.setMessage(msgText);

        Date scheduleDate =
            (bulkMessage.getScheduledTime() != null) ? getCurrentTimeMinsAhead(bulkMessage.getScheduledTime(),
                minsIncrementer) : getCurrentTimeMinsAhead(getCurrentTime(), minsIncrementer);
        messageQueue.setScheduledDate(scheduleDate);
        messageQueue.setSentBy(FROM_ADDRESS);
        messageQueue.setStatus(PENDING);
        messageQueue.setToAddress(subscriber.getAddress());
        return messageQueue;
    }

    private MessageQueue createMessageToQueue(final String message, final BBASubscriber subscriber,
        final BBAState status, final int minsIncrementer)
    {
        MessageQueue messageQueue = new MessageQueue();

        String msgText = formatNotificationMessage(message, subscriber);
        messageQueue.setMessage(msgText);
        messageQueue.setScheduledDate(getCurrentTimeMinsAhead(getCurrentTime(), minsIncrementer));
        messageQueue.setSentBy(FROM_ADDRESS);
        messageQueue.setStatus(status);
        messageQueue.setToAddress(subscriber.getAddress());
        return messageQueue;
    }

    private Date getCurrentTimeMinsAhead(final Date time, final int minsAhead)
    {
        return new DateTime(time.getTime()).plusMinutes(minsAhead).toDate();
    }

    private String formatNotificationMessage(final String message, final BBASubscriber subscriber)
    {

        String msgText = message;

        if (msgText.contains(NAME.getTag()))
        {
            msgText = msgText.replaceAll(NAME.getTag(), subscriber.getFirstName());
        }
        return msgText;
    }

    private String formatMessage(final BulkMessage bulkMessage, final BBASubscriber subscriber)
    {
        String msgText = bulkMessage.getMessage();

        if (msgText.contains(NAME.getTag()))
        {
            String firstName = (subscriber.getFirstName() != null) ? subscriber.getFirstName() : "";
            msgText = msgText.replaceAll(NAME.getTag(), firstName);
        }
        if (msgText.contains(BLOOD_TYPE.getTag()))
        {
            String bloodType = (subscriber.getBloodType() != null) ? subscriber.getBloodType().toString() : "";
            msgText = msgText.replaceAll(BLOOD_TYPE.getTag(), bloodType);
        }
        if (msgText.contains(DISTRICT.getTag()))
        {
            String district = (subscriber.getDistrict() != null) ? subscriber.getDistrict() : "";
            msgText = msgText.replaceAll(DISTRICT.getTag(), district);
        }
        if (msgText.contains(NIC.getTag()))
        {
            String nic = (subscriber.getNic() != null) ? subscriber.getNic() : "";
            msgText = msgText.replaceAll(NIC.getTag(), nic);
        }

        return msgText;
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }

    /**
     * <p>
     * Getter for successRespMap.
     * </p>
     * 
     * @return the successRespMap
     */
    public Map<String, String> getSuccessRespMap()
    {
        return successRespMap;
    }

    /**
     * <p>
     * Setting value for successRespMap.
     * </p>
     * 
     * @param inSuccessRespMap
     *            the successRespMap to set
     */
    public void setSuccessRespMap(final Map<String, String> inSuccessRespMap)
    {
        this.successRespMap = inSuccessRespMap;
    }

    /**
     * <p>
     * Getter for retryRespMap.
     * </p>
     * 
     * @return the retryRespMap
     */
    public Map<String, String> getRetryRespMap()
    {
        return retryRespMap;
    }

    /**
     * <p>
     * Setting value for retryRespMap.
     * </p>
     * 
     * @param inRetryRespMap
     *            the retryRespMap to set
     */
    public void setRetryRespMap(final Map<String, String> inRetryRespMap)
    {
        this.retryRespMap = inRetryRespMap;
    }

    /**
     * <p>
     * Getter for nonRetryRespMap.
     * </p>
     * 
     * @return the nonRetryRespMap
     */
    public Map<String, String> getNonRetryRespMap()
    {
        return nonRetryRespMap;
    }

    /**
     * <p>
     * Setting value for nonRetryRespMap.
     * </p>
     * 
     * @param inNonRetryRespMap
     *            the nonRetryRespMap to set
     */
    public void setNonRetryRespMap(final Map<String, String> inNonRetryRespMap)
    {
        this.nonRetryRespMap = inNonRetryRespMap;
    }

}
