/*
 * FILENAME
 *     HomeLayout.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui;

import static com.hashcode.telcoapp.bba.util.BBAUtils.generateUniqueFileName;
import static com.hashcode.telcoapp.common.handlers.Messages.getMessage;
import static com.hashcode.telcoapp.common.util.Configuration.getCurrentTime;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.List;
import java.util.ListIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVParser;
import au.com.bytecode.opencsv.CSVReader;

import com.hashcode.telcoapp.bba.dao.DonateAsAtDao;
import com.hashcode.telcoapp.bba.dao.DonateAsAtSearchCriteria;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.data.DonateAsAt;
import com.hashcode.telcoapp.bba.enums.BBAState;
import com.hashcode.telcoapp.bba.service.BulkMsgSenderService;
import com.hashcode.telcoapp.bba.ui.subscriber.DonateAsAtTable;
import com.hashcode.telcoapp.bba.util.Configuration;
import com.hashcode.telcoapp.common.dao.SubscriberDao;
import com.hashcode.telcoapp.common.ui.dialog.BBADialog;
import com.hashcode.telcoapp.common.ui.dialog.DialogButtonCode;
import com.hashcode.telcoapp.common.ui.dialog.IWindowCallback;
import com.hashcode.telcoapp.common.util.ServiceResolver;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Layout for home page.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class HomeLayout extends VerticalLayout
{
    private static final long serialVersionUID = 2843015211760971301L;
    private static final Logger LOGGER = LoggerFactory.getLogger(HomeLayout.class);

    private DonateAsAtTable donateAsAtTable;

    private MenuBar menuBar;

    /**
     * <p>
     * Constructor of Home Layout.
     * </p>
     */
    public HomeLayout()
    {
        setSizeFull();
        addComponent(getMenubar());

        VerticalLayout pendingApprovalsLayout = new VerticalLayout();
        pendingApprovalsLayout.setMargin(true);
        pendingApprovalsLayout.addComponent(new Label("<b>Pending Approvals</b>", Label.CONTENT_XHTML));
        pendingApprovalsLayout.addComponent(getDonateAsAtTable().getPaginator());
        pendingApprovalsLayout.addComponent(getDonateAsAtTable());
        addComponent(pendingApprovalsLayout);

        VerticalLayout bulkApproveLayout = new VerticalLayout();
        bulkApproveLayout.setMargin(true);
        final FileUploader uploader = new FileUploader();
        Upload upload = new Upload("Select csv file here to bulk approve donations", uploader);
        upload.setButtonCaption("Start Upload");
        upload.addListener(uploader);
        bulkApproveLayout.addComponent(upload);
        addComponent(bulkApproveLayout);

        VerticalLayout upcomingCampaignsLayout = new VerticalLayout();
        upcomingCampaignsLayout.addComponent(new Label("<b>Upcoming Campaigns</b><br/>TODO : Table",
            Label.CONTENT_XHTML));
    }

    private DonateAsAtTable getDonateAsAtTable()
    {
        if (null == donateAsAtTable)
        {
            donateAsAtTable = new DonateAsAtTable(true);
            DonateAsAtSearchCriteria searchCriteria = new DonateAsAtSearchCriteria();
            searchCriteria.setState(BBAState.PENDING);
            donateAsAtTable.refresh(searchCriteria);
        }

        return donateAsAtTable;
    }

    private MenuBar getMenubar()
    {
        if (null == menuBar)
        {
            menuBar = new MenuBar();
            menuBar.setSizeFull();
        }
        return menuBar;
    }

    /**
     * <p>
     * Refresh layout.
     * </p>
     */
    public void refresh()
    {
        getDonateAsAtTable().refresh();
    }

    /**
     * <p>
     * File uploader.
     * </p>
     * 
     * @author Manuja
     * 
     * @version $Id$
     */
    class FileUploader implements Receiver, SucceededListener
    {
        /**
         * <p>
         * TODO Describe what this data member models and how it's used.
         * </p>
         **/

        private static final long serialVersionUID = 5094981234104672248L;
        private File file;

        public OutputStream receiveUpload(final String filename, final String mimeType)
        {
            FileOutputStream fos = null;
            try
            {
                file =
                    new File(Configuration.getMessage("document.repository") + generateUniqueFileName(filename));
                fos = new FileOutputStream(file);
            }
            catch (final java.io.FileNotFoundException e)
            {
                getWindow().showNotification("Could not open file<br/>", e.getMessage(),
                    Notification.TYPE_ERROR_MESSAGE);
                return null;
            }
            return fos;
        }

        public void uploadSucceeded(final SucceededEvent event)
        {
            processUploadedFile(file);
        }
    };

    private void processUploadedFile(final File file)
    {
        try
        {
            StringBuilder donationErrorSummary = new StringBuilder();
            String[][] data = loadFromFile(file);
            for (String[] dataRow : data)
            {
                String donationCode = dataRow[0];
                DonateAsAtSearchCriteria searchCriteria = new DonateAsAtSearchCriteria();
                searchCriteria.setState(BBAState.PENDING);
                searchCriteria.setRefNo(donationCode);
                List<DonateAsAt> donations = getService(DonateAsAtDao.class).fetchDonationHistories(searchCriteria);
                if (!donations.isEmpty())
                {
                    DonateAsAt donation = donations.get(0);
                    donation.setState(BBAState.APPROVED);
                    getService(DonateAsAtDao.class).update(donation);
                    BBASubscriber subscriber = donation.getSubscriber();
                    subscriber.incrementPoints();
                    subscriber.setLastDonatedDate(getCurrentTime());
                    getService(SubscriberDao.class).update(subscriber);

                    getService(BulkMsgSenderService.class).createAndQueueMessages(
                        getMessage("donation.approval.message", donation.getRefNo()), donation.getSubscriber());
                }
                else
                {
                    if (donationErrorSummary.length() > 0)
                        donationErrorSummary.append("<br/>");

                    donationErrorSummary.append("No donation found for donation code [" + donationCode + "]");
                }
            }
            createMessageWindow("Bulk approval summary",
                donationErrorSummary.length() > 0
                ? donationErrorSummary.toString() : "Success", null, "500px", "300px");
        }
        catch (FileNotFoundException e)
        {
            getWindow().showNotification("Error while processing uploaded file<br/>", e.getMessage(),
                Notification.TYPE_ERROR_MESSAGE);
        }
    }

    private void createMessageWindow(final String caption, final String message, final IWindowCallback windowCallback,
        final String width, final String height, final DialogButtonCode... buttonCodes)
    {
        BBADialog bbaDialog =
            new BBADialog(caption, new Label(message, Label.CONTENT_XHTML), windowCallback, buttonCodes);
        bbaDialog.setWidth(width);
        bbaDialog.setHeight(height);
        getWindow().addWindow(bbaDialog);
    }

    @SuppressWarnings("resource")
    private String[][] loadFromFile(final File file) throws FileNotFoundException
    {
        InputStreamReader in = new InputStreamReader(new FileInputStream(file));
        CSVReader reader =
            new CSVReader(in, CSVParser.DEFAULT_SEPARATOR, CSVParser.DEFAULT_QUOTE_CHARACTER,
                CSVParser.DEFAULT_ESCAPE_CHARACTER, 1);
        String[][] linesAndColumns = null;
        try
        {
            List<String[]> myEntries = reader.readAll();
            ListIterator<String[]> it = myEntries.listIterator();
            linesAndColumns = new String[myEntries.size()][myEntries.get(0).length];
            int j = 0;
            while (it.hasNext())
            {
                String[] val = it.next();
                for (int i = 0; i < val.length; i++)
                    linesAndColumns[j][i] = val[i];
                j++;
            }
        }
        catch (IOException e)
        {
            LOGGER.debug("Error Loading File " + file.getName(), e);
        }
        return linesAndColumns;
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
