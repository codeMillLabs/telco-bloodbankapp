/*
 * FILENAME
 *     BloodStockHistoryDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.dao;

import java.util.List;

import com.hashcode.telcoapp.bba.data.BloodStockHistory;
import com.hashcode.telcoapp.common.dao.GenericDao;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood stock history dao.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public interface BloodStockHistoryDao extends GenericDao<BloodStockHistory, Long>
{
    /**
     * <p>
     * Find blood stock history by search criteria.
     * </p>
     *
     * @param searchCriteria search criteria instance
     * 
     * @return list of blood stock histories
     */
    List<BloodStockHistory> findBySearchCriteria(BloodStockHistorySearchCriteria searchCriteria);
    
    
    /**
     * <p>
     * Count blood stock history by search criteria.
     * </p>
     *
     * @param searchCriteria search criteria instance
     * 
     * @return total no of rows found
     */
    Integer countByCriteria(BloodStockHistorySearchCriteria searchCriteria);
}
