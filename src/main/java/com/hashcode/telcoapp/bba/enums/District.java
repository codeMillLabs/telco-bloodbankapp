/*
 * FILENAME
 *     District.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.enums;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * District enum.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
public enum District
{
    AMPARA("Ampara"),
    ANURADHAPURA("Anuradhapura"),
    BADULLA("Badulla"),
    BATTICALOA("Batticaloa"),
    COLOMBO("Colombo"),
    GALLE("Galle"),
    GAMPAHA("Gampaha"),
    HAMBANTOTA("Hambantota"),
    JAFFNA("Jaffna"),
    KALUTARA("Kalutara"),
    KANDY("Kandy"),
    KEGALLE("Kegalle"),
    KILINOCHCHI("Kilinochchi"),
    KURUNEGALA("Kurunegala"),
    MANNAR("Mannar"),
    MATALE("Matale"),
    MATARA("Matara"),
    MONERAGALA("Moneragala"),
    MULLAITIVU("Mullaitivu"),
    NUWARA_ELIYA("Nuwara Eliya"),
    POLONNARUWA("Polonnaruwa"),
    PUTTALAM("Puttalam"),
    RATNAPURA("Ratnapura"),
    TRINCOMALEE("Trincomalee"),
    VAVUNIYA("Vavuniya");

    final String label;

    private District(final String inLabel)
    {
        this.label = inLabel;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Enum#toString()
     */
    public String toString()
    {
        return label;
    }
}
