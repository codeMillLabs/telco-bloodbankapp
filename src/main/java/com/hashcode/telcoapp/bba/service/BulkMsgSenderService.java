/*
 * FILENAME
 *     BulkMsgSenderService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.service;

import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.data.BulkMessage;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Bulk message Sender service for subscribers.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public interface BulkMsgSenderService
{
    /**
     * <p>
     * Create N Queue the bulk messages to be send.
     * </p>
     * 
     * @param bulkMessage
     *            bulk message meta data.
     */
    void createAndQueueBulkMessages(BulkMessage bulkMessage);
    
    /**
     * <p>
     * Create N Queue the messages to be send.
     * </p>
     * 
     * @param txtMessage
     *            message to send.
     * @param subscriber
     *            subscriber instance
     */
    void createAndQueueMessages(String txtMessage, BBASubscriber subscriber);

    /**
     * <p>
     * Send blood donation eligibility notification to subscribers.
     * </p>
     */
    void sendDonationsEligibaleReminders();
    
    /**
     * <p>
     * Send birth day greetings.
     * </p>
     */
    void sendBirthDayGreetings();

    /**
     * <p>
     * Send messages from queue.
     * </p>
     * 
     */
    void sendMessageFromQueue();

}
