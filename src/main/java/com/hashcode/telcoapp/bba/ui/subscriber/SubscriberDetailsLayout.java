/*
 * FILENAME
 *     SubscriberDetailsLayout.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.subscriber;

import com.hashcode.telcoapp.bba.dao.AppSubscriberDao;
import com.hashcode.telcoapp.bba.dao.DonateAsAtSearchCriteria;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.common.ui.dialog.BBADialog;
import com.hashcode.telcoapp.common.ui.dialog.DialogButtonCode;
import com.hashcode.telcoapp.common.util.ServiceResolver;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.VerticalLayout;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Subscriber details layout.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class SubscriberDetailsLayout extends VerticalLayout
{
    private static final long serialVersionUID = 3682172479338146407L;
    private static final String SAVE = "Save";
    //private static final String DISCARD = "Discard";
    private static final String SEND_MESSAGE = "Send Message";
    private SubscriberForm subscriberForm;
    private DonateAsAtTable donateAsAtTable;
    private MenuBar menuBar;

    /**
     * <p>
     * Constructor of SubscriberDetailsLayout.
     * </p>
     */
    public SubscriberDetailsLayout()
    {
        initiUI();
    }

    private void initiUI()
    {
        setSizeFull();
        addComponent(getMenubar());
        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setMargin(true);
        mainLayout.addComponent(getSubscriberForm());
        mainLayout.addComponent(new Label("<b>Donation History</b>", Label.CONTENT_XHTML));
        mainLayout.addComponent(getDonateAsAtTable().getPaginator());
        mainLayout.addComponent(getDonateAsAtTable());
        addComponent(mainLayout);
        setUIEnabled(false);
    }

    private MenuBar getMenubar()
    {
        if (null == menuBar)
        {
            menuBar = new MenuBar();
            menuBar.setSizeFull();
            final Command command = new Command()
            {
                private static final long serialVersionUID = 49949834422510591L;

                public void menuSelected(final MenuItem inSelectedItem)
                {
                    if (SAVE.equals(inSelectedItem.getText()))
                    {
                        if (getSubscriberForm().validateAllFields())
                        {
                            getService(AppSubscriberDao.class).update(getSubscriberForm().getModel());
                            getWindow().showNotification("Saved ...");
                            getSubscriberForm().setComponentError(null);
                        }
                    }
                    /*
                     * else if (DISCARD.equals(inSelectedItem.getText())) { // TODO : Implement discard feature }
                     */
                    else if (SEND_MESSAGE.equals(inSelectedItem.getText()))
                    {
                        BBADialog bbaDialog = new BBADialog("Send Message", null, DialogButtonCode.OK);
                        getWindow().addWindow(bbaDialog);
                    }
                }

            };

            menuBar.addItem(SAVE, command);
            //menuBar.addItem(DISCARD, command);
        }
        return menuBar;
    }

    /**
     * <p>
     * Enable subscriber details ui.
     * 
     * </p>
     * 
     * @param enabled
     *            boolean value whether enabled or not
     */
    public void setUIEnabled(final boolean enabled)
    {
        getMenubar().setEnabled(enabled);
        getSubscriberForm().setEnabled(enabled);
    }

    /**
     * <p>
     * Set BBA Subscriber.
     * </p>
     * 
     * @param subscriber
     *            subscriber instance
     */
    public void setModel(final BBASubscriber subscriber)
    {
        if (null != subscriber && null != subscriber.getId())
            setUIEnabled(true);
        else
            setUIEnabled(false);

        getSubscriberForm().setModel(subscriber);
        DonateAsAtSearchCriteria searchCriteria = new DonateAsAtSearchCriteria();
        if (null != subscriber && null != subscriber.getId())
            searchCriteria.setSubscriber(subscriber);
        getDonateAsAtTable().refresh(searchCriteria);
    }

    private SubscriberForm getSubscriberForm()
    {
        if (null == subscriberForm)
        {
            subscriberForm = new SubscriberForm(new BBASubscriber());
        }

        return subscriberForm;
    }

    private DonateAsAtTable getDonateAsAtTable()
    {
        if (null == donateAsAtTable)
        {
            donateAsAtTable = new DonateAsAtTable(false);
        }

        return donateAsAtTable;
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
