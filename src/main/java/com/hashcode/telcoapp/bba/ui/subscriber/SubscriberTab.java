/*
 * FILENAME
 *     SubscriberTab.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.subscriber;

import static com.hashcode.telcoapp.bba.util.BBAUtils.getService;

import java.io.Serializable;

import com.hashcode.telcoapp.bba.dao.AppSubscriberDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Subscriber Tab.
 * </p>
 * 
 * @author manuja
 * 
 * @version $Id$
 */
public class SubscriberTab extends VerticalLayout
{
    private static final long serialVersionUID = -4853851852054935076L;
    private SubscriberSummaryLayout subscriberSummaryLayout;
    private SubscriberDetailsLayout subscriberDetailsLayout;
    private final TabSheet tabSheet;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public SubscriberTab()
    {
        setSizeFull();
        tabSheet = new TabSheet();
        tabSheet.setWidth("100%");
        tabSheet.setImmediate(true);
        tabSheet.addTab(getSubscriberSummaryLayout(), "Subscriber Summary", new ThemeResource(
            "images/subscriber-summary.png"));
        tabSheet.addTab(getSubscriberDetailsLayout(), "Subscriber Details", new ThemeResource(
            "images/subscriber-details.png"));

        addComponent(tabSheet);
    }

    private SubscriberSummaryLayout getSubscriberSummaryLayout()
    {
        if (null == subscriberSummaryLayout)
        {
            subscriberSummaryLayout = new SubscriberSummaryLayout(new SubscriberListener()
            {
                private static final long serialVersionUID = 7404878800741317427L;

                public void changed(final BBASubscriber subscriber)
                {
                    if (null != subscriber)
                    {
                        BBASubscriber subscriberRefreshed =
                            getService(AppSubscriberDao.class).findById(subscriber.getId());
                        getSubscriberDetailsLayout().setModel(subscriberRefreshed);
                        tabSheet.setSelectedTab(getSubscriberDetailsLayout());
                    }
                    else
                    {
                        getSubscriberDetailsLayout().setModel(new BBASubscriber());
                    }
                }
            });
        }

        return subscriberSummaryLayout;
    }

    private SubscriberDetailsLayout getSubscriberDetailsLayout()
    {
        if (null == subscriberDetailsLayout)
        {
            subscriberDetailsLayout = new SubscriberDetailsLayout();
        }

        return subscriberDetailsLayout;
    }

    /**
     * <p>
     * Subscriber Listener.
     * </p>
     * 
     * @author Manuja
     * 
     * @version $Id$
     */
    public interface SubscriberListener extends Serializable
    {
        /**
         * <p>
         * This will invoke when selected subscriber changed.
         * </p>
         * 
         * @param subscriber
         *            new subscriber instance
         */
        void changed(BBASubscriber subscriber);
    }

}
