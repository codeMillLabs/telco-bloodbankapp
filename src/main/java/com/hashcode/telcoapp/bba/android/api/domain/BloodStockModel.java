/*
 * FILENAME
 *     BloodStockModel.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.telcoapp.bba.android.api.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.hashcode.telcoapp.bba.enums.BloodType;
import com.hashcode.telcoapp.bba.enums.SubBloodType;
import com.hashcode.telcoapp.bba.util.BBAUtils;

/**
 * <p>
 * Blood stock model base object.
 * </p>
 * 
 * @author Amila Silva
 */
public class BloodStockModel
{

    private String date;
    private String bloodType;
    private String subType;
    private String statusSubType;
    private String centerId;
    private String stockCount;

    /**
     * <p>
     * Parse date.
     * </p>
     *
     * @return date instance
     */
    public Date parseDate()
    {
        if (null != date)
        {
            try
            {
                SimpleDateFormat formatter = new SimpleDateFormat(BBAUtils.ANDROID_API_DATE_FORMAT);
                return formatter.parse(date);
            }
            catch (Exception ex)
            {
            }
        }

        return null;
    }

    /**
     * <p>
     * Setting value for date.
     * </p>
     * 
     * @param inDate
     *            the date to set
     */
    public void setDate(final String inDate)
    {
        this.date = inDate;
    }

    /**
     * <p>
     * Getter for centerId.
     * </p>
     * 
     * @return the centerId
     */
    public String getCenterId()
    {
        return centerId;
    }

    /**
     * <p>
     * Setting value for centerId.
     * </p>
     * 
     * @param inCenterId
     *            the centerId to set
     */
    public void setCenterId(final String inCenterId)
    {
        this.centerId = inCenterId;
    }

    /**
     * <p>
     * Getter for bloodType.
     * </p>
     * 
     * @return the bloodType
     */
    public String getBloodType()
    {
        return bloodType;
    }

    /**
     * <p>
     * Parse bloodType.
     * </p>
     * 
     * @return the bloodType
     */
    public BloodType parseBloodType()
    {
        return BBAUtils.mapBloodType(bloodType);
    }

    /**
     * <p>
     * Setting value for bloodType.
     * </p>
     * 
     * @param inBloodType
     *            the bloodType to set
     */
    public void setBloodType(final String inBloodType)
    {
        this.bloodType = inBloodType;
    }

    /**
     * <p>
     * Getter for subType.
     * </p>
     * 
     * @return the subType
     */
    public String getSubType()
    {
        return subType;
    }

    /**
     * <p>
     * Parse subType.
     * </p>
     * 
     * @return the subType
     */
    public SubBloodType parseSubType()
    {
        return BBAUtils.mapSubBloodType(subType);
    }

    /**
     * <p>
     * Setting value for subType.
     * </p>
     * 
     * @param inSubType
     *            the subType to set
     */
    public void setSubType(final String inSubType)
    {
        this.subType = inSubType;
    }

    /**
     * <p>
     * Get status sub type.
     * </p>
     * 
     * @return the statusSubType
     */
    public String getStatusSubType()
    {
        return statusSubType;
    }

    /**
     * <p>
     * Setting value for subType.
     * </p>
     * 
     * @param inStatusSubType
     *            the subType to set
     */
    public void setStatusSubType(final String inStatusSubType)
    {
        this.statusSubType = inStatusSubType;
    }

    /**
     * <p>
     * Get stock count.
     * </p>
     * 
     * @return the stockCount
     */
    public String getStockCount()
    {
        return stockCount;
    }

    /**
     * <p>
     * Setting value for stock.
     * </p>
     * 
     * @param inStockCount
     *            the stock to set
     */
    public void setStockCount(final String inStockCount)
    {
        this.stockCount = inStockCount;
    }

    /**
     * <p>
     * Get date.
     * </p>
     * 
     * @return the date
     */
    public String getDate()
    {
        return date;
    }

    /**
     * <p>
     * Parse stock.
     * </p>
     * 
     * @return the stock
     */
    public int parseStock()
    {
        try
        {
            return Integer.parseInt(stockCount);
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format(
            "BloodStockModel [date=%s, bloodType=%s, subType=%s, statusSubType=%s, centerId=%s, stockCount=%s]", date,
            bloodType, subType, statusSubType, centerId, stockCount);
    }

}
