/*
 * FILENAME
 *     BulkMessageDetailsLayout.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.telcoapp.bba.ui.bulkmessage;

import com.hashcode.telcoapp.bba.data.BulkMessage;
import com.hashcode.telcoapp.bba.service.BulkMsgSenderService;
import com.hashcode.telcoapp.common.ui.AppData;
import com.hashcode.telcoapp.common.util.ServiceResolver;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.VerticalLayout;

//
//IMPORTS
//NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Bulk Message Details Layout.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class BulkMessageDetailsLayout extends VerticalLayout
{
    private static final long serialVersionUID = 4444326352104418233L;

    private static final String SAVE = "Save";
    private static final String DISCARD = "Discard";
    private BulkMessageSendForm bulkMessageForm;
    private MenuBar menuBar;

    /**
     * <p>
     * Constructor of BulkMessageDetailsLayout.
     * </p>
     */
    public BulkMessageDetailsLayout()
    {
        initiUI();
    }

    private void initiUI()
    {
        setSizeFull();
        addComponent(getMenubar());
        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setMargin(true);
        mainLayout.addComponent(getBulkMessageSendForm());
        addComponent(mainLayout);
        addComponent(createBulkMessageTemplateLable());
    }

    private Label createBulkMessageTemplateLable()
    {
        Label label =
            new Label("<pre>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "**Bulk text message can include the following place holders."
                + " <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&lt;name&gt; -  first name of the subscriber  <br/>"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&lt;district&gt; - district of the subscriber  <br/>"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&lt;blood_type&gt; - blood type of the subscriber  <br/>"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "&lt;nic&gt; - NIC of the subscriber  <br/>"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "[eg: Dear &lt;name&gt;,"
                + " Currently we are having an urgent requirement of &lt;blood_type&gt; blood from "
                + " &lt;district&gt; area]<pre><br/><br/>", Label.CONTENT_XHTML);

        return label;
    }

    /**
     * <p>
     * Set Bulk Message.
     * </p>
     * 
     * @param bulkMessage
     *            Bulk message instance
     */
    public void setModel(final BulkMessage bulkMessage)
    {
        getBulkMessageSendForm().setModel(bulkMessage);
    }

    /**
     * <p>
     * Enable Bulk Message details ui.
     * 
     * </p>
     * 
     * @param enabled
     *            boolean value whether enabled or not
     */
    public void setUIEnabled(final boolean enabled)
    {
        getMenubar().setEnabled(enabled);
        getBulkMessageSendForm().setEnabled(enabled);
    }

    private BulkMessageSendForm getBulkMessageSendForm()
    {
        if (null == bulkMessageForm)
        {
            bulkMessageForm = new BulkMessageSendForm(createNewBulkMsg());
        }

        return bulkMessageForm;
    }

    private MenuBar getMenubar()
    {
        if (null == menuBar)
        {
            menuBar = new MenuBar();
            menuBar.setSizeFull();
            final Command command = new Command()
            {

                private static final long serialVersionUID = 6732819140363402001L;

                public void menuSelected(final MenuItem inSelectedItem)
                {
                    if (SAVE.equals(inSelectedItem.getText()))
                    {
                        BulkMessage bulkMessage = getBulkMessageSendForm().getModel();
                        if (bulkMessage.getId() == null)
                        {
                            getService(BulkMsgSenderService.class).createAndQueueBulkMessages(bulkMessage);
                            getBulkMessageSendForm().setComponentError(null);
                            getWindow().showNotification("Bulk message is scheduled and to be processed ...");

                            getBulkMessageSendForm().setModel(createNewBulkMsg());
                        }
                        else
                        {
                            getWindow().showNotification("Bulk message cannot edit ...");
                        }
                    }
                    else if (DISCARD.equals(inSelectedItem.getText()))
                    {
                        getBulkMessageSendForm().setComponentError(null);
                        getBulkMessageSendForm().setModel(createNewBulkMsg());
                    }
                }

            };

            menuBar.addItem(SAVE, command);
            menuBar.addItem(DISCARD, command);
        }
        return menuBar;
    }

    private BulkMessage createNewBulkMsg()
    {
        BulkMessage bulkMsg = new BulkMessage();
        bulkMsg.setCreatedBy(AppData.getUserData());
        return bulkMsg;
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }

}
