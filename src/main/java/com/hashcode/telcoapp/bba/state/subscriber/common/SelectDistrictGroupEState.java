/*
 * FILENAME
 *     SelectDistrictGroupEState.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.state.subscriber.common;

import com.hashcode.telcoapp.bba.data.UssdSession;
import com.hashcode.telcoapp.bba.state.BBAppState;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Select district group e state.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class SelectDistrictGroupEState extends BBAppState
{
    /**
     * {@inheritDoc}
     */
    public String messageReceived(final UssdSession ussdSession, final String message)
    {
        String responseMessage = "";
        if (message.equals("1"))
        {
            responseMessage = createDistrictSelectedMessage(ussdSession, "Nuwara Eliya");
        }
        else if (message.equals("2"))
        {
            responseMessage = createDistrictSelectedMessage(ussdSession, "Polonnaruwa");
        }
        else if (message.equals("3"))
        {
            responseMessage = createDistrictSelectedMessage(ussdSession, "Puttalam");
        }
        else if (message.equals("4"))
        {
            responseMessage = createDistrictSelectedMessage(ussdSession, "Ratnapura");
        }
        else if (message.equals("5"))
        {
            responseMessage = createDistrictSelectedMessage(ussdSession, "Trincomalee");
        }
        else if (message.equals("6"))
        {
            responseMessage = createDistrictSelectedMessage(ussdSession, "Vavuniya");
        }
        else if (message.equals("7"))
        {
            responseMessage = createDistrictMessage(ussdSession);
        }
        else
        {
            responseMessage = createInvalidMessage(ussdSession);
        }

        return responseMessage;
    }
}
