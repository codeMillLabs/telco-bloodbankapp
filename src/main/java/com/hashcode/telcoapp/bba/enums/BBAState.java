/*
 * FILENAME
 *     BBAState.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.enums;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Enum class to keep the Row status of the BBA application.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public enum BBAState
{
    ACTIVE("Active"),
    APPROVED("Approved"),
    FAST_MSGS("Fast Message"),
    PENDING("Pending"),
    SENT("Sent"),
    REJECT("Reject"),
    RETRY_1("Retry-1"),
    RETRY_2("Retry-2"),
    FAILED("Failed"),
    INVALID("Invalid");

    String desc;

    private BBAState(final String inDesc)
    {
        this.desc = inDesc;
    }

    /**
     * <p>
     * Getter for desc.
     * </p>
     * 
     * @return the desc
     */
    public String getDesc()
    {
        return desc;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString()
    {
        return desc;
    }

}
