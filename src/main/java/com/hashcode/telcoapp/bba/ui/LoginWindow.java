/*
 * FILENAME
 *     LoginWindow.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui;

import static com.hashcode.telcoapp.bba.enums.UserRole.ADMIN;
import static com.hashcode.telcoapp.bba.enums.UserRole.SYS_USER;
import static com.hashcode.telcoapp.bba.util.BBAUtils.getEncryptedPassword;
import static com.hashcode.telcoapp.bba.util.BBAUtils.getService;
import static com.hashcode.telcoapp.bba.util.BBAUtils.isNotNullOrEmpty;
import static com.hashcode.telcoapp.bba.util.Messages.getMessage;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.telcoapp.bba.dao.AppSubscriberDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.enums.UserRole;
import com.hashcode.telcoapp.common.ui.AppData;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.Sizeable;
import com.vaadin.terminal.gwt.client.ApplicationConnection;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.LoginForm;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

/**
 * <p>
 * Login Window Layout.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 * 
 */
public class LoginWindow extends VerticalLayout implements Serializable
{
    private static final long serialVersionUID = -8124047035624472238L;
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginWindow.class);

    /**
     * <p>
     * Constructor of LoginWindow.
     * </p>
     */
    public LoginWindow()
    {
        setMargin(true);
        setStyleName("background-layout");
        final LoginForm login =
            new CustomLoginForm(getMessage("login.form.username.caption"), getMessage("login.form.password.caption"),
                getMessage("login.form.login.button.caption"));

        login.addListener(new LoginForm.LoginListener()
        {
            private static final long serialVersionUID = -2701094768002029464L;

            public void onLogin(final LoginForm.LoginEvent event)
            {

                String username = event.getLoginParameter("username");
                String password = event.getLoginParameter("password");
                try
                {
                    LOGGER.info("User login request received [" + username + "]");

                    if (!(isNotNullOrEmpty(username) || isNotNullOrEmpty(password)))
                    {
                        getWindow().showNotification(getMessage("username.password.required.error"),
                            Window.Notification.TYPE_ERROR_MESSAGE);
                        return;

                    }

                    checkAuthentication(username, password);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    getWindow().showNotification(getMessage("system.error", e), Window.Notification.TYPE_ERROR_MESSAGE);
                }

            }
        });

        Label lineBr = new Label("<br/><br/><br/>", Label.CONTENT_XHTML);

        VerticalLayout footer = createFooterNote();

        addComponent(lineBr);
        addComponent(login);
        addComponent(footer);
        setComponentAlignment(footer, Alignment.MIDDLE_CENTER);

        login.setSizeUndefined();
        setComponentAlignment(login, Alignment.MIDDLE_CENTER);
    }

    private void checkAuthentication(final String username, final String password) throws Exception
    {
        BBASubscriber subscriber = getService(AppSubscriberDao.class).findByUserName(username);
        String encryptedPass = getEncryptedPassword(password);
        LOGGER.debug("Subscriber Details : {}", subscriber);

        if (subscriber == null)
        {
            getWindow().showNotification(getMessage("invalid.user"), Window.Notification.TYPE_ERROR_MESSAGE);
        }
        else if (!subscriber.getPassword().equals(encryptedPass))
        {
            getWindow().showNotification(getMessage("username.password.invalid.error"),
                Window.Notification.TYPE_ERROR_MESSAGE);
        }
        else if (subscriber.getPassword().equals(encryptedPass))
        {
            UserRole[] validRoles = new UserRole[] {
                ADMIN, SYS_USER
            };

            if (subscriber.hasAnyUserRole(validRoles))
            {
                AppData.setUserData(username);
                getApplication().getMainWindow().addComponent(new MainWindow());
                getApplication().getMainWindow().removeComponent(LoginWindow.this);
                LOGGER.info("User login successfull, [Username : {}]", username);
            }
            else
            {
                getWindow().showNotification(getMessage("username.has.no.access.rights"),
                    Window.Notification.TYPE_ERROR_MESSAGE);
            }
        }
        else
        {
            getWindow().showNotification(getMessage("username.password.invalid.error"),
                Window.Notification.TYPE_ERROR_MESSAGE);
        }

    }

    private VerticalLayout createFooterNote()
    {
        VerticalLayout footer = new VerticalLayout();
        footer.setWidth(940, Sizeable.UNITS_PIXELS);
        footer.setMargin(true);
        Link hashcodeNamLink = new Link("hashCode Solutions", new ExternalResource("http://www.hashcodesys.com/"));
        hashcodeNamLink.setTargetName("_blank");
        Label copyRights = new Label("&nbsp;&nbsp;version 2.0 \u00A9  2014. All rights reserved.", Label.CONTENT_XHTML);

        HorizontalLayout foorterLableLayout = new HorizontalLayout();

        foorterLableLayout.addComponent(hashcodeNamLink);
        foorterLableLayout.addComponent(copyRights);
        foorterLableLayout.setComponentAlignment(hashcodeNamLink, Alignment.MIDDLE_RIGHT);
        foorterLableLayout.setComponentAlignment(copyRights, Alignment.MIDDLE_RIGHT);
        foorterLableLayout.setSizeUndefined();

        footer.addComponent(foorterLableLayout);
        footer.setComponentAlignment(foorterLableLayout, Alignment.MIDDLE_CENTER);
        return footer;
    }

    /**
     * <p>
     * Custom Login Form. Source : http://demo.vaadin.com/book-examples/book/#component.loginform.customization
     * </p>
     * 
     * @author manuja
     * 
     * @version $Id$
     */
    private class CustomLoginForm extends LoginForm
    {
        private static final long serialVersionUID = -5591991879129365492L;

        String usernameCaption;
        String passwordCaption;
        String submitCaption;

        /**
         * <p>
         * Custom Login Form Constructor.
         * </p>
         * 
         * @param inUsernameCaption
         *            username field caption
         * @param inPasswordCaption
         *            password field caption
         * @param inSubmitCaption
         *            log in button caption
         */
        public CustomLoginForm(final String inUsernameCaption, final String inPasswordCaption,
            final String inSubmitCaption)
        {
            this.usernameCaption = inUsernameCaption;
            this.passwordCaption = inPasswordCaption;
            this.submitCaption = inSubmitCaption;
        }

        @Override
        protected byte[] getLoginHTML()
        {
            String appUri = getApplication().getURL().toString() + getWindow().getName() + "/";

            String x, h, b;
            x =
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD " + "XHTML 1.0 Transitional//EN\" "
                    + "\"http://www.w3.org/TR/xhtml1/" + "DTD/xhtml1-transitional.dtd\">\n";
            h =
                "<head><script type='text/javascript'>" + "var setTarget = function() {" + "  var uri = '" + appUri
                    + "loginHandler';" + "  var f = document.getElementById('loginf');"
                    + "  document.forms[0].action = uri;" + "  document.forms[0].username.focus();" + "};" + ""
                    + "var styles = window.parent.document.styleSheets;" + "for(var j = 0; j < styles.length; j++) {\n"
                    + "  if(styles[j].href) {" + "    var stylesheet = document.createElement('link');\n"
                    + "    stylesheet.setAttribute('rel', 'stylesheet');\n"
                    + "    stylesheet.setAttribute('type', 'text/css');\n"
                    + "    stylesheet.setAttribute('href', styles[j].href);\n"
                    + "    document.getElementsByTagName('head')[0]" + "                .appendChild(stylesheet);\n"
                    + "  }" + "}\n" + "function submitOnEnter(e) {" + "  var keycode = e.keyCode || e.which;"
                    + "  if (keycode == 13) {document.forms[0].submit();}" + "}\n" + "</script>" + "</head>";
            b =
                "<body onload='setTarget();'" + "  style='margin:0;padding:0; background:transparent;'" + "  class='"
                    + ApplicationConnection.GENERATED_BODY_CLASSNAME + "'>" + "<div class='v-app v-app-loginpage'"
                    + "     style='background:transparent;'>" + "<iframe name='logintarget' style='width:0;height:0;"
                    + "border:0;margin:0;padding:0;'></iframe>" + "<form id='loginf' target='logintarget'"
                    + "      onkeypress='submitOnEnter(event)'" + "      method='post'>" + "<table>" + "<tr><td>"
                    + usernameCaption + "</td>" + "<td><input class='v-textfield' style='display:block;'"
                    + "           type='text' name='username'></td></tr>" + "<tr><td>" + passwordCaption + "</td>"
                    + "    <td><input class='v-textfield'" + "          style='display:block;' type='password'"
                    + "          name='password'></td></tr>" + "</table>" + "<div>"
                    + "<div onclick='document.forms[0].submit();'"
                    + "     tabindex='0' class='v-button' role='button'>" + "<span class='v-button-wrap'>"
                    + "<span class='v-button-caption'>" + submitCaption + "</span>"
                    + "</span></div></div></form></div></body>";

            return (x + "<html>" + h + b + "</html>").getBytes();
        }
    }

}
