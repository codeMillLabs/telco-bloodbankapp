CREATE DATABASE  IF NOT EXISTS `bba` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bba`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: bba
-- ------------------------------------------------------
-- Server version	5.5.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blood_stk`
--

DROP TABLE IF EXISTS `blood_stk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blood_stk` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BLOOD_TYPE` int(11) DEFAULT NULL,
  `MODIFIED_DATE` datetime DEFAULT NULL,
  `STOCK_COUNT` int(11) DEFAULT NULL,
  `SUB_BLOOD_TYPE` int(11) DEFAULT NULL,
  `CENTER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKEE43C2256C10F9D5` (`CENTER_ID`),
  CONSTRAINT `FKEE43C2256C10F9D5` FOREIGN KEY (`CENTER_ID`) REFERENCES `donation_center` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blood_stk_history`
--

DROP TABLE IF EXISTS `blood_stk_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blood_stk_history` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BLOOD_TYPE` int(11) DEFAULT NULL,
  `RECORD_DATE` date DEFAULT NULL,
  `STOCK_COUNT` int(11) DEFAULT NULL,
  `SUB_BLOOD_TYPE` int(11) DEFAULT NULL,
  `CENTER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK33E0E45A6C10F9D5` (`CENTER_ID`),
  CONSTRAINT `FK33E0E45A6C10F9D5` FOREIGN KEY (`CENTER_ID`) REFERENCES `donation_center` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bulk_message`
--

DROP TABLE IF EXISTS `bulk_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bulk_message` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `AP_DONOR` bit(1) DEFAULT NULL,
  `BLOOD_TYPE` int(11) DEFAULT NULL,
  `CREATED_BY` varchar(255) DEFAULT NULL,
  `CREATED_DATE` datetime DEFAULT NULL,
  `DISTRICT` varchar(255) DEFAULT NULL,
  `TEXT_MESSAGE` varchar(255) DEFAULT NULL,
  `NIC` varchar(255) DEFAULT NULL,
  `IS_SCHEDULED_MSG` bit(1) DEFAULT NULL,
  `SCHEDULE_TIME` datetime DEFAULT NULL,
  `SEND_TO_ALL` bit(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `common_subscriber`
--

DROP TABLE IF EXISTS `common_subscriber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `common_subscriber` (
  `DTYPE` varchar(31) NOT NULL,
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `admin` bit(1) NOT NULL,
  `FIRST_NAME` varchar(255) DEFAULT NULL,
  `GENDER` int(11) DEFAULT NULL,
  `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
  `LAST_NAME` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `POINTS` int(11) DEFAULT NULL,
  `REG_DATE` date DEFAULT NULL,
  `SUB_STATE` int(11) DEFAULT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `AP_DONOR` bit(1) DEFAULT NULL,
  `BIRTH_DATE` int(11) DEFAULT NULL,
  `BIRTH_MONTH` int(11) DEFAULT NULL,
  `BLOOD_STOCK_ACCOUNT` bit(1) DEFAULT NULL,
  `BLOOD_TYPE` varchar(255) DEFAULT NULL,
  `DISTRICT` varchar(255) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `HOME_ADDRESS` varchar(255) DEFAULT NULL,
  `LAST_DONATED_DATE` date DEFAULT NULL,
  `NEXT_ELIGIBLE_DATE` date DEFAULT NULL,
  `NIC` varchar(255) DEFAULT NULL,
  `REMARKS` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ADDRESS` (`ADDRESS`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `common_subscriber_user_roles`
--

DROP TABLE IF EXISTS `common_subscriber_user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `common_subscriber_user_roles` (
  `SUBSCRIBER_ID` bigint(20) NOT NULL,
  `USER_ROLE` varchar(255) DEFAULT NULL,
  KEY `FK122EB20C551CCC92` (`SUBSCRIBER_ID`),
  CONSTRAINT `FK122EB20C551CCC92` FOREIGN KEY (`SUBSCRIBER_ID`) REFERENCES `common_subscriber` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `donate_as_at_history`
--

DROP TABLE IF EXISTS `donate_as_at_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donate_as_at_history` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DONATED_DATE` date DEFAULT NULL,
  `REF_NO` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `SUBS_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK55E478C58809B6E7` (`SUBS_ID`),
  CONSTRAINT `FK55E478C58809B6E7` FOREIGN KEY (`SUBS_ID`) REFERENCES `common_subscriber` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `donation_center`
--

DROP TABLE IF EXISTS `donation_center`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donation_center` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `CENTER_NAME` varchar(255) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `CONTACT` varchar(255) DEFAULT NULL,
  `DISTRICT` varchar(255) DEFAULT NULL,
  `DONATION_DATE` date DEFAULT NULL,
  `LOCATION_TYPE` int(11) DEFAULT NULL,
  `MAIN_OFFIER` varchar(255) DEFAULT NULL,
  `ADMIN` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK352BFDA2FCE1638F` (`ADMIN`),
  CONSTRAINT `FK352BFDA2FCE1638F` FOREIGN KEY (`ADMIN`) REFERENCES `common_subscriber` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message_queue`
--

DROP TABLE IF EXISTS `message_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_queue` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MESSAGE` varchar(255) DEFAULT NULL,
  `SCHEDULED_DATE` datetime DEFAULT NULL,
  `SENT_BY` varchar(255) DEFAULT NULL,
  `SENT_DATE` datetime DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `TO_ADDRESS` varchar(255) DEFAULT NULL,
  `BULK_MSG_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKA7DB4699EA88F92` (`BULK_MSG_ID`),
  CONSTRAINT `FKA7DB4699EA88F92` FOREIGN KEY (`BULK_MSG_ID`) REFERENCES `bulk_message` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `request_message`
--

DROP TABLE IF EXISTS `request_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request_message` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `RECEIVED_DATE` date DEFAULT NULL,
  `TEXT_MSG` varchar(255) DEFAULT NULL,
  `RECEIVED_TIME` datetime DEFAULT NULL,
  `SUBSCRIBER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK9AAED217551CCC92` (`SUBSCRIBER_ID`),
  CONSTRAINT `FK9AAED217551CCC92` FOREIGN KEY (`SUBSCRIBER_ID`) REFERENCES `common_subscriber` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `response_msg`
--

DROP TABLE IF EXISTS `response_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response_msg` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SEND_ADDRESS` varchar(255) DEFAULT NULL,
  `MESSAGE` varchar(255) DEFAULT NULL,
  `SEND_FLAG` tinyint(1) NOT NULL DEFAULT '0',
  `ussdMessage` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ussd_session`
--

DROP TABLE IF EXISTS `ussd_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ussd_session` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ACTIVE` bit(1) DEFAULT NULL,
  `CONNECT_CACHE` varchar(255) DEFAULT NULL,
  `currentState` int(11) DEFAULT NULL,
  `SESSION_ID` varchar(255) DEFAULT NULL,
  `SUBS_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK97CABBA68809B6E7` (`SUBS_ID`),
  CONSTRAINT `FK97CABBA68809B6E7` FOREIGN KEY (`SUBS_ID`) REFERENCES `common_subscriber` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-02-25 10:58:32
