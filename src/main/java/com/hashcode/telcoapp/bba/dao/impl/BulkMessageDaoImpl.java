/*
 * FILENAME
 *     BulkMessageDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.telcoapp.bba.dao.impl;

import java.util.Collections;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

import com.hashcode.telcoapp.bba.dao.BulkMessageDao;
import com.hashcode.telcoapp.bba.data.BulkMessage;
import com.hashcode.telcoapp.bba.service.BulkMsgCriteria;
import com.hashcode.telcoapp.common.dao.impl.GenericDaoImpl;

/**
 * <p>
 * Bulk Message Dao Implementation.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 */
public class BulkMessageDaoImpl extends GenericDaoImpl<BulkMessage, Long> implements BulkMessageDao
{

    /**
     * {@inheritDoc}
     * 
     * @see BulkMessageDao#findByCriteria(SearchCriteria)
     */
    @SuppressWarnings("unchecked")
    public List<BulkMessage> findByCriteria(final BulkMsgCriteria criteria)
    {
        System.out.println("::::::::::::::::  Criteria :" + criteria);
        Session session = (Session) entityManager.getDelegate();
        Criteria crit = session.createCriteria(BulkMessage.class);
        crit = crit.createAlias("messages", "msg");

        boolean atLeastOneSearchCriteria = false;

        if (criteria.isSearchAll())
        {
            return crit.list();
        }

        if (criteria.getApDonor() != null)
        {
            crit.add(Restrictions.eq("apDonor", criteria.getApDonor()));
            atLeastOneSearchCriteria = true;
        }

        if (criteria.getBloodType() != null)
        {
            crit.add(Restrictions.eq("bloodType", criteria.getBloodType()));
            atLeastOneSearchCriteria = true;
        }

        if (criteria.getNic() != null)
        {
            String nic = criteria.getNic().replace("*", "%");
            crit.add(Restrictions.ilike("nic", nic));
            atLeastOneSearchCriteria = true;
        }

        if (criteria.getDistrict() != null)
        {
            crit.add(Restrictions.eq("district", criteria.getDistrict()));
            atLeastOneSearchCriteria = true;
        }

        if (criteria.getCreatedBy() != null)
        {
            String createdBy = criteria.getCreatedBy().replace("*", "%");
            crit.add(Restrictions.ilike("createdBy", createdBy));
        }

        if (criteria.getScheduledTime() != null)
        {
            DateTime datetime =
                new DateTime(criteria.getScheduledTime().getTime()).withHourOfDay(0).withMinuteOfHour(0)
                    .withSecondOfMinute(0);
            crit.add(Restrictions.ge("scheduledTime", datetime.toDate()));
        }

        if (criteria.getSentDate() != null)
        {
            DateTime datetime =
                new DateTime(criteria.getSentDate().getTime()).withHourOfDay(0).withMinuteOfHour(0)
                    .withSecondOfMinute(0);
            crit.add(Restrictions.ge("msg.sentDate", datetime.toDate()));
            crit.add(Restrictions.lt("msg.sentDate", datetime.plusDays(1).toDate()));
            atLeastOneSearchCriteria = true;
        }

        if (!criteria.getStatus().isEmpty())
        {

            crit.add(Restrictions.in("msg.status", criteria.getStatus()));
        }

        if (!atLeastOneSearchCriteria)
        {
            return Collections.emptyList();
        }

        if (criteria.getStartPosition() >= 0)
        {
            crit.setFirstResult(criteria.getStartPosition());
            crit.setMaxResults(criteria.getEndPosition());
        }

        return crit.list();
    }

}
