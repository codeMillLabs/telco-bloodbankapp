/*
 * FILENAME
 *     BulkMessageDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.telcoapp.bba.dao;

import java.util.List;

import com.hashcode.telcoapp.bba.data.BulkMessage;
import com.hashcode.telcoapp.bba.service.BulkMsgCriteria;
import com.hashcode.telcoapp.common.dao.GenericDao;

/**
 * <p>
 * Bulk Messages dao interface.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 */
public interface BulkMessageDao extends GenericDao<BulkMessage, Long>
{
    /**
     * <p>
     * Find bulk messages by search criteria.
     * </p>
     *
     * @param criteria search criteria
     * 
     * @return list of bulk messages
     */
    List<BulkMessage> findByCriteria(final BulkMsgCriteria criteria);
}
