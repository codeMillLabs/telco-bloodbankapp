/*
 * FILENAME
 *     DecodedNic.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.data;

import java.util.Date;

import com.hashcode.telcoapp.common.enums.Gender;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * National IC information of an entity.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class NICInfo
{
    private int year;
    private Date dob;
    private Gender gender;

    /**
     * <p>
     * Constructor.
     * </p>
     * 
     * @param inYear
     *            year
     * @param inDob
     *            dob
     * @param inGender
     *            gender
     */
    public NICInfo(final int inYear, final Date inDob, final Gender inGender)
    {
        super();
        this.year = inYear;
        this.dob = inDob;
        this.gender = inGender;
    }

    /**
     * <p>
     * Getter for year.
     * </p>
     * 
     * @return the year
     */
    public int getYear()
    {
        return year;
    }

    /**
     * <p>
     * Setting value for year.
     * </p>
     * 
     * @param inYear
     *            the year to set
     */
    public void setYear(final int inYear)
    {
        this.year = inYear;
    }

    /**
     * <p>
     * Getter for date.
     * </p>
     * 
     * @return the date
     */
    public Date getDob()
    {
        return dob;
    }

    /**
     * <p>
     * Setting value for dob.
     * </p>
     * 
     * @param inDob
     *            the dob to set
     */
    public void setDob(final Date inDob)
    {
        this.dob = inDob;
    }

    /**
     * <p>
     * Getter for gender.
     * </p>
     * 
     * @return the gender
     */
    public Gender getGender()
    {
        return gender;
    }

    /**
     * <p>
     * Setting value for gender.
     * </p>
     * 
     * @param inGender
     *            the gender to set
     */
    public void setGender(final Gender inGender)
    {
        this.gender = inGender;
    }

   /**
    * 
    * {@inheritDoc}
    *
    * @see java.lang.Object#toString()
    */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("NICInfo [year=");
        builder.append(year);
        builder.append(", dob=");
        builder.append(dob);
        builder.append(", gender=");
        builder.append(gender);
        builder.append("]");
        return builder.toString();
    }

}
