/*
 * FILENAME
 *     Message.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * <p>
 * Message Entity class.
 * </p>
 * 
 * @author manuja
 * 
 * @version $Id$
 */
@Entity
@Table(name = "REQUEST_MESSAGE")
public class Message implements Serializable
{
    private static final long serialVersionUID = 7229114144710964904L;
    
    public static final String ID = "id";
    public static final String SUBSCRIBER = "subscriber";
    public static final String MESSAGE = "messageBody";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "REW_MSG_SEQ")
    @SequenceGenerator(name = "REW_MSG_SEQ", sequenceName = "REW_MSG_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false)
    private Long id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "SUBSCRIBER_ID")
    private BBASubscriber subscriber;

    @Column(name = "TEXT_MSG")
    private String messageBody;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "RECEIVED_DATE")
    private Date date;
    
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "RECEIVED_TIME")
    private Date time;


    /**
     * Getter for id.
     * 
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Getter for subscriber.
     * 
     * @return the subscriber
     */
    public BBASubscriber getSubscriber()
    {
        return subscriber;
    }

    /**
     * Setting value for subscriber.
     * 
     * @param inSubscriber
     *            the subscriber to set
     */
    public void setSubscriber(final BBASubscriber inSubscriber)
    {
        this.subscriber = inSubscriber;
    }

    /**
     * <p>
     * Getter for messageBody.
     * </p>
     * 
     * @return the messageBody
     */
    public String getMessageBody()
    {
        return messageBody;
    }

    /**
     * <p>
     * Setting value for messageBody.
     * </p>
     * 
     * @param inMessageBody
     *            the messageBody to set
     */
    public void setMessageBody(final String inMessageBody)
    {
        this.messageBody = inMessageBody;
    }

    /**
     * <p>
     * Getter for date.
     * </p>
     * 
     * @return the date
     */
    public Date getDate()
    {
        return date;
    }

    /**
     * <p>
     * Setting value for date.
     * </p>
     * 
     * @param inDate the date to set
     */
    public void setDate(final Date inDate)
    {
        this.date = inDate;
        setTime(inDate);
    }

    /**
     * <p>
     * Getter for time.
     * </p>
     * 
     * @return the time
     */
    public Date getTime()
    {
        return time;
    }

    /**
     * <p>
     * Setting value for time.
     * </p>
     * 
     * @param inTime the time to set
     */
    public void setTime(final Date inTime)
    {
        this.time = inTime;
    }
}
