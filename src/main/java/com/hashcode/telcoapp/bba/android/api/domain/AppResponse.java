/*
 * FILENAME
 *     StatusCodes.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.telcoapp.bba.android.api.domain;

import java.io.Serializable;

/**
 * <p>
 * App Response base object.
 * </p>
 * 
 * @author Amila Silva
 * 
 */
public class AppResponse implements Serializable
{
    private static final long serialVersionUID = 5862074029443644477L;
    
    private String status;
    private String message;

    /**
     * <p>
     * Get status.
     * </p>
     * 
     * @return the status
     */
    public String getStatus()
    {
        return status;
    }

    /**
     * <p>
     * Set status.
     * </p>
     * 
     * @param inStatus
     *            the status to set
     */
    public void setStatus(final String inStatus)
    {
        this.status = inStatus;
    }

    /**
     * <p>
     * Get messsage.
     * </p>
     * 
     * @return the message
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * <p>
     * Set message.
     * </p>
     * 
     * @param inMessage
     *            the message to set
     */
    public void setMessage(final String inMessage)
    {
        this.message = inMessage;
    }
}
