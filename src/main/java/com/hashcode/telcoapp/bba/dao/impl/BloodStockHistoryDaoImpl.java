/*
 * FILENAME
 *     BloodStockHistoryDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.dao.impl;

import java.util.Collections;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.telcoapp.bba.dao.BloodStockHistoryDao;
import com.hashcode.telcoapp.bba.dao.BloodStockHistorySearchCriteria;
import com.hashcode.telcoapp.bba.data.BloodStockHistory;
import com.hashcode.telcoapp.common.dao.impl.GenericDaoImpl;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood stock history dao implementation.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
@Transactional
@Repository
public class BloodStockHistoryDaoImpl extends GenericDaoImpl<BloodStockHistory, Long> implements BloodStockHistoryDao
{
    /**
     * {@inheritDoc}
     * 
     * @see BloodStockHistoryDao#findBySearchCriteria(BloodStockHistorySearchCriteria)
     */
    @SuppressWarnings("unchecked")
    public List<BloodStockHistory> findBySearchCriteria(final BloodStockHistorySearchCriteria searchCriteria)
    {
        Session session = (Session) entityManager.getDelegate();
        Criteria crit = session.createCriteria(BloodStockHistory.class);
        crit.createAlias("center", "dCenter");

        boolean hasRestrictions = addRestrictions(searchCriteria, crit);

        if (hasRestrictions)
        {
            return crit.list();
        }
        else
        {
            return Collections.emptyList();
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see BloodStockHistoryDao#countByCriteria(BloodStockHistorySearchCriteria)
     */
    public Integer countByCriteria(final BloodStockHistorySearchCriteria searchCriteria)
    {
        Session session = (Session) entityManager.getDelegate();
        Criteria crit = session.createCriteria(BloodStockHistory.class);
        crit.createAlias("center", "dCenter");
        crit = crit.setProjection(Projections.rowCount());

        boolean hasRestrictions = addRestrictions(searchCriteria, crit);

        if (hasRestrictions)
        {
            @SuppressWarnings("rawtypes")
            List results = crit.list();

            if (results != null)
            {
                return ((Long) results.get(0)).intValue();
            }
        }

        return 0;

    }

    private boolean addRestrictions(final BloodStockHistorySearchCriteria searchCriteria, final Criteria crit)
    {
        boolean atLeastOneSearchCriteria = false;

        crit.addOrder(Order.desc("recordDate"));
        crit.addOrder(Order.asc("dCenter.centerName"));

        if (searchCriteria.isSearchAll())
        {
            return true;
        }

        if (searchCriteria.getBloodType() != null)
        {
            crit.add(Restrictions.eq("bloodType", searchCriteria.getBloodType()));
            atLeastOneSearchCriteria = true;
        }

        if (searchCriteria.getSubBloodType() != null)
        {
            crit.add(Restrictions.eq("subBloodType", searchCriteria.getSubBloodType()));
            atLeastOneSearchCriteria = true;
        }

        if (searchCriteria.getCenter() != null)
        {
            crit.add(Restrictions.eq("dCenter.id", searchCriteria.getCenter().getId()));
            atLeastOneSearchCriteria = true;
        }
        
        if (searchCriteria.getBloodStatus() != null)
        {
            crit.add(Restrictions.eq("bloodStatus", searchCriteria.getBloodStatus()));
            atLeastOneSearchCriteria = true;
        }

        if (searchCriteria.getDateTime() != null)
        {
            crit.add(Restrictions.eq("recordDate", searchCriteria.getDateTime()));
            atLeastOneSearchCriteria = true;
        }

        if (!atLeastOneSearchCriteria)
        {
            return false;
        }

        if (searchCriteria.getStartPosition() >= 0)
        {
            crit.setFirstResult(searchCriteria.getStartPosition());
            crit.setMaxResults(searchCriteria.getEndPosition());
        }

        return atLeastOneSearchCriteria;
    }

}
