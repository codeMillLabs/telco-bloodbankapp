/*
 * FILENAME
 *     BloodStockHistorySearchCriteria.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.dao;

import java.util.Date;

import com.hashcode.telcoapp.bba.data.DonationCenter;
import com.hashcode.telcoapp.bba.enums.BloodStatus;
import com.hashcode.telcoapp.bba.enums.BloodType;
import com.hashcode.telcoapp.bba.enums.SubBloodType;
import com.hashcode.telcoapp.common.dao.SearchCriteria;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood stock history search cirtaria.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
public class BloodStockHistorySearchCriteria extends SearchCriteria
{
    private static final long serialVersionUID = -338573186472708640L;
    
    public static final String BLOOD_TYPE = "bloodType";
    public static final String SUB_BLOOD_TYPE = "subBloodType";
    public static final String BLOOD_STATUS = "bloodStatus";
    public static final String CENTER = "center";
    public static final String DATE_TIME = "dateTime";
    
    private BloodType bloodType;
    private SubBloodType subBloodType;
    private DonationCenter center;
    private Date dateTime;
    private BloodStatus bloodStatus;

    /**
     * <p>
     * Getter for bloodType.
     * </p>
     * 
     * @return the bloodType
     */
    public BloodType getBloodType()
    {
        return bloodType;
    }
    
    /**
     * <p>
     * Setting value for bloodType.
     * </p>
     * 
     * @param inBloodType the bloodType to set
     */
    public void setBloodType(final BloodType inBloodType)
    {
        this.bloodType = inBloodType;
    }
    
    /**
     * <p>
     * Getter for subBloodType.
     * </p>
     * 
     * @return the subBloodType
     */
    public SubBloodType getSubBloodType()
    {
        return subBloodType;
    }

    /**
     * <p>
     * Setting value for subBloodType.
     * </p>
     * 
     * @param inSubBloodType the subBloodType to set
     */
    public void setSubBloodType(final SubBloodType inSubBloodType)
    {
        this.subBloodType = inSubBloodType;
    }

    /**
     * <p>
     * Getter for center.
     * </p>
     * 
     * @return the center
     */
    public DonationCenter getCenter()
    {
        return center;
    }
    
    /**
     * <p>
     * Setting value for center.
     * </p>
     * 
     * @param inCenter the center to set
     */
    public void setCenter(final DonationCenter inCenter)
    {
        this.center = inCenter;
    }
    
    /**
     * <p>
     * Getter for dateTime.
     * </p>
     * 
     * @return the dateTime
     */
    public Date getDateTime()
    {
        return dateTime;
    }
    
    /**
     * <p>
     * Setting value for dateTime.
     * </p>
     * 
     * @param inDateTime the dateTime to set
     */
    public void setDateTime(final Date inDateTime)
    {
        this.dateTime = inDateTime;
    }

    /**
     * <p>
     * Getter for bloodStatus.
     * </p>
     * 
     * @return the bloodStatus
     */
    public BloodStatus getBloodStatus()
    {
        return bloodStatus;
    }

    /**
     * <p>
     * Setting value for bloodStatus.
     * </p>
     * 
     * @param inBloodStatus the bloodStatus to set
     */
    public void setBloodStatus(final BloodStatus inBloodStatus)
    {
        this.bloodStatus = inBloodStatus;
    }
}
