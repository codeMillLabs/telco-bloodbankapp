/*
 * FILENAME
 *     UssdSessionDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.dao;

import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.data.UssdSession;
import com.hashcode.telcoapp.common.dao.GenericDao;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Ussd session dao interface.
 * </p>
 * 
 * @author manuja
 * 
 * @version $Id$
 */
public interface UssdSessionDao extends GenericDao<UssdSession, Long>
{
    /**
     * <p>
     * Find ussd session by app subscriber and session id.
     * </p>
     * 
     * @param appSubscriber
     *            app subscriber instance
     * @param sessionId
     *            session Id
     * 
     * @return ussd session instance
     */
    UssdSession findUssdSession(BBASubscriber appSubscriber, String sessionId);
}
