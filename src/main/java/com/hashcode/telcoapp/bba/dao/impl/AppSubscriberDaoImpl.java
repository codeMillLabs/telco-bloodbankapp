/*
 * FILENAME
 *     AppSubscriberDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.dao.impl;

import static com.hashcode.telcoapp.common.enums.SubscriberState.REGISTERED;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.telcoapp.bba.dao.AppSubscriberDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.enums.UserRole;
import com.hashcode.telcoapp.bba.service.BulkMsgCriteria;
import com.hashcode.telcoapp.common.dao.impl.GenericDaoImpl;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * App Subscriber Dao Implementation.
 * </p>
 * 
 * @author manuja
 * 
 * @version $Id$
 */
@Transactional
@Repository
public class AppSubscriberDaoImpl extends GenericDaoImpl<BBASubscriber, Long> implements AppSubscriberDao
{

    private static final String FIND_APP_SUBSCRIBER_BY_ID = " SELECT s FROM " + BBASubscriber.class.getName()
        + " s JOIN FETCH s.userRoles WHERE s.id = :id ";

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.telcoapp.common.dao.impl.GenericDaoImpl#findById(java.io.Serializable)
     */
    @Override
    public BBASubscriber findById(final Long inId)
    {
        try
        {
            TypedQuery<BBASubscriber> query = entityManager.createQuery(FIND_APP_SUBSCRIBER_BY_ID, BBASubscriber.class);
            query.setParameter("id", inId);
            query.setMaxResults(1);
            return query.getSingleResult();
        }
        catch (NoResultException e)
        {
            return null;
        }
    }

    private static final String FIND_APP_SUBSCRIBER_BY_USER_NAME = " SELECT s FROM " + BBASubscriber.class.getName()
        + " s JOIN FETCH s.userRoles WHERE s.userName = :userName " + " AND s.subscriberState = :subscriberState";

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.telcoapp.bba.dao.AppSubscriberDao#findByUserName(java.lang.String)
     */
    public BBASubscriber findByUserName(final String userName)
    {
        try
        {
            TypedQuery<BBASubscriber> query =
                entityManager.createQuery(FIND_APP_SUBSCRIBER_BY_USER_NAME, BBASubscriber.class);
            query.setParameter("userName", userName);
            query.setParameter("subscriberState", REGISTERED);
            query.setMaxResults(1);
            return query.getSingleResult();
        }
        catch (NoResultException e)
        {
            return null;
        }
    }

    private static final String FIND_APP_SUBSCRIBER_BY_USER_NAME_AND_PASSWORD = " SELECT s FROM "
        + BBASubscriber.class.getName()
        + " s JOIN FETCH s.userRoles WHERE s.userName = :userName  AND s.password = :password "
        + " AND s.subscriberState = :subscriberState ";

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.telcoapp.bba.dao.AppSubscriberDao#findByUserNameAndPassword(java.lang.String, java.lang.String)
     */
    public BBASubscriber findByUserNameAndPassword(final String userName, final String password)
    {
        try
        {
            TypedQuery<BBASubscriber> query =
                entityManager.createQuery(FIND_APP_SUBSCRIBER_BY_USER_NAME_AND_PASSWORD, BBASubscriber.class);
            query.setParameter("userName", userName);
            query.setParameter("password", password);
            query.setParameter("subscriberState", REGISTERED);
            query.setMaxResults(1);
            return query.getSingleResult();
        }
        catch (NoResultException e)
        {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see AppSubscriberDao#findSubscribers(SearchCriteria)
     */
    @SuppressWarnings("unchecked")
    public List<BBASubscriber> findSubscribers(final BulkMsgCriteria searchCriteria)
    {
        Session session = (Session) entityManager.getDelegate();
        Criteria crit = session.createCriteria(BBASubscriber.class);
        crit = crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        boolean hasCriteria = addRestrictions(searchCriteria, crit);

        if (hasCriteria)
        {
            return crit.list();
        }
        else
        {
            return Collections.emptyList();
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see AppSubscriberDao#countSubscribers(com.hashcode.telcoapp.bba.service.BulkMsgCriteria)
     */
    public Integer countSubscribers(final BulkMsgCriteria searchCriteria)
    {
        Session session = (Session) entityManager.getDelegate();
        Criteria crit = session.createCriteria(BBASubscriber.class);
        crit = crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        crit = crit.setProjection(Projections.rowCount());

        boolean hasCriteria = addRestrictions(searchCriteria, crit);

        if (hasCriteria)
        {
            @SuppressWarnings("rawtypes")
            List results = crit.list();

            if (results != null)
            {
                return ((Long) results.get(0)).intValue();
            }
        }

        return 0;
    }

    private boolean addRestrictions(final BulkMsgCriteria criteria, final Criteria crit)
    {
        boolean atLeastOneSearchCriteria = false;

        crit.addOrder(Order.asc("id"));

        if (criteria.isSearchAll())
        {
            return true;
        }

        if (criteria.getApDonor() != null)
        {
            crit.add(Restrictions.eq("apDonor", criteria.getApDonor()));
            atLeastOneSearchCriteria = true;
        }

        if (criteria.getBloodType() != null)
        {
            crit.add(Restrictions.eq("bloodType", criteria.getBloodType()));
            atLeastOneSearchCriteria = true;
        }

        if (criteria.getNic() != null)
        {
            String nicNo = criteria.getNic().replace("*", "%");
            crit.add(Restrictions.ilike("nic", nicNo));
            atLeastOneSearchCriteria = true;
        }

        if (criteria.getDistrict() != null)
        {
            crit.add(Restrictions.eq("district", criteria.getDistrict()));
            atLeastOneSearchCriteria = true;
        }

        if (criteria.getPoints() > 0)
        {
            crit.add(Restrictions.ge("points", criteria.getPoints()));
            atLeastOneSearchCriteria = true;
        }

        if (criteria.getGender() != null)
        {
            crit.add(Restrictions.eq("gender", criteria.getGender()));
            atLeastOneSearchCriteria = true;
        }

        if (criteria.getEligibleDateFrom() != null)
        {
            DateTime datetime =
                new DateTime(criteria.getEligibleDateFrom().getTime()).withHourOfDay(0).withMinuteOfHour(0)
                    .withSecondOfMinute(0);
            crit.add(Restrictions.le("nextEligibleDate", datetime.toDate()));
            atLeastOneSearchCriteria = true;
        }

        if (criteria.getEligibleDateOn() != null)
        {
            DateTime datetime =
                new DateTime(criteria.getEligibleDateOn().getTime()).withHourOfDay(0).withMinuteOfHour(0)
                    .withSecondOfMinute(0);
            crit.add(Restrictions.ge("nextEligibleDate", datetime.toDate()));
            crit.add(Restrictions.lt("nextEligibleDate", datetime.plusDays(1).toDate()));
            atLeastOneSearchCriteria = true;
        }

        if (criteria.getBirthMonth() > 0)
        {
            crit.add(Restrictions.eq("birthMonth", criteria.getBirthMonth()));
            atLeastOneSearchCriteria = true;
        }

        if (criteria.getBirthDate() > 0)
        {
            crit.add(Restrictions.eq("birthDate", criteria.getBirthDate()));
            atLeastOneSearchCriteria = true;
        }

        if (criteria.getSubscriberState() != null)
        {
            crit.add(Restrictions.eq("subscriberState", criteria.getSubscriberState()));
            atLeastOneSearchCriteria = true;
        }

        if (!atLeastOneSearchCriteria)
        {
            return false;
        }

        if (criteria.getStartPosition() >= 0)
        {
            crit.setFirstResult(criteria.getStartPosition());
            crit.setMaxResults(criteria.getEndPosition());
        }

        return atLeastOneSearchCriteria;
    }

    private static final String FIND_SUBSCRIBER_BY_ADDRESS_SQL = " SELECT s FROM " + BBASubscriber.class.getName()
        + " s " + " JOIN FETCH s.userRoles WHERE s.address = :address AND s.subscriberState = :subscriberState";

    /**
     * {@inheritDoc}
     */
    public BBASubscriber findSubcriberByAddress(final String address)
    {
        try
        {
            TypedQuery<BBASubscriber> query =
                entityManager.createQuery(FIND_SUBSCRIBER_BY_ADDRESS_SQL, BBASubscriber.class);
            query.setParameter("address", address);
            query.setParameter("subscriberState", REGISTERED);
            query.setMaxResults(1);
            return query.getSingleResult();
        }
        catch (NoResultException e)
        {
            return null;
        }
    }

    private static final String FIND_BLOOD_STOCK_ACCOUNT_SUBSCRIBERS_SQL = " SELECT s FROM "
        + BBASubscriber.class.getName() + " s, IN (s.userRoles) r "
        + " WHERE  r = :roles AND s.subscriberState = :subscriberState  ";

    /**
     * {@inheritDoc}
     * 
     * @see AppSubscriberDao#findBloodStockAdmins()
     */
    public List<BBASubscriber> findBloodStockAdmins()
    {
        Set<UserRole> userRoles = new HashSet<UserRole>();
        userRoles.add(UserRole.CENTER_ADMIN);

        TypedQuery<BBASubscriber> query =
            entityManager.createQuery(FIND_BLOOD_STOCK_ACCOUNT_SUBSCRIBERS_SQL, BBASubscriber.class);
        query.setParameter("roles", userRoles);
        query.setParameter("subscriberState", REGISTERED);
        return query.getResultList();
    }

}
