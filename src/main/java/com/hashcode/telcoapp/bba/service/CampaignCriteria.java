/*
 * FILENAME
 *     CampaignCriteria.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.service;

import java.util.Date;

import com.hashcode.telcoapp.bba.enums.LocationType;
import com.hashcode.telcoapp.common.dao.SearchCriteria;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Criteria class for the Donation campaign search.
 * </p>
 * 
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class CampaignCriteria extends SearchCriteria
{
    private static final long serialVersionUID = -590745930677349797L;

    public static final String CENTER_NAME = "centerName";
    public static final String DISTRICT = "district";
    public static final String DATE = "donationDate";
    public static final String LOC_TYPE = "locationType";
    public static final String CITY = "city";
    public static final String LOCATION_REF = "locationRef";

    private String centerName;
    private String district;
    private Date donationDate;
    private LocationType locationType;
    private String city;
    private String locationRef;

    /**
     * <p>
     * Getter for centerName.
     * </p>
     * 
     * @return the centerName
     */
    public String getCenterName()
    {
        return centerName;
    }

    /**
     * <p>
     * Setting value for centerName.
     * </p>
     * 
     * @param inCenterName
     *            the centerName to set
     */
    public void setCenterName(final String inCenterName)
    {
        this.centerName = inCenterName;
    }

    /**
     * <p>
     * Getter for district.
     * </p>
     * 
     * @return the district
     */
    public String getDistrict()
    {
        return district;
    }

    /**
     * <p>
     * Setting value for district.
     * </p>
     * 
     * @param inDistrict
     *            the district to set
     */
    public void setDistrict(final String inDistrict)
    {
        this.district = inDistrict;
    }

    /**
     * <p>
     * Getter for donationDate.
     * </p>
     * 
     * @return the donationDate
     */
    public Date getDonationDate()
    {
        return donationDate;
    }

    /**
     * <p>
     * Setting value for donationDate.
     * </p>
     * 
     * @param inDonationDate
     *            the donationDate to set
     */
    public void setDonationDate(final Date inDonationDate)
    {
        this.donationDate = inDonationDate;
    }

    /**
     * <p>
     * Getter for locationType.
     * </p>
     * 
     * @return the locationType
     */
    public LocationType getLocationType()
    {
        return locationType;
    }

    /**
     * <p>
     * Setting value for locationType.
     * </p>
     * 
     * @param inLocationType
     *            the locationType to set
     */
    public void setLocationType(final LocationType inLocationType)
    {
        this.locationType = inLocationType;
    }

    /**
     * <p>
     * Getter for city.
     * </p>
     * 
     * @return the city
     */
    public String getCity()
    {
        return city;
    }

    /**
     * <p>
     * Setting value for city.
     * </p>
     * 
     * @param inCity
     *            the city to set
     */
    public void setCity(final String inCity)
    {
        this.city = inCity;
    }

    /**
     * <p>
     * Getter for locationRef.
     * </p>
     * 
     * @return the locationRef
     */
    public String getLocationRef()
    {
        return locationRef;
    }

    /**
     * <p>
     * Setting value for locationRef.
     * </p>
     * 
     * @param inLocationRef
     *            the locationRef to set
     */
    public void setLocationRef(final String inLocationRef)
    {
        this.locationRef = inLocationRef;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format(
            "CampaignCriteria [centerName=%s, district=%s, donationDate=%s, locationType=%s, city=%s, locationRef=%s]",
            centerName, district, donationDate, locationType, city, locationRef);
    }

}
