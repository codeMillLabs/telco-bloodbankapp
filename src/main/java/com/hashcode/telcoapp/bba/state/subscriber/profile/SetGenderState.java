/*
 * FILENAME
 *     SetGenderState.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.state.subscriber.profile;

import static com.hashcode.telcoapp.common.handlers.Messages.getMessage;

import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.data.UssdSession;
import com.hashcode.telcoapp.bba.enums.UssdState;
import com.hashcode.telcoapp.bba.state.BBAppState;
import com.hashcode.telcoapp.common.enums.Gender;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Set gender state.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class SetGenderState extends BBAppState
{

    /**
     * {@inheritDoc}
     * 
     * @see BBAppState#messageReceived(UssdSession, java.lang.String)
     */
    @Override
    public String messageReceived(final UssdSession ussdSession, final String message)
    {
        String responseMessage = "";
        if (message.equals("1"))
        {
            ussdSession.setCurrentState(UssdState.SUBSCRIBER_PROFILE_SUCCESS);
            ussdSession.getSubscriber().setGender(Gender.MALE);
            BBASubscriber subscriber = ussdSession.getSubscriber();
            responseMessage =
                getMessage("ussd.profile.update.details.success.message", subscriber.getFirstName(),
                    subscriber.getNic(), Gender.MALE.toString());
        }
        else if (message.equals("2"))
        {
            ussdSession.setCurrentState(UssdState.SUBSCRIBER_PROFILE_SUCCESS);
            ussdSession.getSubscriber().setGender(Gender.FEMALE);
            BBASubscriber subscriber = ussdSession.getSubscriber();
            responseMessage =
                getMessage("ussd.profile.update.details.success.message", subscriber.getFirstName(),
                    subscriber.getNic(), Gender.FEMALE.toString());
        }
        else if (message.equals("3"))
        {
            responseMessage = createProfileMessage(ussdSession);
        }
        else
        {
            responseMessage = createProfileInvalidMessage(ussdSession);
        }

        return responseMessage;
    }

}
