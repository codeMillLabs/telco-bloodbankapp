/*
 * FILENAME
 *     SelectDistrictState.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.state.subscriber.common;

import static com.hashcode.telcoapp.common.handlers.Messages.getMessage;

import com.hashcode.telcoapp.bba.data.UssdSession;
import com.hashcode.telcoapp.bba.enums.UssdState;
import com.hashcode.telcoapp.bba.state.BBAppState;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Select district state.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class SelectDistrictState extends BBAppState
{
    /**
     * {@inheritDoc}
     */
    public String messageReceived(final UssdSession ussdSession, final String message)
    {
        String responseMessage = "";
        if (message.equals("1"))
        {
            ussdSession.setCurrentState(UssdState.SELECT_DISTRICT_GROPU_A);
            responseMessage = getMessage("ussd.select.district.group.a.message");
        }
        else if (message.equals("2"))
        {
            ussdSession.setCurrentState(UssdState.SELECT_DISTRICT_GROPU_B);
            responseMessage = getMessage("ussd.select.district.group.b.message");
        }
        else if (message.equals("3"))
        {
            ussdSession.setCurrentState(UssdState.SELECT_DISTRICT_GROPU_C);
            responseMessage = getMessage("ussd.select.district.group.c.message");
        }
        else if (message.equals("4"))
        {
            ussdSession.setCurrentState(UssdState.SELECT_DISTRICT_GROPU_D);
            responseMessage = getMessage("ussd.select.district.group.d.message");
        }
        else if (message.equals("5"))
        {
            ussdSession.setCurrentState(UssdState.SELECT_DISTRICT_GROPU_E);
            responseMessage = getMessage("ussd.select.district.group.e.message");
        }
        else if (message.equals("6"))
        {
            String district = ussdSession.getSubscriber().getDistrict();
            String connectCache = ussdSession.getConnectCache();
            if (connectCache.equals(UssdState.FIND_BRANCHES.toString()))
            {
                ussdSession.setCurrentState(UssdState.FIND_BRANCHES);
                responseMessage = getMessage("ussd.find.branches.message", district);
            }
            else if (connectCache.equals(UssdState.FIND_CAMPS.toString()))
            {
                ussdSession.setCurrentState(UssdState.FIND_CAMPS);
                responseMessage = getMessage("ussd.find.camps.message", district);
            }
        }
        else
        {
            responseMessage = createInvalidMessage(ussdSession);
        }

        return responseMessage;
    }
}
