/*
 * FILENAME
 *     DonationCenterDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.telcoapp.bba.dao;

import java.util.Date;
import java.util.List;

import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.data.DonationCenter;
import com.hashcode.telcoapp.bba.enums.LocationType;
import com.hashcode.telcoapp.bba.service.CampaignCriteria;
import com.hashcode.telcoapp.common.dao.GenericDao;

/**
 * <p>
 * Donation center dao interface.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 */
public interface DonationCenterDao extends GenericDao<DonationCenter, Long>
{
    /**
     * <p>
     * Fetch donation centers for district.
     * </p>
     * 
     * @param district
     *            district string
     * @param locationType
     *            location type
     * @param dateFrom
     *            from date
     * @param maxRowCount
     *            max row count
     * 
     * @return list of donation centers
     */
    List<DonationCenter> fetchDonationCentersForDistrict(final String district, final LocationType locationType,
        final Date dateFrom, final int maxRowCount);

    /**
     * 
     * <p>
     * Find the donation centers by given criteria.
     * </p>
     * 
     * 
     * @param criteria
     *            Campaign search criteria.
     * @return List {@link DonationCenter}
     * 
     */
    List<DonationCenter> findByCriteria(final CampaignCriteria criteria);
    
    /**
     * <p>
     * Find donation center by blood stock admin.
     * </p>
     *
     * @param subscriber subscriber reference
     * 
     * @return donation center instance
     */
    DonationCenter findByBloodStockAdmin(BBASubscriber subscriber);
    
    /**
     * <p>
     * Find donation center by given reference id.
     * </p>
     *
     * @param referenceId reference id
     * 
     * @return donation center instance
     */
    DonationCenter findByReferenceId(final String referenceId);
    
    /**
     * <p>
     * Get blood banks.
     * </p>
     *
     * @return list of donations centers labeled as blood banks.
     */
    List<DonationCenter> getBloodBanks();
}
