/*
 * FILENAME
 *     BloodStockTab.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.bs;

import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood stock tab.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class BloodStockTab extends VerticalLayout
{
    private static final long serialVersionUID = 7291235441244257735L;

    private final TabSheet tabSheet;
    private BloodStockSearchLayout bloodStockSearchLayout;
    private AddBloodStockLayout addBloodStockLayout;

    /**
     * <p>
     * Blood stock tab.
     * </p>
     */
    public BloodStockTab()
    {
        setSizeFull();
        tabSheet = new TabSheet();
        tabSheet.setWidth("100%");
        tabSheet.setImmediate(true);
        tabSheet.addTab(getAddBloodStockLayout(), "Add Blood Stock", new ThemeResource("images/bs-add.png"));
        tabSheet.addTab(getBloodStockSearchLayout(), "Blood Stock Search", new ThemeResource("images/bs-search.png"));
        addComponent(tabSheet);
    }

    private BloodStockSearchLayout getBloodStockSearchLayout()
    {
        if (null == bloodStockSearchLayout)
        {
            bloodStockSearchLayout = new BloodStockSearchLayout();
        }

        return bloodStockSearchLayout;
    }

    private AddBloodStockLayout getAddBloodStockLayout()
    {
        if (null == addBloodStockLayout)
        {
            addBloodStockLayout = new AddBloodStockLayout();
        }

        return addBloodStockLayout;
    }

    /**
     * <p>
     * Refresh blood stock tab.
     * </p>
     */
    public void refresh()
    {
        getBloodStockSearchLayout().refresh();
    }
}
