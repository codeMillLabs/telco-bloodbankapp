/*
 * FILENAME
 *     UssdState.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.enums;

import com.hashcode.telcoapp.bba.state.BBAppState;
import com.hashcode.telcoapp.bba.state.bs.BloodStockAmountPCState;
import com.hashcode.telcoapp.bba.state.bs.BloodStockAmountRCState;
import com.hashcode.telcoapp.bba.state.bs.BloodStockHelpMenuState;
import com.hashcode.telcoapp.bba.state.bs.BloodStockInvalidState;
import com.hashcode.telcoapp.bba.state.bs.BloodStockMainMenuState;
import com.hashcode.telcoapp.bba.state.bs.BloodStockSetBloodGroupState;
import com.hashcode.telcoapp.bba.state.bs.BloodStockSuccessState;
import com.hashcode.telcoapp.bba.state.subscriber.SubscriberInvalidRequestState;
import com.hashcode.telcoapp.bba.state.subscriber.SubscriberMainMenuState;
import com.hashcode.telcoapp.bba.state.subscriber.SubscriberSuccessState;
import com.hashcode.telcoapp.bba.state.subscriber.branch.BranchListState;
import com.hashcode.telcoapp.bba.state.subscriber.branch.FindBranchState;
import com.hashcode.telcoapp.bba.state.subscriber.camp.CampListState;
import com.hashcode.telcoapp.bba.state.subscriber.camp.FindCampsState;
import com.hashcode.telcoapp.bba.state.subscriber.common.SelectDistrictGroupAState;
import com.hashcode.telcoapp.bba.state.subscriber.common.SelectDistrictGroupBState;
import com.hashcode.telcoapp.bba.state.subscriber.common.SelectDistrictGroupCState;
import com.hashcode.telcoapp.bba.state.subscriber.common.SelectDistrictGroupDState;
import com.hashcode.telcoapp.bba.state.subscriber.common.SelectDistrictGroupEState;
import com.hashcode.telcoapp.bba.state.subscriber.common.SelectDistrictState;
import com.hashcode.telcoapp.bba.state.subscriber.help.SubscriberContactState;
import com.hashcode.telcoapp.bba.state.subscriber.help.SubscriberHelpState;
import com.hashcode.telcoapp.bba.state.subscriber.help.SubscriberInformationState;
import com.hashcode.telcoapp.bba.state.subscriber.profile.InitialSetNICState;
import com.hashcode.telcoapp.bba.state.subscriber.profile.InitialSetNameState;
import com.hashcode.telcoapp.bba.state.subscriber.profile.SetNameState;
import com.hashcode.telcoapp.bba.state.subscriber.profile.SetGenderState;
import com.hashcode.telcoapp.bba.state.subscriber.profile.SetNICState;
import com.hashcode.telcoapp.bba.state.subscriber.profile.SubscriberProfileInvalidRequestState;
import com.hashcode.telcoapp.bba.state.subscriber.profile.SubscriberProfileState;
import com.hashcode.telcoapp.bba.state.subscriber.profile.SubscriberProfileSuccessState;
import com.hashcode.telcoapp.bba.state.subscriber.profile.donate.DonateState;
import com.hashcode.telcoapp.bba.state.subscriber.profile.location.SubscriberLocationState;
import com.hashcode.telcoapp.bba.state.subscriber.profile.location.SubscriberSetLocationState;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * USSD State.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public enum UssdState
{
    
    INITIAL_SET_NIC(InitialSetNICState.class),
    INITIAL_SET_NAME(InitialSetNameState.class),
    
    SUBSCRIBER_MAIN_MENU(SubscriberMainMenuState.class),

    FIND_BRANCHES(FindBranchState.class),
    FIND_CAMPS(FindCampsState.class),
    SUBSCRIBER_PROFILE(SubscriberProfileState.class),
    SUBSCRIBER_HELP(SubscriberHelpState.class),

    SELECT_DISTRICT(SelectDistrictState.class),
    SELECT_DISTRICT_GROPU_A(SelectDistrictGroupAState.class),
    SELECT_DISTRICT_GROPU_B(SelectDistrictGroupBState.class),
    SELECT_DISTRICT_GROPU_C(SelectDistrictGroupCState.class),
    SELECT_DISTRICT_GROPU_D(SelectDistrictGroupDState.class),
    SELECT_DISTRICT_GROPU_E(SelectDistrictGroupEState.class),

    BRANCH_LIST(BranchListState.class),
    CAMP_LIST(CampListState.class),
    SUBSCRIBER_INFO(SubscriberInformationState.class),
    SUBSCRIBER_CONTACT(SubscriberContactState.class),

    SUBSCRIBER_LOCATION(SubscriberLocationState.class),
    SUBSCRIBER_SET_LOCATION(SubscriberSetLocationState.class),
    
    SET_GENDER(SetGenderState.class),
    SET_NIC(SetNICState.class),
    SET_NAME(SetNameState.class),
    DONATE(DonateState.class),
    SUBSCRIBER_PROFILE_SUCCESS(SubscriberProfileSuccessState.class),
    SUBSCRIBER_PROFILE_INVALID(SubscriberProfileInvalidRequestState.class),

    SUBSCRIBER_SUCCESS(SubscriberSuccessState.class),
    SUBSCRIBER_INVALID(SubscriberInvalidRequestState.class),
    
    BLOOD_STOCK_MAIN_MENU(BloodStockMainMenuState.class),
    BLOOD_STOCK_HELP(BloodStockHelpMenuState.class),
    BLOOD_STOCK_SET_BLOOD_GROUP(BloodStockSetBloodGroupState.class),
    
    BLOOD_STOCK_SET_AMOUNT_RC(BloodStockAmountRCState.class),
    BLOOD_STOCK_SET_AMOUNT_PC(BloodStockAmountPCState.class),
    
    BLOOD_STOCK_SUCCESS(BloodStockSuccessState.class),
    BLOOD_STOCK_INVALID(BloodStockInvalidState.class);
    
    private final Class<?> clazz;

    /**
     * <p>
     * AnonymousState State.
     * </p>
     * 
     * @param inClazzz
     *            AbstractAnonymousState implementation
     */
    private <E extends BBAppState> UssdState(final Class<E> inClazz)
    {
        clazz = inClazz;
    }

    /**
     * <p>
     * Getter for implementation.
     * </p>
     * 
     * @return the implementation
     */
    public BBAppState getImplementation()
    {
        try
        {
            return (BBAppState) clazz.newInstance();
        }
        catch (InstantiationException e)
        {
        }
        catch (IllegalAccessException e)
        {
        }

        return null;
    }
}
