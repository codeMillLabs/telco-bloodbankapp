/*
 * FILENAME
 *     BulkMsgSummaryLayout.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.bulkmessage;

import com.hashcode.telcoapp.bba.data.MessageQueue;
import com.hashcode.telcoapp.bba.service.BulkMsgCriteria;
import com.hashcode.telcoapp.bba.ui.bulkmessage.BulkMessageTab.BulkMessageListener;
import com.vaadin.data.Property;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Bulk message Summary Layout.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class BulkMsgSummaryLayout extends VerticalLayout
{
    private static final long serialVersionUID = -2040300292785702888L;
    
    private BulkMsgSummaryTable summaryTable;
    private BulkMsgSearchForm searchForm;
    private final BulkMessageListener bulkMessageListener;
    
    private static final int RECORDS_PER_PAGE = 100;

    /**
     * <p>
     * Constructor of BulkMsgSummaryLayout.
     * </p>
     * 
     * @param inBulkMessageListener
     *            Bulk Message Listener interface
     * 
     */
    public BulkMsgSummaryLayout(final BulkMessageListener inBulkMessageListener)
    {
        this.bulkMessageListener = inBulkMessageListener;
        initUI();
    }

    private void initUI()
    {
        setMargin(true);

        VerticalLayout searchLayout = new VerticalLayout();
        searchLayout.addComponent(getSearchForm());

        Button searchButton = new Button("Search");
        searchButton.setImmediate(true);
        searchButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = -7291423273138687511L;

            public void buttonClick(final ClickEvent event)
            {
                getSummaryTable().refresh(getSearchForm().getModel());
            }
        });

        Button clearButton = new Button("Clear");
        clearButton.setImmediate(true);
        clearButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = 8293367292559846490L;

            public void buttonClick(final ClickEvent event)
            {
                BulkMsgCriteria msgCrit = new BulkMsgCriteria();
                msgCrit.setRecordsPerPage(RECORDS_PER_PAGE);
                getSearchForm().setModel(msgCrit);
                getSummaryTable().refresh(msgCrit);
            }
        });

        HorizontalLayout searchButtonLayout = new HorizontalLayout();
        searchButtonLayout.setSpacing(true);
        searchButtonLayout.addComponent(searchButton);
        searchButtonLayout.addComponent(clearButton);
        searchLayout.addComponent(searchButtonLayout);
        searchLayout.setComponentAlignment(searchButtonLayout, Alignment.MIDDLE_CENTER);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.addComponent(getSummaryTable().getPaginator());
        tableLayout.addComponent(getSummaryTable());

        addComponent(searchLayout);
        addComponent(tableLayout);
        setSizeFull();
    }

    private BulkMsgSearchForm getSearchForm()
    {
        if (searchForm == null)
        {
            BulkMsgCriteria msgCrit = new BulkMsgCriteria();
            msgCrit.setRecordsPerPage(RECORDS_PER_PAGE);
            searchForm = new BulkMsgSearchForm(msgCrit);
        }

        return searchForm;
    }

    private BulkMsgSummaryTable getSummaryTable()
    {
        if (summaryTable == null)
        {
            summaryTable = new BulkMsgSummaryTable();
            summaryTable.addListener(new Property.ValueChangeListener()
            {

                private static final long serialVersionUID = 2501409996001229014L;

                /**
                 * {@inheritDoc}
                 */
                public void valueChange(final Property.ValueChangeEvent event)
                {
                    bulkMessageListener.changed((MessageQueue) event.getProperty().getValue());
                }
            });
        }

        return summaryTable;
    }

}
