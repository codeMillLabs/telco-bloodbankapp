/*
 * FILENAME
 *     UserRole.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.enums;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood bank system user roles.
 * </p>
 * 
 * @author amila1.fm
 * 
 * @version $Id$
 **/
public enum UserRole
{
    DONOR("Donor", false, false, false, false, false, false, false),
    ADMIN("Admin", true, true, true, true, true, true, true),
    CENTER_ADMIN("Center Admin", false, false, false, false, false, false, false),
    SYS_USER("System User", false, false, false, false, false, false, false);

    private String roleDesc;
    private boolean homePerm;
    private boolean subscriberPerm;
    private boolean donationCenterPerm;
    private boolean bulkMsgPerm;
    private boolean bloodStockPerm;
    private boolean inboxPerm;
    private boolean helpPerm;

    /**
     * <p>
     * Constructor with user friendly description.
     * </p>
     * 
     * @param desc
     *            description
     * 
     */
    private UserRole(final String desc, final boolean inHomePerm, final boolean inSubscriberPerm,
        final boolean inDonationCenterPerm, final boolean inBulkMsgPerm, final boolean inBloodStockPerm,
        final boolean inInboxPerm, final boolean inHelpPerm)
    {
        this.roleDesc = desc;
        this.homePerm = inHomePerm;
        this.subscriberPerm = inSubscriberPerm;
        this.donationCenterPerm = inDonationCenterPerm;
        this.bulkMsgPerm = inBulkMsgPerm;
        this.bloodStockPerm = inBloodStockPerm;
        this.inboxPerm = inInboxPerm;
        this.helpPerm = inHelpPerm;
    }

    /**
     * <p>
     * Getter for roleDesc.
     * </p>
     * 
     * @return the roleDesc
     */
    public String getRoleDesc()
    {
        return roleDesc;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString()
    {
        return this.roleDesc;
    }

    /**
     * <p>
     * Getter for homePerm.
     * </p>
     * 
     * @return the homePerm
     */
    public boolean isHomePerm()
    {
        return homePerm;
    }

    /**
     * <p>
     * Getter for subscriberPerm.
     * </p>
     * 
     * @return the subscriberPerm
     */
    public boolean isSubscriberPerm()
    {
        return subscriberPerm;
    }

    /**
     * <p>
     * Getter for donationCenterPerm.
     * </p>
     * 
     * @return the donationCenterPerm
     */
    public boolean isDonationCenterPerm()
    {
        return donationCenterPerm;
    }

    /**
     * <p>
     * Getter for bulkMsgPerm.
     * </p>
     * 
     * @return the bulkMsgPerm
     */
    public boolean isBulkMsgPerm()
    {
        return bulkMsgPerm;
    }

    /**
     * <p>
     * Getter for bloodStockPerm.
     * </p>
     * 
     * @return the bloodStockPerm
     */
    public boolean isBloodStockPerm()
    {
        return bloodStockPerm;
    }

    /**
     * <p>
     * Getter for inboxPerm.
     * </p>
     * 
     * @return the inboxPerm
     */
    public boolean isInboxPerm()
    {
        return inboxPerm;
    }

    /**
     * <p>
     * Getter for helpPerm.
     * </p>
     * 
     * @return the helpPerm
     */
    public boolean isHelpPerm()
    {
        return helpPerm;
    }
}
