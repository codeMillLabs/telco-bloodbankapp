package com.test.hashcode.telcoapp.bba.services;

import static com.hashcode.telcoapp.common.enums.SubscriberState.REGISTERED;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.telcoapp.bba.dao.AppSubscriberDao;
import com.hashcode.telcoapp.bba.dao.BulkMessageDao;
import com.hashcode.telcoapp.bba.dao.MessageQueueDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.data.BulkMessage;
import com.hashcode.telcoapp.bba.data.MessageQueue;
import com.hashcode.telcoapp.bba.enums.BBAState;
import com.hashcode.telcoapp.bba.enums.BloodType;
import com.hashcode.telcoapp.bba.service.BulkMsgCriteria;
import com.hashcode.telcoapp.bba.service.BulkMsgSenderService;

/**
 * <p>
 * Bulk messaging test cases.
 * </p>
 * 
 * @author Amila
 * 
 * @version $Id$
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/Service.xml" })
public class TestBulkMsgSenderService {

	@Autowired
	private BulkMsgSenderService bulkMsgSenderService; // = getService(BulkMsgSenderService.class);
	
	@Autowired
	private BulkMessageDao bulkMessageDao; // = getService(BulkMessageDao.class);
	
	@Autowired
	private MessageQueueDao messageQueueDao; // = getService(MessageQueueDao.class);
	
	@Autowired
	private AppSubscriberDao appSubscriberDao; // = getService(AppSubscriberDao.class);

	/**
	 * <p>
	 * Test case for
	 * {@link BulkMsgSenderService#createAndQueueMessages(String, BBASubscriber)}
	 * .
	 * </p>
	 */
	@Transactional
	@Test
	public void testCreateNQueueMsg() {
		String txtMsg1 = "Hello <name>, We need <blood_type> urgently.";
		String txtMsg2 = "Hello <name>, We need <blood_type> urgently.";
		String txtMsg3 = "Hello <name> [<nic>], We need <blood_type> urgently.";
		String txtMsg4 = "Hello <name>, We need <blood_type> urgently for <district>.";

		BBASubscriber subscriber1 = createSubscriber("Amila", "Silva",
				"Colombo", BloodType.O_P, "863492033v");
		BBASubscriber subscriber2 = createSubscriber("Nethma", "Silva",
				"Colombo", BloodType.O_N, "865712033v");

		bulkMsgSenderService.createAndQueueMessages(txtMsg1, subscriber1);
		bulkMsgSenderService.createAndQueueMessages(txtMsg2, subscriber2);
		bulkMsgSenderService.createAndQueueMessages(txtMsg3, subscriber1);
		bulkMsgSenderService.createAndQueueMessages(txtMsg4, subscriber2);

		List<MessageQueue> bulkMessages = messageQueueDao.findAll();
		assertThat(bulkMessages.size(), is(4));

	}

	/**
	 * <p>
	 * Test case for
	 * {@link BulkMsgSenderService#createAndQueueMessages(String, BBASubscriber)}
	 * .
	 * </p>
	 */
	@Transactional
	@Test
	public void testCreateBulkMessage() {

		String txtMsg = "Hello <name>, We need <blood_type> urgently for <district>.";

		BBASubscriber subscriber1 = createSubscriber("Amila", "Silva",
				"Colombo", BloodType.O_P, "863492033v");
		BBASubscriber subscriber2 = createSubscriber("Nethma", "Silva",
				"Colombo", BloodType.O_N, "865712033v");
		BBASubscriber subscriber3 = createSubscriber("Rehani", "Silva",
				"Colombo", BloodType.AB_N, "865712033v");
		BBASubscriber subscriber4 = createSubscriber("Nick", "Silva",
				"Colombo", BloodType.B_P, "865712033v");
		BBASubscriber subscriber5 = createSubscriber("Mahinda", "Silva",
				"Colombo", BloodType.B_P, "865712033v");
		BBASubscriber subscriber6 = createSubscriber("Nick", "Silva",
				"Gampaha", BloodType.B_P, "865712033v");

		appSubscriberDao.create(subscriber1);
		appSubscriberDao.create(subscriber2);
		appSubscriberDao.create(subscriber3);
		appSubscriberDao.create(subscriber4);
		appSubscriberDao.create(subscriber5);
		appSubscriberDao.create(subscriber6);

		BulkMessage bulkMessage = new BulkMessage();
		bulkMessage.setDistrict("Colombo");
		bulkMessage.setMessage(txtMsg);

		bulkMsgSenderService.createAndQueueBulkMessages(bulkMessage);

		List<BulkMessage> bulkMessages = bulkMessageDao.findAll();
		assertThat(bulkMessages.size(), is(1));

		List<MessageQueue> queuedMsgs = messageQueueDao.findAll();
		assertThat(queuedMsgs.size(), is(5));

		BulkMsgCriteria criteria = new BulkMsgCriteria();
		criteria.getStatus().add(BBAState.PENDING);
		criteria.getStatus().add(BBAState.FAST_MSGS);

		queuedMsgs = messageQueueDao.findByCriteria(criteria);
		assertThat(queuedMsgs.size(), is(5));
	}

	/**
	 * <p>
	 * Test case for {@link BulkMsgSenderService#sendBirthDayGreetings()}.
	 * </p>
	 * 
	 * @throws Exception
	 *             when error occurs
	 */
	@Transactional
	@Test
	public void testSendBirthdayGreetings() throws Exception {

		BBASubscriber subscriber1 = createSubscriber("Amila", "Silva",
				"Colombo", BloodType.O_P, "863492033v");
		BBASubscriber subscriber2 = createSubscriber("Nethma", "Silva",
				"Colombo", BloodType.O_N, "865712033v");
		BBASubscriber subscriber3 = createSubscriber("Rehani", "Silva",
				"Colombo", BloodType.AB_N, "865712033v");

		DateTime dateTime1 = new DateTime().minusYears(27);
		DateTime dateTime2 = new DateTime().minusYears(20);
		DateTime dateTime3 = new DateTime().minusYears(10).plusMonths(3);

		subscriber1.setDob(dateTime1.toDate());
		subscriber2.setDob(dateTime2.toDate());
		subscriber3.setDob(dateTime3.toDate());

		appSubscriberDao.create(subscriber1);
		appSubscriberDao.create(subscriber2);
		appSubscriberDao.create(subscriber3);

		bulkMsgSenderService.sendBirthDayGreetings();

		BulkMsgCriteria criteria = new BulkMsgCriteria();
		criteria.getStatus().add(BBAState.FAST_MSGS);
		criteria.setMessageSendingCrit(true);

		List<MessageQueue> queuedMsgs = messageQueueDao
				.findByCriteria(criteria);
		assertThat(queuedMsgs.size(), is(2));
	}

	/**
	 * <p>
	 * Create subscriber.
	 * </p>
	 * 
	 * @param firstName
	 *            first name
	 * @param lastname
	 *            last name
	 * @param district
	 *            district
	 * @param bloodType
	 *            blood type
	 * @param nic
	 *            nic
	 * 
	 * @return subscriber instance
	 */
	private BBASubscriber createSubscriber(final String firstName,
			final String lastname, final String district,
			final BloodType bloodType, final String nic) {
		BBASubscriber subscriber = new BBASubscriber();
		subscriber.setFirstName(firstName);
		subscriber.setLastName(lastname);
		subscriber.setDistrict(district);
		subscriber.setBloodType(bloodType);
		subscriber.setSubscriberState(REGISTERED);
		subscriber.setNic(nic);
		return subscriber;
	}

}
