/*
 * FILENAME
 *     SubscriptionNotificationHandler.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.util;

import static com.hashcode.telcoapp.bba.enums.UserRole.DONOR;
import static com.hashcode.telcoapp.common.util.Configuration.getCurrentTime;

import javax.servlet.ServletConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.common.connectors.ILBSConnector;
import com.hashcode.telcoapp.common.enums.SubscriberState;
import com.hashcode.telcoapp.common.service.SubscriberService;
import com.hashcode.telcoapp.common.util.ServiceResolver;

import hms.kite.samples.api.SdpException;
import hms.kite.samples.api.subscription.SubscriptionNotificationListener;
import hms.kite.samples.api.subscription.messages.SubscriptionNotification;

/**
 * <p>
 * Subscription notification handler.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 **/
public class SubscriptionNotificationHandler extends SubscriptionNotification implements
    SubscriptionNotificationListener
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionNotificationHandler.class);

    /**
     * {@inheritDoc}
     * 
     * @see hms.kite.samples.api.subscription.SubscriptionNotificationListener#init(javax.servlet.ServletConfig)
     */
    public void init(final ServletConfig arg0)
    {

    }

    /**
     * {@inheritDoc}
     * 
     * @see SubscriptionNotificationListener#onReceivedSubscription(SubscriptionNotification)
     */
    public void onReceivedSubscription(final SubscriptionNotification subscriptionNotification)
    {
        LOGGER.info("Subscription notification received, {}", subscriptionNotification);
        String address = "tel:" + subscriptionNotification.getSubscriberId();
        BBASubscriber subscriber =
            (BBASubscriber) getService(SubscriberService.class).findSubcriberByAddress(
                address);
        subscriber = createOrUpdateSubscriber(address, subscriber);
    }

    private BBASubscriber createOrUpdateSubscriber(final String address, final BBASubscriber subscriber)
    {
        BBASubscriber newSusbcriber = subscriber;
        if (null == newSusbcriber)
        {
            newSusbcriber = new BBASubscriber();
            newSusbcriber.setAddress(address);
            newSusbcriber.setRegisteredDate(getCurrentTime());
            newSusbcriber.setSubscriberState(SubscriberState.REGISTERED);
            newSusbcriber.getUserRoles().add(DONOR);
            String district = BBAUtils.DEFAULT_DISTRICT;
            try
            {
                district = BBAUtils.getDistrict(getService(ILBSConnector.class).getDistrict(address));
            }
            catch (SdpException e)
            {
                LOGGER.error("Error while gettting district of [{}]", address);
            }
            newSusbcriber.setDistrict(district);
            getService(SubscriberService.class).createSusbcriber(newSusbcriber);
            LOGGER.info("Subscriber created successfully, {}", newSusbcriber);
        }
        else
        {
            getService(SubscriberService.class).updateSubscriber(newSusbcriber);
            LOGGER.info("Subscriber updated successfully, {}", newSusbcriber);
        }

        return newSusbcriber;
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
