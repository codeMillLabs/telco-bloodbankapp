/*
 * FILENAME
 *     UssdSessionDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.dao.impl;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.telcoapp.bba.dao.UssdSessionDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.data.UssdSession;
import com.hashcode.telcoapp.common.dao.impl.GenericDaoImpl;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Ussd session dao implementations.
 * </p>
 * 
 * @author manuja
 * 
 * @version $Id$
 */
@Transactional
@Repository
public class UssdSessionDaoImpl extends GenericDaoImpl<UssdSession, Long> implements UssdSessionDao
{
    private static final String FIND_USSD_SESSION_BY_SUBSCRIBER_AND_SESSION_ID = " SELECT session FROM "
        + UssdSession.class.getName()
        + " session WHERE session.subscriber = :appSubscriber AND session.sessionId = :sessionId";

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.telcoapp.bba.dao.UssdSessionDao#findUssdSession(BBASubscriber, String)
     */
    public UssdSession findUssdSession(final BBASubscriber appSubscriber, final String sessionId)
    {
        try
        {
            TypedQuery<UssdSession> query =
                entityManager.createQuery(FIND_USSD_SESSION_BY_SUBSCRIBER_AND_SESSION_ID, UssdSession.class);
            query.setParameter("appSubscriber", appSubscriber);
            query.setParameter("sessionId", sessionId);
            query.setMaxResults(1);
            return query.getSingleResult();
        }
        catch (NoResultException e)
        {
            return null;
        }
    }
}
