/*
 * FILENAME
 *     MessageQueueDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.telcoapp.bba.dao.impl;

import java.util.Collections;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.telcoapp.bba.dao.MessageQueueDao;
import com.hashcode.telcoapp.bba.data.MessageQueue;
import com.hashcode.telcoapp.bba.service.BulkMsgCriteria;
import com.hashcode.telcoapp.common.dao.SearchCriteria;
import com.hashcode.telcoapp.common.dao.impl.GenericDaoImpl;

/**
 * <p>
 * Message queue Dao Implementation.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 */
public class MessageQueueDaoImpl extends GenericDaoImpl<MessageQueue, Long> implements MessageQueueDao
{

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageQueueDaoImpl.class);

    /**
     * {@inheritDoc}
     * 
     * @see MessageQueueDao#findByCriteria(SearchCriteria)
     */
    @SuppressWarnings("unchecked")
    public List<MessageQueue> findByCriteria(final SearchCriteria criteria)
    {

        BulkMsgCriteria bulkCrit = (BulkMsgCriteria) criteria;

        Session session = (Session) entityManager.getDelegate();
        Criteria crit = session.createCriteria(MessageQueue.class);

        if (!bulkCrit.isMessageSendingCrit())
            crit.createAlias("bulkMessage", "blkMsg");

        boolean hasCriteria = addRestrictions(bulkCrit, crit);

        if (hasCriteria)
        {
            return crit.list();
        }
        else
        {
            return Collections.emptyList();
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see MessageQueueDao#countByCriteria(com.hashcode.telcoapp.common.dao.SearchCriteria)
     */
    public Integer countByCriteria(final SearchCriteria criteria)
    {
        BulkMsgCriteria bulkCrit = (BulkMsgCriteria) criteria;

        Session session = (Session) entityManager.getDelegate();
        Criteria crit = session.createCriteria(MessageQueue.class);
        crit.setProjection(Projections.rowCount());

        if (!bulkCrit.isMessageSendingCrit())
            crit.createAlias("bulkMessage", "blkMsg");

        boolean hasCriteria = addRestrictions(bulkCrit, crit);

        if (hasCriteria)
        {
            @SuppressWarnings("rawtypes")
            List results = crit.list();

            if (results != null)
            {
                return ((Long) results.get(0)).intValue();
            }
        }
        return 0;
    }

    private boolean addRestrictions(final BulkMsgCriteria bulkCrit, final Criteria crit)
    {
        boolean atLeastOneSearchCriteria = false;
        LOGGER.debug("addRestrictions [Criteria  : {} ]", bulkCrit);

        crit.addOrder(Order.asc("id"));

        if (bulkCrit.isSearchAll())
        {
            return true;
        }

        if (!bulkCrit.isMessageSendingCrit())
        {

            if (bulkCrit.getApDonor() != null)
            {
                crit.add(Restrictions.eq("blkMsg.apDonor", bulkCrit.getApDonor()));
                atLeastOneSearchCriteria = true;
            }

            if (bulkCrit.getSendToAll() != null)
            {
                crit.add(Restrictions.eq("blkMsg.sendToAll", bulkCrit.getSendToAll()));
                atLeastOneSearchCriteria = true;
            }

            if (bulkCrit.getBloodType() != null)
            {
                crit.add(Restrictions.eq("blkMsg.bloodType", bulkCrit.getBloodType()));
                atLeastOneSearchCriteria = true;
            }

            if (bulkCrit.getNic() != null)
            {
                String nic = bulkCrit.getNic().replace("*", "%");
                crit.add(Restrictions.ilike("blkMsg.nic", nic));
                atLeastOneSearchCriteria = true;
            }

            if (bulkCrit.getDistrict() != null)
            {
                crit.add(Restrictions.eq("blkMsg.district", bulkCrit.getDistrict()));
                atLeastOneSearchCriteria = true;
            }

            if (bulkCrit.getCreatedBy() != null)
            {
                String createdBy = bulkCrit.getCreatedBy().replace("*", "%");
                crit.add(Restrictions.ilike("blkMsg.createdBy", createdBy));
                atLeastOneSearchCriteria = true;
            }
        }

        if (bulkCrit.getScheduledTime() != null)
        {
            DateTime datetime =
                new DateTime(bulkCrit.getScheduledTime().getTime()).withHourOfDay(0).withMinuteOfHour(0)
                    .withSecondOfMinute(0);
            crit.add(Restrictions.ge("scheduledDate", datetime.toDate()));
            atLeastOneSearchCriteria = true;
        }

        if (bulkCrit.getSenderQueueTimeFrom() != null)
        {
            DateTime datetime =
                new DateTime(bulkCrit.getSenderQueueTimeFrom().getTime()).withSecondOfMinute(0).withMillisOfSecond(0);
            crit.add(Restrictions.ge("scheduledDate", datetime.toDate()));
            atLeastOneSearchCriteria = true;
        }

        if (bulkCrit.getSenderQueueTimeTo() != null)
        {
            DateTime datetime =
                new DateTime(bulkCrit.getSenderQueueTimeTo().getTime()).withSecondOfMinute(0).withMillisOfSecond(0);
            crit.add(Restrictions.lt("scheduledDate", datetime.toDate()));
            atLeastOneSearchCriteria = true;
        }

        if (bulkCrit.getSentDate() != null)
        {
            DateTime datetime =
                new DateTime(bulkCrit.getSentDate().getTime()).withHourOfDay(0).withMinuteOfHour(0)
                    .withSecondOfMinute(0);
            crit.add(Restrictions.ge("sentDate", datetime.toDate()));
            atLeastOneSearchCriteria = true;
        }

        if (bulkCrit.getStatus() != null && !bulkCrit.getStatus().isEmpty())
        {
            crit.add(Restrictions.in("status", bulkCrit.getStatus()));
            atLeastOneSearchCriteria = true;
        }

        if (!atLeastOneSearchCriteria)
        {
            return false;
        }

        if (bulkCrit.getStartPosition() >= 0)
        {
            crit.setFirstResult(bulkCrit.getStartPosition());
            crit.setMaxResults(bulkCrit.getEndPosition());
        }

        return atLeastOneSearchCriteria;
    }

}
