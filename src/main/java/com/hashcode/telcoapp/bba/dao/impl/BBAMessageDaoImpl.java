/*
 * FILENAME
 *     MessageDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.dao.impl;

import java.util.Collections;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.telcoapp.bba.dao.BBAMessageDao;
import com.hashcode.telcoapp.bba.dao.MessageSearchCriteria;
import com.hashcode.telcoapp.bba.data.Message;
import com.hashcode.telcoapp.common.dao.impl.GenericDaoImpl;

/**
 * <p>
 * Message Dao Implementation.
 * </p>
 * 
 * @author manuja
 * 
 * @version $Id$
 */
@Transactional
@Repository
public class BBAMessageDaoImpl extends GenericDaoImpl<Message, Long> implements BBAMessageDao
{
    /**
     * {@inheritDoc}
     * 
     * @see BBAMessageDao#findBySearchCriteria(com.hashcode.telcoapp.bba.dao.MessageSearchCriteria)
     */
    @SuppressWarnings("unchecked")
    public List<Message> findBySearchCriteria(final MessageSearchCriteria criteria)
    {
        Session session = (Session) entityManager.getDelegate();
        Criteria crit = session.createCriteria(Message.class);

        boolean atLeastOneSearchCriteria = false;

        if (criteria.isSearchAll())
        {
            return crit.list();
        }

        if (criteria.getDate() != null)
        {
            DateTime datetime =
                new DateTime(criteria.getDate().getTime()).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);
            crit.add(Restrictions.eq("date", datetime.toDate()));
            atLeastOneSearchCriteria = true;
        }

        if (!atLeastOneSearchCriteria)
        {
            return Collections.emptyList();
        }

        if (criteria.getStartPosition() >= 0)
        {
            crit.setFirstResult(criteria.getStartPosition());
            crit.setMaxResults(criteria.getEndPosition());
        }

        return crit.list();
    }

}
