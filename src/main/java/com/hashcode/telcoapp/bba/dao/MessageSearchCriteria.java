/*
 * FILENAME
 *     MessageSearchCriteria.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.dao;

import java.util.Date;

import com.hashcode.telcoapp.common.dao.SearchCriteria;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Message search criteria.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
public class MessageSearchCriteria extends SearchCriteria
{
    private static final long serialVersionUID = 5151008604670111707L;
    public static final String DATE = "date";
    
    private Date date;

    /**
     * <p>
     * Getter for date.
     * </p>
     * 
     * @return the date
     */
    public Date getDate()
    {
        return date;
    }

    /**
     * <p>
     * Setting value for date.
     * </p>
     * 
     * @param inDate the date to set
     */
    public void setDate(final Date inDate)
    {
        this.date = inDate;
    }
}
