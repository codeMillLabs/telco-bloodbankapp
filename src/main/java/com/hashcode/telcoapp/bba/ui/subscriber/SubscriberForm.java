/*
 * FILENAME
 *     SubscriberForm.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.subscriber;

import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createBloodTypeField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createDateField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createDistrictField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createGenderField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createTextArea;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createTextField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createUserRolesField;

import java.util.Arrays;
import java.util.List;

import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.common.ui.components.MultiColumnForm;
import com.vaadin.data.Item;
import com.vaadin.data.validator.IntegerValidator;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Select;
import com.vaadin.ui.TextField;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Subscriber UI Form.
 * </p>
 * 
 * @author manuja
 * 
 * @version $Id$
 */
public class SubscriberForm extends MultiColumnForm<BBASubscriber>
{
    private static final long serialVersionUID = -3276554935399713080L;

    /**
     * <p>
     * SubscriberForm constructor.
     * </p>
     * 
     * @param subscriber
     *            subscriber instance
     */
    public SubscriberForm(final BBASubscriber subscriber)
    {
        super(2);
        setFormFieldFactory(new SubscriberFormFields());
        setModel(subscriber);
    }

    @Override
    protected List<String> getVisiblePropertyIds()
    {
        return Arrays.asList(new String[] {
            BBASubscriber.ID,
            BBASubscriber.NIC,
            BBASubscriber.FIRST_NAME,
            BBASubscriber.ADDRESS,
            BBASubscriber.GENDER,
            BBASubscriber.DOB,
            BBASubscriber.BLOOD_TYPE,
            BBASubscriber.DISTRICT,
            BBASubscriber.POINTS,
            BBASubscriber.REMARKS,
            BBASubscriber.SUBSCRIBER_STATE,
            BBASubscriber.REGISTERED_DATE,
            BBASubscriber.LAST_MODIFIED_DATE,
            BBASubscriber.LAST_DONATION_DATE,
            BBASubscriber.NEXT_ELIGIBLE_DATE,
            BBASubscriber.BLOOD_STOCK_ACCOUNT,
            BBASubscriber.AP_DONOR,
            BBASubscriber.USER_ROLES
        });
    }

    @Override
    protected int[] getPropertyColumnIndices()
    {
        return new int[] {
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1
        };
    }

    /**
     * <p>
     * Subscriber Form Fields.
     * </p>
     * 
     * @author manuja
     * 
     * @version $Id$
     */
    private class SubscriberFormFields extends DefaultFieldFactory
    {
        private static final long serialVersionUID = 5477657278641589861L;

        /**
         * {@inheritDoc}
         */
        @Override
        public Field createField(final Item item, final Object propertyId, final Component uiContext)
        {
            String pid = (String) propertyId;
            if (pid.equals(BBASubscriber.ID))
            {
                return createTextField("Id", true);
            }
            if (pid.equals(BBASubscriber.NIC))
            {
                return createTextField("NIC", false);
            }
            else if (pid.equals(BBASubscriber.FIRST_NAME))
            {
                return createTextField("Name", false);
            }
            else if (pid.equals(BBASubscriber.ADDRESS))
            {
                return createTextField("Address", true);
            }
            else if (pid.equals(BBASubscriber.GENDER))
            {
                return createGenderField("Gender");
            }
            else if (pid.equals(BBASubscriber.DOB))
            {
                return createDateField("DOB", false);
            }
            else if (pid.equals(BBASubscriber.BLOOD_TYPE))
            {
                return createBloodTypeField("Blood Group");
            }
            else if (pid.equals(BBASubscriber.DISTRICT))
            {
                Select districtField = createDistrictField("District");
                districtField.setRequired(true);
                districtField.setRequiredError("District is required");
                return districtField;
            }
            else if (pid.equals(BBASubscriber.POINTS))
            {
                TextField pointField = createTextField("Points", false);
                pointField.addValidator(new IntegerValidator("Point value is invalid"));
                return pointField;
            }
            else if (pid.equals(BBASubscriber.SUBSCRIBER_STATE))
            {
                return createTextField("Subscriber State", true);
            }
            else if (pid.equals(BBASubscriber.REGISTERED_DATE))
            {
                return createDateField("Registered Date", true);
            }
            else if (pid.equals(BBASubscriber.LAST_MODIFIED_DATE))
            {
                return createDateField("Last Modified Date", true);
            }
            else if (pid.equals(BBASubscriber.LAST_DONATION_DATE))
            {
                return createDateField("Last Donation Date", true);
            }
            else if (pid.equals(BBASubscriber.NEXT_ELIGIBLE_DATE))
            {
                return createDateField("Next Eligible Date", true);
            }
            else if (pid.equals(BBASubscriber.BLOOD_STOCK_ACCOUNT))
            {
                return new CheckBox("Blood Stock Account");
            }
            else if (pid.equals(BBASubscriber.REMARKS))
            {
                return createTextArea("Remarks", false);
            }
            else if (pid.equals(BBASubscriber.AP_DONOR))
            {
                return new CheckBox("AP Donor");
            }
            else if (pid.equals(BBASubscriber.USER_ROLES))
            {
                return createUserRolesField("Roles");
            }

            return super.createField(item, propertyId, uiContext);
        }
    }
}
