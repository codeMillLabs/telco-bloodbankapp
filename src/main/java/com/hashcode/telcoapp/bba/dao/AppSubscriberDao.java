/*
 * FILENAME
 *     AppSubscriberDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.dao;

import java.util.List;

import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.service.BulkMsgCriteria;
import com.hashcode.telcoapp.common.dao.GenericDao;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * App Subscriber Dao Interface.
 * </p>
 * 
 * @author manuja
 * 
 * @version $Id$
 */
public interface AppSubscriberDao extends GenericDao<BBASubscriber, Long>
{

    /**
     * <p>
     * Find app Subscriber By username.
     * </p>
     * 
     * @param userName
     *            user name
     * 
     * @return app subscriber instance
     */
    BBASubscriber findByUserName(String userName);

    /**
     * <p>
     * Find the Subscriber by Address.
     * </p>
     * 
     * @param address
     *            address of the subscriber
     * @return susbcriber instance
     */
    BBASubscriber findSubcriberByAddress(String address);

    /**
     * <p>
     * Find by username and password.
     * </p>
     * 
     * @param userName
     *            username
     * @param password
     *            password
     * 
     * @return app subscriber instance
     */
    BBASubscriber findByUserNameAndPassword(String userName, String password);

    /**
     * <p>
     * Find by subscribers by given criteria.
     * </p>
     * 
     * @param criteria
     *            criteria
     * @return List of {@link BBASubscriber}
     */
    List<BBASubscriber> findSubscribers(BulkMsgCriteria criteria);
    
    /**
     * <p>
     * Count subscribers by given criteria.
     * </p>
     * 
     * @param criteria
     *            criteria
     * @return Integer of total result count
     */
    Integer countSubscribers(BulkMsgCriteria criteria);
    
    /**
     * <p>
     * Find blood stock account subscribers.
     * </p>
     *
     * @return blood stock account subscriber list
     */
    List<BBASubscriber> findBloodStockAdmins();
}
