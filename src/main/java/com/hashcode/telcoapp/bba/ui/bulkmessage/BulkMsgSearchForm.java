/*
 * FILENAME
 *     BulkMsgSearchForm.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.bulkmessage;

import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createBloodTypeField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createDateField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createDistrictField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createMsgStatusField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createTextField;

import java.util.Arrays;
import java.util.List;

import com.hashcode.telcoapp.bba.service.BulkMsgCriteria;
import com.hashcode.telcoapp.common.ui.components.MultiColumnForm;
import com.vaadin.data.Item;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;

/**
 * <p>
 * Bulk Message Search Form.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class BulkMsgSearchForm extends MultiColumnForm<BulkMsgCriteria>
{
    private static final long serialVersionUID = -2145824471415152229L;

    /**
     * <p>
     * Constructor of CampaignSearchForm.
     * </p>
     * 
     * @param criteria
     *            campaign search criteria instance
     */
    public BulkMsgSearchForm(final BulkMsgCriteria criteria)
    {
        super(2);
        setFormFieldFactory(new BulkMsgSearchFormField());
        setModel(criteria);
    }

    @Override
    protected List<String> getVisiblePropertyIds()
    {
        return Arrays.asList(new String[] {
            BulkMsgCriteria.CREATED_BY,
            BulkMsgCriteria.DISTRICT,
            BulkMsgCriteria.NIC,
            BulkMsgCriteria.AP_DONOR,
            BulkMsgCriteria.SEND_TO_ALL,
            BulkMsgCriteria.BLOOD_TYPE,
            BulkMsgCriteria.STATUS,
            BulkMsgCriteria.SCHEDULE_TIME,
            BulkMsgCriteria.SENT_DATE
        });
    }

    @Override
    protected int[] getPropertyColumnIndices()
    {
        return new int[] {
            0, 0, 0, 0, 0, 1, 1, 1, 1
        };
    }

    /**
     * <p>
     * Bulk message search form field.
     * </p>
     * 
     * @author manuja
     * 
     * @version $Id$
     */
    private class BulkMsgSearchFormField extends DefaultFieldFactory
    {
        private static final long serialVersionUID = -2971223549833150064L;

        /**
         * {@inheritDoc}
         */
        @Override
        public Field createField(final Item item, final Object propertyId, final Component uiContext)
        {
            String pid = (String) propertyId;
            if (pid.equals(BulkMsgCriteria.DISTRICT))
            {
                return createDistrictField("District");
            }
            else if (pid.equals(BulkMsgCriteria.BLOOD_TYPE))
            {
                return createBloodTypeField("Blood Type");
            }
            else if (pid.equals(BulkMsgCriteria.SCHEDULE_TIME))
            {
                return createDateField("Scheduled Date", false);
            }
            else if (pid.equals(BulkMsgCriteria.SENT_DATE))
            {
                return createDateField("Sent Date", false);
            }
            else if (pid.equals(BulkMsgCriteria.STATUS))
            {
              return createMsgStatusField("Status");
            }
            else if (pid.equals(BulkMsgCriteria.NIC))
            {
                return createTextField("NIC", false);
            }
            else if (pid.equals(BulkMsgCriteria.CREATED_BY))
            {
                return createTextField("Created By", false);
            }
            else if (pid.equals(BulkMsgCriteria.AP_DONOR))
            {
                return new CheckBox("AP Donor");
            }
            else if (pid.equals(BulkMsgCriteria.SEND_TO_ALL))
            {
                return new CheckBox("Send To All");
            }
            return super.createField(item, propertyId, uiContext);
        }

    }
}
