/*
 * FILENAME
 *     DonateState.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.state.subscriber.profile.donate;

import static com.hashcode.telcoapp.common.handlers.Messages.getMessage;
import static com.hashcode.telcoapp.common.util.Configuration.getCurrentTime;

import com.hashcode.telcoapp.bba.dao.DonateAsAtDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.data.DonateAsAt;
import com.hashcode.telcoapp.bba.data.UssdSession;
import com.hashcode.telcoapp.bba.enums.BBAState;
import com.hashcode.telcoapp.bba.enums.UssdState;
import com.hashcode.telcoapp.bba.state.BBAppState;
import com.hashcode.telcoapp.bba.util.BBAUtils;
import com.hashcode.telcoapp.common.util.ServiceResolver;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Donation state.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class DonateState extends BBAppState
{
    /**
     * {@inheritDoc}
     */
    @Override
    public String messageReceived(final UssdSession ussdSession, final String message)
    {
        String responseMessage = "";
        if (message.equals("1"))
        {
            responseMessage = createMainMenuMessage(ussdSession);
        }
        else
        {
            if (BBAUtils.validateDonationCode(message))
            {
                getService(DonateAsAtDao.class).create(
                    createDonateAsAt(ussdSession.getSubscriber(), message));
                ussdSession.setCurrentState(UssdState.SUBSCRIBER_SUCCESS);
                responseMessage = getMessage("ussd.profile.donate.success.message");
            }
            else
            {
                ussdSession.setCurrentState(UssdState.SUBSCRIBER_INVALID);
                responseMessage = getMessage("ussd.profile.donate.error.message", message);
            }
        }

        return responseMessage;
    }

    private DonateAsAt createDonateAsAt(final BBASubscriber subscriber, final String referenceNumber)
    {
        DonateAsAt donateAsAt = new DonateAsAt();
        donateAsAt.setDonatedDate(getCurrentTime());
        donateAsAt.setRefNo(referenceNumber);
        donateAsAt.setState(BBAState.PENDING);
        donateAsAt.setSubscriber(subscriber);

        return donateAsAt;
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
