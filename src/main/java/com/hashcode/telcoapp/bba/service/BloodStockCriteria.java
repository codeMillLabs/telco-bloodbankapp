/*
 * FILENAME
 *     BloodStockCriteria.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.service;

import com.hashcode.telcoapp.bba.enums.BloodType;
import com.hashcode.telcoapp.common.dao.SearchCriteria;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Criteria class for search Blood Stock.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class BloodStockCriteria extends SearchCriteria
{
    private static final long serialVersionUID = 3687076012078161197L;

    private BloodType bloodType;
    private String centerRef;
    private String centerName;
    private String district;

    /**
     * <p>
     * Getter for centerRef.
     * </p>
     * 
     * @return the centerRef
     */
    public String getCenterRef()
    {
        return centerRef;
    }

    /**
     * <p>
     * Setting value for centerRef.
     * </p>
     * 
     * @param inCenterRef
     *            the centerRef to set
     */
    public void setCenterRef(final String inCenterRef)
    {
        this.centerRef = inCenterRef;
    }

    /**
     * <p>
     * Getter for bloodType.
     * </p>
     * 
     * @return the bloodType
     */
    public BloodType getBloodType()
    {
        return bloodType;
    }

    /**
     * <p>
     * Setting value for bloodType.
     * </p>
     * 
     * @param inBloodType
     *            the bloodType to set
     */
    public void setBloodType(final BloodType inBloodType)
    {
        this.bloodType = inBloodType;
    }

    /**
     * <p>
     * Getter for centerName.
     * </p>
     * 
     * @return the centerName
     */
    public String getCenterName()
    {
        return centerName;
    }

    /**
     * <p>
     * Setting value for centerName.
     * </p>
     * 
     * @param inCenterName
     *            the centerName to set
     */
    public void setCenterName(final String inCenterName)
    {
        this.centerName = inCenterName;
    }

    /**
     * <p>
     * Getter for district.
     * </p>
     * 
     * @return the district
     */
    public String getDistrict()
    {
        return district;
    }

    /**
     * <p>
     * Setting value for district.
     * </p>
     * 
     * @param inDistrict
     *            the district to set
     */
    public void setDistrict(final String inDistrict)
    {
        this.district = inDistrict;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("BloodStockCriteria [bloodType=%s, centerRef=%s, centerName=%s, district=%s]", bloodType,
            centerRef, centerName, district);
    }

}
