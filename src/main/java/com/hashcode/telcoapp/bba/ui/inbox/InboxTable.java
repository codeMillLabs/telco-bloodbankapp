/*
 * FILENAME
 *     InboxTable.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.inbox;

import static com.hashcode.telcoapp.bba.data.Message.ID;
import static com.hashcode.telcoapp.bba.data.Message.SUBSCRIBER;
import static com.hashcode.telcoapp.bba.data.Message.MESSAGE;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.hashcode.telcoapp.bba.dao.BBAMessageDao;
import com.hashcode.telcoapp.bba.dao.MessageSearchCriteria;
import com.hashcode.telcoapp.bba.data.Message;
import com.hashcode.telcoapp.common.ui.pagination.EntityContainer;
import com.hashcode.telcoapp.common.ui.pagination.PageableEntityTable2;
import com.hashcode.telcoapp.common.util.ServiceResolver;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Inbox table.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
public class InboxTable extends PageableEntityTable2<Message>
{
    private static final long serialVersionUID = -1279262390373145102L;

    public static final Object[] NATURAL_COL_ORDER = new Object[] {
        ID, SUBSCRIBER, MESSAGE
    };

    public static final String[] COL_HEADERS = new String[] {
        "ID", "Mobile Number", "Message"
    };

    private MessageSearchCriteria searchCriteria;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public InboxTable()
    {
        super(Message.class);
        setContainerDataSource(new InboxTableContainer());
        InboxTableColumnGenerator columnGenerator = new InboxTableColumnGenerator();
        addGeneratedColumn(ID, columnGenerator);
        addGeneratedColumn(SUBSCRIBER, columnGenerator);
        addGeneratedColumn(MESSAGE, columnGenerator);
        setColumnWidth(ID, 20);
        setColumnWidth(SUBSCRIBER, 100);
        setVisibleColumns(NATURAL_COL_ORDER);
        setColumnHeaders(COL_HEADERS);
        setSizeFull();
    }

    /**
     * <p>
     * Inbox Table Column Generator.
     * </p>
     * 
     * @author Manuja
     * 
     * @version $Id$
     */
    private class InboxTableColumnGenerator implements ColumnGenerator
    {
        private static final long serialVersionUID = 7112348344147112940L;

        /**
         * {@inheritDoc}
         */
        public Component generateCell(final Table source, final Object itemId, final Object columnId)
        {
            Message message = (Message) itemId;
            if (columnId.equals(ID))
            {
                return new Label(String.valueOf(message.getId()));
            }
            else if (columnId.equals(SUBSCRIBER))
            {
                String address =
                    (message.getSubscriber() != null) ? message.getSubscriber().getAddress() : "N/A";
                return new Label(address);
            }
            else if (columnId.equals(MESSAGE))
            {
                return new Label(message.getMessageBody());
            }

            return null;
        }
    }

    /**
     * <p>
     * Inbox Table Container.
     * </p>
     * 
     * @author Manuja
     * 
     * @version $Id$
     */
    private class InboxTableContainer extends EntityContainer<Message>
    {
        private static final long serialVersionUID = -5348074042760713432L;

        public InboxTableContainer()
        {
            super(Message.class);
        }

        @Override
        public List<Message> getData()
        {
            List<Message> results = null;

            if (searchCriteria == null)
            {
                return Collections.emptyList();
            }

            if (searchCriteria.getNumOfRecords() < 0)
            {
                results = getService(BBAMessageDao.class).findBySearchCriteria(searchCriteria);
                searchCriteria.setNumOfRecords(results.size());
            }

            searchCriteria.setStartPosition(getPage() * getPageSize());
            searchCriteria.setEndPosition(getPageSize());

            results = getService(BBAMessageDao.class).findBySearchCriteria(searchCriteria);

            return new ArrayList<Message>(results);
        }

        @Override
        public int getTotalRows()
        {
            return ((searchCriteria == null) ? 0 : searchCriteria.getNumOfRecords());
        }

        private <T> T getService(final Class<T> serviceType)
        {
            return ServiceResolver.findService(serviceType);
        }
    }

    private boolean isSearchInfoNull()
    {
        return null == searchCriteria;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setPage(final int page)
    {
        if (!isSearchInfoNull())
        {
            searchCriteria.setCurrentPage(page);
            refresh();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPage()
    {
        return isSearchInfoNull() ? 0 : searchCriteria.getCurrentPage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPageSize()
    {
        return isSearchInfoNull() ? 10 : searchCriteria.getRecordsPerPage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPageSize(final int pageSize)
    {
        if (!isSearchInfoNull())
        {
            searchCriteria.setRecordsPerPage(pageSize);
            refresh();
        }
    }

    /**
     * <p>
     * Refresh table.
     * </p>
     * 
     * @param inSearchCriteria
     *            search criteria to set
     */
    public void refresh(final MessageSearchCriteria inSearchCriteria)
    {
        inSearchCriteria.resetResultProperties();
        searchCriteria = inSearchCriteria;
        refresh();
    }
}
