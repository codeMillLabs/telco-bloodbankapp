/*
 * FILENAME
 *     BloodStockUpdate.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.data;

import static com.hashcode.telcoapp.common.util.Configuration.getCurrentTime;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.hashcode.telcoapp.bba.enums.BloodStatus;
import com.hashcode.telcoapp.bba.enums.BloodType;
import com.hashcode.telcoapp.bba.enums.SubBloodType;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood stock history entity.
 * </p>
 * 
 * @author Amila Silva
 * @version $Id$
 */
@Entity
@Table(name = "BLOOD_STK_HISTORY")
public class BloodStockHistory implements Serializable
{
    private static final long serialVersionUID = 6913794634022022370L;
    public static final String BLOOD_TYPE = "bloodType";
    public static final String SUB_BLOOD_TYPE = "subBloodType";
    public static final String BLOOD_STATUS = "bloodStatus";
    public static final String CENTER = "center";
    public static final String RECORD_DATE = "recordDate";
    public static final String STOCK_COUNT = "stockCount";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "BLD_STCK_HIS_SEQ")
    @SequenceGenerator(name = "BLD_STCK_HIS_SEQ", sequenceName = "BLD_STCK_HIS_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "BLOOD_TYPE")
    @Enumerated(EnumType.ORDINAL)
    private BloodType bloodType;

    @Column(name = "SUB_BLOOD_TYPE")
    @Enumerated(EnumType.ORDINAL)
    private SubBloodType subBloodType;
    
    @Column(name = "BLOOD_STATUS")
    @Enumerated(EnumType.ORDINAL)
    private BloodStatus bloodStatus;

    @ManyToOne
    @JoinColumn(name = "CENTER_ID")
    private DonationCenter center;

    @Column(name = "RECORD_DATE")
    @Temporal(TemporalType.DATE)
    private Date recordDate;

    @Column(name = "STOCK_COUNT")
    private int stockCount = 0;

    /**
     * <p>
     * Constructor of blood stock history.
     * </p>
     */
    public BloodStockHistory()
    {
        this.recordDate = getCurrentTime();
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * <p>
     * Getter for bloodType.
     * </p>
     * 
     * @return the bloodType
     */
    public BloodType getBloodType()
    {
        return bloodType;
    }

    /**
     * <p>
     * Setting value for bloodType.
     * </p>
     * 
     * @param inBloodType
     *            the bloodType to set
     */
    public void setBloodType(final BloodType inBloodType)
    {
        this.bloodType = inBloodType;
    }

    /**
     * <p>
     * Getter for subBloodType.
     * </p>
     * 
     * @return the subBloodType
     */
    public SubBloodType getSubBloodType()
    {
        return subBloodType;
    }

    /**
     * <p>
     * Setting value for subBloodType.
     * </p>
     * 
     * @param inSubBloodType
     *            the subBloodType to set
     */
    public void setSubBloodType(final SubBloodType inSubBloodType)
    {
        this.subBloodType = inSubBloodType;
    }
    
    /**
     * <p>
     * Getter for bloodStatus.
     * </p>
     * 
     * @return the bloodStatus
     */
    public BloodStatus getBloodStatus()
    {
        return bloodStatus;
    }

    /**
     * <p>
     * Setting value for bloodStatus.
     * </p>
     * 
     * @param inBloodStatus the bloodStatus to set
     */
    public void setBloodStatus(final BloodStatus inBloodStatus)
    {
        this.bloodStatus = inBloodStatus;
    }

    /**
     * <p>
     * Getter for center.
     * </p>
     * 
     * @return the center
     */
    public DonationCenter getCenter()
    {
        return center;
    }

    /**
     * <p>
     * Setting value for center.
     * </p>
     * 
     * @param inCenter
     *            the center to set
     */
    public void setCenter(final DonationCenter inCenter)
    {
        this.center = inCenter;
    }

    /**
     * <p>
     * Getter for recordDate.
     * </p>
     * 
     * @return the recordDate
     */
    public Date getRecordDate()
    {
        return recordDate;
    }

    /**
     * <p>
     * Setting value for recordDate.
     * </p>
     * 
     * @param inRecordDate
     *            the recordDate to set
     */
    public void setRecordDate(final Date inRecordDate)
    {
        this.recordDate = inRecordDate;
    }

    /**
     * <p>
     * Getter for stockCount.
     * </p>
     * 
     * @return the stockCount
     */
    public int getStockCount()
    {
        return stockCount;
    }

    /**
     * <p>
     * Setting value for stockCount.
     * </p>
     * 
     * @param inStockCount
     *            the stockCount to set
     */
    public void setStockCount(final int inStockCount)
    {
        this.stockCount = inStockCount;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("BloodStockHistory [id=%s, bloodType=%s, center=%s, dateTime=%s, stockCount=%s]", id,
            bloodType, center, recordDate, stockCount);
    }

}
