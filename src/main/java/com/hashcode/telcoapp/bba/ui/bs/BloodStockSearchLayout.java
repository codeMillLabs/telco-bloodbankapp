/*
 * FILENAME
 *     BloodStockSearchLayout.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.bs;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import au.com.bytecode.opencsv.CSVWriter;

import com.hashcode.telcoapp.bba.dao.BloodStockHistoryDao;
import com.hashcode.telcoapp.bba.dao.BloodStockHistorySearchCriteria;
import com.hashcode.telcoapp.bba.dao.DonationCenterDao;
import com.hashcode.telcoapp.bba.data.BloodStockHistory;
import com.hashcode.telcoapp.bba.data.DonationCenter;
import com.hashcode.telcoapp.bba.enums.BloodStatus;
import com.hashcode.telcoapp.bba.enums.BloodType;
import com.hashcode.telcoapp.bba.enums.SubBloodType;
import com.hashcode.telcoapp.bba.util.Configuration;
import com.hashcode.telcoapp.common.util.ServiceResolver;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood stock tab.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class BloodStockSearchLayout extends VerticalLayout
{
    private static final long serialVersionUID = 8811265418426859480L;

    private BloodStockHistorySearchForm bloodStockHistorySearchForm;
    private BloodStockHistoryTable bloodStockHistoryTable;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public BloodStockSearchLayout()
    {
        initUI();
        setSizeFull();
    }

    private void initUI()
    {
        setMargin(true);

        VerticalLayout searchLayout = new VerticalLayout();
        searchLayout.addComponent(getBloodStockHistorySearchForm());

        Button searchButton = new Button("Search");
        searchButton.setImmediate(true);
        searchButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = -7291423273138687511L;

            public void buttonClick(final ClickEvent event)
            {
                if (getBloodStockHistorySearchForm().validateAllFields())
                {
                    getBloodStockHistoryTable().refresh(getBloodStockHistorySearchForm().getModel());
                    getBloodStockHistorySearchForm().setComponentError(null);
                }
            }
        });

        Button clearButton = new Button("Clear");
        clearButton.setImmediate(true);
        clearButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = 8293367292559846490L;

            public void buttonClick(final ClickEvent event)
            {
                getBloodStockHistorySearchForm().setModel(new BloodStockHistorySearchCriteria());
                getBloodStockHistoryTable().refresh(new BloodStockHistorySearchCriteria());
            }
        });

        Button exportButton = new Button("Export");
        exportButton.setImmediate(true);
        exportButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = 1110901242823603580L;

            public void buttonClick(final ClickEvent event)
            {
                if (getBloodStockHistorySearchForm().validateAllFields())
                {
                    BloodStockHistorySearchCriteria searchCriteria = getBloodStockHistorySearchForm().getModel();
                    try
                    {
                        String fileName = writeCSVData(searchCriteria.getDateTime());
                        String fileURL = Configuration.getMessage("csv.download.path") + fileName;
                        getWindow().open(new ExternalResource(fileURL));
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        });

        HorizontalLayout searchButtonLayout = new HorizontalLayout();
        searchButtonLayout.setSpacing(true);
        searchButtonLayout.addComponent(searchButton);
        searchButtonLayout.addComponent(clearButton);
        searchButtonLayout.addComponent(exportButton);
        searchLayout.addComponent(searchButtonLayout);
        searchLayout.setComponentAlignment(searchButtonLayout, Alignment.MIDDLE_CENTER);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.addComponent(getBloodStockHistoryTable().getPaginator());
        tableLayout.addComponent(getBloodStockHistoryTable());

        addComponent(searchLayout);
        addComponent(tableLayout);
        setSizeFull();
    }

    private BloodStockHistoryTable getBloodStockHistoryTable()
    {
        if (bloodStockHistoryTable == null)
        {
            bloodStockHistoryTable = new BloodStockHistoryTable();
            bloodStockHistoryTable.refresh();
        }

        return bloodStockHistoryTable;
    }

    private BloodStockHistorySearchForm getBloodStockHistorySearchForm()
    {
        if (bloodStockHistorySearchForm == null)
        {
            bloodStockHistorySearchForm = new BloodStockHistorySearchForm();
        }

        return bloodStockHistorySearchForm;
    }

    /**
     * <p>
     * Refresh blood stock tab.
     * </p>
     */
    public void refresh()
    {
        getBloodStockHistoryTable().refresh(new BloodStockHistorySearchCriteria());
    }

    private String writeCSVData(final Date date) throws IOException
    {
        String fileName = String.valueOf(System.currentTimeMillis()) + ".csv";
        String csv = Configuration.getMessage("csv.generation.path") + fileName;
        CSVWriter csvWriter = new CSVWriter(new FileWriter(csv), ',');
        List<String[]> data = new ArrayList<String[]>();

        data.add(new String[] {
            "#",
            "Name",
            "Category",
            "A+",
            "A-",
            "B+",
            "B-",
            "AB+",
            "AB-",
            "O+",
            "O-",
            "Total",
            "Unlabled",
            "Unscreen",
            "Grand Total",
        });

        List<DonationCenter> bloodBanks = getService(DonationCenterDao.class).getBloodBanks();
        int count = 1;
        for (DonationCenter donationCenter : bloodBanks)
        {
            data.add(generateRow(String.valueOf(count), donationCenter.getCenterName(), donationCenter, date,
                SubBloodType.RCC, BloodStatus.CROSS_MATCHED, "RCC Cross Matched"));
            data.add(generateRow("", "", donationCenter, date, SubBloodType.RCC, BloodStatus.UN_CROSS_MATCHED,
                "RCC Un Cross Matched"));
            data.add(generateRow("", "", donationCenter, date, SubBloodType.PLATELETS, null, "Platelets"));

            count++;
        }

        csvWriter.writeAll(data);
        csvWriter.close();

        return fileName;
    }

    private String[] generateRow(final String count, final String name, final DonationCenter donationCenter,
        final Date date, final SubBloodType subBloodType, final BloodStatus bloodStatus, final String caption)
    {
        BloodStockHistorySearchCriteria bloodStockHistorySearchCriteria = new BloodStockHistorySearchCriteria();
        bloodStockHistorySearchCriteria.setCenter(donationCenter);
        bloodStockHistorySearchCriteria.setDateTime(date);
        bloodStockHistorySearchCriteria.setSubBloodType(subBloodType);
        bloodStockHistorySearchCriteria.setBloodStatus(bloodStatus);
        List<BloodStockHistory> historyList =
            getService(BloodStockHistoryDao.class).findBySearchCriteria(bloodStockHistorySearchCriteria);

        int aPositive = 0;
        int aNegative = 0;
        int bPositive = 0;
        int bNegative = 0;
        int abPositive = 0;
        int abNegative = 0;
        int oPositive = 0;
        int oNegative = 0;
        int total = 0;
        int unLabled = 0;
        int unScreened = 0;
        int grandTotal = 0;

        for (BloodStockHistory bloodStockHistory : historyList)
        {
            if (bloodStockHistory.getBloodType() == BloodType.A_P)
                aPositive += bloodStockHistory.getStockCount();
            if (bloodStockHistory.getBloodType() == BloodType.A_N)
                aNegative += bloodStockHistory.getStockCount();
            if (bloodStockHistory.getBloodType() == BloodType.B_P)
                bPositive += bloodStockHistory.getStockCount();
            if (bloodStockHistory.getBloodType() == BloodType.B_N)
                bNegative += bloodStockHistory.getStockCount();
            if (bloodStockHistory.getBloodType() == BloodType.AB_P)
                abPositive += bloodStockHistory.getStockCount();
            if (bloodStockHistory.getBloodType() == BloodType.AB_N)
                abNegative += bloodStockHistory.getStockCount();
            if (bloodStockHistory.getBloodType() == BloodType.O_P)
                oPositive += bloodStockHistory.getStockCount();
            if (bloodStockHistory.getBloodType() == BloodType.A_N)
                oNegative += bloodStockHistory.getStockCount();
            total = aPositive + aNegative + bPositive + bNegative + abPositive + abNegative + oPositive + oNegative;
        }

        return new String[] {
            count,
            name,
            caption,
            String.valueOf(aPositive),
            String.valueOf(aNegative),
            String.valueOf(bPositive),
            String.valueOf(bNegative),
            String.valueOf(abPositive),
            String.valueOf(abNegative),
            String.valueOf(oPositive),
            String.valueOf(oNegative),
            String.valueOf(total),
            String.valueOf(unLabled),
            String.valueOf(unScreened),
            String.valueOf(grandTotal)
        };
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
