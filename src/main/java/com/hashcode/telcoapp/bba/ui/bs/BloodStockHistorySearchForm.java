/*
 * FILENAME
 *     BloodStockHistorySearchForm.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.bs;

import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createBloodStatusField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createBloodTypeField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createSubBloodTypeField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createDonationCenterSelectFieldByLocationType;

import java.util.Arrays;
import java.util.List;

import com.hashcode.telcoapp.bba.dao.BloodStockHistorySearchCriteria;
import com.hashcode.telcoapp.bba.data.BloodStockHistory;
import com.hashcode.telcoapp.bba.enums.LocationType;
import com.hashcode.telcoapp.bba.enums.SubBloodType;
import com.hashcode.telcoapp.common.ui.components.MultiColumnForm;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Select;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood stock history search form.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class BloodStockHistorySearchForm extends MultiColumnForm<BloodStockHistorySearchCriteria>
{
    private static final long serialVersionUID = -5402421318986755724L;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public BloodStockHistorySearchForm()
    {
        super(1);
        setFormFieldFactory(new BloodStockHistorySearchFormFields());
        setModel(new BloodStockHistorySearchCriteria());
    }

    @Override
    protected List<String> getVisiblePropertyIds()
    {
        return Arrays.asList(new String[] {
            BloodStockHistorySearchCriteria.DATE_TIME,
            BloodStockHistorySearchCriteria.BLOOD_TYPE,
            BloodStockHistorySearchCriteria.SUB_BLOOD_TYPE,
            BloodStockHistorySearchCriteria.BLOOD_STATUS,
            BloodStockHistorySearchCriteria.CENTER
        });
    }

    /**
     * {@inheritDoc}.
     *
     * @see com.hashcode.telcoapp.common.ui.components.MultiColumnForm#setModel(java.io.Serializable)
     */
    @Override
    public void setModel(final BloodStockHistorySearchCriteria model)
    {
        super.setModel(model);
        
        Field subTypeField = getField(BloodStockHistory.SUB_BLOOD_TYPE);
        final Field bloodStatusField = getField(BloodStockHistory.BLOOD_STATUS);

        subTypeField.addListener(new Property.ValueChangeListener()
        {
            private static final long serialVersionUID = -8389095013884899736L;

            public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event)
            {
                Object value = event.getProperty().getValue();

                if (SubBloodType.RCC.equals(value))
                {
                    bloodStatusField.setEnabled(true);
                    bloodStatusField.setRequired(true);
                }
                else
                {
                    bloodStatusField.setEnabled(false);
                    bloodStatusField.setRequired(false);
                }
                bloodStatusField.setValue(null);
            }
        });
    }

    @Override
    protected int[] getPropertyColumnIndices()
    {
        return new int[] {
            0, 0, 0, 0,  0
        };
    }

    /**
     * <p>
     * Blood stock history search form fields.
     * </p>
     *
     * @author Manuja
     *
     * @version $Id$
     */
    private class BloodStockHistorySearchFormFields extends DefaultFieldFactory
    {
        private static final long serialVersionUID = 4774543254289652037L;

        /**
         * {@inheritDoc}
         */
        @Override
        public Field createField(final Item item, final Object propertyId, final Component uiContext)
        {
            String pid = (String) propertyId;
            if (pid.equals(BloodStockHistorySearchCriteria.DATE_TIME))
            {
                DateField dateField = new DateField("Date");
                dateField.setImmediate(true);
                dateField.setResolution(DateField.RESOLUTION_DAY);
                dateField.setRequired(true);
                dateField.setRequiredError("Date is required");
                return dateField;
            }
            else if (pid.equals(BloodStockHistorySearchCriteria.BLOOD_TYPE))
            {
                return createBloodTypeField("Blood Type");
            }
            else if (pid.equals(BloodStockHistorySearchCriteria.SUB_BLOOD_TYPE))
            {
                return createSubBloodTypeField("Sub Type");
            }
            else if (pid.equals(BloodStockHistorySearchCriteria.BLOOD_STATUS))
            {
                Select subBloodTypeField = createBloodStatusField("Status");
                subBloodTypeField.setEnabled(false);
                return subBloodTypeField;
            }
            else if (pid.equals(BloodStockHistorySearchCriteria.CENTER))
            {
                return createDonationCenterSelectFieldByLocationType("Blood Bank", LocationType.BLOOD_BANK);
            }
            return super.createField(item, propertyId, uiContext);
        }
    }
}
