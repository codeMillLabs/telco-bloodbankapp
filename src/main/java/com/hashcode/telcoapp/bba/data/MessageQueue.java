/*
 * FILENAME
 *     OutBoundMessage.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.telcoapp.bba.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.hashcode.telcoapp.bba.enums.BBAState;

//
//IMPORTS
//NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Entity for blood donation campaign or centers.
 * </p>
 * 
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
@Entity
@Table(name = "MESSAGE_QUEUE")
public class MessageQueue implements Serializable
{

    private static final long serialVersionUID = -6308944318197036810L;
    public static final String ID = "id";
    public static final String TEXT_MESSAGE = "message";
    public static final String SCHEDULED_DATE = "scheduledDate";
    public static final String SENT_DATE = "sentDate";
    public static final String TO_ADDRESS = "toAddress";
    public static final String STATUS = "status";
    public static final String RESP_MSG = "responseMsg";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "OUTBOUND_SEQ")
    @SequenceGenerator(name = "OUTBOUND_SEQ", sequenceName = "OUTBOUND_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "TO_ADDRESS")
    private String toAddress;

    @Column(name = "MESSAGE")
    private String message;

    @Column(name = "SENT_BY")
    private String sentBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SCHEDULED_DATE")
    private Date scheduledDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SENT_DATE")
    private Date sentDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private BBAState status;
    
    @Column(name = "RESP_MSG")
    private String responseMsg;

    @ManyToOne
    @JoinColumn(name = "BULK_MSG_ID")
    private BulkMessage bulkMessage;

    /**
     * <p>
     * Getter for toAddress.
     * </p>
     * 
     * @return the toAddress
     */
    public String getToAddress()
    {
        return toAddress;
    }

    /**
     * <p>
     * Setting value for toAddress.
     * </p>
     * 
     * @param inToAddress
     *            the toAddress to set
     */
    public void setToAddress(final String inToAddress)
    {
        this.toAddress = inToAddress;
    }

    /**
     * <p>
     * Getter for message.
     * </p>
     * 
     * @return the message
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * <p>
     * Setting value for message.
     * </p>
     * 
     * @param inMessage
     *            the message to set
     */
    public void setMessage(final String inMessage)
    {
        this.message = inMessage;
    }

    /**
     * <p>
     * Getter for sentBy.
     * </p>
     * 
     * @return the sentBy
     */
    public String getSentBy()
    {
        return sentBy;
    }

    /**
     * <p>
     * Setting value for sentBy.
     * </p>
     * 
     * @param inSentBy
     *            the sentBy to set
     */
    public void setSentBy(final String inSentBy)
    {
        this.sentBy = inSentBy;
    }

    /**
     * <p>
     * Getter for scheduledDate.
     * </p>
     * 
     * @return the scheduledDate
     */
    public Date getScheduledDate()
    {
        return scheduledDate;
    }

    /**
     * <p>
     * Setting value for scheduledDate.
     * </p>
     * 
     * @param inScheduledDate
     *            the scheduledDate to set
     */
    public void setScheduledDate(final Date inScheduledDate)
    {
        this.scheduledDate = inScheduledDate;
    }

    /**
     * <p>
     * Getter for sentDate.
     * </p>
     * 
     * @return the sentDate
     */
    public Date getSentDate()
    {
        return sentDate;
    }

    /**
     * <p>
     * Setting value for sentDate.
     * </p>
     * 
     * @param inSentDate
     *            the sentDate to set
     */
    public void setSentDate(final Date inSentDate)
    {
        this.sentDate = inSentDate;
    }

    /**
     * <p>
     * Getter for status.
     * </p>
     * 
     * @return the status
     */
    public BBAState getStatus()
    {
        return status;
    }

    /**
     * <p>
     * Setting value for status.
     * </p>
     * 
     * @param inStatus
     *            the status to set
     */
    public void setStatus(final BBAState inStatus)
    {
        this.status = inStatus;
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * <p>
     * Getter for bulkMessage.
     * </p>
     * 
     * @return the bulkMessage
     */
    public BulkMessage getBulkMessage()
    {
        return bulkMessage;
    }

    /**
     * <p>
     * Setting value for bulkMessage.
     * </p>
     * 
     * @param inBulkMessage
     *            the bulkMessage to set
     */
    public void setBulkMessage(final BulkMessage inBulkMessage)
    {
        this.bulkMessage = inBulkMessage;
    }

    /**
     * <p>
     * Getter for responseMsg.
     * </p>
     * 
     * @return the responseMsg
     */
    public String getResponseMsg()
    {
        return responseMsg;
    }

    /**
     * <p>
     * Setting value for responseMsg.
     * </p>
     * 
     * @param inResponseMsg the responseMsg to set
     */
    public void setResponseMsg(final String inResponseMsg)
    {
        this.responseMsg = inResponseMsg;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("MessageQueue [id=");
        builder.append(id);
        builder.append(", toAddress=");
        builder.append(toAddress);
        builder.append(", message=");
        builder.append(message);
        builder.append(", sentBy=");
        builder.append(sentBy);
        builder.append(", scheduledDate=");
        builder.append(scheduledDate);
        builder.append(", sentDate=");
        builder.append(sentDate);
        builder.append(", status=");
        builder.append(status);
        builder.append(", responseMsg=");
        builder.append(responseMsg);
        builder.append(", bulkMessage=");
        builder.append(bulkMessage);
        builder.append("]");
        return builder.toString();
    }

}
