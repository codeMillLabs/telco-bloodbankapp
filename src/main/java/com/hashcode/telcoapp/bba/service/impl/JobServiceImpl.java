/*
 * FILENAME
 *     JobServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.telcoapp.bba.service.JobService;
import com.hashcode.telcoapp.common.dao.ResponseMsgDao;
import com.hashcode.telcoapp.common.entity.ResponseMsg;
import com.hashcode.telcoapp.common.handlers.IMessageSender;
import com.hashcode.telcoapp.common.util.ServiceResolver;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Job Service Implementation.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class JobServiceImpl implements JobService
{

    protected static final Logger LOGGER = LoggerFactory.getLogger(JobService.class);

    private IMessageSender appzoneMessageSender;
    private IMessageSender ideaProMessageSender;

    /**
     * {@inheritDoc}
     * 
     * @see com.appmart.util.IMessageProcessor#sendAlert()
     */
    public void sendAlert()
    {
        try
        {
            ResponseMsg alert = getService(ResponseMsgDao.class).findNewAlert();

            if (null != alert)
            {
                LOGGER.info("-- Etisalat Enabled [" + com.hashcode.telcoapp.common.util.Configuration.ETISALAT_ENABLED
                    + "], Dialog Enabled [" + com.hashcode.telcoapp.common.util.Configuration.DIALOG_ENABLED + "]");

                if (com.hashcode.telcoapp.common.util.Configuration.ETISALAT_ENABLED)
                {
                    LOGGER.info("-- Sending Alert to Etisalat Subscribers");
                    appzoneMessageSender.broadcastMessage(alert.getMessage());
                }

                if (com.hashcode.telcoapp.common.util.Configuration.DIALOG_ENABLED)
                {
                    LOGGER.info("-- Sending Alert to Dialog Subscribers");
                    ideaProMessageSender.broadcastMessage(alert.getMessage());
                }
                alert.setSent(true);
                getService(ResponseMsgDao.class).update(alert);
                LOGGER.info("-- Sending Alert : " + alert.getMessage());
            }
            else
            {
                LOGGER.error("-- No new alert found ............................");
                return;
            }
        }
        catch (Exception e)
        {
            LOGGER.error("-- Error sending message >>> " + e.getMessage());
        }
    }

    /**
     * <p>
     * Setting value for appzoneMessageSender.
     * </p>
     * 
     * @param inAppzoneMessageSender
     *            the appzoneMessageSender to set
     */
    public void setAppzoneMessageSender(final IMessageSender inAppzoneMessageSender)
    {
        this.appzoneMessageSender = inAppzoneMessageSender;
    }

    /**
     * <p>
     * Setting value for ideaProMessageSender.
     * </p>
     * 
     * @param inIdeaProMessageSender
     *            the ideaProMessageSender to set
     */
    public void setIdeaProMessageSender(final IMessageSender inIdeaProMessageSender)
    {
        this.ideaProMessageSender = inIdeaProMessageSender;
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
