/*
 * FILENAME
 *     InboxSearchForm.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.inbox;

import java.util.Arrays;
import java.util.List;

import com.hashcode.telcoapp.bba.dao.MessageSearchCriteria;
import com.hashcode.telcoapp.common.ui.components.MultiColumnForm;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Inbox search form.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
public class InboxSearchForm extends MultiColumnForm<MessageSearchCriteria>
{
    private static final long serialVersionUID = 78137417727774432L;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public InboxSearchForm()
    {
        super(1);
        setFormFieldFactory(new InboxSearchFormFields());
        setModel(new MessageSearchCriteria());
    }

    @Override
    protected List<String> getVisiblePropertyIds()
    {
        return Arrays.asList(new String[] {
            MessageSearchCriteria.DATE
        });
    }

    @Override
    protected int[] getPropertyColumnIndices()
    {
        return new int[] {
            0
        };
    }

    /**
     * <p>
     * Blood stock history search form fields.
     * </p>
     *
     * @author Manuja
     *
     * @version $Id$
     */
    private class InboxSearchFormFields extends DefaultFieldFactory
    {
        private static final long serialVersionUID = -6862926607164935036L;

        /**
         * {@inheritDoc}
         */
        @Override
        public Field createField(final Item item, final Object propertyId, final Component uiContext)
        {
            String pid = (String) propertyId;
            if (pid.equals(MessageSearchCriteria.DATE))
            {
                DateField dateField = new DateField("Date");
                dateField.setImmediate(true);
                dateField.setResolution(DateField.RESOLUTION_DAY);
                dateField.setRequired(true);
                dateField.setRequiredError("Date is required");
                return dateField;
            }
            return super.createField(item, propertyId, uiContext);
        }
    }
}
