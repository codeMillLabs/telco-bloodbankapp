/*
 * FILENAME
 *     DonateAsAt.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.hashcode.telcoapp.bba.enums.BBAState;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood donation history of the subscriber.
 * </p>
 * 
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
@Entity
@Table(name = "DONATE_AS_AT_HISTORY")
public class DonateAsAt implements Serializable
{
    private static final long serialVersionUID = 2767349035462856214L;

    public static final String ID = "id";
    public static final String DONATION_DATE = "donatedDate";
    public static final String REFERENCE_NUMBER = "refNo";
    public static final String STATE = "state";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "DONATE_SEQ")
    @SequenceGenerator(name = "DONATE_SEQ", sequenceName = "DONATE_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Temporal(TemporalType.DATE)
    @Column(name = "DONATED_DATE")
    private Date donatedDate;

    @ManyToOne
    @JoinColumn(name = "SUBS_ID")
    private BBASubscriber subscriber;

    @Column(name = "REF_NO")
    private String refNo;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private BBAState state;

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * <p>
     * Getter for donatedDate.
     * </p>
     * 
     * @return the donatedDate
     */
    public Date getDonatedDate()
    {
        return donatedDate;
    }

    /**
     * <p>
     * Setting value for donatedDate.
     * </p>
     * 
     * @param inDonatedDate
     *            the donatedDate to set
     */
    public void setDonatedDate(final Date inDonatedDate)
    {
        this.donatedDate = inDonatedDate;
    }

    /**
     * <p>
     * Getter for subscriber.
     * </p>
     * 
     * @return the subscriber
     */
    public BBASubscriber getSubscriber()
    {
        return subscriber;
    }

    /**
     * <p>
     * Setting value for subscriber.
     * </p>
     * 
     * @param inSubscriber
     *            the subscriber to set
     */
    public void setSubscriber(final BBASubscriber inSubscriber)
    {
        this.subscriber = inSubscriber;
    }

    /**
     * <p>
     * Getter for refNo.
     * </p>
     * 
     * @return the refNo
     */
    public String getRefNo()
    {
        return refNo;
    }

    /**
     * <p>
     * Setting value for refNo.
     * </p>
     * 
     * @param inRefNo
     *            the refNo to set
     */
    public void setRefNo(final String inRefNo)
    {
        this.refNo = inRefNo.toUpperCase();
    }

    /**
     * <p>
     * Getter for state.
     * </p>
     * 
     * @return the state
     */
    public BBAState getState()
    {
        return state;
    }

    /**
     * <p>
     * Setting value for state.
     * </p>
     * 
     * @param inState
     *            the state to set
     */
    public void setState(final BBAState inState)
    {
        this.state = inState;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((donatedDate == null) ? 0 : donatedDate.hashCode());
        result = prime * result + ((refNo == null) ? 0 : refNo.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DonateAsAt other = (DonateAsAt) obj;
        if (donatedDate == null)
        {
            if (other.donatedDate != null)
                return false;
        }
        else if (!donatedDate.equals(other.donatedDate))
            return false;
        if (refNo == null)
        {
            if (other.refNo != null)
                return false;
        }
        else if (!refNo.equals(other.refNo))
            return false;
        return true;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("DonateAsAt [id=");
        builder.append(id);
        builder.append(", donatedDate=");
        builder.append(donatedDate);
        builder.append(", subscriber=");
        builder.append(subscriber);
        builder.append(", refNo=");
        builder.append(refNo);
        builder.append(", state=");
        builder.append(state);
        builder.append("]");
        return builder.toString();
    }
}
