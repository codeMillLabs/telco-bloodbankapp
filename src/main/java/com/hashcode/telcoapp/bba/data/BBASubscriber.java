/*
 * FILENAME
 *     AppSubscriber.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.data;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.joda.time.DateTime;

import com.hashcode.telcoapp.bba.enums.BloodType;
import com.hashcode.telcoapp.bba.enums.UserRole;
import com.hashcode.telcoapp.bba.util.Configuration;
import com.hashcode.telcoapp.common.entity.Subscriber;
import com.hashcode.telcoapp.common.enums.Gender;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Entity for Question Subscriber.
 * </p>
 * 
 * @author
 * 
 * @version $Id$
 */
@Entity
@DiscriminatorValue("BBA")
public class BBASubscriber extends Subscriber implements Serializable
{
    private static final long serialVersionUID = 5157255841077893544L;

    public static final String ID = "id";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String ADDRESS = "address";
    public static final String SUBSCRIBER_STATE = "subscriberState";
    public static final String REGISTERED_DATE = "registeredDate";
    public static final String LAST_MODIFIED_DATE = "lastModifiedDate";
    public static final String GENDER = "gender";
    public static final String POINTS = "points";
    public static final String BLOOD_TYPE = "bloodType";
    public static final String DISTRICT = "district";
    public static final String LAST_DONATION_DATE = "lastDonatedDate";
    public static final String NEXT_ELIGIBLE_DATE = "nextEligibleDate";
    public static final String BLOOD_STOCK_ACCOUNT = "bloodStockAccount";
    public static final String NIC = "nic";
    public static final String REMARKS = "remarks";
    public static final String AP_DONOR = "apDonor";
    public static final String DOB = "dob";
    public static final String USER_ROLES = "userRoles";

    private static final int POINTS_INCREMENT_VALUE = 1;

    @Enumerated(EnumType.STRING)
    @Column(name = "BLOOD_TYPE")
    private BloodType bloodType;

    @Column(name = "HOME_ADDRESS")
    private String homeAddress;

    @Column(name = "DISTRICT")
    private String district;

    @Column(name = "EMAIL")
    private String email;

    @Temporal(TemporalType.DATE)
    @Column(name = "LAST_DONATED_DATE")
    private Date lastDonatedDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "NEXT_ELIGIBLE_DATE")
    private Date nextEligibleDate;

    @OneToMany(cascade = {
        CascadeType.PERSIST, CascadeType.MERGE
    }, mappedBy = "subscriber")
    private Set<DonateAsAt> donatedHistory;

    @Column(name = "NIC")
    private String nic;

    @Column(name = "BLOOD_STOCK_ACCOUNT")
    private boolean bloodStockAccount = false;

    @Column(name = "REMARKS")
    private String remarks;

    @Column(name = "AP_DONOR")
    private boolean apDonor;

    @Temporal(TemporalType.DATE)
    @Column(name = "DOB")
    private Date dob;

    @Column(name = "BIRTH_MONTH")
    private int birthMonth = 0;

    @Column(name = "BIRTH_DATE")
    private int birthDate = 0;

    @ElementCollection(targetClass = UserRole.class)
    @Enumerated(EnumType.STRING)
    @JoinTable(name = "COMMON_SUBSCRIBER_USER_ROLES", joinColumns = {
        @JoinColumn(name = "SUBSCRIBER_ID")
    })
    @Column(name = "USER_ROLE")
    private Set<UserRole> userRoles;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public BBASubscriber()
    {
        setPoints(0);
    }

    /**
     * <p>
     * This will check current user has the required role to proceed..
     * </p>
     * 
     * @param userRole
     *            required user role
     * @return if the required role is assigned returns true otherwise false
     * 
     */
    public boolean hasUserRole(final UserRole userRole)
    {

        for (UserRole myRole : getUserRoles())
        {
            if (myRole == userRole)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * <p>
     * This will check current user has any of the required user role to proceed with current functionality.
     * </p>
     * 
     * @param inUserRoles
     *            required user roles
     * @return if any of the required role is assigned returns true otherwise false
     * 
     */
    public boolean hasAnyUserRole(final UserRole... inUserRoles)
    {

        for (UserRole userRole : inUserRoles)
        {
            if (getUserRoles().contains(userRole))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * <p>
     * Setting value for donatedHistory.
     * </p>
     * 
     * @param inDonationAsAt
     *            the donatedHistory to set
     */
    public void addDonatedHistory(final DonateAsAt inDonationAsAt)
    {
        this.getDonatedHistory().add(inDonationAsAt);
    }

    /**
     * <p>
     * Getter for lastDonatedDate.
     * </p>
     * 
     * @return the lastDonatedDate
     */
    public Date getLastDonatedDate()
    {
        return lastDonatedDate;
    }

    /**
     * <p>
     * Setting value for lastDonatedDate.
     * </p>
     * 
     * @param inLastDonatedDate
     *            the lastDonatedDate to set
     */
    public void setLastDonatedDate(final Date inLastDonatedDate)
    {
        this.lastDonatedDate = inLastDonatedDate;

        DateTime dateTime = new DateTime().plusMonths(Configuration.DONATION_FREQUENCY);
        setNextEligibleDate(dateTime.toDate());
    }

    /**
     * <p>
     * Getter for donatedHistory.
     * </p>
     * 
     * @return the donatedHistory
     */
    public Set<DonateAsAt> getDonatedHistory()
    {
        if (donatedHistory == null)
        {
            donatedHistory = new HashSet<DonateAsAt>();
        }
        return donatedHistory;
    }

    /**
     * <p>
     * Setting value for donatedHistory.
     * </p>
     * 
     * @param inDonatedHistory
     *            the donatedHistory to set
     */
    public void setDonatedHistory(final Set<DonateAsAt> inDonatedHistory)
    {
        this.donatedHistory = inDonatedHistory;
    }

    /**
     * <p>
     * Getter for bloodType.
     * </p>
     * 
     * @return the bloodType
     */
    public BloodType getBloodType()
    {
        return bloodType;
    }

    /**
     * <p>
     * Setting value for bloodType.
     * </p>
     * 
     * @param inBloodType
     *            the bloodType to set
     */
    public void setBloodType(final BloodType inBloodType)
    {
        this.bloodType = inBloodType;
    }

    /**
     * <p>
     * Getter for homeAddress.
     * </p>
     * 
     * @return the homeAddress
     */
    public String getHomeAddress()
    {
        return homeAddress;
    }

    /**
     * <p>
     * Setting value for homeAddress.
     * </p>
     * 
     * @param inHomeAddress
     *            the homeAddress to set
     */
    public void setHomeAddress(final String inHomeAddress)
    {
        this.homeAddress = inHomeAddress;
    }

    /**
     * <p>
     * Getter for district.
     * </p>
     * 
     * @return the district
     */
    public String getDistrict()
    {
        return district;
    }

    /**
     * <p>
     * Setting value for district.
     * </p>
     * 
     * @param inDistrict
     *            the district to set
     */
    public void setDistrict(final String inDistrict)
    {
        this.district = inDistrict;
    }

    /**
     * <p>
     * Getter for email.
     * </p>
     * 
     * @return the email
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * <p>
     * Setting value for email.
     * </p>
     * 
     * @param inEmail
     *            the email to set
     */
    public void setEmail(final String inEmail)
    {
        this.email = inEmail;
    }

    /**
     * <p>
     * Getter for nextEligibleDate.
     * </p>
     * 
     * @return the nextEligibleDate
     */
    public Date getNextEligibleDate()
    {
        return nextEligibleDate;
    }

    /**
     * <p>
     * Setting value for nextEligibleDate.
     * </p>
     * 
     * @param inNextEligibleDate
     *            the nextEligibleDate to set
     */
    public void setNextEligibleDate(final Date inNextEligibleDate)
    {
        this.nextEligibleDate = inNextEligibleDate;
    }

    /**
     * <p>
     * Getter for bloodStockAccount.
     * </p>
     * 
     * @return the bloodStockAccount
     */
    public boolean isBloodStockAccount()
    {
        return bloodStockAccount;
    }

    /**
     * <p>
     * Setting value for bloodStockAccount.
     * </p>
     * 
     * @param inBloodStockAccount
     *            the bloodStockAccount to set
     */
    public void setBloodStockAccount(final boolean inBloodStockAccount)
    {
        this.bloodStockAccount = inBloodStockAccount;
    }

    /**
     * <p>
     * Getter for nic.
     * </p>
     * 
     * @return the nic
     */
    public String getNic()
    {
        return nic;
    }

    /**
     * <p>
     * Setting value for nic.
     * </p>
     * 
     * @param inNic
     *            the nic to set
     */
    public void setNic(final String inNic)
    {
        this.nic = inNic.toUpperCase();
    }

    /**
     * <p>
     * Increment points.
     * </p>
     */
    public void incrementPoints()
    {
        setPoints(getPoints() + POINTS_INCREMENT_VALUE);
    }

    /**
     * <p>
     * Getter for remarks.
     * </p>
     * 
     * @return the remarks
     */
    public String getRemarks()
    {
        return remarks;
    }

    /**
     * <p>
     * Setting value for remarks.
     * </p>
     * 
     * @param inRemarks
     *            the remarks to set
     */
    public void setRemarks(final String inRemarks)
    {
        this.remarks = inRemarks;
    }

    /**
     * <p>
     * Get full name.
     * </p>
     * 
     * @return full name string
     */
    public String getFullName()
    {
        if (null != getFirstName() && null != getLastName())
        {
            return getFirstName() + " " + getLastName();
        }
        else
        {
            return null;
        }
    }

    /**
     * <p>
     * Getter for apDonor.
     * </p>
     * 
     * @return the apDonor
     */
    public boolean isApDonor()
    {
        return apDonor;
    }

    /**
     * <p>
     * Setting value for apDonor.
     * </p>
     * 
     * @param inApDonor
     *            the apDonor to set
     */
    public void setApDonor(final boolean inApDonor)
    {
        this.apDonor = inApDonor;
    }

    /**
     * <p>
     * Getter for dob.
     * </p>
     * 
     * @return the dob
     */
    public Date getDob()
    {
        return dob;
    }

    /**
     * <p>
     * Setting value for dob.
     * </p>
     * 
     * @param inDob
     *            the dob to set
     */
    public void setDob(final Date inDob)
    {
        this.dob = inDob;
        if (this.dob != null)
        {
            DateTime dateTime = new DateTime(this.dob.getTime());
            setBirthMonth(dateTime.getMonthOfYear());
            setBirthDate(dateTime.getDayOfMonth());
        }
    }

    /**
     * <p>
     * Getter for birthMonth.
     * </p>
     * 
     * @return the birthMonth
     */
    public int getBirthMonth()
    {
        return birthMonth;
    }

    /**
     * <p>
     * Setting value for birthMonth.
     * </p>
     * 
     * @param inBirthMonth
     *            the birthMonth to set
     */
    public void setBirthMonth(final int inBirthMonth)
    {
        this.birthMonth = inBirthMonth;
    }

    /**
     * <p>
     * Getter for birthDate.
     * </p>
     * 
     * @return the birthDate
     */
    public int getBirthDate()
    {
        return birthDate;
    }

    /**
     * <p>
     * Setting value for birthDate.
     * </p>
     * 
     * @param inBirthDate
     *            the birthDate to set
     */
    public void setBirthDate(final int inBirthDate)
    {
        this.birthDate = inBirthDate;
    }

    /**
     * {@inheritDoc}
     * 
     * @see Subscriber#setGender(com.hashcode.telcoapp.common.enums.Gender)
     */
    @Override
    public void setGender(final Gender inGender)
    {
        super.setGender(inGender);
    }

    /**
     * <p>
     * Getter for userRoles.
     * </p>
     * 
     * @return the userRoles
     */
    public Set<UserRole> getUserRoles()
    {
        if (this.userRoles == null)
        {
            this.userRoles = new HashSet<UserRole>();
        }
        return userRoles;
    }

    /**
     * <p>
     * Setting value for userRoles.
     * </p>
     * 
     * @param inUserRoles
     *            the userRoles to set
     */
    public void setUserRoles(final Set<UserRole> inUserRoles)
    {
        this.userRoles = inUserRoles;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BBASubscriber other = (BBASubscriber) obj;
        if (getId() == null)
        {
            if (other.getId() != null)
                return false;
        }
        else if (!getId().equals(other.getId()))
            return false;
        return true;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("BBASubscriber [bloodType=");
        builder.append(bloodType);
        builder.append(", homeAddress=");
        builder.append(homeAddress);
        builder.append(", district=");
        builder.append(district);
        builder.append(", email=");
        builder.append(email);
        builder.append(", lastDonatedDate=");
        builder.append(lastDonatedDate);
        builder.append(", nextEligibleDate=");
        builder.append(nextEligibleDate);
        builder.append(", nic=");
        builder.append(nic);
        builder.append(", bloodStockAccount=");
        builder.append(bloodStockAccount);
        builder.append(", remarks=");
        builder.append(remarks);
        builder.append(", apDonor=");
        builder.append(apDonor);
        builder.append(", dob=");
        builder.append(dob);
        builder.append(", birthMonth=");
        builder.append(birthMonth);
        builder.append(", birthDate=");
        builder.append(birthDate);
        builder.append("]");
        return builder.toString();
    }
}
