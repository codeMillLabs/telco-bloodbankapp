/*
 * FILENAME
 *     InitialSetNameState.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.state.subscriber.profile;

import static com.hashcode.telcoapp.common.handlers.Messages.getMessage;

import com.hashcode.telcoapp.bba.data.UssdSession;
import com.hashcode.telcoapp.bba.enums.UssdState;
import com.hashcode.telcoapp.bba.state.BBAppState;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Set first name state.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class InitialSetNameState extends BBAppState
{

    @Override
    public String messageReceived(final UssdSession ussdSession, final String message)
    {
        ussdSession.setCurrentState(UssdState.INITIAL_SET_NIC);
        ussdSession.getSubscriber().setFirstName(message);
        return getMessage("ussd.initial.set.nic.message");
    }

}
