/*
 * FILENAME
 *     DonateSearchCriteria.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.dao;

import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.enums.BBAState;
import com.hashcode.telcoapp.common.dao.SearchCriteria;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Donate search criteria.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 */
public class DonateAsAtSearchCriteria extends SearchCriteria
{
    private static final long serialVersionUID = 1401505742391943065L;

    private BBASubscriber subscriber;
    private BBAState state;
    private String refNo;

    /**
     * <p>
     * Getter for subscriber.
     * </p>
     * 
     * @return the subscriber
     */
    public BBASubscriber getSubscriber()
    {
        return subscriber;
    }

    /**
     * <p>
     * Setting value for subscriber.
     * </p>
     * 
     * @param inSubscriber
     *            the subscriber to set
     */
    public void setSubscriber(final BBASubscriber inSubscriber)
    {
        this.subscriber = inSubscriber;
    }

    /**
     * <p>
     * Getter for state.
     * </p>
     * 
     * @return the state
     */
    public BBAState getState()
    {
        return state;
    }

    /**
     * <p>
     * Setting value for state.
     * </p>
     * 
     * @param inState the state to set
     */
    public void setState(final BBAState inState)
    {
        this.state = inState;
    }

    /**
     * <p>
     * Getter for refNo.
     * </p>
     * 
     * @return the refNo
     */
    public String getRefNo()
    {
        return refNo;
    }

    /**
     * <p>
     * Setting value for refNo.
     * </p>
     * 
     * @param inRefNo the refNo to set
     */
    public void setRefNo(final String inRefNo)
    {
        this.refNo = inRefNo;
    }
}
