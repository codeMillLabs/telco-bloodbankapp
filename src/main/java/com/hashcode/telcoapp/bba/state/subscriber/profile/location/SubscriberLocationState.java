/*
 * FILENAME
 *     SubscriberLocationState.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.state.subscriber.profile.location;

import static com.hashcode.telcoapp.common.handlers.Messages.getMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hms.kite.samples.api.SdpException;

import com.hashcode.telcoapp.bba.data.UssdSession;
import com.hashcode.telcoapp.bba.enums.UssdState;
import com.hashcode.telcoapp.bba.state.BBAppState;
import com.hashcode.telcoapp.bba.util.BBAUtils;
import com.hashcode.telcoapp.common.connectors.ILBSConnector;
import com.hashcode.telcoapp.common.util.ServiceResolver;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Subscriber location state.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class SubscriberLocationState extends BBAppState
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(SubscriberLocationState.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public String messageReceived(final UssdSession ussdSession, final String message)
    {
        String responseMessage = "";
        if (message.equals("1"))
        {
            ussdSession.setCurrentState(UssdState.SUBSCRIBER_SET_LOCATION);
            String district = BBAUtils.DEFAULT_DISTRICT;
            String address = ussdSession.getSubscriber().getAddress();
            try
            {
                district = BBAUtils.getDistrict(getService(ILBSConnector.class).getDistrict(address));
            }
            catch (SdpException e)
            {
                LOGGER.error("Error while gettting district of [{}]", address);
            }
            ussdSession.setConnectCache(district);
            responseMessage = getMessage("ussd.profile.set.location.message", district);
        }
        else if (message.equals("2"))
        {
            responseMessage = createProfileMessage(ussdSession);
        }
        else
        {
            responseMessage = createInvalidMessage(ussdSession);
        }

        return responseMessage;
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
