/*
 * FILENAME
 *     BloodStockTempTable.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.bs;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood stock temporary table.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class BloodStockTempTable extends Table
{
    private static final long serialVersionUID = 6479624581768346354L;

    /**
     * <p>
     * Constructor of blood stock temp table.
     * </p>
     */
    public BloodStockTempTable()
    {
        addContainerProperty("Blood Type", String.class, null);
        addContainerProperty("Sub Type", String.class, null);
        addContainerProperty("Status", String.class, null);
        addContainerProperty("Date", String.class, null);
        addContainerProperty("Stock", String.class, null);
        addContainerProperty("Action", HorizontalLayout.class, null);
    }
}
