/*
 * FILENAME
 *     TestBBAUtils.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.test.hashcode.telcoapp.bba.util;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.Calendar;

import org.junit.Test;

import com.hashcode.telcoapp.bba.data.NICInfo;
import com.hashcode.telcoapp.bba.util.BBAUtils;
//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//
import com.hashcode.telcoapp.common.enums.Gender;

/**
 * <p>
 * Test class for {@link BBAUtils}.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class TestBBAUtils
{
    /**
     * <p>
     * Test method for {@link BBAUtils#getDistrict(String)}.
     * </p>
     */
    @Test
    public void testGetDistrict()
    {
        assertNotNull(BBAUtils.getDistrict("Ampara"));
        assertNotNull(BBAUtils.getDistrict("Ampara"));
        assertNotNull(BBAUtils.getDistrict("Anuradhapura"));
        assertNotNull(BBAUtils.getDistrict("Badulla"));
        assertNotNull(BBAUtils.getDistrict("Batticaloa"));
        assertNotNull(BBAUtils.getDistrict("Colombo"));
        assertNotNull(BBAUtils.getDistrict("Galle"));
        assertNotNull(BBAUtils.getDistrict("Gampaha"));
        assertNotNull(BBAUtils.getDistrict("Hambantota"));
        assertNotNull(BBAUtils.getDistrict("Jaffna"));
        assertNotNull(BBAUtils.getDistrict("Kalutara"));
        assertNotNull(BBAUtils.getDistrict("Kandy"));
        assertNotNull(BBAUtils.getDistrict("Kegalle"));
        assertNotNull(BBAUtils.getDistrict("Kilinochchi"));
        assertNotNull(BBAUtils.getDistrict("Kurunegala"));
        assertNotNull(BBAUtils.getDistrict("Mannar"));
        assertNotNull(BBAUtils.getDistrict("Matale"));
        assertNotNull(BBAUtils.getDistrict("Matara"));
        assertNotNull(BBAUtils.getDistrict("Moneragala"));
        assertNotNull(BBAUtils.getDistrict("Mullaitivu"));
        assertNotNull(BBAUtils.getDistrict("Nuwara Eliya"));
        assertNotNull(BBAUtils.getDistrict("Polonnaruwa"));
        assertNotNull(BBAUtils.getDistrict("Puttalam"));
        assertNotNull(BBAUtils.getDistrict("Ratnapura"));
        assertNotNull(BBAUtils.getDistrict("Trincomalee"));
        assertNotNull(BBAUtils.getDistrict("Vavuniya"));
        assertNotNull(BBAUtils.getDistrict("Invalid"));
    }

    /**
     * <p>
     * Test method for {@link BBAUtils#generateDob(String, com.hashcode.telcoapp.common.enums.Gender)} .
     * </p>
     */
    @Test
    public void testGenerateDob1()
    {
        NICInfo decodedNic = BBAUtils.decodeNICInfo("861882365V");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(decodedNic.getDob());
        assertThat(calendar.get(Calendar.YEAR), is(1986));
        assertThat(calendar.get(Calendar.MONTH), is(6));
        assertThat(calendar.get(Calendar.DATE), is(6));
        assertThat(decodedNic.getGender(), is(Gender.MALE));

        decodedNic = BBAUtils.decodeNICInfo("840612365V");
        calendar = Calendar.getInstance();
        calendar.setTime(decodedNic.getDob());
        assertThat(calendar.get(Calendar.YEAR), is(1984));
        assertThat(calendar.get(Calendar.MONTH), is(2));
        assertThat(calendar.get(Calendar.DATE), is(1));
        assertThat(decodedNic.getGender(), is(Gender.MALE));

        decodedNic = BBAUtils.decodeNICInfo("850622365V");
        calendar = Calendar.getInstance();
        calendar.setTime(decodedNic.getDob());
        assertThat(calendar.get(Calendar.YEAR), is(1985));
        assertThat(calendar.get(Calendar.MONTH), is(2));
        assertThat(calendar.get(Calendar.DATE), is(2));
        assertThat(decodedNic.getGender(), is(Gender.MALE));

        decodedNic = BBAUtils.decodeNICInfo("866552126V");
        calendar = Calendar.getInstance();
        calendar.setTime(decodedNic.getDob());
        assertThat(calendar.get(Calendar.YEAR), is(1986));
        assertThat(calendar.get(Calendar.MONTH), is(5));
        assertThat(calendar.get(Calendar.DATE), is(3));
        assertThat(decodedNic.getGender(), is(Gender.FEMALE));

        decodedNic = BBAUtils.decodeNICInfo("755512365V");
        calendar = Calendar.getInstance();
        calendar.setTime(decodedNic.getDob());
        assertThat(calendar.get(Calendar.YEAR), is(1975));
        assertThat(calendar.get(Calendar.MONTH), is(1));
        assertThat(calendar.get(Calendar.DATE), is(19));
        assertThat(decodedNic.getGender(), is(Gender.FEMALE));

        decodedNic = BBAUtils.decodeNICInfo("755602365V");
        calendar = Calendar.getInstance();
        calendar.setTime(decodedNic.getDob());
        assertThat(calendar.get(Calendar.YEAR), is(1975));
        assertThat(calendar.get(Calendar.MONTH), is(1));
        assertThat(calendar.get(Calendar.DATE), is(28));
        assertThat(decodedNic.getGender(), is(Gender.FEMALE));

        decodedNic = BBAUtils.decodeNICInfo("755612365V");
        calendar = Calendar.getInstance();
        calendar.setTime(decodedNic.getDob());
        assertThat(calendar.get(Calendar.YEAR), is(1975));
        assertThat(calendar.get(Calendar.MONTH), is(2));
        assertThat(calendar.get(Calendar.DATE), is(1));
        assertThat(decodedNic.getGender(), is(Gender.FEMALE));

        decodedNic = BBAUtils.decodeNICInfo("765512365V");
        calendar = Calendar.getInstance();
        calendar.setTime(decodedNic.getDob());
        assertThat(calendar.get(Calendar.YEAR), is(1976));
        assertThat(calendar.get(Calendar.MONTH), is(1));
        assertThat(calendar.get(Calendar.DATE), is(20));
        assertThat(decodedNic.getGender(), is(Gender.FEMALE));

        decodedNic = BBAUtils.decodeNICInfo("765602365V");
        calendar = Calendar.getInstance();
        calendar.setTime(decodedNic.getDob());
        assertThat(calendar.get(Calendar.YEAR), is(1976));
        assertThat(calendar.get(Calendar.MONTH), is(1));
        assertThat(calendar.get(Calendar.DATE), is(29));
        assertThat(decodedNic.getGender(), is(Gender.FEMALE));

        decodedNic = BBAUtils.decodeNICInfo("765612365V");
        calendar = Calendar.getInstance();
        calendar.setTime(decodedNic.getDob());
        assertThat(calendar.get(Calendar.YEAR), is(1976));
        assertThat(calendar.get(Calendar.MONTH), is(2));
        assertThat(calendar.get(Calendar.DATE), is(1));
        assertThat(decodedNic.getGender(), is(Gender.FEMALE));

        decodedNic = BBAUtils.decodeNICInfo("780612365V");
        calendar = Calendar.getInstance();
        calendar.setTime(decodedNic.getDob());
        assertThat(calendar.get(Calendar.YEAR), is(1978));
        assertThat(calendar.get(Calendar.MONTH), is(2));
        assertThat(calendar.get(Calendar.DATE), is(1));
        assertThat(decodedNic.getGender(), is(Gender.MALE));

    }

    /**
     * <p>
     * Test method for {@link BBAUtils#generateDob(String, com.hashcode.telcoapp.common.enums.Gender)} .
     * </p>
     */
    @Test
    public void testGenerateDob2()
    {
        assertNull(BBAUtils.decodeNICInfo("8655565"));
    }
    
    /**
     * <p>
     * Test method for {@link BBAUtils#validateDonationCode(String)} .
     * </p>
     */
    @Test
    public void testValidateDonationCode()
    {
        assertThat(BBAUtils.validateDonationCode("11BB22222"), is(true));
        assertThat(BBAUtils.validateDonationCode("111B22222"), is(false));
        assertThat(BBAUtils.validateDonationCode("11ABCD2222"), is(true));

    }
}
