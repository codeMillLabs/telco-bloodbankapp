/*
 * FILENAME
 *     DonationCenterDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.telcoapp.bba.dao.impl;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.telcoapp.bba.dao.DonationCenterDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.data.DonationCenter;
import com.hashcode.telcoapp.bba.enums.LocationType;
import com.hashcode.telcoapp.bba.service.CampaignCriteria;
import com.hashcode.telcoapp.common.dao.impl.GenericDaoImpl;

/**
 * <p>
 * DAO service blood donation campaign or centers.
 * </p>
 * 
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
@Transactional
@Repository
public class DonationCenterDaoImpl extends GenericDaoImpl<DonationCenter, Long> implements DonationCenterDao
{

    private static final Logger LOGGER = LoggerFactory.getLogger(DonationCenterDaoImpl.class);

    private static final String DONATION_CENTERS_BY_DISTRICT_SQL = "SELECT d FROM " + DonationCenter.class.getName()
        + " d " + " WHERE d.district = :district AND d.locationType = :locationType  AND "
        + "(d.donationDate IS NULL OR  d.donationDate >= :dateFrom)" + " ORDER BY d.donationDate DESC ";

    /**
     * {@inheritDoc}
     */
    public List<DonationCenter> fetchDonationCentersForDistrict(final String district, final LocationType locationType,
        final Date dateFrom, final int maxRowCount)
    {

        TypedQuery<DonationCenter> query =
            entityManager.createQuery(DONATION_CENTERS_BY_DISTRICT_SQL, DonationCenter.class);

        query.setParameter("district", district);
        query.setParameter("locationType", locationType);
        query.setParameter("dateFrom", dateFrom);
        if (maxRowCount > 0)
            query.setMaxResults(maxRowCount);

        List<DonationCenter> donationCenters = query.getResultList();

        LOGGER.info("Donation centers fetch for the district, [ District : {}, Count : {}]", district,
            donationCenters.size());

        return donationCenters;
    }

    /**
     * {@inheritDoc}
     * 
     * @see DonationCenterDao#findByCriteria(CampaignCriteria)
     */
    @SuppressWarnings("unchecked")
    public List<DonationCenter> findByCriteria(final CampaignCriteria criteria)
    {
        Session session = (Session) entityManager.getDelegate();
        Criteria crit = session.createCriteria(DonationCenter.class);
        crit = crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        boolean atLeastOneSearchCriteria = false;

        if (criteria.isSearchAll())
        {
            return crit.list();
        }

        if (criteria.getCenterName() != null)
        {
            String centerName = criteria.getCenterName().replace("*", "%");
            crit.add(Restrictions.ilike("centerName", centerName));
            atLeastOneSearchCriteria = true;
        }
        if (criteria.getCity() != null)
        {
            String city = criteria.getCity().replace("*", "%");
            crit.add(Restrictions.ilike("city", city));
            atLeastOneSearchCriteria = true;
        }
        if (criteria.getDistrict() != null)
        {
            crit.add(Restrictions.eq("district", criteria.getDistrict()));
            atLeastOneSearchCriteria = true;
        }
        if (criteria.getLocationType() != null)
        {
            crit.add(Restrictions.eq("locationType", criteria.getLocationType()));
            atLeastOneSearchCriteria = true;
        }
        if (criteria.getDonationDate() != null)
        {
            DateTime datetime =
                new DateTime(criteria.getDonationDate().getTime()).withHourOfDay(0).withMinuteOfHour(0)
                    .withSecondOfMinute(0);
            crit.add(Restrictions.ge("donationDate", datetime.toDate()));
            atLeastOneSearchCriteria = true;
        }

        if (!atLeastOneSearchCriteria)
        {
            return Collections.emptyList();
        }

        if (criteria.getStartPosition() >= 0)
        {
            crit.setFirstResult(criteria.getStartPosition());
            crit.setMaxResults(criteria.getEndPosition());
        }

        return crit.list();
    }

    private static final String FIND_DONATION_CENTER_BY_BLOOD_STOCK_ADMIN_SQL = " SELECT dc FROM "
        + DonationCenter.class.getName() + " dc WHERE dc.bloodStockAdmin = :bloodStockAdmin ";

    /**
     * {@inheritDoc}
     * 
     * @see DonationCenterDao#findByBloodStockAdmin(com.hashcode.telcoapp.bba.data.BBASubscriber)
     */
    public DonationCenter findByBloodStockAdmin(final BBASubscriber subscriber)
    {
        try
        {
            TypedQuery<DonationCenter> query =
                entityManager.createQuery(FIND_DONATION_CENTER_BY_BLOOD_STOCK_ADMIN_SQL, DonationCenter.class);
            query.setParameter("bloodStockAdmin", subscriber);
            query.setMaxResults(1);
            return query.getSingleResult();
        }
        catch (NoResultException e)
        {
            return null;
        }
    }

    private static final String FIND_DONATION_CENTER_BY_REFERENCE_ID_SQL = " SELECT dc FROM "
        + DonationCenter.class.getName() + " dc WHERE dc.referenceId = :referenceId ";

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.telcoapp.bba.dao.DonationCenterDao#findByReferenceId(java.lang.String)
     */
    @Override
    public DonationCenter findByReferenceId(final String referenceId)
    {
        try
        {
            TypedQuery<DonationCenter> query =
                entityManager.createQuery(FIND_DONATION_CENTER_BY_REFERENCE_ID_SQL, DonationCenter.class);
            query.setParameter("referenceId", referenceId);
            query.setMaxResults(1);
            return query.getSingleResult();
        }
        catch (NoResultException e)
        {
            return null;
        }
    }

    private static final String GET_BLOOD_BANKS_QUERY = "SELECT d FROM " + DonationCenter.class.getName() + " d "
        + " WHERE  d.referenceId IS NOT NULL ORDER BY d.centerName";

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.telcoapp.bba.dao.DonationCenterDao#getBloodBanks()
     */
    @Override
    public List<DonationCenter> getBloodBanks()
    {
        TypedQuery<DonationCenter> query =
            entityManager.createQuery(GET_BLOOD_BANKS_QUERY, DonationCenter.class);

        List<DonationCenter> donationCenters = query.getResultList();

        LOGGER.info("Blood banks [ Count : {}]", donationCenters.size());

        return donationCenters;
    }
}
