/*
 * FILENAME
 *     MainWindow.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui;

import static com.hashcode.telcoapp.bba.util.BBAUtils.getService;

import com.hashcode.telcoapp.bba.dao.AppSubscriberDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.enums.UserRole;
import com.hashcode.telcoapp.bba.ui.bs.BloodStockTab;
import com.hashcode.telcoapp.bba.ui.bulkmessage.BulkMessageTab;
import com.hashcode.telcoapp.bba.ui.campaings.CampaignTab;
import com.hashcode.telcoapp.bba.ui.help.HelpTab;
import com.hashcode.telcoapp.bba.ui.inbox.InboxTab;
import com.hashcode.telcoapp.bba.ui.subscriber.SubscriberTab;
import com.hashcode.telcoapp.common.ui.AppData;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.Sizeable;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.themes.Reindeer;

/**
 * <p>
 * Main Window Layout.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class MainWindow extends VerticalLayout
{

    private static final long serialVersionUID = -4044416506719217257L;
    private HomeLayout homeLayout;
    private BloodStockTab bloodStockTab;

    /**
     * <p>
     * Constructor of MainWindow.
     * </p>
     */
    public MainWindow()
    {
        HorizontalLayout header = new HorizontalLayout();
        header.setWidth(940, Sizeable.UNITS_PIXELS);
        header.setStyleName("header");

        header.setMargin(false, true, true, true);

        VerticalLayout logoPanel = new VerticalLayout();
        logoPanel.setStyleName("home-banner");
        logoPanel.setHeight("60px");
        logoPanel.setWidth("450px");

        VerticalLayout logoutPanel = new VerticalLayout();
        Label message = new Label("Welcome, " + AppData.getUserData());
        Button logoutBtn = new Button("Logout", new Button.ClickListener()
        {
            private static final long serialVersionUID = -1403191253264959059L;

            public void buttonClick(final Button.ClickEvent event)
            {
                getApplication().close();
            }
        });
        message.setSizeUndefined();
        message.setStyleName(Reindeer.LABEL_SMALL);
        logoutBtn.setStyleName(Reindeer.BUTTON_SMALL);

        logoutPanel.addComponent(message);
        logoutPanel.addComponent(logoutBtn);
        logoutPanel.setSizeUndefined();

        header.addComponent(logoPanel);
        header.addComponent(logoutPanel);
        header.setComponentAlignment(logoPanel, Alignment.MIDDLE_LEFT);
        header.setComponentAlignment(logoutPanel, Alignment.TOP_RIGHT);

        VerticalLayout body = new VerticalLayout();
        body.setWidth(940, Sizeable.UNITS_PIXELS);
        body.setMargin(false, true, true, true);
        body.setSpacing(true);

        final TabSheet tabsheet = new TabSheet();
        tabsheet.setWidth("100%");
        tabsheet.setImmediate(true);

        BBASubscriber subscriber = getService(AppSubscriberDao.class).findByUserName(AppData.getUserData());
        boolean homePerm = false;
        boolean subscriberPerm = false;
        boolean donationCenterPerm = false;
        boolean bulkMsgPerm = false;
        boolean bloodStockPerm = false;
        boolean inboxPerm = false;
        boolean helpPerm = false;

        for (UserRole userRole : subscriber.getUserRoles())
        {
            if (userRole.isHomePerm())
                homePerm = true;
            if (userRole.isSubscriberPerm())
                subscriberPerm = true;
            if (userRole.isDonationCenterPerm())
                donationCenterPerm = true;
            if (userRole.isBulkMsgPerm())
                bulkMsgPerm = true;
            if (userRole.isBloodStockPerm())
                bloodStockPerm = true;
            if (userRole.isInboxPerm())
                inboxPerm = true;
            if (userRole.isHelpPerm())
                helpPerm = true;
        }

        if (homePerm)
            tabsheet.addTab(getHomeLayout(), "Home", new ThemeResource("images/home.png"));
        if (subscriberPerm)
            tabsheet.addTab(new SubscriberTab(), "Subscriber", new ThemeResource("images/subscriber.png"));
        if (donationCenterPerm)
            tabsheet.addTab(new CampaignTab(), "Donation Centers", new ThemeResource("images/campaign.png"));
        if (bulkMsgPerm)
            tabsheet.addTab(new BulkMessageTab(), "Bulk Message", new ThemeResource("images/bulk-message.png"));
        if (bloodStockPerm)
            tabsheet.addTab(getBloodStockTab(), "Blood Stock", new ThemeResource("images/blood-stock.png"));
        if (inboxPerm)
            tabsheet.addTab(new InboxTab(), "Inbox", new ThemeResource("images/inbox.png"));
        if (helpPerm)
            tabsheet.addTab(new HelpTab(), "Help", new ThemeResource("images/help.png"));

        tabsheet.addListener(new TabSheet.SelectedTabChangeListener()
        {
            private static final long serialVersionUID = 908250441291979202L;

            public void selectedTabChange(final SelectedTabChangeEvent event)
            {
                if (event.getTabSheet().getSelectedTab().equals(getHomeLayout()))
                {
                    getHomeLayout().refresh();
                }
                else if (event.getTabSheet().getSelectedTab().equals(getBloodStockTab()))
                {
                    getBloodStockTab().refresh();
                }
            }
        });

        body.addComponent(tabsheet);
        VerticalLayout footer = createFooter();

        addComponent(header);
        addComponent(body);
        addComponent(footer);

        setComponentAlignment(header, Alignment.MIDDLE_CENTER);
        setComponentAlignment(body, Alignment.MIDDLE_CENTER);
        setComponentAlignment(footer, Alignment.MIDDLE_CENTER);
    }

    private VerticalLayout createFooter()
    {
        VerticalLayout footer = new VerticalLayout();
        footer.setWidth(940, Sizeable.UNITS_PIXELS);
        footer.setMargin(true);
        Link hashcodeNamLink = new Link("hashCode Solutions", new ExternalResource("http://www.hashcodesys.com/"));
        hashcodeNamLink.setTargetName("_blank");
        Label copyRights = new Label("&nbsp;&nbsp;version 1.0 \u00A9  2013. All rights reserved.", Label.CONTENT_XHTML);

        HorizontalLayout foorterLableLayout = new HorizontalLayout();

        foorterLableLayout.addComponent(hashcodeNamLink);
        foorterLableLayout.addComponent(copyRights);
        foorterLableLayout.setComponentAlignment(hashcodeNamLink, Alignment.MIDDLE_RIGHT);
        foorterLableLayout.setComponentAlignment(copyRights, Alignment.MIDDLE_RIGHT);
        foorterLableLayout.setSizeUndefined();

        footer.addComponent(foorterLableLayout);
        footer.setComponentAlignment(foorterLableLayout, Alignment.MIDDLE_CENTER);
        return footer;
    }

    private HomeLayout getHomeLayout()
    {
        if (null == homeLayout)
        {
            homeLayout = new HomeLayout();
        }

        return homeLayout;
    }

    private BloodStockTab getBloodStockTab()
    {
        if (null == bloodStockTab)
        {
            bloodStockTab = new BloodStockTab();
        }

        return bloodStockTab;
    }
}
