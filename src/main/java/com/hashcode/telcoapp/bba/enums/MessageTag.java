/*
 * FILENAME
 *     MessageTag.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.enums;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Enum class to hold the message tags.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public enum MessageTag
{
    NAME("<name>"),
    DISTRICT("<district>"),
    BLOOD_TYPE("<blood_type>"),
    NIC("<nic>");

    private String tag;

    private MessageTag(final String inTag)
    {
        this.tag = inTag;
    }

    /**
     * <p>
     * Getter for tag.
     * </p>
     * 
     * @return the tag
     */
    public String getTag()
    {
        return tag;
    }

    @Override
    public String toString()
    {
        return this.tag;
    }
}
