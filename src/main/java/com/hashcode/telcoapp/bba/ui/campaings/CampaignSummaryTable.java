/*
 * FILENAME
 *     CampaignSummaryTable.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.campaings;

import static com.hashcode.telcoapp.bba.data.DonationCenter.ADDRESS;
import static com.hashcode.telcoapp.bba.data.DonationCenter.CENTER_NAME;
import static com.hashcode.telcoapp.bba.data.DonationCenter.CITY;
import static com.hashcode.telcoapp.bba.data.DonationCenter.CONTACT;
import static com.hashcode.telcoapp.bba.data.DonationCenter.DISTRICT;
import static com.hashcode.telcoapp.bba.data.DonationCenter.DONATION_DATE;
import static com.hashcode.telcoapp.bba.data.DonationCenter.ID;
import static com.hashcode.telcoapp.bba.data.DonationCenter.LOC_TYPE;
import static com.hashcode.telcoapp.bba.data.DonationCenter.MAIN_OFFICER;
import static com.hashcode.telcoapp.bba.util.BBAUtils.formatDate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.hashcode.telcoapp.bba.dao.DonationCenterDao;
import com.hashcode.telcoapp.bba.data.DonationCenter;
import com.hashcode.telcoapp.bba.service.CampaignCriteria;
import com.hashcode.telcoapp.common.ui.pagination.EntityContainer;
import com.hashcode.telcoapp.common.ui.pagination.PageableEntityTable2;
import com.hashcode.telcoapp.common.util.ServiceResolver;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Campaign Summary Table.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class CampaignSummaryTable extends PageableEntityTable2<DonationCenter>
{
    private static final long serialVersionUID = 72358873349497101L;

    public static final Object[] NATURAL_COL_ORDER = new Object[] {
        DonationCenter.ID,
        DonationCenter.CENTER_NAME,
        DonationCenter.LOC_TYPE,
        DonationCenter.DONATION_DATE,
        DonationCenter.MAIN_OFFICER,
        DonationCenter.CONTACT,
        DonationCenter.ADDRESS,
        DonationCenter.CITY,
        DonationCenter.DISTRICT
    };

    public static final String[] COL_HEADERS = new String[] {
        "Id",
        "Center Name",
        "Location Type",
        "Date",
        "Officer In Charge",
        "Contact",
        "Address",
        "City",
        "District"
    };

    private CampaignCriteria searchCriteria;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public CampaignSummaryTable()
    {
        super(DonationCenter.class);
        setContainerDataSource(new CampaignSummaryTableContainer());
        CampaignSummaryTableColumnGenerator columnGenerator = new CampaignSummaryTableColumnGenerator();
        addGeneratedColumn(DonationCenter.ID, columnGenerator);
        addGeneratedColumn(DonationCenter.ADDRESS, columnGenerator);
        addGeneratedColumn(DonationCenter.CENTER_NAME, columnGenerator);
        addGeneratedColumn(DonationCenter.CITY, columnGenerator);
        addGeneratedColumn(DonationCenter.CONTACT, columnGenerator);
        addGeneratedColumn(DonationCenter.DISTRICT, columnGenerator);
        addGeneratedColumn(DonationCenter.DONATION_DATE, columnGenerator);
        addGeneratedColumn(DonationCenter.LOC_TYPE, columnGenerator);
        addGeneratedColumn(DonationCenter.MAIN_OFFICER, columnGenerator);
        setVisibleColumns(NATURAL_COL_ORDER);
        setColumnHeaders(COL_HEADERS);
        setSizeFull();
    }
    
    /**
     * <p>
     * Refresh table.
     * </p>
     * 
     * @param inSearchCriteria
     *            search criteria to set
     */
    public void refresh(final CampaignCriteria inSearchCriteria)
    {
        inSearchCriteria.resetResultProperties();
        searchCriteria = inSearchCriteria;
        refresh();
    }

    private boolean isSearchInfoNull()
    {
        return null == searchCriteria;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setPage(final int page)
    {
        if (!isSearchInfoNull())
        {
            searchCriteria.setCurrentPage(page);
            refresh();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPage()
    {
        return isSearchInfoNull() ? 0 : searchCriteria.getCurrentPage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPageSize()
    {
        return isSearchInfoNull() ? 10 : searchCriteria.getRecordsPerPage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPageSize(final int pageSize)
    {
        if (!isSearchInfoNull())
        {
            searchCriteria.setRecordsPerPage(pageSize);
            refresh();
        }
    }

    /**
     * <p>
     * CampaignSummaryTable column generator.
     * </p>
     *
     * @author manuja
     *
     * @version $Id$
     */
    private class CampaignSummaryTableColumnGenerator implements ColumnGenerator
    {
        private static final long serialVersionUID = -5775174113663832430L;

        /**
         * {@inheritDoc}
         */
        public Component generateCell(final Table source, final Object itemId, final Object columnId)
        {
            DonationCenter center = (DonationCenter) itemId;
            if (columnId.equals(ID))
            {
                return new Label(String.valueOf(center.getId()));
            }
            else if (columnId.equals(ADDRESS))
            {
                return new Label(center.getAddress());
            }
            else if (columnId.equals(CENTER_NAME))
            {
                return new Label(center.getCenterName());
            }
            else if (columnId.equals(CITY))
            {
                return new Label(center.getCity());
            }
            else if (columnId.equals(DISTRICT))
            {
                return new Label(center.getDistrict());
            }
            else if (columnId.equals(CONTACT))
            {
                return new Label(center.getContact());
            }
            else if (columnId.equals(DONATION_DATE))
            {
                return new Label(formatDate(center.getDonationDate()));
            }
            else if (columnId.equals(LOC_TYPE))
            {
                return new Label(center.getLocationType().toString());
            }
            else if (columnId.equals(MAIN_OFFICER))
            {
                return new Label(center.getMainOffier());
            }
            return null;
        }
    }

    /**
     * <p>
     * CampaignSummaryTable Container.
     * </p>
     * 
     * @author manuja.
     * 
     * @version $Id$
     */
    private class CampaignSummaryTableContainer extends EntityContainer<DonationCenter>
    {
        private static final long serialVersionUID = -3543045058436601701L;

        public CampaignSummaryTableContainer()
        {
            super(DonationCenter.class);
        }

        @Override
        public List<DonationCenter> getData()
        {
            List<DonationCenter> results = null;

            if (searchCriteria == null)
            {
                return Collections.emptyList();
            }

            if (searchCriteria.getNumOfRecords() < 0)
            {
                results = getService(DonationCenterDao.class).findByCriteria(searchCriteria);
                searchCriteria.setNumOfRecords(results.size());
            }

            searchCriteria.setStartPosition(getPage() * getPageSize());
            searchCriteria.setEndPosition(getPageSize());

            results = getService(DonationCenterDao.class).findByCriteria(searchCriteria);

            return new ArrayList<DonationCenter>(results);
        }

        @Override
        public int getTotalRows()
        {
            return ((searchCriteria == null) ? 0 : searchCriteria.getNumOfRecords());
        }

        private <T> T getService(final Class<T> serviceType)
        {
            return ServiceResolver.findService(serviceType);
        }
    }
}
