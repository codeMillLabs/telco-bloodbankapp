/*
 * FILENAME
 *     DonationCenter.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.hashcode.telcoapp.bba.enums.LocationType;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Entity for blood donation campaign or centers.
 * </p>
 * 
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
@Entity
@Table(name = "DONATION_CENTER")
public class DonationCenter implements Serializable
{
    private static final long serialVersionUID = 816574107905387878L;
    public static final String ID = "id";
    public static final String CENTER_NAME = "centerName";
    public static final String LOCATION_REF = "locationRef";
    public static final String ADDRESS = "address";
    public static final String CITY = "city";
    public static final String DISTRICT = "district";
    public static final String MAIN_OFFICER = "mainOffier";
    public static final String DONATION_DATE = "donationDate";
    public static final String LOC_TYPE = "locationType";
    public static final String CONTACT = "contact";
    public static final String BLOOD_STOCK_ADMIN = "bloodStockAdmin";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "DONATION_CENTER_SEQ")
    @SequenceGenerator(name = "DONATION_CENTER_SEQ", sequenceName = "DONATION_CENTER_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "CENTER_NAME")
    private String centerName;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "CITY")
    private String city;

    @Column(name = "DISTRICT")
    private String district;

    @Column(name = "MAIN_OFFIER")
    private String mainOffier;

    @Column(name = "DONATION_DATE")
    @Temporal(TemporalType.DATE)
    private Date donationDate;

    @Enumerated(value = EnumType.ORDINAL)
    @Column(name = "LOCATION_TYPE")
    private LocationType locationType;

    @Column(name = "CONTACT")
    private String contact;
    
    @ManyToOne
    @JoinColumn(name = "ADMIN")
    private BBASubscriber bloodStockAdmin;
    
    //@Column(unique = true, )
    private String referenceId;

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * <p>
     * Getter for centerName.
     * </p>
     * 
     * @return the centerName
     */
    public String getCenterName()
    {
        return centerName;
    }

    /**
     * <p>
     * Setting value for centerName.
     * </p>
     * 
     * @param inCenterName
     *            the centerName to set
     */
    public void setCenterName(final String inCenterName)
    {
        this.centerName = inCenterName;
    }

    /**
     * <p>
     * Getter for address.
     * </p>
     * 
     * @return the address
     */
    public String getAddress()
    {
        return address;
    }

    /**
     * <p>
     * Setting value for address.
     * </p>
     * 
     * @param inAddress
     *            the address to set
     */
    public void setAddress(final String inAddress)
    {
        this.address = inAddress;
    }

    /**
     * <p>
     * Getter for city.
     * </p>
     * 
     * @return the city
     */
    public String getCity()
    {
        return city;
    }

    /**
     * <p>
     * Setting value for city.
     * </p>
     * 
     * @param inCity
     *            the city to set
     */
    public void setCity(final String inCity)
    {
        this.city = inCity;
    }

    /**
     * <p>
     * Getter for district.
     * </p>
     * 
     * @return the district
     */
    public String getDistrict()
    {
        return district;
    }

    /**
     * <p>
     * Setting value for district.
     * </p>
     * 
     * @param inDistrict
     *            the district to set
     */
    public void setDistrict(final String inDistrict)
    {
        this.district = inDistrict;
    }

    /**
     * <p>
     * Getter for mainOffier.
     * </p>
     * 
     * @return the mainOffier
     */
    public String getMainOffier()
    {
        return mainOffier;
    }

    /**
     * <p>
     * Setting value for mainOffier.
     * </p>
     * 
     * @param inMainOffier
     *            the mainOffier to set
     */
    public void setMainOffier(final String inMainOffier)
    {
        this.mainOffier = inMainOffier;
    }

    /**
     * <p>
     * Getter for donationDate.
     * </p>
     * 
     * @return the donationDate
     */
    public Date getDonationDate()
    {
        return donationDate;
    }

    /**
     * <p>
     * Setting value for donationDate.
     * </p>
     * 
     * @param inDonationDate
     *            the donationDate to set
     */
    public void setDonationDate(final Date inDonationDate)
    {
        this.donationDate = inDonationDate;
    }

    /**
     * <p>
     * Getter for locationType.
     * </p>
     * 
     * @return the locationType
     */
    public LocationType getLocationType()
    {
        return locationType;
    }

    /**
     * <p>
     * Setting value for locationType.
     * </p>
     * 
     * @param inLocationType
     *            the locationType to set
     */
    public void setLocationType(final LocationType inLocationType)
    {
        this.locationType = inLocationType;
    }

    /**
     * <p>
     * Getter for contact.
     * </p>
     * 
     * @return the contact
     */
    public String getContact()
    {
        return contact;
    }

    /**
     * <p>
     * Setting value for contact.
     * </p>
     * 
     * @param inContact
     *            the contact to set
     */
    public void setContact(final String inContact)
    {
        this.contact = inContact;
    }

    /**
     * <p>
     * Getter for bloodStockAdmin.
     * </p>
     * 
     * @return the bloodStockAdmin
     */
    public BBASubscriber getBloodStockAdmin()
    {
        return bloodStockAdmin;
    }

    /**
     * <p>
     * Setting value for bloodStockAdmin.
     * </p>
     * 
     * @param inBloodStockAdmin the bloodStockAdmin to set
     */
    public void setBloodStockAdmin(final BBASubscriber inBloodStockAdmin)
    {
        this.bloodStockAdmin = inBloodStockAdmin;
    }
    
    /**
     * <p>
     * Getter for referenceId.
     * </p>
     * 
     * @return the referenceId
     */
    public String getReferenceId()
    {
        return referenceId;
    }

    /**
     * <p>
     * Setting value for referenceId.
     * </p>
     * 
     * @param inReferenceId the referenceId to set
     */
    public void setReferenceId(final String inReferenceId)
    {
        this.referenceId = inReferenceId;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        return result;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DonationCenter other = (DonationCenter) obj;
        if (getId() == null)
        {
            if (other.getId() != null)
                return false;
        }
        else if (!getId().equals(other.getId()))
            return false;
        return true;
    }
}
