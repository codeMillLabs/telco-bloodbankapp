/*
 * FILENAME
 *     BloodStockAmountState.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.state.bs;

import static com.hashcode.telcoapp.common.handlers.Messages.getMessage;

import org.springframework.transaction.annotation.Transactional;

import com.hashcode.telcoapp.bba.dao.BloodStockHistoryDao;
import com.hashcode.telcoapp.bba.dao.DonationCenterDao;
import com.hashcode.telcoapp.bba.data.DonationCenter;
import com.hashcode.telcoapp.bba.data.UssdSession;
import com.hashcode.telcoapp.bba.enums.BloodType;
import com.hashcode.telcoapp.bba.enums.SubBloodType;
import com.hashcode.telcoapp.bba.enums.UssdState;
import com.hashcode.telcoapp.bba.state.BBAppState;
import com.hashcode.telcoapp.common.util.ServiceResolver;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood stock amount.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class BloodStockAmountRCState extends BBAppState
{
    /**
     * {@inheritDoc}
     * 
     * @see BBAppState#messageReceived(UssdSession, java.lang.String)
     */
    @Transactional
    @Override
    public String messageReceived(final UssdSession ussdSession, final String message)
    {
        String responseMessage = "";
        try
        {
            String bloodGroup = ussdSession.getConnectCache();
            BloodType bloodType = BloodType.valueOf(bloodGroup);

            DonationCenter donationCenter =
                getService(DonationCenterDao.class).findByBloodStockAdmin(ussdSession.getSubscriber());
            if (null != donationCenter)
            {
                int amount = Integer.parseInt(message);

                ussdSession.setCurrentState(UssdState.BLOOD_STOCK_SET_AMOUNT_PC);

                getService(BloodStockHistoryDao.class).create(
                    createBloodStockHistory(bloodType, amount, donationCenter, SubBloodType.RCC));

                responseMessage =
                    getMessage("ussd.bs.set.pc.amount.message", amount, bloodType.toString(),
                         donationCenter.getCenterName(), bloodType);
            }
            else
            {
                ussdSession.setCurrentState(UssdState.BLOOD_STOCK_INVALID);
                responseMessage = getMessage("ussd.bs.stock.not.attached.to.a.center");
            }
        }
        catch (Exception ex)
        {
            responseMessage = createBloodStockInvalidMessage(ussdSession);
        }
        return responseMessage;
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
