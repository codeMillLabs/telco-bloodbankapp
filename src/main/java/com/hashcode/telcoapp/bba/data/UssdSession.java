/*
 * FILENAME
 *     UssdSession.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.hashcode.telcoapp.bba.enums.UssdState;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Ussd Session details.
 * </p>
 * 
 * @author manuja
 * 
 * @version $Id$
 */
@Entity
@Table(name = "USSD_SESSION")
public class UssdSession implements Serializable
{
    private static final long serialVersionUID = 8000358561003175041L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "USSD_SESS_SEQ")
    @SequenceGenerator(name = "USSD_SESS_SEQ", sequenceName = "USSD_SESS_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "SUBS_ID")
    private BBASubscriber subscriber;

    @Enumerated(EnumType.ORDINAL)
    private UssdState currentState;

    @Column(name = "SESSION_ID")
    private String sessionId;

    @Column(name = "IS_ACTIVE")
    private boolean active;

    @Column(name = "CONNECT_CACHE")
    private String connectCache;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public UssdSession()
    {
        this.active = true;
    }

    /**
     * <p>
     * Getter for subscriber.
     * </p>
     * 
     * @return the subscriber
     */
    public BBASubscriber getSubscriber()
    {
        return subscriber;
    }

    /**
     * <p>
     * Setting value for subscriber.
     * </p>
     * 
     * @param inSubscriber
     *            the subscriber to set
     */
    public void setSubscriber(final BBASubscriber inSubscriber)
    {
        this.subscriber = inSubscriber;
    }

    /**
     * <p>
     * Getter for sessionId.
     * </p>
     * 
     * @return the sessionId
     */
    public String getSessionId()
    {
        return sessionId;
    }

    /**
     * <p>
     * Setting value for sessionId.
     * </p>
     * 
     * @param inSessionId
     *            the sessionId to set
     */
    public void setSessionId(final String inSessionId)
    {
        this.sessionId = inSessionId;
    }

    /**
     * <p>
     * Getter for active.
     * </p>
     * 
     * @return the active
     */
    public boolean isActive()
    {
        return active;
    }

    /**
     * <p>
     * Setting value for active.
     * </p>
     * 
     * @param inActive
     *            the active to set
     */
    public void setActive(final boolean inActive)
    {
        this.active = inActive;
    }

    /**
     * <p>
     * Getter for currentState.
     * </p>
     * 
     * @return the currentState
     */
    public UssdState getCurrentState()
    {
        return currentState;
    }

    /**
     * <p>
     * Setting value for currentState.
     * </p>
     * 
     * @param inCurrentState
     *            the currentState to set
     */
    public void setCurrentState(final UssdState inCurrentState)
    {
        this.currentState = inCurrentState;
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * <p>
     * Getter for connectCache.
     * </p>
     * 
     * @return the connectCache
     */
    public String getConnectCache()
    {
        return connectCache;
    }

    /**
     * <p>
     * Setting value for connectCache.
     * </p>
     * 
     * @param inConnectCache
     *            the connectCache to set
     */
    public void setConnectCache(final String inConnectCache)
    {
        this.connectCache = inConnectCache;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("UssdSession [id=");
        builder.append(id);
        builder.append(", subscriber=");
        builder.append(subscriber);
        builder.append(", currentState=");
        builder.append(currentState);
        builder.append(", sessionId=");
        builder.append(sessionId);
        builder.append(", active=");
        builder.append(active);
        builder.append(", connectCache=");
        builder.append(connectCache);
        builder.append("]");
        return builder.toString();
    }
}
