/*
 * FILENAME
 *     SubscriberSummaryTable.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.subscriber;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.hashcode.telcoapp.bba.dao.AppSubscriberDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.service.BulkMsgCriteria;
import com.hashcode.telcoapp.bba.util.BBAUtils;
import com.hashcode.telcoapp.common.ui.pagination.EntityContainer;
import com.hashcode.telcoapp.common.ui.pagination.PageableEntityTable2;
import com.hashcode.telcoapp.common.util.ServiceResolver;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Subscriber summary table.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class SubscriberSummaryTable extends PageableEntityTable2<BBASubscriber>
{
    private static final long serialVersionUID = -5359812219529603059L;

    public static final Object[] NATURAL_COL_ORDER = new Object[] {
        BBASubscriber.ID,
        BBASubscriber.NIC,
        BBASubscriber.FIRST_NAME,
        BBASubscriber.ADDRESS,
        BBASubscriber.GENDER,
        BBASubscriber.BLOOD_TYPE,
        BBASubscriber.DISTRICT,
        BBASubscriber.POINTS,
        BBASubscriber.SUBSCRIBER_STATE,
        BBASubscriber.REGISTERED_DATE,
        BBASubscriber.LAST_DONATION_DATE,
        BBASubscriber.NEXT_ELIGIBLE_DATE,
        BBASubscriber.BLOOD_STOCK_ACCOUNT,
        BBASubscriber.AP_DONOR,
        BBASubscriber.REMARKS
    };

    public static final String[] COL_HEADERS = new String[] {
        "ID",
        "NIC",
        "Name",
        "Address",
        "Gender",
        "Blood Group",
        "District",
        "Points",
        "Subscriber State",
        "Registered Date",
        "Last Donation Date",
        "Next Eligible Date",
        "Blood Stock Account",
        "AP Donor",
        "Remarks"
    };

    private BulkMsgCriteria searchCriteria;

    /**
     * <p>
     * Subscriber summary table constructor.
     * </p>
     */
    public SubscriberSummaryTable()
    {
        super(BBASubscriber.class);
        setContainerDataSource(new SubscriberSummaryTableContainer());
        SubscriberSummaryTableColumnGenerator columnGenerator = new SubscriberSummaryTableColumnGenerator();
        addGeneratedColumn(BBASubscriber.ID, columnGenerator);
        addGeneratedColumn(BBASubscriber.NIC, columnGenerator);
        addGeneratedColumn(BBASubscriber.FIRST_NAME, columnGenerator);
        addGeneratedColumn(BBASubscriber.ADDRESS, columnGenerator);
        addGeneratedColumn(BBASubscriber.GENDER, columnGenerator);
        addGeneratedColumn(BBASubscriber.BLOOD_TYPE, columnGenerator);
        addGeneratedColumn(BBASubscriber.DISTRICT, columnGenerator);
        addGeneratedColumn(BBASubscriber.POINTS, columnGenerator);
        addGeneratedColumn(BBASubscriber.SUBSCRIBER_STATE, columnGenerator);
        addGeneratedColumn(BBASubscriber.REGISTERED_DATE, columnGenerator);
        addGeneratedColumn(BBASubscriber.LAST_DONATION_DATE, columnGenerator);
        addGeneratedColumn(BBASubscriber.NEXT_ELIGIBLE_DATE, columnGenerator);
        addGeneratedColumn(BBASubscriber.BLOOD_STOCK_ACCOUNT, columnGenerator);
        addGeneratedColumn(BBASubscriber.AP_DONOR, columnGenerator);
        addGeneratedColumn(BBASubscriber.REMARKS, columnGenerator);
        setVisibleColumns(NATURAL_COL_ORDER);
        setColumnHeaders(COL_HEADERS);
        setSizeFull();
    }

    /**
     * <p>
     * Refresh table.
     * </p>
     * 
     * @param inSearchCriteria
     *            search criteria to set
     */
    public void refresh(final BulkMsgCriteria inSearchCriteria)
    {
        inSearchCriteria.resetResultProperties();
        searchCriteria = inSearchCriteria;
        refresh();
    }

    private boolean isSearchInfoNull()
    {
        return null == searchCriteria;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setPage(final int page)
    {
        if (!isSearchInfoNull())
        {
            searchCriteria.setCurrentPage(page);
            refresh();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPage()
    {
        return isSearchInfoNull() ? 0 : searchCriteria.getCurrentPage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPageSize()
    {
        return isSearchInfoNull() ? 10 : searchCriteria.getRecordsPerPage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPageSize(final int pageSize)
    {
        if (!isSearchInfoNull())
        {
            searchCriteria.setRecordsPerPage(pageSize);
            refresh();
        }
    }

    /**
     * 
     <p>
     * SubscriberSummaryTable Table column generator.
     * </p>
     * 
     * @author manuja
     * 
     * @version $Id$
     */
    public class SubscriberSummaryTableColumnGenerator implements ColumnGenerator
    {
        private static final long serialVersionUID = -7815976487089180682L;

        /**
         * {@inheritDoc}
         */
        public Component generateCell(final Table source, final Object itemId, final Object columnId)
        {
            BBASubscriber subscriber = (BBASubscriber) itemId;
            if (columnId.equals(BBASubscriber.ID))
            {
                return new Label(String.valueOf(subscriber.getId()));
            }
            else if (columnId.equals(BBASubscriber.NIC))
            {
                return new Label(null != subscriber.getNic() ? subscriber.getNic() : "");
            }
            else if (columnId.equals(BBASubscriber.FIRST_NAME))
            {
                return new Label(null != subscriber.getFirstName() ? subscriber.getFirstName() : "");
            }
            else if (columnId.equals(BBASubscriber.ADDRESS))
            {
                return new Label(subscriber.getAddress());
            }
            else if (columnId.equals(BBASubscriber.GENDER))
            {
                return new Label(getToStringValue(subscriber.getGender()));
            }
            else if (columnId.equals(BBASubscriber.BLOOD_TYPE))
            {
                return new Label(getToStringValue(subscriber.getBloodType()));
            }
            else if (columnId.equals(BBASubscriber.DISTRICT))
            {
                return new Label(subscriber.getDistrict());
            }
            else if (columnId.equals(BBASubscriber.POINTS))
            {
                return new Label(String.valueOf(subscriber.getPoints()));
            }
            else if (columnId.equals(BBASubscriber.SUBSCRIBER_STATE))
            {
                return new Label(getToStringValue(subscriber.getSubscriberState()));
            }
            else if (columnId.equals(BBASubscriber.REGISTERED_DATE))
            {
                return new Label(BBAUtils.formatDate(subscriber.getRegisteredDate()));
            }
            else if (columnId.equals(BBASubscriber.LAST_MODIFIED_DATE))
            {
                return new Label(BBAUtils.formatDate(subscriber.getLastModifiedDate()));
            }
            else if (columnId.equals(BBASubscriber.LAST_DONATION_DATE))
            {
                return new Label(BBAUtils.formatDate(subscriber.getLastDonatedDate()));
            }
            else if (columnId.equals(BBASubscriber.NEXT_ELIGIBLE_DATE))
            {
                return new Label(BBAUtils.formatDate(subscriber.getNextEligibleDate()));
            }
            else if (columnId.equals(BBASubscriber.BLOOD_STOCK_ACCOUNT))
            {
                CheckBox checkBox = new CheckBox();
                checkBox.setValue(subscriber.isBloodStockAccount());
                checkBox.setReadOnly(true);
                return checkBox;
            }
            else if (columnId.equals(BBASubscriber.AP_DONOR))
            {
                CheckBox checkBox = new CheckBox();
                checkBox.setValue(subscriber.isApDonor());
                checkBox.setReadOnly(true);
                return checkBox;
            }
            else if (columnId.equals(BBASubscriber.REMARKS))
            {
                return new Label(getToStringValue(subscriber.getRemarks()));
            }

            return null;
        }
    }

    /**
     * <p>
     * SubscriberSummaryTable Container.
     * </p>
     * 
     * @author manuja.
     * 
     * @version $Id$
     */
    public class SubscriberSummaryTableContainer extends EntityContainer<BBASubscriber>
    {
        private static final long serialVersionUID = 2688207438796015695L;

        /**
         * <p>
         * Default constructor.
         * </p>
         */
        public SubscriberSummaryTableContainer()
        {
            super(BBASubscriber.class);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<BBASubscriber> getData()
        {
            List<BBASubscriber> results = null;

            if (searchCriteria == null)
            {
                return Collections.emptyList();
            }
            if (searchCriteria.getNumOfRecords() < 0)
            {
                Integer count = getService(AppSubscriberDao.class).countSubscribers(searchCriteria);
                searchCriteria.setNumOfRecords(count);
            }

            searchCriteria.setStartPosition(getPage() * getPageSize());
            searchCriteria.setEndPosition(getPageSize());

            results = getService(AppSubscriberDao.class).findSubscribers(searchCriteria);

            return new ArrayList<BBASubscriber>(results);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int getTotalRows()
        {
            return ((searchCriteria == null) ? 0 : searchCriteria.getNumOfRecords());
        }

    }

    private String getToStringValue(final Object value)
    {
        return null != value ? value.toString() : "";
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
