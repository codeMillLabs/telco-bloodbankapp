/*
 * FILENAME
 *     BloodStockHistoryTable.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.bs;

import static com.hashcode.telcoapp.bba.data.BloodStockHistory.BLOOD_TYPE;
import static com.hashcode.telcoapp.bba.data.BloodStockHistory.CENTER;
import static com.hashcode.telcoapp.bba.data.BloodStockHistory.RECORD_DATE;
import static com.hashcode.telcoapp.bba.data.BloodStockHistory.STOCK_COUNT;
import static com.hashcode.telcoapp.bba.data.BloodStockHistory.SUB_BLOOD_TYPE;
import static com.hashcode.telcoapp.bba.data.BloodStockHistory.BLOOD_STATUS;
import static com.hashcode.telcoapp.bba.util.BBAUtils.formatDate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.hashcode.telcoapp.bba.dao.BloodStockHistoryDao;
import com.hashcode.telcoapp.bba.dao.BloodStockHistorySearchCriteria;
import com.hashcode.telcoapp.bba.data.BloodStockHistory;
import com.hashcode.telcoapp.common.ui.pagination.EntityContainer;
import com.hashcode.telcoapp.common.ui.pagination.PageableEntityTable2;
import com.hashcode.telcoapp.common.util.ServiceResolver;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood stock history table.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class BloodStockHistoryTable extends PageableEntityTable2<BloodStockHistory>
{
    private static final long serialVersionUID = -6559112053116902532L;

    public static final Object[] NATURAL_COL_ORDER = new Object[] {
        CENTER, BLOOD_TYPE, SUB_BLOOD_TYPE, BLOOD_STATUS, RECORD_DATE, STOCK_COUNT
    };

    public static final String[] COL_HEADERS = new String[] {
        "Center", "Blood Type", "Sub Type", "Status", "Date", "Stock"
    };

    private BloodStockHistorySearchCriteria searchCriteria;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public BloodStockHistoryTable()
    {
        super(BloodStockHistory.class);
        setContainerDataSource(new BloodStockHistoryTableContainer());
        BloodStockHistoryTableColumnGenerator columnGenerator = new BloodStockHistoryTableColumnGenerator();
        addGeneratedColumn(CENTER, columnGenerator);
        addGeneratedColumn(BLOOD_TYPE, columnGenerator);
        addGeneratedColumn(SUB_BLOOD_TYPE, columnGenerator);
        addGeneratedColumn(BLOOD_STATUS, columnGenerator);
        addGeneratedColumn(RECORD_DATE, columnGenerator);
        addGeneratedColumn(STOCK_COUNT, columnGenerator);

        setVisibleColumns(NATURAL_COL_ORDER);
        setColumnHeaders(COL_HEADERS);
        setSizeFull();
    }

    /**
     * <p>
     * BloodStockHistory Table ColumnGenerator.
     * </p>
     * 
     * @author Manuja
     * 
     * @version $Id$
     */
    private class BloodStockHistoryTableColumnGenerator implements ColumnGenerator
    {
        private static final long serialVersionUID = -6348837229153900158L;

        /**
         * {@inheritDoc}
         */
        public Component generateCell(final Table source, final Object itemId, final Object columnId)
        {
            BloodStockHistory bloodStockHistory = (BloodStockHistory) itemId;
            if (columnId.equals(CENTER))
            {
                return new Label(String.valueOf(bloodStockHistory.getCenter().getCenterName()));
            }
            else if (columnId.equals(BLOOD_TYPE))
            {
                String bloodType =
                    (bloodStockHistory.getBloodType() != null) ? bloodStockHistory.getBloodType().toString() : "N/A";
                return new Label(bloodType);
            }
            else if (columnId.equals(SUB_BLOOD_TYPE))
            {
                String bloodType =
                    (bloodStockHistory.getSubBloodType() != null) ? bloodStockHistory.getSubBloodType().toString()
                        : "N/A";
                return new Label(bloodType);
            }
            else if (columnId.equals(BLOOD_STATUS))
            {
                String bloodType =
                    (bloodStockHistory.getBloodStatus() != null) ? bloodStockHistory.getBloodStatus().toString()
                        : "N/A";
                return new Label(bloodType);
            }
            else if (columnId.equals(RECORD_DATE))
            {
                String recordDate =
                    (bloodStockHistory.getRecordDate() != null) ? formatDate(bloodStockHistory.getRecordDate()) : "N/A";
                return new Label(recordDate);
            }
            else if (columnId.equals(STOCK_COUNT))
            {
                return new Label(String.valueOf(bloodStockHistory.getStockCount()));
            }

            return null;
        }
    }

    /**
     * <p>
     * BloodStockHistory Table Container.
     * </p>
     * 
     * @author Manuja
     * 
     * @version $Id$
     */
    private class BloodStockHistoryTableContainer extends EntityContainer<BloodStockHistory>
    {
        private static final long serialVersionUID = 8809314073979178339L;

        public BloodStockHistoryTableContainer()
        {
            super(BloodStockHistory.class);
        }

        @Override
        public List<BloodStockHistory> getData()
        {
            List<BloodStockHistory> results = null;

            if (searchCriteria == null)
            {
                return Collections.emptyList();
            }

            if (searchCriteria.getNumOfRecords() < 0)
            {
                Integer count = getService(BloodStockHistoryDao.class).countByCriteria(searchCriteria);
                searchCriteria.setNumOfRecords(count);
            }

            searchCriteria.setStartPosition(getPage() * getPageSize());
            searchCriteria.setEndPosition(getPageSize());

            results = getService(BloodStockHistoryDao.class).findBySearchCriteria(searchCriteria);

            return new ArrayList<BloodStockHistory>(results);
        }

        @Override
        public int getTotalRows()
        {
            return ((searchCriteria == null) ? 0 : searchCriteria.getNumOfRecords());
        }

        private <T> T getService(final Class<T> serviceType)
        {
            return ServiceResolver.findService(serviceType);
        }
    }

    private boolean isSearchInfoNull()
    {
        return null == searchCriteria;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPage(final int page)
    {
        if (!isSearchInfoNull())
        {
            searchCriteria.setCurrentPage(page);
            refresh();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPage()
    {
        return isSearchInfoNull() ? 0 : searchCriteria.getCurrentPage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPageSize()
    {
        return isSearchInfoNull() ? 10 : searchCriteria.getRecordsPerPage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPageSize(final int pageSize)
    {
        if (!isSearchInfoNull())
        {
            searchCriteria.setRecordsPerPage(pageSize);
            refresh();
        }
    }

    /**
     * <p>
     * Refresh table.
     * </p>
     * 
     * @param inSearchCriteria
     *            search criteria to set
     */
    public void refresh(final BloodStockHistorySearchCriteria inSearchCriteria)
    {
        inSearchCriteria.resetResultProperties();
        searchCriteria = inSearchCriteria;
        refresh();
    }
}
