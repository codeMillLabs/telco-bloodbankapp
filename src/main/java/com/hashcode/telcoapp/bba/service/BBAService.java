/*
 * FILENAME
 *     BBAService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.service;

import java.util.List;
import java.util.Map;

import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.common.model.Location;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * BBA Service to handle all the features of the system.
 * </p>
 * 
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public interface BBAService
{
    /**
     * <p>
     * Save subscriber.
     * </p>
     * 
     * @param subscriber
     *            subscriber instance
     * 
     */
    void saveSubscriber(BBASubscriber subscriber);

    /**
     * <p>
     * Update subscriber.
     * </p>
     * 
     * @param subscriber
     *            subscriber instance
     */
    void updateSubscriber(BBASubscriber subscriber);

    /**
     * <p>
     * Fetch Subscriber by address.
     * </p>
     * 
     * @param address
     *            adress of the subscriber
     * @return return subscriber instance
     */
    BBASubscriber fetchBBASubscriber(String address);

    /**
     * <p>
     * Get current location.
     * </p>
     * 
     * @return get current location
     */
    Location getCurrentLocation();

    /**
     * <p>
     * Get districts list.
     * </p>
     * 
     * @return list of districts
     */
    List<String> getDistricts();

    /**
     * <p>
     * Get districts Map.
     * </p>
     * 
     * @return list of districts
     */
    Map<Integer, String> getDistrictMap();

    /**
     * 
     * <p>
     * Add points to subscriber.
     * </p>
     * 
     * @param subscriber
     *            subscriber instance
     * @param points
     *            points to be add
     */
    void addPoints(BBASubscriber subscriber, int points);

}
