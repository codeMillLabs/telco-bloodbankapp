/*
 * FILENAME
 *     CampaingsTab.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.campaings;

import com.hashcode.telcoapp.bba.dao.DonationCenterDao;
import com.hashcode.telcoapp.bba.data.DonationCenter;
import com.hashcode.telcoapp.common.util.ServiceResolver;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Campaign Tab.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class CampaignTab extends VerticalLayout
{
    private static final long serialVersionUID = -8776009849731409444L;

    private CampaignSummaryLayout summaryLayout;
    private CampaignDetailsLayout detailLayout;
    private final TabSheet tabSheet;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public CampaignTab()
    {
        setSizeFull();
        tabSheet = new TabSheet();
        tabSheet.setWidth("100%");
        tabSheet.setImmediate(true);
        tabSheet.addTab(getSummaryLayout(), "Campaigns", new ThemeResource("images/campaign-summary.png"));
        tabSheet.addTab(getDetailsLayout(), "Campaigns Details", new ThemeResource("images/campaign-details.png"));

        addComponent(tabSheet);
    }

    private CampaignSummaryLayout getSummaryLayout()
    {
        if (summaryLayout == null)
        {
            summaryLayout = new CampaignSummaryLayout(new CampaignListener()
            {
                public void changed(final DonationCenter donationCenter)
                {
                    if (null != donationCenter)
                    {
                        DonationCenter donationCenterUpdated =
                            getService(DonationCenterDao.class).findById(donationCenter.getId());
                        getDetailsLayout().setModel(donationCenterUpdated);
                        tabSheet.setSelectedTab(getDetailsLayout());
                    }
                    else
                    {
                        getDetailsLayout().setModel(new DonationCenter());
                    }
                }

                public void refresh()
                {
                    // Nothing to do
                }
            });
        }

        return summaryLayout;
    }

    private CampaignDetailsLayout getDetailsLayout()
    {
        if (detailLayout == null)
        {
            detailLayout = new CampaignDetailsLayout(new CampaignListener()
            {
                public void refresh()
                {
                    getSummaryLayout().refresh();
                }
                
                public void changed(final DonationCenter donationCenter)
                {
                    // Nothing to do
                }
            });
        }

        return detailLayout;
    }

    /**
     * <p>
     * Campaign Listener.
     * </p>
     * 
     * @author Amila Silva
     * 
     * @version $Id$
     */
    public interface CampaignListener
    {
        /**
         * <p>
         * This will invoke when selected subscriber changed.
         * </p>
         * 
         * @param donationCenter
         *            {@link DonationCenter}
         */
        void changed(DonationCenter donationCenter);
        
        /**
         * <p>
         * Refresh.
         * </p>
         */
        void refresh();
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
