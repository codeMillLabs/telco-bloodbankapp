/*
 * FILENAME
 *     BloodType.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.enums;

/**
 * <p>
 * Enum for Blood types.
 * </p>
 * 
 * @author Amila Silva
 * @version $Id$
 */
public enum BloodType
{

    A_P("A+"),
    A_N("A-"),
    B_P("B+"),
    B_N("B-"),
    AB_P("AB+"),
    AB_N("AB-"),
    O_P("O+"),
    O_N("O-");

    String desc;

    private BloodType(final String inDesc)
    {
        this.desc = inDesc;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString()
    {
        return this.desc;
    }

}
