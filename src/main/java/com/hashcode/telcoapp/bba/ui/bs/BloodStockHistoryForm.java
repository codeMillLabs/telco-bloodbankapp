/*
 * FILENAME
 *     BloodStockHistoryForm.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.bs;

import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createBloodTypeField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createDonationCenterSelectFieldByLocationType;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createSubBloodTypeField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createBloodStatusField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createTextField;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import com.hashcode.telcoapp.bba.data.BloodStockHistory;
import com.hashcode.telcoapp.bba.enums.LocationType;
import com.hashcode.telcoapp.bba.enums.SubBloodType;
import com.hashcode.telcoapp.common.ui.components.MultiColumnForm;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Select;
import com.vaadin.ui.TextField;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood stock history form.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class BloodStockHistoryForm extends MultiColumnForm<BloodStockHistory>
{
    private static final long serialVersionUID = 6671729329126153311L;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public BloodStockHistoryForm()
    {
        super(1);
        setFormFieldFactory(new BloodStockHistoryFormFields());
        setModel(new BloodStockHistory());
    }

    @Override
    protected List<String> getVisiblePropertyIds()
    {
        return Arrays.asList(new String[] {
            BloodStockHistory.RECORD_DATE,
            BloodStockHistory.BLOOD_TYPE,
            BloodStockHistory.SUB_BLOOD_TYPE,
            BloodStockHistory.BLOOD_STATUS,
            BloodStockHistory.CENTER,
            BloodStockHistory.STOCK_COUNT
        });
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.telcoapp.common.ui.components.MultiColumnForm#setModel(java.io.Serializable)
     */
    @Override
    public void setModel(final BloodStockHistory model)
    {
        model.setStockCount(1);
        super.setModel(model);
        getField(BloodStockHistory.RECORD_DATE).setValue(Calendar.getInstance().getTime());
        //BBASubscriber subscriber = getService(AppSubscriberDao.class).findByUserName(AppData.getUserData());
        //DonationCenter donationCenter = getService(DonationCenterDao.class).findByBloodStockAdmin(subscriber);
        //getField(BloodStockHistory.CENTER).setValue(donationCenter);
        //getField(BloodStockHistory.CENTER).setReadOnly(true);
        Field subTypeField = getField(BloodStockHistory.SUB_BLOOD_TYPE);
        final Field bloodStatusField = getField(BloodStockHistory.BLOOD_STATUS);

        subTypeField.addListener(new Property.ValueChangeListener()
        {
            private static final long serialVersionUID = -6052728486631627598L;

            public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event)
            {
                Object value = event.getProperty().getValue();

                if (SubBloodType.RCC.equals(value))
                {
                    bloodStatusField.setEnabled(true);
                    bloodStatusField.setRequired(true);
                }
                else
                {
                    bloodStatusField.setEnabled(false);
                    bloodStatusField.setRequired(false);
                }
                bloodStatusField.setValue(null);
            }
        });
    }

    @Override
    protected int[] getPropertyColumnIndices()
    {
        return new int[] {
            0, 0, 0, 0, 0, 0
        };
    }

    /**
     * <p>
     * Blood stock history form fields.
     * </p>
     * 
     * @author Manuja
     * 
     * @version $Id$
     */
    private class BloodStockHistoryFormFields extends DefaultFieldFactory
    {
        private static final long serialVersionUID = 2287432731301687940L;

        /**
         * {@inheritDoc}
         */
        @Override
        public Field createField(final Item item, final Object propertyId, final Component uiContext)
        {
            String pid = (String) propertyId;
            if (pid.equals(BloodStockHistory.RECORD_DATE))
            {
                DateField dateField = new DateField("Date");
                dateField.setImmediate(true);
                dateField.setResolution(DateField.RESOLUTION_DAY);
                dateField.setRequired(true);
                dateField.setRequiredError("Date is required");
                return dateField;
            }
            else if (pid.equals(BloodStockHistory.BLOOD_TYPE))
            {
                Select bloodTypeField = createBloodTypeField("Blood Type");
                bloodTypeField.setRequired(true);
                bloodTypeField.setRequiredError("Blood Type is required.");
                return bloodTypeField;
            }
            else if (pid.equals(BloodStockHistory.SUB_BLOOD_TYPE))
            {
                Select subBloodTypeField = createSubBloodTypeField("Sub Type");
                subBloodTypeField.setRequired(true);
                subBloodTypeField.setRequiredError("Sub Type is required.");
                return subBloodTypeField;
            }
            else if (pid.equals(BloodStockHistory.BLOOD_STATUS))
            {
                Select subBloodTypeField = createBloodStatusField("Status");
                subBloodTypeField.setRequiredError("Status is required.");
                subBloodTypeField.setEnabled(false);
                return subBloodTypeField;
            }
            else if (pid.equals(BloodStockHistory.CENTER))
            {
                Select centerField =
                    createDonationCenterSelectFieldByLocationType("Blood Bank", LocationType.BLOOD_BANK);
                centerField.setRequired(true);
                centerField.setRequiredError("Center is required.");
                return centerField;
            }
            else if (pid.equals(BloodStockHistory.STOCK_COUNT))
            {
                TextField stockCountField = createTextField("Stock", false);
                stockCountField.setRequired(true);
                stockCountField.setRequiredError("Stock is required.");
                stockCountField.addValidator(new StockValidator());
                return stockCountField;
            }
            return super.createField(item, propertyId, uiContext);
        }
    }

    /**
     * <p>
     * Stock validator.
     * </p>
     * 
     * @author Manuja
     * 
     * @version $Id$
     */
    public class StockValidator implements Validator
    {
        private static final long serialVersionUID = -3483743451476181808L;

        @Override
        public void validate(final Object value) throws InvalidValueException
        {
            if (!isValid(value))
                throw new InvalidValueException("Stock should be more than 0.");
        }

        /**
         * <p>
         * check validity.
         * </p>
         * 
         * @param value
         *            value
         * @return boolean valud whether valid or not
         */

        public boolean isValid(final Object value)
        {
            try
            {
                int valueInt = Integer.parseInt(String.valueOf(value));
                if (valueInt > 0)
                    return true;
            }
            catch (Exception ex)
            {

            }
            return false;
        }
    }
}
