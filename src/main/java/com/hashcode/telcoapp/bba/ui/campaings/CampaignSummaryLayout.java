/*
 * FILENAME
 *     CampaignSummaryLayout.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.campaings;

import com.hashcode.telcoapp.bba.data.DonationCenter;
import com.hashcode.telcoapp.bba.service.CampaignCriteria;
import com.hashcode.telcoapp.bba.ui.campaings.CampaignTab.CampaignListener;
import com.vaadin.data.Property;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Campaign Summary Layout.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class CampaignSummaryLayout extends VerticalLayout
{
    private static final long serialVersionUID = 8898851679079741302L;
    
    private CampaignSummaryTable summaryTable;
    private CampaignSearchForm searchForm;
    private final CampaignListener campaignListener;

    /**
     * <p>
     * Constructor of CampaignSummaryLayout.
     * </p>
     *
     * @param inCampaignListener campaign listener interface
     *
     */
    public CampaignSummaryLayout(final CampaignListener inCampaignListener)
    {
        this.campaignListener = inCampaignListener;
        initUI();
    }

    private void initUI()
    {
        setMargin(true);

        VerticalLayout searchLayout = new VerticalLayout();
        searchLayout.addComponent(getSearchForm());

        Button searchButton = new Button("Search");
        searchButton.setImmediate(true);
        searchButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = -4125772377246561985L;

            public void buttonClick(final ClickEvent event)
            {
                getSummaryTable().refresh(getSearchForm().getModel());
            }
        });

        Button clearButton = new Button("Clear");
        clearButton.setImmediate(true);
        clearButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = 8293367292559846490L;

            public void buttonClick(final ClickEvent event)
            {
                getSearchForm().setModel(new CampaignCriteria());
                getSummaryTable().refresh(new CampaignCriteria());
            }
        });

        HorizontalLayout searchButtonLayout = new HorizontalLayout();
        searchButtonLayout.setSpacing(true);
        searchButtonLayout.addComponent(searchButton);
        searchButtonLayout.addComponent(clearButton);
        searchLayout.addComponent(searchButtonLayout);
        searchLayout.setComponentAlignment(searchButtonLayout, Alignment.MIDDLE_CENTER);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.addComponent(getSummaryTable().getPaginator());
        tableLayout.addComponent(getSummaryTable());

        addComponent(searchLayout);
        addComponent(tableLayout);
        setSizeFull();
    }

    private CampaignSearchForm getSearchForm()
    {
        if (searchForm == null)
        {
            searchForm = new CampaignSearchForm(new CampaignCriteria());
        }

        return searchForm;
    }

    private CampaignSummaryTable getSummaryTable()
    {
        if (summaryTable == null)
        {
            summaryTable = new CampaignSummaryTable();
            summaryTable.addListener(new Property.ValueChangeListener()
            {
                private static final long serialVersionUID = 547953106845087906L;

                /**
                 * {@inheritDoc}
                 */
                public void valueChange(final Property.ValueChangeEvent event)
                {
                    campaignListener.changed((DonationCenter) event.getProperty().getValue());
                }
            });
        }

        return summaryTable;
    }
    
    /**
     * <p>
     * Refresh campaign summary layout.
     * </p>
     */
    public void refresh()
    {
        getSummaryTable().refresh();
    }
}
