/*
 * FILENAME
 *     BulkMessageSendForm.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.telcoapp.bba.ui.bulkmessage;

import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createBloodTypeField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createDistrictField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createTextArea;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createTextField;

import java.util.Arrays;
import java.util.List;

import com.hashcode.telcoapp.bba.data.BulkMessage;
import com.hashcode.telcoapp.common.ui.components.MultiColumnForm;
import com.vaadin.data.Item;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Select;
import com.vaadin.ui.TextArea;

//
//IMPORTS
//NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Bulk message send form.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class BulkMessageSendForm extends MultiColumnForm<BulkMessage>
{
    private static final long serialVersionUID = -3120887708826635704L;

    /**
     * <p>
     * Constructor of BulkMessageSendForm.
     * </p>
     *
     * @param bulkMessage bulk message instance
     */
    public BulkMessageSendForm(final BulkMessage bulkMessage)
    {
        super(1);
        setFormFieldFactory(new BulkMsgFormFields());
        setModel(bulkMessage);
    }

    @Override
    protected List<String> getVisiblePropertyIds()
    {
        return Arrays.asList(new String[] {
            BulkMessage.DISTRICT,
            BulkMessage.BLOOD_TYPE,
            BulkMessage.NIC,
            BulkMessage.AP_DONOR,
            BulkMessage.SEND_TO_ALL,
            BulkMessage.IS_SCHEDULED,
            BulkMessage.SCHEDULED_TIME,
            BulkMessage.MESSAGE
        });
    }

    @Override
    protected int[] getPropertyColumnIndices()
    {
        return new int[] {
            0, 0, 0, 0, 0, 0, 0, 0
        };
    }

    /**
     * <p>
     * Bulk Message field factory implementation.
     * </p>
     * 
     * @author Amila Silva
     * 
     * @version $Id$
     */
    private class BulkMsgFormFields extends DefaultFieldFactory
    {
        private static final long serialVersionUID = -8215352750715639707L;

        /**
         * {@inheritDoc}
         */
        @Override
        public Field createField(final Item item, final Object propertyId, final Component uiContext)
        {
            String pid = (String) propertyId;

            if (pid.equals(BulkMessage.DISTRICT))
            {
                return createDistrictField("District");
            }
            else if (pid.equals(BulkMessage.BLOOD_TYPE))
            {
                return createBloodTypeField("Blood Type");
            }
            else if (pid.equals(BulkMessage.MESSAGE))
            {
                TextArea bulkMsgField = createTextArea("Text Message", false);
                bulkMsgField.setRequired(true);
                bulkMsgField.setRequiredError("Text Message is required");
                return bulkMsgField;
            }
            else if (pid.equals(BulkMessage.IS_SCHEDULED))
            {
                Select select = new Select("Scheduled");
                select.setImmediate(true);
                select.addItem("True");
                select.addItem("False");
                return select;
            }
            else if (pid.equals(BulkMessage.SCHEDULED_TIME))
            {
                DateField dateField = new DateField("Scheduled Date");
                dateField.setImmediate(true);
                dateField.setResolution(DateField.RESOLUTION_MIN);
                return dateField;
            }
            else if (pid.equals(BulkMessage.NIC))
            {
                return createTextField("NIC", false);
            }
            else if (pid.equals(BulkMessage.AP_DONOR))
            {
                return new CheckBox("AP Donor");
            }
            else if (pid.equals(BulkMessage.SEND_TO_ALL))
            {
                return new CheckBox("Send To All");
            }

            return super.createField(item, propertyId, uiContext);
        }
    }

}
