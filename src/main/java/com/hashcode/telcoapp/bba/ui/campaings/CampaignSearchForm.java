/*
 * FILENAME
 *     CampainSearchForm.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.campaings;

import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createDateField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createDistrictField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createLocationTypeField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createTextField;

import java.util.Arrays;
import java.util.List;

import com.hashcode.telcoapp.bba.service.CampaignCriteria;
import com.hashcode.telcoapp.common.ui.components.MultiColumnForm;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;

/**
 * <p>
 * Campaign Search Form.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class CampaignSearchForm extends MultiColumnForm<CampaignCriteria>
{
    private static final long serialVersionUID = 8778599196969137056L;

    /**
     * <p>
     * Constructor of CampaignSearchForm.
     * </p>
     * 
     * @param criteria
     *            campaign search criteria instance
     */
    public CampaignSearchForm(final CampaignCriteria criteria)
    {
        super(2);
        setFormFieldFactory(new CampaignSearchFormField());
        setModel(criteria);
    }

    @Override
    protected List<String> getVisiblePropertyIds()
    {
        return Arrays.asList(new String[] {
            CampaignCriteria.DISTRICT,
            CampaignCriteria.CITY,
            CampaignCriteria.CENTER_NAME,
            CampaignCriteria.LOC_TYPE,
            CampaignCriteria.DATE
        });
    }

    @Override
    protected int[] getPropertyColumnIndices()
    {
        return new int[] {
            0, 0, 0, 1, 1
        };
    }

    /**
     * <p>
     * Campaign search form field.
     * </p>
     *
     * @author manuja
     *
     * @version $Id$
     */
    private class CampaignSearchFormField extends DefaultFieldFactory
    {

        private static final long serialVersionUID = -5755467446007035089L;

        /**
         * {@inheritDoc}
         */
        @Override
        public Field createField(final Item item, final Object propertyId, final Component uiContext)
        {
            String pid = (String) propertyId;
            if (pid.equals(CampaignCriteria.DISTRICT))
            {
                return createDistrictField("District");
            }
            else if (pid.equals(CampaignCriteria.LOC_TYPE))
            {
                return createLocationTypeField("Location Type");
            }
            else if (pid.equals(CampaignCriteria.CITY))
            {
                return createTextField("City", false);
            }
            else if (pid.equals(CampaignCriteria.DATE))
            {
                return createDateField("Campaign Date", false);
            }
            else if (pid.equals(CampaignCriteria.CENTER_NAME))
            {
                return createTextField("Center Name", false);
            }
            return super.createField(item, propertyId, uiContext);
        }
    }

}
