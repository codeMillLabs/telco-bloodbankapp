/*
 * FILENAME
 *     BulkMessage.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.telcoapp.bba.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.hashcode.telcoapp.bba.enums.BloodType;

//
//IMPORTS
//NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Entity for Bulk Messages.
 * </p>
 * 
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
@Entity
@Table(name = "BULK_MESSAGE")
public class BulkMessage implements Serializable
{

    private static final long serialVersionUID = -5646505598954892206L;

    public static final String ID = "id";
    public static final String MESSAGE = "message";
    public static final String IS_SCHEDULED = "scheduledMsg";
    public static final String SCHEDULED_TIME = "scheduledTime";
    public static final String BLOOD_TYPE = "bloodType";
    public static final String DISTRICT = "district";
    public static final String NIC = "nic";
    public static final String AP_DONOR = "apDonor";
    public static final String SEND_TO_ALL = "sendToAll";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "BULK_SEQ")
    @SequenceGenerator(name = "BULK_SEQ", sequenceName = "BULK_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "TEXT_MESSAGE")
    private String message;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "IS_SCHEDULED_MSG")
    private boolean scheduledMsg;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SCHEDULE_TIME")
    private Date scheduledTime;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "BLOOD_TYPE")
    private BloodType bloodType;

    @Column(name = "DISTRICT")
    private String district;

    @Column(name = "NIC")
    private String nic;

    @Column(name = "AP_DONOR")
    private boolean apDonor;

    @Column(name = "SEND_TO_ALL")
    private boolean sendToAll;

    @OneToMany(cascade = {
        CascadeType.PERSIST, CascadeType.MERGE
    }, mappedBy = "bulkMessage")
    private List<MessageQueue> messages;

    /**
     * 
     * <p>
     * Add messages related to bulk message.
     * </p>
     * 
     * 
     * @param msg
     *            message queue
     */
    public void addMessage(final MessageQueue msg)
    {
        getMessages().add(msg);
        msg.setBulkMessage(this);
    }

    /**
     * <p>
     * Getter for messages.
     * </p>
     * 
     * @return the messages
     */
    public List<MessageQueue> getMessages()
    {
        if (messages == null)
        {
            this.messages = new ArrayList<MessageQueue>();
        }
        return messages;
    }

    /**
     * <p>
     * Setting value for messages.
     * </p>
     * 
     * @param inMessages
     *            the messages to set
     */
    public void setMessages(final List<MessageQueue> inMessages)
    {
        this.messages = inMessages;
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * <p>
     * Setting value for id.
     * </p>
     * 
     * @param inId
     *            the id to set
     */
    public void setId(final Long inId)
    {
        this.id = inId;
    }

    /**
     * <p>
     * Getter for message.
     * </p>
     * 
     * @return the message
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * <p>
     * Setting value for message.
     * </p>
     * 
     * @param inMessage
     *            the message to set
     */
    public void setMessage(final String inMessage)
    {
        this.message = inMessage;
    }

    /**
     * <p>
     * Getter for createdDate.
     * </p>
     * 
     * @return the createdDate
     */
    public Date getCreatedDate()
    {
        return createdDate;
    }

    /**
     * <p>
     * Setting value for createdDate.
     * </p>
     * 
     * @param inCreatedDate
     *            the createdDate to set
     */
    public void setCreatedDate(final Date inCreatedDate)
    {
        this.createdDate = inCreatedDate;
    }

    /**
     * <p>
     * Getter for scheduledMsg.
     * </p>
     * 
     * @return the scheduledMsg
     */
    public boolean isScheduledMsg()
    {
        return scheduledMsg;
    }

    /**
     * <p>
     * Setting value for scheduledMsg.
     * </p>
     * 
     * @param inScheduledMsg
     *            the scheduledMsg to set
     */
    public void setScheduledMsg(final boolean inScheduledMsg)
    {
        this.scheduledMsg = inScheduledMsg;
    }

    /**
     * <p>
     * Getter for scheduledTime.
     * </p>
     * 
     * @return the scheduledTime
     */
    public Date getScheduledTime()
    {
        return scheduledTime;
    }

    /**
     * <p>
     * Setting value for scheduledTime.
     * </p>
     * 
     * @param inScheduledTime
     *            the scheduledTime to set
     */
    public void setScheduledTime(final Date inScheduledTime)
    {
        this.scheduledTime = inScheduledTime;
    }

    /**
     * <p>
     * Getter for createdBy.
     * </p>
     * 
     * @return the createdBy
     */
    public String getCreatedBy()
    {
        return createdBy;
    }

    /**
     * <p>
     * Setting value for createdBy.
     * </p>
     * 
     * @param inCreatedBy
     *            the createdBy to set
     */
    public void setCreatedBy(final String inCreatedBy)
    {
        this.createdBy = inCreatedBy;
    }

    /**
     * <p>
     * Getter for bloodType.
     * </p>
     * 
     * @return the bloodType
     */
    public BloodType getBloodType()
    {
        return bloodType;
    }

    /**
     * <p>
     * Setting value for bloodType.
     * </p>
     * 
     * @param inBloodType
     *            the bloodType to set
     */
    public void setBloodType(final BloodType inBloodType)
    {
        this.bloodType = inBloodType;
    }

    /**
     * <p>
     * Getter for district.
     * </p>
     * 
     * @return the district
     */
    public String getDistrict()
    {
        return district;
    }

    /**
     * <p>
     * Setting value for district.
     * </p>
     * 
     * @param inDistrict
     *            the district to set
     */
    public void setDistrict(final String inDistrict)
    {
        this.district = inDistrict;
    }

    /**
     * <p>
     * Getter for nic.
     * </p>
     * 
     * @return the nic
     */
    public String getNic()
    {
        return nic;
    }

    /**
     * <p>
     * Setting value for nic.
     * </p>
     * 
     * @param inNic
     *            the nic to set
     */
    public void setNic(final String inNic)
    {
        this.nic = inNic.toUpperCase();
    }

    /**
     * <p>
     * Getter for apDonor.
     * </p>
     * 
     * @return the apDonor
     */
    public boolean isApDonor()
    {
        return apDonor;
    }

    /**
     * <p>
     * Setting value for apDonor.
     * </p>
     * 
     * @param inApDonor
     *            the apDonor to set
     */
    public void setApDonor(final boolean inApDonor)
    {
        this.apDonor = inApDonor;
    }

    /**
     * <p>
     * Getter for sendToAll.
     * </p>
     * 
     * @return the sendToAll
     */
    public boolean isSendToAll()
    {
        return sendToAll;
    }

    /**
     * <p>
     * Setting value for sendToAll.
     * </p>
     * 
     * @param inSendToAll
     *            the sendToAll to set
     */
    public void setSendToAll(final boolean inSendToAll)
    {
        this.sendToAll = inSendToAll;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("BulkMessage [id=");
        builder.append(id);
        builder.append(", message=");
        builder.append(message);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append(", scheduledMsg=");
        builder.append(scheduledMsg);
        builder.append(", scheduledTime=");
        builder.append(scheduledTime);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", bloodType=");
        builder.append(bloodType);
        builder.append(", district=");
        builder.append(district);
        builder.append(", nic=");
        builder.append(nic);
        builder.append(", apDonor=");
        builder.append(apDonor);
        builder.append(", sendToAll=");
        builder.append(sendToAll);
        builder.append("]");
        return builder.toString();
    }

}
