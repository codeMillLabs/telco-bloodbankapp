/*
 * FILENAME
 *     BulkMsgCriteria.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.service;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.joda.time.DateTime;

import com.hashcode.telcoapp.bba.enums.BBAState;
import com.hashcode.telcoapp.bba.enums.BloodType;
import com.hashcode.telcoapp.common.dao.SearchCriteria;
import com.hashcode.telcoapp.common.enums.Gender;
import com.hashcode.telcoapp.common.enums.SubscriberState;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Bulk message filtering criteria class.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class BulkMsgCriteria extends SearchCriteria
{
    private static final long serialVersionUID = -6096469674983460852L;

    public static final String NIC = "nic";
    public static final String DISTRICT = "district";
    public static final String BLOOD_TYPE = "bloodType";
    public static final String GENDER = "gender";
    public static final String ELIGIBLE_DATE = "eligibleDateFrom";
    public static final String POINTS = "points";

    public static final String STATUS = "status";
    public static final String SCHEDULE_TIME = "scheduledTime";
    public static final String SENT_DATE = "sentDate";
    public static final String CREATED_BY = "createdBy";
    public static final String AP_DONOR = "apDonor";
    public static final String SEND_TO_ALL = "sendToAll";

    private String nic;
    private String district;
    private BloodType bloodType;
    private String createdBy;
    private Date sentDate;
    private Gender gender;
    private Date eligibleDateFrom;
    private Date eligibleDateOn;
    private int points = 0;
    private Date dob;
    private int birthMonth = 0;
    private int birthDate = 0;
    private Boolean apDonor;
    private Boolean sendToAll;

    private Set<BBAState> status;
    private Date scheduledTime;

    private Date senderQueueTimeFrom;
    private Date senderQueueTimeTo;
    
    private SubscriberState subscriberState;
    private boolean messageSendingCrit = false;

    /**
     * <p>
     * Getter for nic.
     * </p>
     * 
     * @return the nic
     */
    public String getNic()
    {
        return nic;
    }

    /**
     * <p>
     * Setting value for nic.
     * </p>
     * 
     * @param inNic
     *            the nic to set
     */
    public void setNic(final String inNic)
    {
        this.nic = (nic != null) ? inNic.toUpperCase() : inNic;
    }

    /**
     * <p>
     * Getter for district.
     * </p>
     * 
     * @return the district
     */
    public String getDistrict()
    {
        return district;
    }

    /**
     * <p>
     * Setting value for district.
     * </p>
     * 
     * @param inDistrict
     *            the district to set
     */
    public void setDistrict(final String inDistrict)
    {
        this.district = inDistrict;
    }

    /**
     * <p>
     * Getter for bloodType.
     * </p>
     * 
     * @return the bloodType
     */
    public BloodType getBloodType()
    {
        return bloodType;
    }

    /**
     * <p>
     * Setting value for bloodType.
     * </p>
     * 
     * @param inBloodType
     *            the bloodType to set
     */
    public void setBloodType(final BloodType inBloodType)
    {
        this.bloodType = inBloodType;
    }

    /**
     * <p>
     * Getter for gender.
     * </p>
     * 
     * @return the gender
     */
    public Gender getGender()
    {
        return gender;
    }

    /**
     * <p>
     * Setting value for gender.
     * </p>
     * 
     * @param inGender
     *            the gender to set
     */
    public void setGender(final Gender inGender)
    {
        this.gender = inGender;
    }

    /**
     * <p>
     * Getter for points.
     * </p>
     * 
     * @return the points
     */
    public int getPoints()
    {
        return points;
    }

    /**
     * <p>
     * Setting value for points.
     * </p>
     * 
     * @param inPoints
     *            the points to set
     */
    public void setPoints(final int inPoints)
    {
        this.points = inPoints;
    }

    /**
     * <p>
     * Getter for eligibleDateFrom.
     * </p>
     * 
     * @return the eligibleDateFrom
     */
    public Date getEligibleDateFrom()
    {
        return eligibleDateFrom;
    }

    /**
     * <p>
     * Setting value for eligibleDateFrom.
     * </p>
     * 
     * @param inEligibleDateFrom
     *            the eligibleDateFrom to set
     */
    public void setEligibleDateFrom(final Date inEligibleDateFrom)
    {
        this.eligibleDateFrom = inEligibleDateFrom;
    }

    /**
     * <p>
     * Getter for createdBy.
     * </p>
     * 
     * @return the createdBy
     */
    public String getCreatedBy()
    {
        return createdBy;
    }

    /**
     * <p>
     * Setting value for createdBy.
     * </p>
     * 
     * @param inCreatedBy
     *            the createdBy to set
     */
    public void setCreatedBy(final String inCreatedBy)
    {
        this.createdBy = inCreatedBy;
    }

    /**
     * <p>
     * Getter for sendDate.
     * </p>
     * 
     * @return the sendDate
     */
    public Date getSentDate()
    {
        return sentDate;
    }

    /**
     * <p>
     * Setting value for sentDate.
     * </p>
     * 
     * @param inSentDate
     *            the sentDate to set
     */
    public void setSentDate(final Date inSentDate)
    {
        this.sentDate = inSentDate;
    }

    /**
     * <p>
     * Getter for eligibleDateOn.
     * </p>
     * 
     * @return the eligibleDateOn
     */
    public Date getEligibleDateOn()
    {
        return eligibleDateOn;
    }

    /**
     * <p>
     * Setting value for eligibleDateOn.
     * </p>
     * 
     * @param inEligibleDateOn
     *            the eligibleDateOn to set
     */
    public void setEligibleDateOn(final Date inEligibleDateOn)
    {
        this.eligibleDateOn = inEligibleDateOn;
    }

    /**
     * <p>
     * Setting value for status.
     * </p>
     * 
     * @param inStatus
     *            the status to set
     */
    public void setStatus(final Set<BBAState> inStatus)
    {
        this.status = inStatus;
    }

    /**
     * <p>
     * Getter for status.
     * </p>
     * 
     * @return the status
     */
    public Set<BBAState> getStatus()
    {
        if (status == null)
        {
            status = new HashSet<BBAState>();
        }
        return status;
    }

    /**
     * <p>
     * Getter for scheduledTime.
     * </p>
     * 
     * @return the scheduledTime
     */
    public Date getScheduledTime()
    {
        return scheduledTime;
    }

    /**
     * <p>
     * Setting value for scheduledTime.
     * </p>
     * 
     * @param inScheduledTime
     *            the scheduledTime to set
     */
    public void setScheduledTime(final Date inScheduledTime)
    {
        this.scheduledTime = inScheduledTime;
    }

    /**
     * <p>
     * Getter for apDonor.
     * </p>
     * 
     * @return the apDonor
     */
    public Boolean getApDonor()
    {
        return apDonor;
    }

    /**
     * <p>
     * Setting value for apDonor.
     * </p>
     * 
     * @param inApDonor
     *            the apDonor to set
     */
    public void setApDonor(final Boolean inApDonor)
    {
        this.apDonor = inApDonor;
    }

    /**
     * <p>
     * Getter for dob.
     * </p>
     * 
     * @return the dob
     */
    public Date getDob()
    {
        return dob;
    }

    /**
     * <p>
     * Setting value for dob.
     * </p>
     * 
     * @param inDob
     *            the dob to set
     */
    public void setDob(final Date inDob)
    {
        this.dob = inDob;

        if (this.dob != null)
        {
            DateTime dateTime = new DateTime(this.dob.getTime());
            setBirthMonth(dateTime.getMonthOfYear());
            setBirthDate(dateTime.getDayOfMonth());
        }
    }

    /**
     * <p>
     * Getter for birthMonth.
     * </p>
     * 
     * @return the birthMonth
     */
    public int getBirthMonth()
    {
        return birthMonth;
    }

    /**
     * <p>
     * Setting value for birthMonth.
     * </p>
     * 
     * @param inBirthMonth
     *            the birthMonth to set
     */
    public void setBirthMonth(final int inBirthMonth)
    {
        this.birthMonth = inBirthMonth;
    }

    /**
     * <p>
     * Getter for birthDate.
     * </p>
     * 
     * @return the birthDate
     */
    public int getBirthDate()
    {
        return birthDate;
    }

    /**
     * <p>
     * Setting value for birthDate.
     * </p>
     * 
     * @param inBirthDate
     *            the birthDate to set
     */
    public void setBirthDate(final int inBirthDate)
    {
        this.birthDate = inBirthDate;
    }

    /**
     * <p>
     * Getter for sendToAll.
     * </p>
     * 
     * @return the sendToAll
     */
    public Boolean getSendToAll()
    {
        return sendToAll;
    }

    /**
     * <p>
     * Setting value for sendToAll.
     * </p>
     * 
     * @param inSendToAll
     *            the sendToAll to set
     */
    public void setSendToAll(final Boolean inSendToAll)
    {
        this.sendToAll = inSendToAll;
    }

    /**
     * <p>
     * Getter for senderQueueTimeFrom.
     * </p>
     * 
     * @return the senderQueueTimeFrom
     */
    public Date getSenderQueueTimeFrom()
    {
        return senderQueueTimeFrom;
    }

    /**
     * <p>
     * Setting value for senderQueueTimeFrom.
     * </p>
     * 
     * @param inSenderQueueTimeFrom the senderQueueTimeFrom to set
     */
    public void setSenderQueueTimeFrom(final Date inSenderQueueTimeFrom)
    {
        this.senderQueueTimeFrom = inSenderQueueTimeFrom;
    }

    /**
     * <p>
     * Getter for senderQueueTimeTo.
     * </p>
     * 
     * @return the senderQueueTimeTo
     */
    public Date getSenderQueueTimeTo()
    {
        return senderQueueTimeTo;
    }

    /**
     * <p>
     * Setting value for senderQueueTimeTo.
     * </p>
     * 
     * @param inSenderQueueTimeTo the senderQueueTimeTo to set
     */
    public void setSenderQueueTimeTo(final Date inSenderQueueTimeTo)
    {
        this.senderQueueTimeTo = inSenderQueueTimeTo;
    }

    /**
     * <p>
     * Getter for messageSendingCrit.
     * </p>
     * 
     * @return the messageSendingCrit
     */
    public boolean isMessageSendingCrit()
    {
        return messageSendingCrit;
    }

    /**
     * <p>
     * Setting value for messageSendingCrit.
     * </p>
     * 
     * @param inMessageSendingCrit the messageSendingCrit to set
     */
    public void setMessageSendingCrit(final boolean inMessageSendingCrit)
    {
        this.messageSendingCrit = inMessageSendingCrit;
    }

    /**
     * <p>
     * Getter for subscriberState.
     * </p>
     * 
     * @return the subscriberState
     */
    public SubscriberState getSubscriberState()
    {
        return subscriberState;
    }

    /**
     * <p>
     * Setting value for subscriberState.
     * </p>
     * 
     * @param inSubscriberState the subscriberState to set
     */
    public void setSubscriberState(final SubscriberState inSubscriberState)
    {
        this.subscriberState = inSubscriberState;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("BulkMsgCriteria [nic=");
        builder.append(nic);
        builder.append(", district=");
        builder.append(district);
        builder.append(", bloodType=");
        builder.append(bloodType);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", sentDate=");
        builder.append(sentDate);
        builder.append(", gender=");
        builder.append(gender);
        builder.append(", eligibleDateFrom=");
        builder.append(eligibleDateFrom);
        builder.append(", eligibleDateOn=");
        builder.append(eligibleDateOn);
        builder.append(", points=");
        builder.append(points);
        builder.append(", dob=");
        builder.append(dob);
        builder.append(", birthMonth=");
        builder.append(birthMonth);
        builder.append(", birthDate=");
        builder.append(birthDate);
        builder.append(", apDonor=");
        builder.append(apDonor);
        builder.append(", sendToAll=");
        builder.append(sendToAll);
        builder.append(", status=");
        builder.append(status);
        builder.append(", scheduledTime=");
        builder.append(scheduledTime);
        builder.append(", senderQueueTimeFrom=");
        builder.append(senderQueueTimeFrom);
        builder.append(", senderQueueTimeTo=");
        builder.append(senderQueueTimeTo);
        builder.append(", subscriberState=");
        builder.append(subscriberState);
        builder.append(", messageSendingCrit=");
        builder.append(messageSendingCrit);
        builder.append("]");
        return builder.toString();
    }
}
