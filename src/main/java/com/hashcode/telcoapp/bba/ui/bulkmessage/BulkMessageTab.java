/*
 * FILENAME
 *     BulkMessageTab.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.telcoapp.bba.ui.bulkmessage;

import com.hashcode.telcoapp.bba.data.BulkMessage;
import com.hashcode.telcoapp.bba.data.MessageQueue;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

//
//IMPORTS
//NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Bulk message Tab.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class BulkMessageTab extends VerticalLayout
{
    private static final long serialVersionUID = 9202102823571308447L;
    private BulkMsgSummaryLayout summaryLayout;
    private BulkMessageDetailsLayout detailLayout;
    private final TabSheet tabSheet;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public BulkMessageTab()
    {
        setSizeFull();
        tabSheet = new TabSheet();
        tabSheet.setWidth("100%");
        tabSheet.setImmediate(true);
        tabSheet.addTab(getSummaryLayout(), "Bulk Message Log", new ThemeResource("images/campaign-summary.png"));
        tabSheet.addTab(getDetailsLayout(), "Bulk Message Detail", new ThemeResource("images/campaign-details.png"));

        addComponent(tabSheet);
    }

    private BulkMsgSummaryLayout getSummaryLayout()
    {
        if (summaryLayout == null)
        {
            summaryLayout = new BulkMsgSummaryLayout(new BulkMessageListener()
            {

                public void changed(final MessageQueue messageQueue)
                {
                    if (null == messageQueue)
                    {
                        getDetailsLayout().setModel(new BulkMessage());
                    }
                }
            });
        }

        return summaryLayout;
    }

    private BulkMessageDetailsLayout getDetailsLayout()
    {
        if (detailLayout == null)
        {
            detailLayout = new BulkMessageDetailsLayout();
        }

        return detailLayout;
    }

    /**
     * <p>
     * Bulk message Listener.
     * </p>
     * 
     * @author Amila Silva
     * 
     * @version $Id$
     */
    public interface BulkMessageListener
    {
        /**
         * <p>
         * This will invoke when selected message queue changed.
         * </p>
         * 
         * @param messageQueue
         *            {@link MessageQueue}
         */
        void changed(MessageQueue messageQueue);
    }

}
