/*
 * FILENAME
 *     OutboundMessageDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.telcoapp.bba.dao;

import java.util.List;

import com.hashcode.telcoapp.bba.data.MessageQueue;
import com.hashcode.telcoapp.common.dao.GenericDao;
import com.hashcode.telcoapp.common.dao.SearchCriteria;

/**
 * <p>
 * Outbound message dao interface.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 */
public interface MessageQueueDao extends GenericDao<MessageQueue, Long>
{

    /**
     * 
     * <p>
     * Find the Messages in queue by given criteria.
     * </p>
     * 
     * 
     * @param criteria
     *            MessageQueue search criteria.
     * @return List {@link MessageQueue}
     * 
     */
    List<MessageQueue> findByCriteria(final SearchCriteria criteria);

    /**
     * 
     * <p>
     * Count Messages in queue by given criteria.
     * </p>
     * 
     * 
     * @param criteria
     *            MessageQueue search criteria.
     * @return Integer message count
     * 
     */
    Integer countByCriteria(final SearchCriteria criteria);

}
