/*
 * FILENAME
 *     Configuration.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.util;

import static java.text.MessageFormat.format;
import static java.util.Arrays.asList;
import static java.util.ResourceBundle.getBundle;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Configuration Holder.
 * </p>
 * 
 * @author manuja
 * 
 * @version $Id$
 */
public final class Configuration
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Configuration.class);

    public static final String ADMIN_BUNDLE_NAME = "admin";
    private static ResourceBundle resourceBundle = getBundle(ADMIN_BUNDLE_NAME);
    public static final String REG = getMessage("reg.key");
    public static final String HELP = getMessage("help.key");
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    public static final int DONATION_FREQUENCY = Integer.parseInt(getMessage("donation.freequency"));

    private Configuration()
    {
    }

    /**
     * <p>
     * Resolve a message by a key and argument replacements.
     * </p>
     * 
     * @see MessageFormat#format(String, Object...)
     * @param key
     *            the message to look up
     * @param arguments
     *            optional message arguments
     * @return the resolved message
     **/
    public static String getMessage(final String key, final Object... arguments)
    {
        try
        {
            if (arguments != null)
                return format(resourceBundle.getString(key), arguments);
            return resourceBundle.getString(key);
        }
        catch (MissingResourceException e)
        {
            LOGGER.error("Message key not found: " + key);
            return '!' + key + '!';
        }
    }

    /**
     * <p>
     * Use to generate new password.
     * </p>
     * 
     * @return 4 character password.
     */
    public static synchronized String generateNewPassword()
    {
        List<String> digitList = asList(getMessage("digit.list").split("\\s*,\\s*"));
        List<String> characterList = asList(getMessage("character.list").split("\\s*,\\s*"));
        List<String> allList = new ArrayList<String>();
        for (String digit : digitList)
        {
            allList.add(digit);
        }

        for (String character : characterList)
        {
            allList.add(character);
        }

        StringBuilder passwordBuilder = new StringBuilder();
        Collections.shuffle(allList);
        passwordBuilder.append(allList.get(0));
        Collections.shuffle(allList);
        passwordBuilder.append(allList.get(0));
        Collections.shuffle(allList);
        passwordBuilder.append(allList.get(0));
        Collections.shuffle(allList);
        passwordBuilder.append(allList.get(0));

        LOGGER.info("########### generated password ############ " + passwordBuilder.toString());
        return passwordBuilder.toString().toUpperCase();
    }
}
