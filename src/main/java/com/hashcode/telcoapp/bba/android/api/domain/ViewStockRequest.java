/*
 * FILENAME
 *     ViewStockRequest.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.android.api.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.hashcode.telcoapp.bba.enums.BloodType;
import com.hashcode.telcoapp.bba.enums.SubBloodType;
import com.hashcode.telcoapp.bba.util.BBAUtils;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * View Stock Request.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
public class ViewStockRequest  implements Serializable
{
    private static final long serialVersionUID = -7971431318645796815L;

    private String date;
    private String centerId;
    private String bloodType;
    private String subType;

    /**
     * <p>
     * Getter for date.
     * </p>
     * 
     * @return the date
     */
    public String getDate()
    {
        return date;
    }

    /**
     * <p>
     * Return date in java date type.
     * </p>
     * 
     * @return date value
     */
    public Date parseDate()
    {
        if (null != date)
        {
            try
            {
                SimpleDateFormat formatter = new SimpleDateFormat(BBAUtils.ANDROID_API_DATE_FORMAT);
                return formatter.parse(date);
            }
            catch (Exception ex)
            {
            }
        }

        return null;
    }

    /**
     * <p>
     * Setting value for date.
     * </p>
     * 
     * @param inDate
     *            the date to set
     */
    public void setDate(final String inDate)
    {
        this.date = inDate;
    }

    /**
     * <p>
     * Getter for centerId.
     * </p>
     * 
     * @return the centerId
     */
    public String getCenterId()
    {
        return centerId;
    }

    /**
     * <p>
     * Setting value for centerId.
     * </p>
     * 
     * @param inCenterId
     *            the centerId to set
     */
    public void setCenterId(final String inCenterId)
    {
        this.centerId = inCenterId;
    }

    /**
     * <p>
     * Getter for bloodType.
     * </p>
     * 
     * @return the bloodType
     */
    public String getBloodType()
    {
        return bloodType;
    }

    /**
     * <p>
     * Parse bloodType.
     * </p>
     * 
     * @return the bloodType
     */
    public BloodType parseBloodType()
    {
        return BBAUtils.mapBloodType(bloodType);
    }

    /**
     * <p>
     * Setting value for bloodType.
     * </p>
     * 
     * @param inBloodType
     *            the bloodType to set
     */
    public void setBloodType(final String inBloodType)
    {
        this.bloodType = inBloodType;
    }

    /**
     * <p>
     * Getter for subType.
     * </p>
     * 
     * @return the subType
     */
    public String getSubType()
    {
        return subType;
    }

    /**
     * <p>
     * Parse subType.
     * </p>
     * 
     * @return the subType
     */
    public SubBloodType parseSubType()
    {
        return BBAUtils.mapSubBloodType(subType);
    }

    /**
     * <p>
     * Setting value for subType.
     * </p>
     * 
     * @param inSubType
     *            the subType to set
     */
    public void setSubType(final String inSubType)
    {
        this.subType = inSubType;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("UpdateStockRequest [date=");
        builder.append(date);
        builder.append(", centerId=");
        builder.append(centerId);
        builder.append(", bloodType=");
        builder.append(bloodType);
        builder.append(", subType=");
        builder.append(subType);
        builder.append("]");
        return builder.toString();
    }
}
