/*
 * FILENAME
 *     FixTimeOutJob.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.telcoapp.bba.service.BulkMsgSenderService;
import com.hashcode.telcoapp.common.util.ServiceResolver;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Fix Time Job.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class MinuteJob implements Job
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(MinuteJob.class);

    /**
     * {@inheritDoc}
     * 
     * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
     */
    public void execute(final JobExecutionContext arg0) throws JobExecutionException
    {
        LOGGER.info(" >>>>>>>> Minute Job Started");
        getService(BulkMsgSenderService.class).sendMessageFromQueue();
        LOGGER.info(" >>>>>>>> Minute Job Ended");
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
