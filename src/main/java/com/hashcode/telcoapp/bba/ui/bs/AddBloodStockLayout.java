/*
 * FILENAME
 *     AddBloodStockLayout.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.bs;

import java.util.ArrayList;
import java.util.List;

import com.hashcode.telcoapp.bba.dao.BloodStockHistoryDao;
import com.hashcode.telcoapp.bba.data.BloodStockHistory;
import com.hashcode.telcoapp.common.util.ServiceResolver;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.BaseTheme;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood stock tab.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class AddBloodStockLayout extends VerticalLayout
{
    private static final long serialVersionUID = -4162312859521278861L;
    private BloodStockHistoryForm bloodStockHistorySearchForm;
    private BloodStockTempTable bloodStockTempTable;

    private List<BloodStockHistory> selectedHistories = new ArrayList<BloodStockHistory>();

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public AddBloodStockLayout()
    {
        initUI();
        setSizeFull();
    }

    private void initUI()
    {
        setMargin(true);

        VerticalLayout searchLayout = new VerticalLayout();
        searchLayout.addComponent(getBloodStockHistoryForm());

        Button searchButton = new Button("Add");
        searchButton.setImmediate(true);
        searchButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = -6758964146897948624L;

            public void buttonClick(final ClickEvent event)
            {
                if (getBloodStockHistoryForm().validateAllFields())
                {
                    BloodStockHistory bloodStockHistory = getBloodStockHistoryForm().getModel();
                    addItem(bloodStockHistory);
                    getBloodStockHistoryForm().setModel(new BloodStockHistory());
                    selectedHistories.add(bloodStockHistory);
                }
            }
        });

        Button clearButton = new Button("Clear");
        clearButton.setImmediate(true);
        clearButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = -8017319890142574083L;

            public void buttonClick(final ClickEvent event)
            {
                getBloodStockHistoryForm().setModel(new BloodStockHistory());
            }
        });

        HorizontalLayout searchButtonLayout = new HorizontalLayout();
        searchButtonLayout.setSpacing(true);
        searchButtonLayout.addComponent(searchButton);
        searchButtonLayout.addComponent(clearButton);
        searchLayout.addComponent(searchButtonLayout);
        searchLayout.setComponentAlignment(searchButtonLayout, Alignment.MIDDLE_CENTER);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.setMargin(new MarginInfo(true, false, false, false));
        tableLayout.setSpacing(true);
        tableLayout.addComponent(getBloodStockTempTable());

        Button completeButton = new Button("Complete");
        completeButton.setImmediate(true);
        completeButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = -9118506329321230368L;

            public void buttonClick(final ClickEvent event)
            {
                if (!selectedHistories.isEmpty())
                {
                    for (BloodStockHistory bloodStockHistory : selectedHistories)
                    {
                        getService(BloodStockHistoryDao.class).create(bloodStockHistory);
                    }
                    getBloodStockHistoryForm().setModel(new BloodStockHistory());
                    getBloodStockTempTable().removeAllItems();
                    getBloodStockHistoryForm().setComponentError(null);
                    selectedHistories = new ArrayList<BloodStockHistory>();
                    getWindow().showNotification("Saved ...");
                }
            }
        });

        Button cancelButton = new Button("Cancel");
        cancelButton.setImmediate(true);
        cancelButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = -4936061169274554494L;

            public void buttonClick(final ClickEvent event)
            {
                getBloodStockHistoryForm().setModel(new BloodStockHistory());
                getBloodStockTempTable().removeAllItems();
                getBloodStockHistoryForm().setComponentError(null);
                selectedHistories = new ArrayList<BloodStockHistory>();
            }
        });

        HorizontalLayout tableButtonLayout = new HorizontalLayout();
        tableButtonLayout.setSpacing(true);
        tableButtonLayout.addComponent(completeButton);
        tableButtonLayout.addComponent(cancelButton);
        tableLayout.addComponent(tableButtonLayout);
        tableLayout.setComponentAlignment(tableButtonLayout, Alignment.MIDDLE_CENTER);

        addComponent(searchLayout);
        addComponent(tableLayout);
        setSizeFull();
    }

    /**
     * <p>
     * Add item to the table.
     * </p>
     * 
     * @param bloodStockHistory
     *            blood stock history instance
     */
    public void addItem(final BloodStockHistory bloodStockHistory)
    {
        getBloodStockTempTable().addItem(
            new Object[] {
                bloodStockHistory.getBloodType(),
                bloodStockHistory.getSubBloodType(),
                bloodStockHistory.getBloodStatus(),
                bloodStockHistory.getRecordDate(),
                bloodStockHistory.getStockCount(),
                createActionColumn(bloodStockHistory)
            }, bloodStockHistory);
    }

    private HorizontalLayout createActionColumn(final BloodStockHistory bloodStockHistory)
    {
        HorizontalLayout actionLayout = new HorizontalLayout();
        Button removeButton = new Button("Remove");
        removeButton.setStyleName(BaseTheme.BUTTON_LINK);
        removeButton.setImmediate(true);
        removeButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = 5997387632610759404L;

            public void buttonClick(final ClickEvent event)
            {
                getBloodStockTempTable().removeItem(bloodStockHistory);
                selectedHistories.remove(bloodStockHistory);
            }
        });

        actionLayout.setSpacing(true);
        actionLayout.addComponent(removeButton);

        return actionLayout;
    }

    private BloodStockHistoryForm getBloodStockHistoryForm()
    {
        if (bloodStockHistorySearchForm == null)
        {
            bloodStockHistorySearchForm = new BloodStockHistoryForm();
        }

        return bloodStockHistorySearchForm;
    }

    private BloodStockTempTable getBloodStockTempTable()
    {
        if (null == bloodStockTempTable)
        {
            bloodStockTempTable = new BloodStockTempTable();
            bloodStockTempTable.setSizeFull();
        }

        return bloodStockTempTable;
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }

    /**
     * <p>
     * Refresh blood stock tab.
     * </p>
     */
    public void refresh()
    {
    }
}
