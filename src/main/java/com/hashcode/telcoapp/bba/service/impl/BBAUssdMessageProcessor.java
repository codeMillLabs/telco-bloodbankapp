/*
 * FILENAME
 *     BBAUssdMessageProcessor.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.service.impl;

import static com.hashcode.telcoapp.common.util.Configuration.getCurrentTime;
import static com.hashcode.telcoapp.common.handlers.Messages.getMessage;
import static com.hashcode.telcoapp.bba.enums.UserRole.DONOR;

import hms.kite.samples.api.SdpException;
import hms.kite.samples.api.ussd.messages.MoUssdReq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.telcoapp.bba.dao.UssdSessionDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.data.UssdSession;
import com.hashcode.telcoapp.bba.enums.UssdState;
import com.hashcode.telcoapp.bba.util.BBAUtils;
import com.hashcode.telcoapp.common.connectors.ILBSConnector;
import com.hashcode.telcoapp.common.enums.SubscriberState;
import com.hashcode.telcoapp.common.handlers.IUssdMessageProcessor;
import com.hashcode.telcoapp.common.service.SubscriberService;
import com.hashcode.telcoapp.common.util.ServiceResolver;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * BBA USSD Message Processor.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
@Transactional
@Service
public class BBAUssdMessageProcessor implements IUssdMessageProcessor
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(BBAUssdMessageProcessor.class);

    /**
     * {@inheritDoc}
     * 
     * @see IUssdMessageProcessor#generateResponse(MoUssdReq)
     */
    public String generateResponse(final MoUssdReq moUssdReq)
    {
        LOGGER.info("USSD Messge Received , {}", moUssdReq.toString());

        String address = moUssdReq.getSourceAddress();
        BBASubscriber subscriber = (BBASubscriber) getService(SubscriberService.class).findSubcriberByAddress(address);
        subscriber = createOrUpdateSubscriber(address, subscriber);

        String sessionId = moUssdReq.getSessionId();
        UssdSession ussdSession = getService(UssdSessionDao.class).findUssdSession(subscriber, sessionId);

        if (ussdSession == null)
        {
            String responseMessage = null;
            UssdState ussdState = null;
            if (subscriber.isBloodStockAccount())
            {
                ussdState = UssdState.BLOOD_STOCK_MAIN_MENU;
                responseMessage = getMessage("ussd.bs.main.menu.message");
            }
            else
            {
                if (null == subscriber.getFirstName() || null == subscriber.getNic() || null == subscriber.getDob())
                {
                    ussdState = UssdState.INITIAL_SET_NAME;
                    responseMessage = getMessage("ussd.initial.set.name.message");
                }
                else
                {
                    ussdState = UssdState.SUBSCRIBER_MAIN_MENU;
                    responseMessage = getMessage("ussd.main.menu.message");
                }
            }

            ussdSession = createUssdSession(subscriber, sessionId, ussdState);
            getService(UssdSessionDao.class).create(ussdSession);
            LOGGER.info("USSD new session was created.");
            return responseMessage;
        }

        String responseMessage =
            ussdSession.getCurrentState().getImplementation().messageReceived(ussdSession, moUssdReq.getMessage());
        getService(UssdSessionDao.class).update(ussdSession);
        createOrUpdateSubscriber(address, subscriber);

        return responseMessage;
    }

    private BBASubscriber createOrUpdateSubscriber(final String address, final BBASubscriber subscriber)
    {
        BBASubscriber newSusbcriber = subscriber;
        if (null == newSusbcriber)
        {
            newSusbcriber = new BBASubscriber();
            newSusbcriber.setAddress(address);
            newSusbcriber.setRegisteredDate(getCurrentTime());
            newSusbcriber.setSubscriberState(SubscriberState.REGISTERED);
            newSusbcriber.getUserRoles().add(DONOR);
            String district = BBAUtils.DEFAULT_DISTRICT;
            try
            {
                district = BBAUtils.getDistrict(getService(ILBSConnector.class).getDistrict(address));
            }
            catch (SdpException e)
            {
                LOGGER.error("Error while gettting district of [{}]", address);
            }
            newSusbcriber.setDistrict(district);
            getService(SubscriberService.class).createSusbcriber(newSusbcriber);
            LOGGER.info("Subscriber created successfully, {}", newSusbcriber);
        }
        else
        {
            getService(SubscriberService.class).updateSubscriber(newSusbcriber);
            LOGGER.info("Subscriber updated successfully, {}", newSusbcriber);
        }

        return newSusbcriber;
    }

    private UssdSession createUssdSession(final BBASubscriber appSubscriber, final String sessionId,
        final UssdState ussdState)
    {
        UssdSession ussdSession = new UssdSession();
        ussdSession.setCurrentState(ussdState);
        ussdSession.setSubscriber(appSubscriber);
        ussdSession.setSessionId(sessionId);

        return ussdSession;
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
