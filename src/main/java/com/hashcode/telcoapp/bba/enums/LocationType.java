/*
 * FILENAME
 *     LocationType.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.enums;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Location type.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
public enum LocationType
{
    BLOOD_BANK("Blood Bank"),
    MOBILE_CAMP("Mobile Camp");

    private String label;

    private LocationType(final String inLabel)
    {
        this.label = inLabel;
    }

    @Override
    public String toString()
    {
        return this.label;
    }
}
