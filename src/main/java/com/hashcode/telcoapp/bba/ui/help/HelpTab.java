/*
 * FILENAME
 *     HelpTab.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.help;

import static com.hashcode.telcoapp.common.handlers.Messages.getMessage;

import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Help ui tab.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
public class HelpTab extends VerticalLayout
{
    private static final long serialVersionUID = -3272251211505306501L;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public HelpTab()
    {
        addComponent(new Label(getMessage("help.html"), Label.CONTENT_XHTML));
        setSizeFull();
    }
}
