/*
 * FILENAME
 *     CampaignDetailsLayout.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.campaings;

import com.hashcode.telcoapp.bba.dao.DonationCenterDao;
import com.hashcode.telcoapp.bba.data.DonationCenter;
import com.hashcode.telcoapp.bba.ui.campaings.CampaignTab.CampaignListener;
import com.hashcode.telcoapp.common.util.ServiceResolver;
import com.vaadin.ui.Button;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.VerticalLayout;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Campaign Details Layout.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class CampaignDetailsLayout extends VerticalLayout
{
    private static final long serialVersionUID = -8749007679159501869L;

    private static final String SAVE = "Save";
    private static final String DISCARD = "Discard";
    private CampaignForm campaignForm;
    private MenuBar menuBar;

    private CampaignListener campaignListener;

    /**
     * <p>
     * Constructor of CampaignDetailsLayout.
     * </p>
     * 
     * @param inCampaignListener
     *            campaign listener
     */
    public CampaignDetailsLayout(final CampaignListener inCampaignListener)
    {
        this.campaignListener = inCampaignListener;
        initiUI();
    }

    private void initiUI()
    {
        setSizeFull();
        addComponent(getMenubar());
        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setMargin(true);
        mainLayout.addComponent(getCampaignForm());

        Button saveButton = new Button("Save");
        saveButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = -8429440738380129818L;

            public void buttonClick(final ClickEvent event)
            {
                save();
            }
        });
        mainLayout.addComponent(saveButton);
        addComponent(mainLayout);
    }

    /**
     * <p>
     * Set Donation Center.
     * </p>
     * 
     * @param donationCenter
     *            Donation Center instance
     */
    public void setModel(final DonationCenter donationCenter)
    {
        getCampaignForm().setModel(donationCenter);
    }

    /**
     * <p>
     * Enable Donation Campaign details ui.
     * 
     * </p>
     * 
     * @param enabled
     *            boolean value whether enabled or not
     */
    public void setUIEnabled(final boolean enabled)
    {
        getMenubar().setEnabled(enabled);
        getCampaignForm().setEnabled(enabled);
    }

    private CampaignForm getCampaignForm()
    {
        if (null == campaignForm)
        {
            campaignForm = new CampaignForm(new DonationCenter());
        }

        return campaignForm;
    }

    private MenuBar getMenubar()
    {
        if (null == menuBar)
        {
            menuBar = new MenuBar();
            menuBar.setSizeFull();
            final Command command = new Command()
            {
                private static final long serialVersionUID = 1L;

                public void menuSelected(final MenuItem inSelectedItem)
                {
                    if (SAVE.equals(inSelectedItem.getText()))
                    {
                        save();
                    }
                    else if (DISCARD.equals(inSelectedItem.getText()))
                    {
                        getCampaignForm().setComponentError(null);
                        getCampaignForm().setModel(new DonationCenter());
                    }
                }
            };

            menuBar.addItem(SAVE, command);
            menuBar.addItem(DISCARD, command);
        }
        return menuBar;
    }

    private void save()
    {
        if (getCampaignForm().validateAllFields())
        {
            DonationCenter donationCenter = getCampaignForm().getModel();
            DonationCenter adminDonationCenter =
                getService(DonationCenterDao.class).findByBloodStockAdmin(donationCenter.getBloodStockAdmin());

            if (donationCenter.getId() == null)
            {
                if (null == adminDonationCenter)
                {
                    getService(DonationCenterDao.class).create(donationCenter);
                    getCampaignForm().setComponentError(null);
                    getWindow().showNotification("Saved ...");
                    getCampaignForm().setModel(new DonationCenter());
                }
                else
                {
                    getWindow().showNotification("Selected admin already assigned to a Blood Bank",
                        Window.Notification.TYPE_ERROR_MESSAGE);
                }
            }
            else
            {
                if (donationCenter.getId().equals(adminDonationCenter.getId()))
                {
                    getCampaignForm().setComponentError(null);
                    getService(DonationCenterDao.class).update(donationCenter);
                    getWindow().showNotification("Updated ...");
                }
                else
                {
                    getWindow().showNotification("Selected admin already assigned to a Blood Bank",
                        Window.Notification.TYPE_ERROR_MESSAGE);
                }
            }

            campaignListener.refresh();
        }

    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
