/*
 * FILENAME
 *     BloodStockSetBloodGroupState.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.state.bs;

import static com.hashcode.telcoapp.common.handlers.Messages.getMessage;

import com.hashcode.telcoapp.bba.data.UssdSession;
import com.hashcode.telcoapp.bba.enums.BloodType;
import com.hashcode.telcoapp.bba.enums.UssdState;
import com.hashcode.telcoapp.bba.state.BBAppState;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood bank set blood group state.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class BloodStockSetBloodGroupState extends BBAppState
{
    /**
     * {@inheritDoc}
     */
    @Override
    public String messageReceived(final UssdSession ussdSession, final String message)
    {
        String responseMessage = "";
        if (message.equals("1"))
        {
            ussdSession.setCurrentState(UssdState.BLOOD_STOCK_SET_AMOUNT_RC);
            ussdSession.setConnectCache(BloodType.A_P.name());
            responseMessage =
                getMessage("ussd.bs.set.rc.amount.message", BloodType.A_P.toString());
        }
        else if (message.equals("2"))
        {
            ussdSession.setCurrentState(UssdState.BLOOD_STOCK_SET_AMOUNT_RC);
            ussdSession.setConnectCache(BloodType.A_N.name());
            responseMessage =
                getMessage("ussd.bs.set.rc.amount.message", BloodType.A_N.toString());
        }
        else if (message.equals("3"))
        {
            ussdSession.setCurrentState(UssdState.BLOOD_STOCK_SET_AMOUNT_RC);
            ussdSession.setConnectCache(BloodType.B_P.name());
            responseMessage =
                getMessage("ussd.bs.set.rc.amount.message", BloodType.B_P.toString());
        }
        else if (message.equals("4"))
        {
            ussdSession.setCurrentState(UssdState.BLOOD_STOCK_SET_AMOUNT_RC);
            ussdSession.setConnectCache(BloodType.B_N.name());
            responseMessage =
                getMessage("ussd.bs.set.rc.amount.message", BloodType.B_N.toString());
        }
        else if (message.equals("5"))
        {
            ussdSession.setCurrentState(UssdState.BLOOD_STOCK_SET_AMOUNT_RC);
            ussdSession.setConnectCache(BloodType.AB_P.name());
            responseMessage =
                getMessage("ussd.bs.set.rc.amount.message", BloodType.AB_P.toString());
        }
        else if (message.equals("6"))
        {
            ussdSession.setCurrentState(UssdState.BLOOD_STOCK_SET_AMOUNT_RC);
            ussdSession.setConnectCache(BloodType.AB_N.name());
            responseMessage =
                getMessage("ussd.bs.set.rc.amount.message", BloodType.AB_N.toString());
        }
        else if (message.equals("7"))
        {
            ussdSession.setCurrentState(UssdState.BLOOD_STOCK_SET_AMOUNT_RC);
            ussdSession.setConnectCache(BloodType.O_P.name());
            responseMessage =
                getMessage("ussd.bs.set.rc.amount.message", BloodType.O_P.toString());
        }
        else if (message.equals("8"))
        {
            ussdSession.setCurrentState(UssdState.BLOOD_STOCK_SET_AMOUNT_RC);
            ussdSession.setConnectCache(BloodType.O_N.name());
            responseMessage =
                getMessage("ussd.bs.set.rc.amount.message", BloodType.O_N.toString());
        }
        else if (message.equals("9"))
        {
            responseMessage = createBloodStockMainMenuMessage(ussdSession);
        }
        else
        {
            responseMessage = createBloodStockInvalidMessage(ussdSession);
        }

        return responseMessage;
    }
}
