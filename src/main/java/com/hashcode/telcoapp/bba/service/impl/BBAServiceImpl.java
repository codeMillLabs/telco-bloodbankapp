/*
 * FILENAME
 *     BBAServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.telcoapp.bba.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.telcoapp.bba.dao.AppSubscriberDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.service.BBAService;
import com.hashcode.telcoapp.common.model.Location;
import com.hashcode.telcoapp.common.service.SubscriberService;
import com.hashcode.telcoapp.common.util.ServiceResolver;

/**
 * <p>
 * Implementation of the {@link BBAService}.
 * </p>
 * 
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 * 
 */
@Transactional
@Service
public class BBAServiceImpl implements BBAService
{

    protected static final Logger LOGGER = LoggerFactory.getLogger(BBAServiceImpl.class);

    private Map<Integer, String> districtMap = new HashMap<Integer, String>();

    /**
     * {@inheritDoc}
     */
    public void saveSubscriber(final BBASubscriber subscriber)
    {
        getService(AppSubscriberDao.class).create(subscriber);
    }

    /**
     * {@inheritDoc}
     */
    public void updateSubscriber(final BBASubscriber subscriber)
    {
        getService(AppSubscriberDao.class).update(subscriber);
    }

    /**
     * {@inheritDoc}
     */
    public BBASubscriber fetchBBASubscriber(final String address)
    {
        BBASubscriber subscriber = (BBASubscriber) getService(SubscriberService.class).findSubcriberByAddress(address);
        return subscriber;
    }

    /**
     * {@inheritDoc}
     */
    public Location getCurrentLocation()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public List<String> getDistricts()
    {

        return (List<String>) getDistrictMap().values();
    }

    /**
     * {@inheritDoc}
     */
    public void addPoints(BBASubscriber subscriber, final int points)
    {
        subscriber = (BBASubscriber) getService(AppSubscriberDao.class).findById(subscriber.getId());
        int totalNewPoints = subscriber.getPoints() + points;

        LOGGER.info("Points updated to subscriber, [ Subscriber : {},  Total points : {} ]", subscriber.getAddress(),
            totalNewPoints);

        subscriber.setPoints(totalNewPoints);
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }

    /**
     * {@inheritDoc}
     */
    public Map<Integer, String> getDistrictMap()
    {
        return districtMap;
    }

    /**
     * {@inheritDoc}
     */
    public void setDistrictMap(final Map<Integer, String> inDistrictMap)
    {
        this.districtMap = inDistrictMap;
    }
}
