/*
 * FILENAME
 *     DonateAsAtTable.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.subscriber;

import static com.hashcode.telcoapp.bba.data.DonateAsAt.DONATION_DATE;
import static com.hashcode.telcoapp.bba.data.DonateAsAt.ID;
import static com.hashcode.telcoapp.bba.data.DonateAsAt.REFERENCE_NUMBER;
import static com.hashcode.telcoapp.bba.data.DonateAsAt.STATE;
import static com.hashcode.telcoapp.common.handlers.Messages.getMessage;
import static com.hashcode.telcoapp.common.util.Configuration.getCurrentTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.hashcode.telcoapp.bba.dao.AppSubscriberDao;
import com.hashcode.telcoapp.bba.dao.DonateAsAtDao;
import com.hashcode.telcoapp.bba.dao.DonateAsAtSearchCriteria;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.data.DonateAsAt;
import com.hashcode.telcoapp.bba.enums.BBAState;
import com.hashcode.telcoapp.bba.service.BulkMsgSenderService;
import com.hashcode.telcoapp.bba.util.BBAUtils;
import com.hashcode.telcoapp.common.ui.dialog.BBADialog;
import com.hashcode.telcoapp.common.ui.dialog.DialogButtonCode;
import com.hashcode.telcoapp.common.ui.dialog.IWindowCallback;
import com.hashcode.telcoapp.common.ui.pagination.EntityContainer;
import com.hashcode.telcoapp.common.ui.pagination.PageableEntityTable2;
import com.hashcode.telcoapp.common.util.ServiceResolver;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.BaseTheme;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Donation as at table.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class DonateAsAtTable extends PageableEntityTable2<DonateAsAt>
{
    private static final long serialVersionUID = -254092208241738040L;
    private static final String ACTION = "action";

    public static final Object[] NATURAL_COL_ORDER1 = new Object[] {
        ACTION,
        ID,
        BBASubscriber.FIRST_NAME,
        BBASubscriber.NIC,
        BBASubscriber.ADDRESS,
        DONATION_DATE,
        REFERENCE_NUMBER
    };

    public static final String[] COL_HEADERS1 = new String[] {
        "Action", "ID", "Name", "NIC", "Address", "Donation Date", "Reference Number"
    };

    public static final Object[] NATURAL_COL_ORDER2 = new Object[] {
        ACTION, ID, DONATION_DATE, REFERENCE_NUMBER, STATE
    };

    public static final String[] COL_HEADERS2 = new String[] {
        "Action", "ID", "Donation Date", "Reference Number", "State"
    };

    private DonateAsAtSearchCriteria searchCriteria;

    /**
     * <p>
     * Subscriber summary table constructor.
     * </p>
     * 
     * @param fullView
     *            full view
     */
    public DonateAsAtTable(final boolean fullView)
    {
        super(DonateAsAt.class);
        setContainerDataSource(new DonateAsAtTableContainer());
        DonateAsAtColumnGenerator columnGenerator = new DonateAsAtColumnGenerator();
        addGeneratedColumn(ACTION, columnGenerator);
        addGeneratedColumn(ID, columnGenerator);
        addGeneratedColumn(DONATION_DATE, columnGenerator);
        addGeneratedColumn(REFERENCE_NUMBER, columnGenerator);
       

        if (fullView)
        {
            addGeneratedColumn(BBASubscriber.FIRST_NAME, columnGenerator);
            addGeneratedColumn(BBASubscriber.NIC, columnGenerator);
            addGeneratedColumn(BBASubscriber.ADDRESS, columnGenerator);
            setColumnWidth(BBASubscriber.FIRST_NAME, 160);
            setColumnWidth(BBASubscriber.NIC, 80);
            setVisibleColumns(NATURAL_COL_ORDER1);
            setColumnHeaders(COL_HEADERS1);
        }
        else
        {
            addGeneratedColumn(STATE, columnGenerator);
            setVisibleColumns(NATURAL_COL_ORDER2);
            setColumnHeaders(COL_HEADERS2);
        }
        setSizeFull();
    }

    /**
     * <p>
     * Refresh table.
     * </p>
     * 
     * @param inSearchCriteria
     *            search criteria to set
     */
    public void refresh(final DonateAsAtSearchCriteria inSearchCriteria)
    {
        inSearchCriteria.resetResultProperties();
        searchCriteria = inSearchCriteria;
        refresh();
    }

    private boolean isSearchInfoNull()
    {
        return null == searchCriteria;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPage()
    {
        return isSearchInfoNull() ? 0 : searchCriteria.getCurrentPage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPageSize()
    {
        return isSearchInfoNull() ? 10 : searchCriteria.getRecordsPerPage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPageSize(final int pageSize)
    {
        if (!isSearchInfoNull())
        {
            searchCriteria.setRecordsPerPage(pageSize);
            refresh();
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setPage(final int page)
    {
        if (!isSearchInfoNull())
        {
            searchCriteria.setCurrentPage(page);
            refresh();
        }
    }

    /**
     * 
     <p>
     * DonateAsAt table column generator.
     * </p>
     * 
     * @author manuja
     * 
     * @version $Id$
     */
    public class DonateAsAtColumnGenerator implements ColumnGenerator
    {
        private static final long serialVersionUID = -3149455993455688593L;

        /**
         * {@inheritDoc}
         */
        public Component generateCell(final Table source, final Object itemId, final Object columnId)
        {
            DonateAsAt donateAsAt = (DonateAsAt) itemId;
            if (columnId.equals(ACTION))
            {
                return createActionColumn(donateAsAt);
            }
            if (columnId.equals(ID))
            {
                return new Label(String.valueOf(donateAsAt.getId()));
            }
            else if (columnId.equals(BBASubscriber.FIRST_NAME))
            {
                return new Label(donateAsAt.getSubscriber().getFirstName());
            }
            else if (columnId.equals(BBASubscriber.NIC))
            {
                return new Label(donateAsAt.getSubscriber().getNic());
            }
            else if (columnId.equals(BBASubscriber.ADDRESS))
            {
                return new Label(donateAsAt.getSubscriber().getAddress());
            }
            else if (columnId.equals(DONATION_DATE))
            {
                return new Label(BBAUtils.formatDate(donateAsAt.getDonatedDate()));
            }
            else if (columnId.equals(REFERENCE_NUMBER))
            {
                return new Label(getToStringValue(donateAsAt.getRefNo()));
            }
            else if (columnId.equals(STATE))
            {
                return new Label(getToStringValue(donateAsAt.getState()));
            }

            return null;
        }

        private HorizontalLayout createActionColumn(final DonateAsAt donateAsAt)
        {
            HorizontalLayout actionColumn = new HorizontalLayout();
            actionColumn.setImmediate(true);
            actionColumn.setSpacing(true);

            Button approveButton = new Button("Approve");
            approveButton.setStyleName(BaseTheme.BUTTON_LINK);
            approveButton.setImmediate(true);
            approveButton.addListener(new ClickListener()
            {
                private static final long serialVersionUID = 2559792940455795501L;

                public void buttonClick(final ClickEvent event)
                {
                    BBADialog bbaDialog =
                        new BBADialog("Approve donation", new Label("Are you sure want to approve this donation ?"),
                            new IWindowCallback()
                            {
                                private static final long serialVersionUID = 1571121871086365911L;

                                public void windowClosed(final Window source, final DialogButtonCode returnCode)
                                {
                                    if (returnCode == DialogButtonCode.CONFIRM)
                                    {
                                        donateAsAt.setState(BBAState.APPROVED);
                                        donateAsAt.getSubscriber().incrementPoints();
                                        donateAsAt.getSubscriber().setLastDonatedDate(getCurrentTime());
                                        getService(DonateAsAtDao.class).update(donateAsAt);
                                        getService(AppSubscriberDao.class).update(donateAsAt.getSubscriber());
                                        refresh(searchCriteria);

                                        getService(BulkMsgSenderService.class).createAndQueueMessages(
                                            getMessage("donation.approval.message", donateAsAt.getRefNo()),
                                            donateAsAt.getSubscriber());
                                    }
                                }
                            }, DialogButtonCode.CONFIRM, DialogButtonCode.CANCEL);

                    getWindow().addWindow(bbaDialog);
                }
            });
            approveButton.setEnabled(donateAsAt.getState() == BBAState.PENDING);
            actionColumn.addComponent(approveButton);
            return actionColumn;
        }

    }

    /**
     * <p>
     * DonateAsAt Container.
     * </p>
     * 
     * @author manuja.
     * 
     * @version $Id$
     */
    public class DonateAsAtTableContainer extends EntityContainer<DonateAsAt>
    {
        private static final long serialVersionUID = 7118635800768356152L;

        /**
         * <p>
         * Default constructor.
         * </p>
         */
        public DonateAsAtTableContainer()
        {
            super(DonateAsAt.class);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<DonateAsAt> getData()
        {
            List<DonateAsAt> results = null;

            if (searchCriteria == null)
            {
                return Collections.emptyList();
            }
            if (searchCriteria.getNumOfRecords() < 0)
            {
                results = getService(DonateAsAtDao.class).fetchDonationHistories(searchCriteria);
                searchCriteria.setNumOfRecords(results.size());
            }

            searchCriteria.setStartPosition(getPage() * getPageSize());
            searchCriteria.setEndPosition(getPageSize());

            results = getService(DonateAsAtDao.class).fetchDonationHistories(searchCriteria);

            return new ArrayList<DonateAsAt>(results);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int getTotalRows()
        {
            return ((searchCriteria == null) ? 0 : searchCriteria.getNumOfRecords());
        }

    }

    private String getToStringValue(final Object value)
    {
        return null != value ? value.toString() : "";
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
