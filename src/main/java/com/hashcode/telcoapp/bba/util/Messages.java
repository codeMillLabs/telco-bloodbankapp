/*
 * FILENAME
 *     Messages.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.util;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Utility class for resolve the messages from the given resource bundle.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 **/
public final class Messages
{
    public static final String BUNDLE_NAME = "messages"; //$NON-NLS-1$
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
    private static final Logger LOGGER = LoggerFactory.getLogger(Messages.class);

    private Messages()
    {
    }

    /**
     * <p>
     * Resolve a message by a key and argument replacements.
     * </p>
     * 
     * @see MessageFormat#format(String, Object...)
     * @param key
     *            the message to look up
     * @param arguments
     *            optional message arguments
     * @return the resolved message
     **/
    public static String getMessage(final String key, final Object... arguments)
    {
        try
        {
            if (arguments != null)
                return MessageFormat.format(resourceBundle.getString(key), arguments);
            return resourceBundle.getString(key);
        }
        catch (MissingResourceException e)
        {
            LOGGER.error("Message key not found: " + key);
            return '!' + key + '!';
        }
    }

}
