/*
 * FILENAME
 *     BulkMsgSummaryTable.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.bulkmessage;

import static com.hashcode.telcoapp.bba.data.DonationCenter.ID;
import static com.hashcode.telcoapp.bba.data.MessageQueue.RESP_MSG;
import static com.hashcode.telcoapp.bba.data.MessageQueue.SCHEDULED_DATE;
import static com.hashcode.telcoapp.bba.data.MessageQueue.SENT_DATE;
import static com.hashcode.telcoapp.bba.data.MessageQueue.STATUS;
import static com.hashcode.telcoapp.bba.data.MessageQueue.TEXT_MESSAGE;
import static com.hashcode.telcoapp.bba.data.MessageQueue.TO_ADDRESS;
import static com.hashcode.telcoapp.bba.util.BBAUtils.formatDateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.hashcode.telcoapp.bba.dao.MessageQueueDao;
import com.hashcode.telcoapp.bba.data.MessageQueue;
import com.hashcode.telcoapp.bba.service.BulkMsgCriteria;
import com.hashcode.telcoapp.common.ui.pagination.EntityContainer;
import com.hashcode.telcoapp.common.ui.pagination.PageableEntityTable2;
import com.hashcode.telcoapp.common.util.ServiceResolver;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Campaign Summary Table.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class BulkMsgSummaryTable extends PageableEntityTable2<MessageQueue>
{
    private static final long serialVersionUID = 8056966242147715586L;

    public static final Object[] NATURAL_COL_ORDER = new Object[] {
        ID, TO_ADDRESS, TEXT_MESSAGE, SCHEDULED_DATE, SENT_DATE, STATUS, RESP_MSG
    };

    public static final String[] COL_HEADERS = new String[] {
        "Id", "To Address", "Text Message", "Scheduled Date", "Sent Time", "Status", "Response Message"
    };

    private BulkMsgCriteria searchCriteria;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public BulkMsgSummaryTable()
    {
        super(MessageQueue.class);
        setContainerDataSource(new BulkMsgSummaryTableContainer());
        BulkMsgSummaryTableColumnGenerator columnGenerator = new BulkMsgSummaryTableColumnGenerator();
        addGeneratedColumn(ID, columnGenerator);
        addGeneratedColumn(TO_ADDRESS, columnGenerator);
        addGeneratedColumn(TEXT_MESSAGE, columnGenerator);
        addGeneratedColumn(SCHEDULED_DATE, columnGenerator);
        addGeneratedColumn(SENT_DATE, columnGenerator);
        addGeneratedColumn(STATUS, columnGenerator);
        addGeneratedColumn(RESP_MSG, columnGenerator);

        setVisibleColumns(NATURAL_COL_ORDER);
        setColumnHeaders(COL_HEADERS);
        setSizeFull();
    }

    /**
     * <p>
     * Bulk Msg Summary Table column generator.
     * </p>
     * 
     * @author Amila Silva
     * 
     * @version $Id$
     */
    private class BulkMsgSummaryTableColumnGenerator implements ColumnGenerator
    {
        private static final long serialVersionUID = 474884755522857818L;

        /**
         * {@inheritDoc}
         */
        public Component generateCell(final Table source, final Object itemId, final Object columnId)
        {
            MessageQueue message = (MessageQueue) itemId;
            if (columnId.equals(ID))
            {
                return new Label(String.valueOf(message.getId()));
            }
            else if (columnId.equals(TO_ADDRESS))
            {
                String toAddress = (message.getToAddress() != null) ? message.getToAddress() : "N/A";
                return new Label(toAddress);
            }
            else if (columnId.equals(TEXT_MESSAGE))
            {
                String txtMessage = (message.getMessage() != null) ? message.getMessage() : "N/A";
                return new Label(txtMessage);
            }
            else if (columnId.equals(SCHEDULED_DATE))
            {
                String dateTime =
                    (message.getScheduledDate() != null) ? formatDateTime(message.getScheduledDate()) : "N/A";
                return new Label(dateTime);
            }
            else if (columnId.equals(SENT_DATE))
            {
                String dateTime = (message.getSentDate() != null) ? formatDateTime(message.getSentDate()) : "N/A";
                return new Label(dateTime);
            }
            else if (columnId.equals(MessageQueue.STATUS))
            {
                return new Label(message.getStatus().getDesc());
            }
            else if (columnId.equals(MessageQueue.RESP_MSG))
            {
                String respMessage = (message.getResponseMsg() != null) ? message.getResponseMsg() : "N/A";
                return new Label(respMessage);
            }
            return null;
        }
    }

    /**
     * <p>
     * BulkMsgSummaryTable Container.
     * </p>
     * 
     * @author Amila Silva.
     * 
     * @version $Id$
     */
    private class BulkMsgSummaryTableContainer extends EntityContainer<MessageQueue>
    {
        private static final long serialVersionUID = -1006272422621255140L;

        public BulkMsgSummaryTableContainer()
        {
            super(MessageQueue.class);
        }

        @Override
        public List<MessageQueue> getData()
        {
            List<MessageQueue> results = null;

            if (searchCriteria == null)
            {
                return Collections.emptyList();
            }

            if (searchCriteria.getNumOfRecords() < 0)
            {
                Integer count = getService(MessageQueueDao.class).countByCriteria(searchCriteria);
                searchCriteria.setNumOfRecords(count);
            }

            searchCriteria.setStartPosition(getPage() * getPageSize());
            searchCriteria.setEndPosition(getPageSize());

            results = getService(MessageQueueDao.class).findByCriteria(searchCriteria);

            return new ArrayList<MessageQueue>(results);
        }

        @Override
        public int getTotalRows()
        {
            return ((searchCriteria == null) ? 0 : searchCriteria.getNumOfRecords());
        }

        private <T> T getService(final Class<T> serviceType)
        {
            return ServiceResolver.findService(serviceType);
        }
    }

    private boolean isSearchInfoNull()
    {
        return null == searchCriteria;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPage(final int page)
    {
        if (!isSearchInfoNull())
        {
            searchCriteria.setCurrentPage(page);
            refresh();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPage()
    {
        return isSearchInfoNull() ? 0 : searchCriteria.getCurrentPage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPageSize()
    {
        return isSearchInfoNull() ? 10 : searchCriteria.getRecordsPerPage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPageSize(final int pageSize)
    {
        if (!isSearchInfoNull())
        {
            searchCriteria.setRecordsPerPage(pageSize);
            refresh();
        }
    }

    /**
     * <p>
     * Refresh table.
     * </p>
     * 
     * @param inSearchCriteria
     *            search criteria to set
     */
    public void refresh(final BulkMsgCriteria inSearchCriteria)
    {
        inSearchCriteria.resetResultProperties();
        searchCriteria = inSearchCriteria;
        refresh();
    }

}
