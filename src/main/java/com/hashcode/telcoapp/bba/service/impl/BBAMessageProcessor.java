/*
 * FILENAME
 *     BBAMessageProcessor.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.service.impl;

import static com.hashcode.telcoapp.bba.enums.UserRole.DONOR;
import static com.hashcode.telcoapp.bba.util.Configuration.REG;
import static com.hashcode.telcoapp.common.util.Configuration.getCurrentTime;
import static com.hashcode.telcoapp.bba.util.Messages.getMessage;
import static com.hashcode.telcoapp.common.enums.SubscriberState.REGISTERED;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.telcoapp.bba.dao.BBAMessageDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.data.Message;
import com.hashcode.telcoapp.bba.util.Configuration;
import com.hashcode.telcoapp.common.entity.ResponseMsg;
import com.hashcode.telcoapp.common.handlers.IMessageProcessor;
import com.hashcode.telcoapp.common.service.SubscriberService;
import com.hashcode.telcoapp.common.util.ApplicationConfiguration;
import com.hashcode.telcoapp.common.util.ServiceResolver;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * BBA Message Processor implementation.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
@Transactional
@Service
public class BBAMessageProcessor implements IMessageProcessor
{

    protected static final Logger LOGGER = LoggerFactory.getLogger(BBAMessageProcessor.class);

    /**
     * {@inheritDoc}
     * 
     * @see IMessageProcessor#generateResponse(String, String, ApplicationConfiguration)
     */
    public List<ResponseMsg> generateResponse(final String message, final String address,
        final ApplicationConfiguration applicationConfiguration)
    {
        LOGGER.info("New message received : [{}], from : [{}]", message, address);
        BBASubscriber subscriber = (BBASubscriber) getService(SubscriberService.class).findSubcriberByAddress(address);

        String[] splitMessageArray = message.trim().split("\\s+");
        Date currentTime = getCurrentTime();
        List<ResponseMsg> responses = new ArrayList<ResponseMsg>();

        subscriber = createOrUpdateSubscriber(address, subscriber, currentTime);

        if (REG.equalsIgnoreCase(splitMessageArray[0]))
        {
            LOGGER.info("Registration request received to process");
            subscriber.getUserRoles().add(DONOR);
            subscriber.setSubscriberState(REGISTERED);
            subscriber.setRegisteredDate(getCurrentTime());
            getService(SubscriberService.class).updateSubscriber(subscriber);

            responses = createResponseList(getMessage("welcome.message"), null);
        }
        else if (isHelpRequest(splitMessageArray))
        {
            LOGGER.info("Help request received to process");
            responses.add(createResponse(getMessage("help.message"), null, null));
            return responses;
        }
        else
        {
            LOGGER.info("Other request received");
            Message requestMessage = new Message();
            requestMessage.setSubscriber(subscriber);
            requestMessage.setMessageBody(message);
            requestMessage.setDate(getCurrentTime());
            getService(BBAMessageDao.class).create(requestMessage);
            responses = createResponseList(getMessage("request.success"), null);
        }

        return responses;
    }

    private boolean isHelpRequest(final String[] splitMessageArray)
    {
        return splitMessageArray.length == 2 && Configuration.HELP.equalsIgnoreCase(splitMessageArray[1]);
    }

    private BBASubscriber createOrUpdateSubscriber(final String address, BBASubscriber subscriber,
        final Date currentTime)
    {
        if (null == subscriber)
        {
            subscriber = new BBASubscriber();
            subscriber.setAddress(address);
            subscriber.setRegisteredDate(currentTime);
            subscriber.getUserRoles().add(DONOR);
            subscriber.setSubscriberState(REGISTERED);
            getService(SubscriberService.class).createSusbcriber(subscriber);
            LOGGER.info("Subscriber created successfully, {}", subscriber.getUserName());
        }
        else
        {
            getService(SubscriberService.class).updateSubscriber(subscriber);
            LOGGER.info("Subscriber updated successfully, {}", subscriber.getUserName());
        }
        return subscriber;
    }

    private List<ResponseMsg> createResponseList(final String message, final String ussdMessage)
    {
        List<ResponseMsg> alerts = new ArrayList<ResponseMsg>();
        alerts.add(createResponse(message, null, ussdMessage));
        return alerts;
    }

    private ResponseMsg createResponse(final String message, final String address, final String ussdMessage)
    {
        ResponseMsg alert = new ResponseMsg();
        alert.setMessage(message);
        alert.setUssdMessage(ussdMessage);
        if (null != address)
            alert.setAddress(address);
        return alert;
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
