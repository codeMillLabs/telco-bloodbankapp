/*
 * FILENAME
 *     MessageService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.dao;

import java.util.List;

import com.hashcode.telcoapp.bba.data.Message;
import com.hashcode.telcoapp.common.dao.GenericDao;

/**
 * <p>
 * Message Dao.
 * </p>
 * 
 * @author manuja
 * 
 * @version $Id$
 */
public interface BBAMessageDao extends GenericDao<Message, Long>
{
    /**
     * <p>
     * Find by search criteria.
     * </p>
     *
     * @param searchCriteria search criteria
     * 
     * @return list of messages
     */
    List<Message> findBySearchCriteria(MessageSearchCriteria searchCriteria);
}
