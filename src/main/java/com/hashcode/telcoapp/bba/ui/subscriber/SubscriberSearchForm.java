/*
 * FILENAME
 *     SubscriberSearchForm.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2007 Genix Ventures Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     Genix Ventures ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with Genix Ventures.
 */

package com.hashcode.telcoapp.bba.ui.subscriber;

import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createBloodTypeField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createDateField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createDistrictField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createTextField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createGenderField;

import java.util.Arrays;
import java.util.List;

import com.hashcode.telcoapp.bba.service.BulkMsgCriteria;
import com.hashcode.telcoapp.common.ui.components.MultiColumnForm;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Subscriber Search Form.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class SubscriberSearchForm extends MultiColumnForm<BulkMsgCriteria>
{
    private static final long serialVersionUID = 7809500383598783545L;

    /**
     * <p>
     * SubscriberForm constructor.
     * </p>
     * 
     * @param subscriber
     *            subscriber instance
     */
    public SubscriberSearchForm(final BulkMsgCriteria subscriber)
    {
        super(2);
        setFormFieldFactory(new SubscriberSearchFormFields());
        setModel(subscriber);
    }

    @Override
    protected List<String> getVisiblePropertyIds()
    {
        return Arrays.asList(new String[] {
            BulkMsgCriteria.NIC,
            BulkMsgCriteria.DISTRICT,
            BulkMsgCriteria.BLOOD_TYPE,
            BulkMsgCriteria.GENDER,
            BulkMsgCriteria.ELIGIBLE_DATE,
            BulkMsgCriteria.POINTS,
        });
    }

    @Override
    protected int[] getPropertyColumnIndices()
    {
        return new int[] {
            0, 0, 0, 1, 1, 1
        };
    }

    /**
     * <p>
     * Subscriber Form Fields.
     * </p>
     * 
     * @author manuja
     * 
     * @version $Id$
     */
    private class SubscriberSearchFormFields extends DefaultFieldFactory
    {
        private static final long serialVersionUID = 6748765749920212212L;

        /**
         * {@inheritDoc}
         */
        @Override
        public Field createField(final Item item, final Object propertyId, final Component uiContext)
        {
            String pid = (String) propertyId;
            if (pid.equals(BulkMsgCriteria.NIC))
            {
                return createTextField("NIC", false);
            }
            else if (pid.equals(BulkMsgCriteria.DISTRICT))
            {
                return createDistrictField("District");
            }
            else if (pid.equals(BulkMsgCriteria.BLOOD_TYPE))
            {
                return createBloodTypeField("Blood Group");
            }
            else if (pid.equals(BulkMsgCriteria.GENDER))
            {
                return createGenderField("Gender");
            }
            else if (pid.equals(BulkMsgCriteria.ELIGIBLE_DATE))
            {
                return createDateField("Eligible Date", false);
            }
            else if (pid.equals(BulkMsgCriteria.POINTS))
            {
                return createTextField("Points", false);
            }

            return super.createField(item, propertyId, uiContext);
        }
    }
}
