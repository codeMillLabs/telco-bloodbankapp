/*
 * FILENAME
 *     SubscriberSummaryLayout.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.subscriber;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import au.com.bytecode.opencsv.CSVWriter;

import com.hashcode.telcoapp.bba.dao.AppSubscriberDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.service.BulkMsgCriteria;
import com.hashcode.telcoapp.bba.ui.subscriber.SubscriberTab.SubscriberListener;
import com.hashcode.telcoapp.bba.util.BBAUtils;
import com.hashcode.telcoapp.bba.util.Configuration;
import com.hashcode.telcoapp.common.util.ServiceResolver;
import com.vaadin.data.Property;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.VerticalLayout;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Subscriber summary layout.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class SubscriberSummaryLayout extends VerticalLayout
{
    private static final long serialVersionUID = -8335540581158440173L;
    private static final String EXPORT_ALL = "Export All";
    private SubscriberSummaryTable subscriberSummaryTable;
    private SubscriberSearchForm subscriberSearchForm;
    private final SubscriberListener subscriberListener;
    private MenuBar menuBar;

    private static final int RECORDS_PER_PAGE = 15;

    /**
     * <p>
     * Constructor of SubscriberSummaryLayout.
     * </p>
     * 
     * @param inSubscriberListener
     *            subscriber listner reference
     */
    public SubscriberSummaryLayout(final SubscriberListener inSubscriberListener)
    {
        this.subscriberListener = inSubscriberListener;
        initUI();
    }

    private void initUI()
    {
        addComponent(getMenubar());

        VerticalLayout searchLayout = new VerticalLayout();
        searchLayout.addComponent(getSubscriberSearchForm());

        Button searchButton = new Button("Search");
        searchButton.setImmediate(true);
        searchButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = -7291423273138687511L;

            public void buttonClick(final ClickEvent event)
            {
                getSubscriberSummaryTable().refresh(getSubscriberSearchForm().getModel());
            }
        });

        Button clearButton = new Button("Clear");
        clearButton.setImmediate(true);
        clearButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = -7291423273138687511L;

            public void buttonClick(final ClickEvent event)
            {
                BulkMsgCriteria msgCrit = new BulkMsgCriteria();
                msgCrit.setRecordsPerPage(RECORDS_PER_PAGE);
                getSubscriberSearchForm().setModel(msgCrit);
                getSubscriberSummaryTable().refresh(msgCrit);
            }
        });

        HorizontalLayout searchButtonLayout = new HorizontalLayout();
        searchButtonLayout.setSpacing(true);
        searchButtonLayout.addComponent(searchButton);
        searchButtonLayout.addComponent(clearButton);
        searchLayout.addComponent(searchButtonLayout);
        searchLayout.setComponentAlignment(searchButtonLayout, Alignment.MIDDLE_CENTER);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.addComponent(getSubscriberSummaryTable().getPaginator());
        tableLayout.addComponent(getSubscriberSummaryTable());

        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setMargin(true);
        mainLayout.addComponent(searchLayout);
        mainLayout.addComponent(tableLayout);

        addComponent(mainLayout);
        setSizeFull();
    }

    private SubscriberSummaryTable getSubscriberSummaryTable()
    {
        if (null == subscriberSummaryTable)
        {
            subscriberSummaryTable = new SubscriberSummaryTable();
            subscriberSummaryTable.addListener(new Property.ValueChangeListener()
            {
                private static final long serialVersionUID = -5477721735252648455L;

                /**
                 * {@inheritDoc}
                 */
                public void valueChange(final Property.ValueChangeEvent event)
                {
                    subscriberListener.changed((BBASubscriber) event.getProperty().getValue());
                }
            });
        }

        return subscriberSummaryTable;
    }

    private SubscriberSearchForm getSubscriberSearchForm()
    {
        if (null == subscriberSearchForm)
        {
            BulkMsgCriteria msgCrit = new BulkMsgCriteria();
            msgCrit.setRecordsPerPage(RECORDS_PER_PAGE);
            subscriberSearchForm = new SubscriberSearchForm(msgCrit);
        }

        return subscriberSearchForm;
    }

    private MenuBar getMenubar()
    {
        if (null == menuBar)
        {
            menuBar = new MenuBar();
            menuBar.setSizeFull();
            final Command command = new Command()
            {
                private static final long serialVersionUID = -7536145265164180948L;

                public void menuSelected(final MenuItem inSelectedItem)
                {
                    if (EXPORT_ALL.equals(inSelectedItem.getText()))
                    {
                        try
                        {
                            String fileName = writeCSVData((getService(AppSubscriberDao.class)).findAll());
                            String fileURL = Configuration.getMessage("csv.download.path") + fileName;
                            getWindow().open(new ExternalResource(fileURL));
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }

            };

            menuBar.addItem(EXPORT_ALL, command);
        }
        return menuBar;
    }

    private String writeCSVData(final List<BBASubscriber> subscribers) throws IOException
    {
        String fileName = String.valueOf(System.currentTimeMillis()) + ".csv";
        String csv = Configuration.getMessage("csv.generation.path") + fileName;
        CSVWriter csvWriter = new CSVWriter(new FileWriter(csv), ',');
        List<String[]> data = toStringArray(subscribers);
        csvWriter.writeAll(data);
        csvWriter.close();

        return fileName;
    }

    private List<String[]> toStringArray(final List<BBASubscriber> subscribers)
    {
        List<String[]> records = new ArrayList<String[]>();
        records.add(new String[] {
            "ID",
            "NIC",
            "Name",
            "MSISDN",
            "Gender",
            "Blood Type",
            "District",
            "Points",
            "Subscriber State",
            "Registered Date",
            "Last Modified Date",
            "Last Donation Date",
            "Next Eligible Date",
            "Blood Stock Account",
            "AP Donor",
            "Remarks"
        });
        Iterator<BBASubscriber> it = subscribers.iterator();
        while (it.hasNext())
        {
            BBASubscriber subscriber = it.next();
            records.add(new String[] {
                String.valueOf(subscriber.getId()),
                subscriber.getNic(),
                subscriber.getFirstName(),
                subscriber.getAddress(),
                getToStringValue(subscriber.getGender()),
                getToStringValue(subscriber.getBloodType()),
                subscriber.getDistrict(),
                String.valueOf(subscriber.getPoints()),
                getToStringValue(subscriber.getSubscriberState()),
                BBAUtils.formatDate(subscriber.getRegisteredDate()),
                BBAUtils.formatDate(subscriber.getLastModifiedDate()),
                BBAUtils.formatDate(subscriber.getLastDonatedDate()),
                BBAUtils.formatDate(subscriber.getNextEligibleDate()),
                String.valueOf(subscriber.isBloodStockAccount()),
                String.valueOf(subscriber.isApDonor()),
                subscriber.getRemarks()
            });
        }
        return records;
    }

    private String getToStringValue(final Object value)
    {
        return null != value ? value.toString() : "";
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
