/*
 * FILENAME
 *     StatusCodes.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.android.api.domain;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Status Codes.
 * </p>
 *
 * @see TODO Related Information
 *
 * @author Manuja
 *
 * @version $Id$
 *
 */
public enum StatusCodes
{
    S1000("S1000", "Success"),
    S5000("S5000", "Error"),
    S4012("S4012", "No Data"),
    S4014("S4014", "Invalid Center");
    
    String shortCode;
    String desc;

    private StatusCodes(final String inShortCode, final String inDesc)
    {
        this.shortCode = inShortCode;
        this.desc = inDesc;
    }

    /**
     * <p>
     * Getter for desc.
     * </p>
     * 
     * @return the desc
     */
    public String getDesc()
    {
        return desc;
    }
    
    /**
     * <p>
     * Getter for shortCode.
     * </p>
     * 
     * @return the shortCode
     */
    public String getShortCode()
    {
        return shortCode;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString()
    {
        return desc;
    }
}
