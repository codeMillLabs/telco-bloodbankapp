/*
 * FILENAME
 *     BBAppState.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.state;

import static com.hashcode.telcoapp.common.handlers.Messages.getMessage;
import static com.hashcode.telcoapp.common.util.Configuration.getCurrentTime;

import java.util.Date;
import java.util.List;

import com.hashcode.telcoapp.bba.dao.DonationCenterDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.data.BloodStockHistory;
import com.hashcode.telcoapp.bba.data.DonationCenter;
import com.hashcode.telcoapp.bba.data.UssdSession;
import com.hashcode.telcoapp.bba.enums.BloodType;
import com.hashcode.telcoapp.bba.enums.LocationType;
import com.hashcode.telcoapp.bba.enums.SubBloodType;
import com.hashcode.telcoapp.bba.enums.UssdState;
import com.hashcode.telcoapp.bba.util.BBAUtils;
import com.hashcode.telcoapp.common.util.ServiceResolver;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood bank application state interface.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public abstract class BBAppState
{
    /**
     * <p>
     * Message Received.
     * </p>
     * 
     * @param ussdSession
     *            ussd session
     * @param message
     *            message
     * 
     * @return string response
     */
    public abstract String messageReceived(UssdSession ussdSession, String message);

    /**
     * <p>
     * Create invalid message.
     * </p>
     * 
     * @param ussdSession
     *            ussd session
     * 
     * @return invalid message
     */
    protected String createInvalidMessage(final UssdSession ussdSession)
    {
        ussdSession.setCurrentState(UssdState.SUBSCRIBER_INVALID);
        return getMessage("ussd.invalid.message");
    }
    
    /**
     * <p>
     * Create invalid message.
     * </p>
     * 
     * @param ussdSession
     *            ussd session
     * 
     * @return invalid message
     */
    protected String createProfileInvalidMessage(final UssdSession ussdSession)
    {
        ussdSession.setCurrentState(UssdState.SUBSCRIBER_PROFILE_INVALID);
        return getMessage("ussd.profile.invalid.message");
    }

    /**
     * <p>
     * Create blood stock invalid message.
     * </p>
     * 
     * @param ussdSession
     *            ussd session
     * 
     * @return invalid message
     */
    protected String createBloodStockInvalidMessage(final UssdSession ussdSession)
    {
        ussdSession.setCurrentState(UssdState.BLOOD_STOCK_INVALID);
        return getMessage("ussd.invalid.message");
    }

    /**
     * <p>
     * Create main menu message.
     * </p>
     * 
     * @param ussdSession
     *            ussd session
     * 
     * @return main menu message
     */
    protected String createMainMenuMessage(final UssdSession ussdSession)
    {
        ussdSession.setCurrentState(UssdState.SUBSCRIBER_MAIN_MENU);
        return getMessage("ussd.main.menu.message");
    }

    /**
     * <p>
     * Create district message.
     * </p>
     * 
     * @param ussdSession
     *            ussd session
     * @return district message
     */
    protected String createDistrictMessage(final UssdSession ussdSession)
    {
        ussdSession.setCurrentState(UssdState.SELECT_DISTRICT);
        return getMessage("ussd.select.district.message");
    }

    /**
     * <p>
     * Create first name message.
     * </p>
     * 
     * @param ussdSession
     *            ussd session
     * @return first name message
     */
    protected String createSetNameMessage(final UssdSession ussdSession)
    {
        ussdSession.setCurrentState(UssdState.SET_NAME);
        return getMessage("ussd.profile.set.name.message");
    }

    /**
     * <p>
     * Create blood stock main menu message.
     * </p>
     * 
     * @param ussdSession
     *            ussd session
     * @return blood stock main menu message
     */
    protected String createBloodStockMainMenuMessage(final UssdSession ussdSession)
    {
        ussdSession.setCurrentState(UssdState.BLOOD_STOCK_MAIN_MENU);
        return getMessage("ussd.bs.main.menu.message");
    }

    /**
     * <p>
     * Create profile message.
     * </p>
     * 
     * @param ussdSession
     *            ussd session
     * @return district message
     */
    protected String createProfileMessage(final UssdSession ussdSession)
    {
        ussdSession.setCurrentState(UssdState.SUBSCRIBER_PROFILE);
        BBASubscriber subscriber = ussdSession.getSubscriber();
        StringBuilder profileDetails = new StringBuilder();
        if (null != subscriber.getNic())
        {
            profileDetails.append("NIC : ");
            profileDetails.append(subscriber.getNic());
            profileDetails.append("\n");
        }
        if (null != subscriber.getFirstName())
        {
            profileDetails.append("Name : ");
            profileDetails.append(subscriber.getFirstName());
            profileDetails.append("\n");
        }
        if (null != subscriber.getGender())
        {
            profileDetails.append("Gender : ");
            profileDetails.append(subscriber.getGender());
            profileDetails.append("\n");
        }
        if (null != subscriber.getBloodType())
        {
            profileDetails.append("Blood Group : ");
            profileDetails.append(subscriber.getBloodType());
            profileDetails.append("\n");
        }
        profileDetails.append("Points : ");
        profileDetails.append(subscriber.getPoints());
        profileDetails.append("\n");
        return getMessage("ussd.subscriber.profile.message", profileDetails.toString());
    }

    /**
     * <p>
     * Blood stock menu, set blood group message.
     * </p>
     * 
     * @param ussdSession
     *            ussd session
     * 
     * @return set blood group message
     */
    protected String createSetBloodGroupMessage(final UssdSession ussdSession)
    {
        ussdSession.setCurrentState(UssdState.BLOOD_STOCK_SET_BLOOD_GROUP);
        return getMessage("ussd.profile.set.blood.group.message");
    }

    /**
     * <p>
     * Create find branch list message.
     * </p>
     * 
     * @param ussdSession
     *            ussd session
     * @param district
     *            selected district
     * 
     * @return main menu message
     */
    protected String createDistrictSelectedMessage(final UssdSession ussdSession, final String district)
    {
        String responseMessage = "";
        String connectCache = getAndRemoveConnectCache(ussdSession);
        if (connectCache.equals(UssdState.FIND_BRANCHES.toString()))
        {
            ussdSession.setCurrentState(UssdState.BRANCH_LIST);
            responseMessage =
                getMessage("ussd.find.branches.result.message",
                    getCampaignResponse(district, LocationType.BLOOD_BANK));
        }
        else if (connectCache.equals(UssdState.FIND_CAMPS.toString()))
        {
            ussdSession.setCurrentState(UssdState.CAMP_LIST);
            responseMessage =
                getMessage("ussd.find.camps.result.message",
                    getCampaignResponse(district, LocationType.MOBILE_CAMP));
        }
        else if (connectCache.equals(UssdState.SUBSCRIBER_SET_LOCATION.toString()))
        {
            ussdSession.setCurrentState(UssdState.SUBSCRIBER_SUCCESS);
            ussdSession.getSubscriber().setDistrict(district);
            responseMessage = getMessage("ussd.profile.set.location.success.message", district);
        }

        return responseMessage;
    }

    private String getCampaignResponse(final String district, final LocationType locationType)
    {
        StringBuilder responseMessage = new StringBuilder();
        List<DonationCenter> donationCenters =
            getService(DonationCenterDao.class).fetchDonationCentersForDistrict(district, locationType,
                getCurrentTime(), -1);
        if (donationCenters.isEmpty())
        {
            responseMessage.append("No results found");
        }
        else
        {
            // int count = 1;
            for (DonationCenter donationCenter : donationCenters)
            {
                if (responseMessage.length() > 0)
                {
                    responseMessage.append("\n");
                }
                Date donationDate = donationCenter.getDonationDate();
                responseMessage.append("** ");
                responseMessage.append(donationCenter.getAddress());
                responseMessage.append(", ");
                responseMessage.append(donationCenter.getCity());
                responseMessage.append(donationDate == null ? "" : " on " + BBAUtils.formatDate(donationDate));
                // count++;
            }
        }
        return responseMessage.toString();
    }

    /**
     * <p>
     * Get connect cache and remove that value from ussd session.
     * </p>
     * 
     * @param ussdSession
     *            ussd session
     * @return string connect cache
     */
    protected String getAndRemoveConnectCache(final UssdSession ussdSession)
    {
        String connectCache = ussdSession.getConnectCache();
        ussdSession.setConnectCache(null);
        return connectCache;
    }
    
    /**
     * <p>
     * Create blood stock history instance.
     * </p>
     *
     * @param bloodType blood group
     * @param amount amount of pints
     * @param donationCenter donation center
     * @param subBloodType sub blood type
     * 
     * @return blood stock history instance
     */
    protected BloodStockHistory createBloodStockHistory(final BloodType bloodType, final int amount,
        final DonationCenter donationCenter, final SubBloodType subBloodType)
    {
        BloodStockHistory bloodStockHistory = new BloodStockHistory();
        bloodStockHistory.setBloodType(bloodType);
        bloodStockHistory.setStockCount(amount);
        bloodStockHistory.setCenter(donationCenter);
        bloodStockHistory.setSubBloodType(subBloodType);
        return bloodStockHistory;
    }

    private <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
