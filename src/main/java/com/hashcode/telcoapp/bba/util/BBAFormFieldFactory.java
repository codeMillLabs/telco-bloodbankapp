/*
 * FILENAME
 *     BBAFields.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.util;

import static com.hashcode.telcoapp.bba.enums.BBAState.FAILED;
import static com.hashcode.telcoapp.bba.enums.BBAState.FAST_MSGS;
import static com.hashcode.telcoapp.bba.enums.BBAState.PENDING;
import static com.hashcode.telcoapp.bba.enums.BBAState.SENT;

import java.util.List;

import com.hashcode.telcoapp.bba.dao.AppSubscriberDao;
import com.hashcode.telcoapp.bba.dao.DonationCenterDao;
import com.hashcode.telcoapp.bba.data.BBASubscriber;
import com.hashcode.telcoapp.bba.data.DonationCenter;
import com.hashcode.telcoapp.bba.enums.BloodStatus;
import com.hashcode.telcoapp.bba.enums.BloodType;
import com.hashcode.telcoapp.bba.enums.LocationType;
import com.hashcode.telcoapp.bba.enums.SubBloodType;
import com.hashcode.telcoapp.bba.enums.UserRole;
import com.hashcode.telcoapp.bba.service.CampaignCriteria;
import com.hashcode.telcoapp.common.enums.Gender;
import com.hashcode.telcoapp.common.util.ServiceResolver;
import com.vaadin.ui.DateField;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Select;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * BBA Form Field Factory.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public final class BBAFormFieldFactory
{
    private BBAFormFieldFactory()
    {
    }

    /**
     * <p>
     * Create text field.
     * </p>
     * 
     * @param caption
     *            caption string
     * @param readOnly
     *            whether read only or not
     * 
     * @return text field instance
     */
    public static TextField createTextField(final String caption, final boolean readOnly)
    {
        TextField textField = new TextField(caption);
        textField.setReadOnly(readOnly);
        textField.setNullRepresentation("");
        return textField;
    }

    /**
     * <p>
     * Create text area.
     * </p>
     * 
     * @param caption
     *            caption string
     * @param readOnly
     *            whether read only or not
     * 
     * @return text area instance
     */
    public static TextArea createTextArea(final String caption, final boolean readOnly)
    {
        TextArea textArea = new TextArea(caption);
        textArea.setReadOnly(readOnly);
        textArea.setRows(5);
        textArea.setWidth("200px");
        textArea.setNullRepresentation("");
        return textArea;
    }

    /**
     * <p>
     * Create date field.
     * </p>
     * 
     * @param caption
     *            caption string
     * @param readOnly
     *            whether read only or not
     * 
     * @return date field instance
     */
    public static DateField createDateField(final String caption, final boolean readOnly)
    {
        DateField dateField = new DateField(caption);
        dateField.setReadOnly(readOnly);
        dateField.setResolution(DateField.RESOLUTION_DAY);
        return dateField;
    }

    /**
     * <p>
     * Create blood type select field.
     * </p>
     * 
     * @param caption
     *            caption string
     * 
     * @return blood type select field instance
     */
    public static Select createBloodTypeField(final String caption)
    {
        Select select = new Select(caption);
        select.setImmediate(true);
        for (BloodType bloodType : BloodType.values())
        {
            select.addItem(bloodType);
        }
        return select;
    }
    
    /**
     * <p>
     * Create sub blood type select field.
     * </p>
     * 
     * @param caption
     *            caption string
     * 
     * @return blood type select field instance
     */
    public static Select createSubBloodTypeField(final String caption)
    {
        Select select = new Select(caption);
        select.setImmediate(true);
        for (SubBloodType subBloodType : SubBloodType.values())
        {
            select.addItem(subBloodType);
        }
        return select;
    }
    
    /**
     * <p>
     * Create blood status select field.
     * </p>
     * 
     * @param caption
     *            caption string
     * 
     * @return blood status select field instance
     */
    public static Select createBloodStatusField(final String caption)
    {
        Select select = new Select(caption);
        select.setImmediate(true);
        for (BloodStatus bloodStatus : BloodStatus.values())
        {
            select.addItem(bloodStatus);
        }
        return select;
    }

    /**
     * <p>
     * Create location type select field.
     * </p>
     * 
     * @param caption
     *            caption string
     * 
     * @return location type select field instance
     */
    public static Select createLocationTypeField(final String caption)
    {
        Select select = new Select(caption);
        select.setImmediate(true);
        LocationType[] locationTypes = LocationType.values();
        for (LocationType locationType : locationTypes)
        {
            select.addItem(locationType);
        }
        return select;
    }

    /**
     * <p>
     * Create district field.
     * </p>
     * 
     * @param caption
     *            caption string
     * 
     * @return district select field
     */
    public static Select createDistrictField(final String caption)
    {
        Select select = new Select(caption);
        select.setImmediate(true);
        select.addItem("Ampara");
        select.addItem("Anuradhapura");
        select.addItem("Badulla");
        select.addItem("Batticaloa");
        select.addItem("Colombo");
        select.addItem("Galle");
        select.addItem("Gampaha");
        select.addItem("Hambantota");
        select.addItem("Jaffna");
        select.addItem("Kalutara");
        select.addItem("Kandy");
        select.addItem("Kegalle");
        select.addItem("Kilinochchi");
        select.addItem("Kurunegala");
        select.addItem("Mannar");
        select.addItem("Matale");
        select.addItem("Matara");
        select.addItem("Moneragala");
        select.addItem("Mullaitivu");
        select.addItem("Nuwara Eliya");
        select.addItem("Polonnaruwa");
        select.addItem("Puttalam");
        select.addItem("Ratnapura");
        select.addItem("Trincomalee");
        select.addItem("Vavuniya");

        return select;
    }

    /**
     * <p>
     * Create gender select field.
     * </p>
     * 
     * @param caption
     *            caption string
     * 
     * @return gender select field instane
     */
    public static Select createGenderField(final String caption)
    {
        Select select = new Select(caption);
        select.setImmediate(true);
        for (Gender gender : Gender.values())
        {
            select.addItem(gender);
        }
        return select;
    }

    /**
     * <p>
     * Select blood stock admin select field.
     * </p>
     * 
     * @param caption
     *            caption string
     * 
     * @return blood stock admin select field
     */
    public static Select createBloodStockAdminSelectField(final String caption)
    {
        Select select = new Select(caption);
        select.setImmediate(true);
        List<BBASubscriber> bloodSotckAdminSubscriberList =
            getService(AppSubscriberDao.class).findBloodStockAdmins();
        for (BBASubscriber subscriber : bloodSotckAdminSubscriberList)
        {
            if (null != subscriber.getFirstName())
            {
                select.addItem(subscriber);
                select.setItemCaption(subscriber, subscriber.getFirstName());
            }
        }
        return select;
    }

    /**
     * <p>
     * Create donation center select field by location type.
     * </p>
     *
     * @param caption caption string
     * @param locationType location type
     * 
     * @return created select field
     */
    public static Select createDonationCenterSelectFieldByLocationType(final String caption,
        final LocationType locationType)
    {
        Select select = new Select(caption);
        select.setImmediate(true);
        CampaignCriteria campaignCriteria = new CampaignCriteria();
        campaignCriteria.setLocationType(locationType);
        List<DonationCenter> donationCenters = getService(DonationCenterDao.class).findByCriteria(campaignCriteria);
        for (DonationCenter center : donationCenters)
        {
            select.addItem(center);
            select.setItemCaption(center, center.getCenterName());
        }
        return select;
    }
    
    /**
     * <p>
     * Select User Roles Select Field.
     * </p>
     * 
     * @param caption
     *            caption string
     * 
     * @return User Roles select field
     */
    public static OptionGroup createUserRolesField(final String caption)
    {
        OptionGroup optionGrp = new OptionGroup(caption);
        optionGrp.setMultiSelect(true);
        optionGrp.setImmediate(true);

        for (UserRole role : UserRole.values())
        {
            optionGrp.addItem(role);
            optionGrp.setItemCaption(role, role.getRoleDesc());
        }
        return optionGrp;
    }
    
    /**
     * <p>
     * Message status Select Field.
     * </p>
     * 
     * @param caption
     *            caption string
     * 
     * @return Message status select field
     */
    public static OptionGroup createMsgStatusField(final String caption)
    {
        OptionGroup optionGrp = new OptionGroup(caption);
        optionGrp.setMultiSelect(true);
        optionGrp.setImmediate(true);

        optionGrp.addItem(PENDING);
        optionGrp.setItemCaption(PENDING, PENDING.getDesc());
        
        optionGrp.addItem(FAST_MSGS);
        optionGrp.setItemCaption(FAST_MSGS, FAST_MSGS.getDesc());
        
        optionGrp.addItem(SENT);
        optionGrp.setItemCaption(SENT, SENT.getDesc());
        
        optionGrp.addItem(FAILED);
        optionGrp.setItemCaption(FAILED, FAILED.getDesc());
        
        return optionGrp;
    }

    private static <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }
}
