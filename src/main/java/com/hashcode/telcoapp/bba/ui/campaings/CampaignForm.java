/*
 * FILENAME
 *     CampaignForm.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.campaings;

import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createDateField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createDistrictField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createLocationTypeField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createTextField;
import static com.hashcode.telcoapp.bba.util.BBAFormFieldFactory.createBloodStockAdminSelectField;

import java.util.Arrays;
import java.util.List;

import com.hashcode.telcoapp.bba.data.DonationCenter;
import com.hashcode.telcoapp.bba.enums.LocationType;
import com.hashcode.telcoapp.common.ui.components.MultiColumnForm;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Select;
import com.vaadin.ui.TextField;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Campaign Form.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class CampaignForm extends MultiColumnForm<DonationCenter>
{
    private static final long serialVersionUID = 1069870129793763923L;

    /**
     * <p>
     * Constructor of CampaignForm.
     * </p>
     * 
     * @param donationCenter
     *            donation center instance
     */
    public CampaignForm(final DonationCenter donationCenter)
    {
        super(1);
        setFormFieldFactory(new CampaignFormField());
        setModel(donationCenter);
    }

    /**
     * {@inheritDoc}.
     * 
     * @see com.hashcode.telcoapp.bba.ui.components.MultiColumnForm#setModel(java.io.Serializable)
     */
    @Override
    public void setModel(final DonationCenter model)
    {
        super.setModel(model);

        Field locationTypeField = getField(DonationCenter.LOC_TYPE);
        final Field donationDateField = getField(DonationCenter.DONATION_DATE);
        final Field bloodStockAdminField = getField(DonationCenter.BLOOD_STOCK_ADMIN);
        if (LocationType.MOBILE_CAMP.equals(locationTypeField.getValue()))
        {
            donationDateField.setEnabled(true);
            donationDateField.setRequired(true);
            bloodStockAdminField.setEnabled(false);
        }
        else if (LocationType.BLOOD_BANK.equals(locationTypeField.getValue()))
        {
            bloodStockAdminField.setEnabled(true);
            bloodStockAdminField.setRequired(true);
            donationDateField.setEnabled(false);
        }

        locationTypeField.addListener(new Property.ValueChangeListener()
        {
            private static final long serialVersionUID = -1329693432753695949L;

            public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event)
            {
                Object value = event.getProperty().getValue();

                if (LocationType.BLOOD_BANK.equals(value))
                {
                    donationDateField.setEnabled(false);
                    donationDateField.setValue(null);
                    donationDateField.setRequired(false);
                    bloodStockAdminField.setEnabled(true);
                    bloodStockAdminField.setValue(null);
                    bloodStockAdminField.setRequired(true);
                }
                else
                {
                    donationDateField.setEnabled(true);
                    donationDateField.setValue(null);
                    donationDateField.setRequired(true);
                    bloodStockAdminField.setEnabled(false);
                    bloodStockAdminField.setValue(null);
                    bloodStockAdminField.setRequired(false);
                }
                donationDateField.setValue(null);
            }
        });
    }

    @Override
    protected List<String> getVisiblePropertyIds()
    {
        return Arrays.asList(new String[] {
            DonationCenter.CENTER_NAME,
            DonationCenter.LOCATION_REF,
            DonationCenter.LOC_TYPE,
            DonationCenter.DONATION_DATE,
            DonationCenter.MAIN_OFFICER,
            DonationCenter.CONTACT,
            DonationCenter.ADDRESS,
            DonationCenter.CITY,
            DonationCenter.DISTRICT,
            DonationCenter.BLOOD_STOCK_ADMIN
        });
    }

    @Override
    protected int[] getPropertyColumnIndices()
    {
        return new int[] {
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        };
    }

    /**
     * <p>
     * Campaign form field factory implementation.
     * </p>
     * 
     * @author manuja
     * 
     * @version $Id$
     */
    private class CampaignFormField extends DefaultFieldFactory
    {

        private static final long serialVersionUID = -5755467446007035089L;

        /**
         * {@inheritDoc}
         */
        @Override
        public Field createField(final Item item, final Object propertyId, final Component uiContext)
        {
            String pid = (String) propertyId;
            if (pid.equals(DonationCenter.DISTRICT))
            {
                Select districtField = createDistrictField("District");
                districtField.setNullSelectionAllowed(false);
                districtField.setRequired(true);
                districtField.setRequiredError("District is required");
                return districtField;
            }
            else if (pid.equals(DonationCenter.LOC_TYPE))
            {
                Select locationTypeField = createLocationTypeField("Location Type");
                locationTypeField.setNullSelectionAllowed(false);
                locationTypeField.setRequired(true);
                locationTypeField.setRequiredError("Location Type is required");
                return locationTypeField;
            }
            else if (pid.equals(DonationCenter.DONATION_DATE))
            {
                DateField donationDateField = createDateField("Campaign Date", false);
                donationDateField.setEnabled(false);
                donationDateField.setRequiredError("Campaign Date is required");
                return donationDateField;
            }
            else if (pid.equals(DonationCenter.CITY))
            {
                TextField cityField = createTextField("City", false);
                cityField.setRequired(true);
                cityField.setRequiredError("City is rqeuired");
                return cityField;
            }
            else if (pid.equals(DonationCenter.CENTER_NAME))
            {
                TextField centerNameField = createTextField("Center Name", false);
                centerNameField.setRequired(true);
                centerNameField.setRequiredError("Center Name is rqeuired");
                return centerNameField;
            }
            else if (pid.equals(DonationCenter.ADDRESS))
            {
                TextField addressField = createTextField("Address", false);
                addressField.setRequired(true);
                addressField.setRequiredError("Address is rqeuired");
                return addressField;
            }
            else if (pid.equals(DonationCenter.LOCATION_REF))
            {
                TextField refNoField = createTextField("Ref No", false);
                refNoField.setRequired(true);
                refNoField.setRequiredError("Ref No is rqeuired");
                return refNoField;
            }
            else if (pid.equals(DonationCenter.MAIN_OFFICER))
            {
                TextField officerInChargeField = createTextField("Organizer", false);
                officerInChargeField.setRequired(true);
                officerInChargeField.setRequiredError("Organizer is rqeuired");
                return officerInChargeField;
            }
            else if (pid.equals(DonationCenter.CONTACT))
            {
                TextField contactField = createTextField("Contact", false);
                contactField.setRequired(true);
                contactField.setRequiredError("Contact is rqeuired");
                return contactField;
            }
            else if (pid.equals(DonationCenter.BLOOD_STOCK_ADMIN))
            {
                Select bloodStockAdminField = createBloodStockAdminSelectField("Blood Stock Admin");
                bloodStockAdminField.setRequiredError("Blood Stock Admin is rqeuired");
                bloodStockAdminField.setEnabled(false);
                return bloodStockAdminField;
            }
            return super.createField(item, propertyId, uiContext);
        }
    }

}
