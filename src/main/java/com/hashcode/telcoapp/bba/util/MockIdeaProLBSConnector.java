/*
 * FILENAME
 *     MockIdeaProLBSConnector.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.util;

import hms.kite.samples.api.SdpException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.telcoapp.common.connectors.ILBSConnector;
import com.hashcode.telcoapp.common.model.Location;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * LBS Connector.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public class MockIdeaProLBSConnector implements ILBSConnector
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(MockIdeaProLBSConnector.class);

    private List<String> sampleAddressList = Arrays.asList(new String[] {
        "Colombo",
        "Gampaha",
        "Nittambuwa",
        "Kaluthara",
        "Polgahawela",
        "Veyangoda",
        "Kandy",
        "Nuwara Eliya",
        "Rathnapura",
        "Jaffna"
    });

    private List<Location> sampleLocations = null;

    /**
     * {@inheritDoc}
     * 
     * @see com.appmart.common.util.ILBSConnector#getLocation(java.lang.String)
     */
    public String getLocation(final String address) throws SdpException
    {
        int randomValue = ((int) Math.random() * 100000) % 10;
        return sampleAddressList.get(randomValue);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.appmart.common.util.ILBSConnector#getLocationInLatLong(java.lang.String)
     */
    public Location getLocationInLatLong(final String address) throws SdpException
    {
        initiateLocations();
        int randomValue = ((int) Math.random() * 100000) % 10;
        return sampleLocations.get(randomValue);
    }

    private void initiateLocations()
    {
        if (null == sampleLocations)
        {
            sampleLocations = new ArrayList<Location>();

            Location location1 = new Location();
            location1.setLatitude("7.023001");
            location1.setLongitude("79.899012");
            sampleLocations.add(location1);

            Location location2 = new Location();
            location2.setLatitude("7.024000");
            location2.setLongitude("79.899012");
            sampleLocations.add(location2);

            Location location3 = new Location();
            location3.setLatitude("7.025000");
            location3.setLongitude("79.899012");
            sampleLocations.add(location3);

            Location location4 = new Location();
            location4.setLatitude("7.026000");
            location4.setLongitude("79.899012");
            sampleLocations.add(location4);

            Location location5 = new Location();
            location5.setLatitude("7.027000");
            location5.setLongitude("79.899012");
            sampleLocations.add(location5);

            Location location6 = new Location();
            location6.setLatitude("7.028000");
            location6.setLongitude("79.899012");
            sampleLocations.add(location6);

            Location location7 = new Location();
            location7.setLatitude("7.029000");
            location7.setLongitude("79.899012");
            sampleLocations.add(location7);

            Location location8 = new Location();
            location8.setLatitude("7.030000");
            location8.setLongitude("79.899012");
            sampleLocations.add(location8);

            Location location9 = new Location();
            location9.setLatitude("7.031071");
            location9.setLongitude("79.899012");
            sampleLocations.add(location9);

            Location location10 = new Location();
            location10.setLatitude("7.032071");
            location10.setLongitude("79.899012");
            sampleLocations.add(location10);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.telcoapp.common.connectors.ILBSConnector#getDistrict(java.lang.String)
     */
    public String getDistrict(final String address) throws SdpException
    {
        return "Colombo";
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.telcoapp.common.connectors.ILBSConnector#getDistrictFromLatAndLng(java.math.BigDecimal,
     *      java.math.BigDecimal)
     */
    public String getDistrictFromLatAndLng(final BigDecimal lat, final BigDecimal lng) throws SdpException
    {
        return "Colombo";
    }

}
