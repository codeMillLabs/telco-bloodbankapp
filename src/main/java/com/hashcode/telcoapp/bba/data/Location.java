/*
 * FILENAME
 *     Location.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.data;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Location model class.
 * </p>
 * 
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class Location
{
    private double longitute;
    private double latitute;
    private String district;
    private String city;

    /**
     * <p>
     * Getter for longitute.
     * </p>
     * 
     * @return the longitute
     */
    public double getLongitute()
    {
        return longitute;
    }

    /**
     * <p>
     * Setting value for longitute.
     * </p>
     * 
     * @param inLongitute
     *            the longitute to set
     */
    public void setLongitute(final double inLongitute)
    {
        this.longitute = inLongitute;
    }

    /**
     * <p>
     * Getter for latitute.
     * </p>
     * 
     * @return the latitute
     */
    public double getLatitute()
    {
        return latitute;
    }

    /**
     * <p>
     * Setting value for latitute.
     * </p>
     * 
     * @param inLatitute
     *            the latitute to set
     */
    public void setLatitute(final double inLatitute)
    {
        this.latitute = inLatitute;
    }

    /**
     * <p>
     * Getter for district.
     * </p>
     * 
     * @return the district
     */
    public String getDistrict()
    {
        return district;
    }

    /**
     * <p>
     * Setting value for district.
     * </p>
     * 
     * @param inDistrict
     *            the district to set
     */
    public void setDistrict(final String inDistrict)
    {
        this.district = inDistrict;
    }

    /**
     * <p>
     * Getter for city.
     * </p>
     * 
     * @return the city
     */
    public String getCity()
    {
        return city;
    }

    /**
     * <p>
     * Setting value for city.
     * </p>
     * 
     * @param inCity
     *            the city to set
     */
    public void setCity(final String inCity)
    {
        this.city = inCity;
    }

}
