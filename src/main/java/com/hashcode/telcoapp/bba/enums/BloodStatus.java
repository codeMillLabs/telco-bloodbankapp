/*
 * FILENAME
 *     BloodStatus.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.enums;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Blood Status.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
public enum BloodStatus
{
    CROSS_MATCHED("Cross Matched", "CrossMatched"),
    UN_CROSS_MATCHED("Un-cross Matched", "UnCrossMatched");
    
    String desc;
    String apiValue;

    private BloodStatus(final String inDesc, final String inApiValue)
    {
        this.desc = inDesc;
        this.apiValue = inApiValue;
    }

    /**
     * <p>
     * Getter for apiValue.
     * </p>
     * 
     * @return the apiValue
     */
    public String getApiValue()
    {
        return apiValue;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString()
    {
        return this.desc;
    }
}
