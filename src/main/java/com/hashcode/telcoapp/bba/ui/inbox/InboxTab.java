/*
 * FILENAME
 *     InboxLayout.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.ui.inbox;

import com.hashcode.telcoapp.bba.dao.MessageSearchCriteria;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Inbox tab.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
public class InboxTab extends VerticalLayout
{
    private static final long serialVersionUID = -2488992571427187631L;
    
    private InboxSearchForm inboxSearchForm;
    private InboxTable inboxTable;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public InboxTab()
    {
        initUI();
        setSizeFull();
    }

    private void initUI()
    {
        setMargin(true);

        VerticalLayout searchLayout = new VerticalLayout();
        searchLayout.addComponent(getInboxSearchForm());

        Button searchButton = new Button("Search");
        searchButton.setImmediate(true);
        searchButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = -8898451804489346024L;

            public void buttonClick(final ClickEvent event)
            {
                if (getInboxSearchForm().validateAllFields())
                {
                    getInboxTable().refresh(getInboxSearchForm().getModel());
                    getInboxSearchForm().setComponentError(null);
                }
            }
        });

        Button clearButton = new Button("Clear");
        clearButton.setImmediate(true);
        clearButton.addListener(new ClickListener()
        {
            private static final long serialVersionUID = 6438675641753377171L;

            public void buttonClick(final ClickEvent event)
            {
                getInboxSearchForm().setModel(new MessageSearchCriteria());
                getInboxTable().refresh(new MessageSearchCriteria());
            }
        });

        HorizontalLayout searchButtonLayout = new HorizontalLayout();
        searchButtonLayout.setSpacing(true);
        searchButtonLayout.addComponent(searchButton);
        searchButtonLayout.addComponent(clearButton);
        searchLayout.addComponent(searchButtonLayout);
        searchLayout.setComponentAlignment(searchButtonLayout, Alignment.MIDDLE_CENTER);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.addComponent(getInboxTable().getPaginator());
        tableLayout.addComponent(getInboxTable());

        addComponent(searchLayout);
        addComponent(tableLayout);
        setSizeFull();
    }

    private InboxTable getInboxTable()
    {
        if (inboxTable == null)
        {
            inboxTable = new InboxTable();
            inboxTable.refresh();
        }

        return inboxTable;
    }

    private InboxSearchForm getInboxSearchForm()
    {
        if (inboxSearchForm == null)
        {
            inboxSearchForm = new InboxSearchForm();
        }

        return inboxSearchForm;
    }

    /**
     * <p>
     * Refresh blood stock tab.
     * </p>
     */
    public void refresh()
    {
        getInboxTable().refresh(new MessageSearchCriteria());
    }
}
