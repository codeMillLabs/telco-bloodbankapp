/*
 * FILENAME
 *     PromoManagerUtils.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.telcoapp.bba.util;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.joda.time.DateTime;

import com.hashcode.telcoapp.bba.data.NICInfo;
import com.hashcode.telcoapp.bba.enums.BloodType;
import com.hashcode.telcoapp.bba.enums.District;
import com.hashcode.telcoapp.bba.enums.SubBloodType;
import com.hashcode.telcoapp.common.enums.Gender;
import com.hashcode.telcoapp.common.handlers.IMessageSender;
import com.hashcode.telcoapp.common.util.ServiceResolver;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Util class for BBA.
 * </p>
 * 
 * @author Manuja
 * 
 * @version $Id$
 */
public final class BBAUtils
{
    private static final String ENCRYPTION_ALGORITHM = "MD5";
    private static String dateFormat = "dd MMM yyyy";
    private static String dateTimeFormat = "dd MMM yyyy HH:mm";

    private static final SimpleDateFormat SDF1 = new SimpleDateFormat(dateFormat);
    private static final SimpleDateFormat SDF2 = new SimpleDateFormat(dateTimeFormat);

    private static final Pattern DONATION_CODE_PATTERN = Pattern.compile(Configuration
        .getMessage("blood.donation.verfication.code"));

    public static final String DEFAULT_DISTRICT = "Colombo";
    private static final int YEAR_BREAK = 60;
    private static final int Y1900 = 1900;
    private static final int Y2000 = 2000;

    public static final String ANDROID_API_DATE_FORMAT = "yyyy/MM/dd";

    private BBAUtils()
    {
    }

    /**
     * Encrypt password. source : http://www.mkyong.com/java/java-md5-hashing-example/
     * 
     * @param password
     *            password to encrypt
     * 
     * @return encrypted password
     * 
     * @throws Exception
     *             throws when no such algorithm
     */
    public static String getEncryptedPassword(final String password) throws Exception
    {
        MessageDigest md = MessageDigest.getInstance(ENCRYPTION_ALGORITHM);
        md.update(password.getBytes());

        byte[] byteData = md.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++)
        {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < byteData.length; i++)
        {
            String hex = Integer.toHexString(0xff & byteData[i]);
            if (hex.length() == 1)
                hexString.append('0');
            hexString.append(hex);
        }

        return hexString.toString();
    }

    /**
     * <p>
     * Format the given date value according to current dateFormat.
     * </p>
     * 
     * @see #getDateFormat()
     * @see #setDateFormat(String)
     * @param date
     *            the date to be formatted
     * @return formatted string. Empty string if null date is given.
     */
    public static String formatDate(final Date date)
    {
        if (date == null)
            return "";
        return SDF1.format(date);
    }

    /**
     * <p>
     * Format the given date value according to current dateFormat.
     * </p>
     * 
     * @see #getDateFormat()
     * @see #setDateFormat(String)
     * @param date
     *            the date to be formatted
     * @return formatted string. Empty string if null date is given.
     */
    public static String formatDateTime(final Date date)
    {
        if (date == null)
            return "";
        return SDF2.format(date);
    }

    /**
     * <p>
     * This will check the given String value is null or empty..
     * </p>
     * 
     * 
     * @param value
     *            string value
     * @return if not null or empty returns true otherwise false
     * 
     */
    public static boolean isNotNullOrEmpty(final String value)
    {
        return (value == null || value.isEmpty()) ? false : true;
    }

    /**
     * <p>
     * Get district value of.
     * </p>
     * 
     * @param district
     *            district name string
     * 
     * @return district enum value
     */
    public static String getDistrict(final String district)
    {
        try
        {
            District districtEnum = District.valueOf(district.toUpperCase().replaceAll("\\s+", "_"));
            return districtEnum.toString();
        }
        catch (IllegalArgumentException ex)
        {
            return DEFAULT_DISTRICT;
        }
    }

    /**
     * <p>
     * Generate date of birth for given national identity card number and gender.
     * </p>
     * 
     * @param nicNo
     *            national identity card number
     * 
     * @return decoded nic instance
     */
    public static NICInfo decodeNICInfo(final String nicNo)
    {
        if (nicNo.length() != 10)
            return null;

        int year = Integer.valueOf(nicNo.substring(0, 2));
        int dayOfYear = Integer.valueOf(nicNo.substring(2, 5));

        // TODO: refactor the logic after 2000
        year = (year > YEAR_BREAK) ? year + Y1900 : year + Y2000;
        Gender gender = (dayOfYear < 500) ? Gender.MALE : Gender.FEMALE;
        dayOfYear = (dayOfYear < 500) ? dayOfYear : (dayOfYear - 500);

        dayOfYear = (year % 4 != 0) ? dayOfYear - 1 : dayOfYear;

        DateTime dateTime = new DateTime().withYear(year).withDayOfYear(dayOfYear);

        NICInfo nicInfo = new NICInfo(year, new Date(dateTime.getMillis()), gender);

        return nicInfo;
    }

    /**
     * <p>
     * Donation code verifier.
     * </p>
     * 
     * @param donationCode
     *            donation code string value
     * 
     * @return whether validate success or not
     */
    public static boolean validateDonationCode(final String donationCode)
    {

        return DONATION_CODE_PATTERN.matcher(donationCode).matches();
    }

    /**
     * <p>
     * Notify subscriber.
     * </p>
     * 
     * @param address
     *            subscriber address
     * @param message
     *            message to send
     */
    public static void notifySubscriber(final String address, final String message)
    {
        try
        {
            getService(IMessageSender.class).sendMessage(address, message);
        }
        catch (Exception e)
        {
        }
    }

    /**
     * <p>
     * Generate unique file name. This will generate a file name according to current time.
     * </p>
     * 
     * @param originalName
     *            original file name.
     * 
     * @return generated file name
     */
    public static String generateUniqueFileName(final String originalName)
    {
        String[] splittedOriginalName = originalName.split("\\.");
        return String.valueOf(System.currentTimeMillis()) + "." + splittedOriginalName[1];
    }

    /**
     * <p>
     * Get service.
     * </p>
     * 
     * @param serviceType
     *            service class
     * @param <T>
     *            type
     * 
     * @return instance of service class
     */
    public static <T> T getService(final Class<T> serviceType)
    {
        return ServiceResolver.findService(serviceType);
    }

    /**
     * <p>
     * Map blood type for given string.
     * </p>
     * 
     * @param bloodType
     *            blood type string
     * 
     * @return blood type enum value
     */
    public static BloodType mapBloodType(final String bloodType)
    {
        if (null == bloodType)
            return null;
        else if (bloodType.equals("A+"))
            return BloodType.A_P;
        else if (bloodType.equals("A-"))
            return BloodType.A_N;
        else if (bloodType.equals("B+"))
            return BloodType.B_P;
        else if (bloodType.equals("B-"))
            return BloodType.B_N;
        else if (bloodType.equals("AB+"))
            return BloodType.AB_P;
        else if (bloodType.equals("AB-"))
            return BloodType.AB_N;
        else if (bloodType.equals("O+"))
            return BloodType.O_P;
        else if (bloodType.equals("O-"))
            return BloodType.O_N;
        else
            return null;
    }

    /**
     * <p>
     * Map sub blood type to enum value.
     * </p>
     *
     * @param subBloodType sub blood type string
     * 
     * @return enum sub blood type value
     */
    public static SubBloodType mapSubBloodType(final String subBloodType)
    {
        if (null == subBloodType)
            return null;
        else if (subBloodType.equals("Red Blood Cells") || subBloodType.equals("RCC"))
            return SubBloodType.RCC;
        else if (subBloodType.equals("Platelet Cells") || subBloodType.equals("Platelet"))
            return SubBloodType.PLATELETS;
        else
            return null;
    }
}
